//
//  MainFlowCoordinator.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 5/3/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class MainFlowCoordinator: NSObject, HomeViewDelegate {
    
    var window: UIWindow
    var tabController : UITabBarController!
    init(window: UIWindow) {
        self.window = window
        super.init()
    }
    
    private func setRoot(viewController : UIViewController) -> Void{
        
        if(self.window.rootViewController != nil)
        {
            UIView.transition(from: (self.window.rootViewController?.view)!,
                              to: viewController.view,
                              duration: 0.66,
                              options: .transitionFlipFromLeft, completion: { (finished: Bool) in
                                self.window.rootViewController = viewController
                                self.window.makeKeyAndVisible()
            });
        }
        else
        {
            self.window.rootViewController = viewController
        }
    }
    
    private func setNavigationRoot(navController : UINavigationController) -> Void{
        
        if(self.window.rootViewController != nil)
        {
            UIView.transition(from: (self.window.rootViewController?.view)!,
                              to: navController.view,
                              duration: 0.66,
                              options: .transitionFlipFromLeft, completion: { (finished: Bool) in
                                self.window.rootViewController = navController
                                self.window.makeKeyAndVisible()
            });
        }
        else
        {
            self.window.rootViewController = navController
        }
    }
    
    func presentLandingSavingsCard()
    {
        var controllers = self.tabController.viewControllers
        var flag : Bool = false
        var index : Int = 0
        for controller in controllers!
        {
            if (controller is SavingsCardViewController)
            {
                flag = false
                controllers?.remove(at: index)
                break
            }
            else
            {
                flag = true
            }
            index = index + 1
        }
        
        if(flag)
        {
//            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//
//            let controller : CouponViewController = mainStoryboard.instantiateViewController(withIdentifier: "CouponViewController") as! CouponViewController
            
            
            let controller : SavingsCardViewController = SavingsCardViewController()
            
            self.tabController.addChildViewController(controller)
        }
        else
        {
            let controller : SavingsCardViewController = SavingsCardViewController()
            
            self.tabController.addChildViewController(controller)
        }
        self.tabController.selectedIndex = index
    }
    
    func presentLandingProductMatch()
    {
        let controllers = self.tabController.viewControllers
        var flag : Bool = false
        var index : Int = 0
        for controller in controllers!
        {
            if (controller is LatestProductMatchViewController)
            {
                flag = false
                break
            }
            else
            {
                flag = true
            }
            index = index + 1
        }
        
        if(flag)
        {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let controller : LatestProductMatchViewController = mainStoryboard.instantiateViewController(withIdentifier: "LatestProductMatchViewController") as! LatestProductMatchViewController
            self.tabController.addChildViewController(controller)
        }
        self.tabController.selectedIndex = index
    }
    
    func presentLandingFindRetailer()
    {
        let controllers = self.tabController.viewControllers
        var flag : Bool = false
        var index : Int = 0
        for controller in controllers!
        {
            if(controller is UINavigationController)
            {
                if (controller.childViewControllers[0] is LocatorOptionViewController)
                {
                    flag = false
                    break
                }
                else
                {
                    flag = true
                }
            }
            
            index = index + 1
        }
        
        if(flag)
        {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let controller : LocatorOptionViewController = mainStoryboard.instantiateViewController(withIdentifier: "LocatorOptionViewController") as! LocatorOptionViewController
            self.tabController.addChildViewController(controller)
        }
        TransitionManager.sharedInstance.setStoryboardID(storyboardID: "LocationDetailsViewController")
        UserManager.sharedInstance.setSearchOption(searchOption: .retailer)
        self.tabController.selectedIndex = index
    }
    
    
    func presentLandingBlinkExamDashboard()
    {
    
        let controllers = self.tabController.viewControllers
        var flag : Bool = false
        var index : Int = 0
        for controller in controllers!
        {
            if (controller is BlinkExamDetailsViewController)
            {
                flag = false
                break
            }
            else
            {
                flag = true
            }
            index = index + 1
        }
        
        if(flag)
        {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let controller : BlinkExamDetailsViewController = mainStoryboard.instantiateViewController(withIdentifier: "BlinkExamDetailsViewController") as! BlinkExamDetailsViewController
            self.tabController.addChildViewController(controller)
        }
        self.tabController.selectedIndex = index
        
    }
    
    func presentEyeCareProffesional() -> Void
    {
        TransitionManager.sharedInstance.setStoryboardID(storyboardID: "LocationDetailsViewController")
        UserManager.sharedInstance.setSearchOption(searchOption: .eyeCareProfessionals)
        self.presentHome(2)
    }
    
    func presentJourney() -> Void
    {
//        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        
//        let controller : MainViewController = mainStoryboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
//        let navController : UINavigationController = UINavigationController(rootViewController: controller)
//        
//        navController.setNavigationBarHidden(true, animated: false)
        
        
        let controller : MainViewController = MainViewController(optionShown: .firstFlow)
        let navController : UINavigationController = UINavigationController(rootViewController: controller)
        navController.setNavigationBarHidden(true, animated: false)
        
        
        self.setRoot(viewController: navController)
        
    }
    
    func presentContinueJourneyLeftViewController() -> Void
    {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let navController : UINavigationController!
        if let journeyStation = UserManager.sharedInstance.getJourneyStation() {
             navController = journeyStation
        }
        else
        {
            let controller : GuideMainViewController = mainStoryboard.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
            navController = UINavigationController(rootViewController: controller)
        }
        
        navController.setNavigationBarHidden(true, animated: false)
        self.setNavigationRoot(navController: navController)
    }
    
    func presentHome(_ selectedIndex : Int?) -> Void
    {
        self.setupTabController()
        if(selectedIndex != nil)
        {
            self.tabController.selectedIndex = selectedIndex!
        }
    
        self.setRoot(viewController: self.tabController)
    }
    
    func presentMain() -> Void
    {
        if(!TransitionManager.sharedInstance.getFirstLaunchFlow() && UserManager.sharedInstance.getJourneyStation() == nil)
        {
            self.presentJourney()
        }
        else
        {
            self.presentHome(nil)
        }
    }
    
    
    //MARK: NotificationCenter
    
    func presentStartNewJourneyViewController()
    {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let controller : GuideMainViewController = mainStoryboard.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
        let navController : UINavigationController = UINavigationController(rootViewController: controller)
        
        navController.setNavigationBarHidden(true, animated: false)
        self.setRoot(viewController: navController)
    }
    
    func presentFindRetailer()
    {
        self.setupTabController()
        
        for (index, controller) in (self.tabController.viewControllers?.enumerated())! {
            if let navController = controller as? UINavigationController, let root = navController.viewControllers[0] as? LocatorOptionViewController {
                self.tabController.selectedIndex = index
                root.setupLocationDetailsViewController()
                UserManager.sharedInstance.setSearchOption(searchOption: .retailer)
                break
            }
        }
        self.setRoot(viewController: self.tabController)
    }
    
    func presentLandingProductDetails()
    {
        self.tabController.selectedIndex = 1
    }
    
    func presentProducts()
    {
        self.presentHome(1)
    }
    
    func selectNotification()
    {
        self.tabController.selectedIndex = 4
        TransitionManager.sharedInstance.setStoryboardID(storyboardID: "Notification")        
    }
    
    func presentLocator()
    {
        self.presentHome(2)
    }
    
    func presentEducation()
    {
        self.presentHome(3)
    }
    
    func presentProductDetails()
    {
        self.presentHome(1)
    }
    
    func presentPollenForecast()
    {
        self.setupTabController()
        
        TransitionManager.sharedInstance.setStoryboardID(storyboardID: "PollenForecastViewController")
        self.setRoot(viewController: self.tabController)
    }
    
    func presentHomeSelection()
    {
        self.tabController.selectedIndex = 0
    }
    
    func presentHomeViewController()
    {
       self.presentHome(nil)
    }
    
    func presentLatestProductMatchViewController()
    {
        self.setupTabController()
        
        let controllers = self.tabController.viewControllers
        var flag : Bool = false
        var index : Int = 0
        for controller in controllers!
        {
            if (controller is LatestProductMatchViewController)
            {
                flag = false
                break
            }
            else
            {
                flag = true
            }
            index = index + 1
        }
        
        if(flag)
        {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let controller : LatestProductMatchViewController = mainStoryboard.instantiateViewController(withIdentifier: "LatestProductMatchViewController") as! LatestProductMatchViewController
            self.tabController.addChildViewController(controller)
        }
        self.tabController.selectedIndex = index
        self.setRoot(viewController: self.tabController)

    }
    
    func presentBlinkExamChartViewController()
    {
        self.setupTabController()
        
        let controllers = self.tabController.viewControllers
        var flag : Bool = false
        var index : Int = 0
        for controller in controllers!
        {
            if (controller is BlinkExamDetailsViewController)
            {
                flag = false
                break
            }
            else
            {
                flag = true
            }
            index = index + 1
        }
        
        if(flag)
        {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let controller : BlinkExamDetailsViewController = mainStoryboard.instantiateViewController(withIdentifier: "BlinkExamDetailsViewController") as! BlinkExamDetailsViewController
            self.tabController.addChildViewController(controller)
        }
        self.tabController.selectedIndex = index
        self.setRoot(viewController: self.tabController)
    }
    
    
    //MARK: HomeViewDelegate
    
    func presentDiagnosedWithDryEyeJourney()
    {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let controller : GuideMainViewController = mainStoryboard.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
        
        let navController : UINavigationController = UINavigationController(rootViewController: controller)
        
        
        let diagnosedStoryboard : UIStoryboard = UIStoryboard(name: "DiagnosedFlow", bundle: nil)
        let contactLensViewController : ContactLensIntroductionViewController = diagnosedStoryboard.instantiateViewController(withIdentifier: "ContactLensIntroductionViewController") as! ContactLensIntroductionViewController
        contactLensViewController.isPushed = true
        navController.viewControllers.append(contactLensViewController)
        navController.setNavigationBarHidden(true, animated: false)
        self.setNavigationRoot(navController: navController)
        
//        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//
//        let controller : GuideMainViewController = mainStoryboard.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
//        controller.isBeingDiagnosedWithDryEyes = true
//        TransitionManager.sharedInstance.diagnosedWithDryEyeButton = true
//        let navController : UINavigationController = UINavigationController(rootViewController: controller)
//        navController.setNavigationBarHidden(true, animated: false)
//        self.setRoot(viewController: navController)
    }
    
    func presentJourneyAgain()
    {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let controller : GuideMainViewController = mainStoryboard.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
        let navController : UINavigationController = UINavigationController(rootViewController: controller)
        
        navController.setNavigationBarHidden(true, animated: false)
        self.setRoot(viewController: navController)
        
        //        let controller : StartANewJourneyViewController = mainStoryboard.instantiateViewController(withIdentifier: "StartANewJourneyViewController") as! StartANewJourneyViewController
        //        let navController : UINavigationController = UINavigationController(rootViewController: controller)
        //        TransitionManager.sharedInstance.journeyAgain = true
        //        self.setRoot(viewController: navController)
    }
    
    func presentCouponSavingCards()
    {
        self.setupTabController()
        
        let controllers = self.tabController.viewControllers
        var flag : Bool = false
        var index : Int = 0
        for controller in controllers!
        {
            if (controller is SavingsCardViewController)
            {
                flag = false
                break
            }
            else
            {
                flag = true
            }
            index = index + 1
        }
        
        if(flag)
        {
//            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//
//            let controller : CouponViewController = mainStoryboard.instantiateViewController(withIdentifier: "CouponViewController") as! CouponViewController
            
            let controller : SavingsCardViewController = SavingsCardViewController()

            
            self.tabController.addChildViewController(controller)
        }
        self.tabController.selectedIndex = index
        self.setRoot(viewController: self.tabController)
    }
    
    
    //MARK: Setup & Support Methods
    
    func setupTabController()
    {
        if(self.tabController == nil)
        {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            self.tabController  = mainStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
            (self.tabController.viewControllers![0] as! HomeViewController).delegate = self
            self.tabController.tabBar.barTintColor = .white
            self.tabController.tabBar.isTranslucent = false
        }
    }
}
