//
//  OptionalView.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/19/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class OptionalView: UIView {

    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 6
        self.layer.masksToBounds = true
    }
    

}
