//
//  Constants.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/5/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

var useHTTPS = true
var httpStr = useHTTPS == true ? "https://" : "http://"

struct trackingEvents
{
    
    static let closedAppKey = "Closed app"
    static let startedAppkey = "Started app"
    static let loadAppkey = "Load app"
    
    static let pollenForecastkey = "4 day pollen forecast viewed"
    static let dryEyeForecastKey = "4 day dry eye forecast viewed"
    static let savingsCardKey = "Saving card is viewed"
    static let blinkExamCompletedKey = "Blink test is completed"
    static let locatorUsedOptometricsKey = "Locator feature used - Optometrist"
    static let locatorUsedPharmacyKey = "Locator feature used - Pharmacy"
    static let dryEyeInformationKey = "Dry eye information section is viewed"
    static let productMatchProvidedKey = "Product Match provided"
    static let buyNowOptionChemistWarehouseKey = "Used BUY NOW option to go to chemist warehouse"
    static let productContentViewKey = "Product content is viewed"
    static let mostPopularViewKey = "Which product is most popular - Most viewed"
    static let journeyAgainKey = "Journey Again"
    static let productMostMatchedKey = "Which product is  most matched"

    static let userCompleteDiagnosedJourneyKey = "User completes diagnosed journey"
    static let userCompleteNonDiagnosedJourneyKey = "User complete non diagnosed journey"
}

struct k
{
    struct terms {
        static let privacyNotice = "This App is to help you understand your dry eye symptoms and product selection. It does not contain any cookies (or other tracking devices) except those that help the App work. Please know that Alcon will not collect any information about your symptoms, and product match, or other personal information, but since this App saves information on your mobile device it may still be seen by others if your mobile device is lost or stolen. Always remember to lock your mobile device for safety.\n\nAlcon developed the App to help patients identify their dry eye symptoms and match with a convenient product or pack. \n\nAlcon provides this App free of charge as part of its ongoing commitment to developing and maintaining a sensitive understanding of the needs of health care professionals and patients. \n\nFor further information please contact Alcon on 1800 224 153\n© 2018 Alcon AU-3936\n"
        
        static let termsConditions1 = "1. EyeTopia™ (the “App”) is made available to you by Alcon Laboratories (Australia) Pty Limited ABN 88 000 740 830 (\"Alcon\") free of charge on your smartphone, or other mobile device (\"Device\"). You may be charged standard rates or fees by wireless or telephone carriers or other parties that are not connected to Alcon for the download and use of the App. You are responsible for obtaining, maintaining and paying for all hardware and telecommunications and other services needed to use the App. You are also responsible for paying for any devices (including the Device), products or services that you choose to use in connection with the App. By downloading and using this App, you are agreeing to these Terms of Use.\n\n2. The App is controlled or operated (or both) from Australia and is not intended to subject Alcon to any non-Australian jurisdiction or law. The App may not be appropriate or available for use in some non-Australian jurisdictions. Any use of the App is at your own risk, and you must comply with all applicable laws, rules and regulations in doing so. We may limit the App’s availability at any time, in whole or in part, to any person, geographic area or jurisdiction that we choose.\n\n3. THE APP DOES NOT PROVIDE MEDICAL ADVICE, DIAGNOSIS OR TREATMENT. THE APP AND THE CONTENT (AS DEFINED BELOW) ARE PROVIDED BY ALCON FOR GENERAL INFORMATIONAL PURPOSES ONLY AND ARE NOT INTENDED TO BE A SUBSTITUTE FOR PROFESSIONAL MEDICAL ADVICE, DIAGNOSIS OR TREATMENT. YOU SHOULD ALWAYS SEEK THE ADVICE OF YOUR OWN MEDICAL DOCTOR OR HEALTH CARE PROVIDER WITH ANY QUESTIONS YOU HAVE ABOUT A MEDICAL CONDITION. YOU SHOULD NEVER DELAY SEEKING MEDICAL ADVICE OR DISREGARD PROFESSIONAL MEDICAL ADVICE BASED ON INFORMATION PROVIDED IN THIS APP AND/OR THE CONTENT. RELIANCE ON THE APP AND/OR CONTENT IS AT YOUR OWN RISK.\n\n4. The App is licensed (not sold) to you as an end user. Alcon grants a licence to you, on a limited, non-exclusive, revocable, non-transferable, non-sublicensable basis, to install and use the App, including any material, products, programs or services included in the App (collectively, the “Content”), on a Device that you own or control, solely for your own non-commercial use for as long as we permit you to use the App, and provided that you comply with these Terms of Use and do not remove any copyright or other proprietary rights notices applicable to any Content that you download, display, print or otherwise use (such activities, “Use”). This is the only licence or right that you receive with respect to the App and Content.\n\n5. Our trade names, trademarks and service marks include Alcon, Alcon Pharmaceuticals, and EyeTopia™ and any associated logos. All trade names, trademarks, service marks and logos within the App not owned by us are the property of their respective owners. You may not use our trade names, trademarks, service marks or logos in connection with any product or service that is not ours or in any manner that is likely to cause confusion. Nothing contained within the App should be construed as granting any right to use any trade names, trademarks, service marks or logos without the express prior written consent of the owner. Except as otherwise mentioned, this App was developed by or for Alcon and is protected by Alcon copyright. Any unauthorised reproduction is illegal.\n\n6. Both the App and the Content are provided “as is”. While Alcon has taken care to ensure the accuracy of the App and the Content, it makes no assurances or guarantees as to the accuracy, completeness, reliability or availability of the App or the Content, except for any consumer guarantees that are implied by the Australian Consumer Law (“ACL”).\n\n7. Alcon may update, modify, interrupt or discontinue any or all of the functionality of the App or the Content (including these Terms of Use) and make changes, corrections and/or improvements to the App or the Content (including these Terms of Use), at any time without notice to you. Alcon is not responsible for any costs, loss or damage that you incur as the result of any modification, interruption or discontinuance of any or all functionality of the App or the Content.\n\n8. When you Use the App and the Content, you are solely responsible for:\n• Ensuring that you are using the latest available version of the App.\n• Ensuring that all operating systems and security software for the Device are the most up to date versions available.\n• Any damage to your Device, and any loss or corruption of information that you store in the Device that results from the Use of the App or Content or the interaction of this App or Content with other applications or programs that you may download or use.\n• Maintaining the security and confidentiality of any personal identification numbers, account information, passwords and the like related to your use of the App or Content.\n• Maintaining the physical security of your Device.\n• Maintaining the security and confidentiality of Personal Information that you store on your Device. \"Personal Information\" is information that alone or in combination with other information, identifies you or can be used to identify you or your personal characteristics, such as your name, contact information or information about your health that is collected and/or transmitted by the App.\n\n9. To the extent permitted under the ACL, Alcon is not responsible for any costs, loss or damage that you incur as the result of your failure to meet any of these responsibilities.\n\n10.\tWhen you Use the App and the Content, you must not:\n• Reproduce, modify, adapt, translate, create derivative works of, edit, improve, sell, rent, lease, loan, timeshare, distribute, resell or otherwise exploit any portion of (or any use of) the App, except as expressly authorised herein, without our express prior written consent.\n• Reverse engineer, decompile or disassemble any portion of the App, except where such restriction is expressly prohibited by applicable law.\n• Disrupt or otherwise interfere with the operation of the App in any way.\n• Engage in systematic copying and widespread distribution of Content to the public.\n• Use the App or the Content for any purpose that is fraudulent or otherwise tortious or unlawful.\n• Transmit or otherwise make available through or in connection with the App any virus, worm, Trojan horse, Easter egg, time bomb, spyware or other computer code, file or program that is or is potentially harmful or invasive or intended to damage or hijack the operation of, or to monitor the use of, any hardware, software or equipment.\n• Transmit or otherwise make available through or in connection with the App any materials that are or may be: (a) threatening, harassing, degrading, hateful or intimidating, or otherwise fail to respect the rights and dignity of others; (b) defamatory, libelous, fraudulent or otherwise tortious; (c) obscene, indecent, pornographic or otherwise objectionable; or (d) protected by copyright, trademark, trade secret, right of publicity or privacy or any other proprietary right, without the express prior written consent of the applicable owner.\n• Violate these Terms of Use.\n"
        
        static let termsConditions2 = "11.\tYour submission of information through the App is governed by our Privacy Statement. You represent and warrant that any information you provide in connection with the App is and will remain accurate and complete and that you will maintain and update such information as needed. Except as provided in the Privacy Statement, any information or material that you transmit to Alcon from or about the App, including any data, questions, comments, suggestions or the like: (1) is, and will be treated as, gratuitous, unsolicited, unrestricted, non-confidential and non-proprietary to you, and our receipt of such information or material does not place Alcon under any fiduciary or other obligation; and (2) becomes the property of Alcon or its affiliates and may be used for any purpose, including reproduction, disclosure, transmission, publication, broadcast and posting. Without limiting the generality of the foregoing, Alcon is free to use any ideas, concepts, know-how, or techniques contained in any communication you send to Alcon from or about the App for any purpose whatsoever including developing, manufacturing and marketing products.\n\n12.\tExcept to the extent prohibited under applicable law, you agree to defend, indemnify and hold harmless, Alcon and its affiliate entities, from and against all claims, losses, costs and expenses (including legal fees) arising out of: (a) your use of, or activities in connection with, the App (including the Content); and (b) any violation or alleged violation of the Terms of Use by you.\n\n13.\tYou hereby release and agree to hold harmless Alcon and its affiliates, and each of their officers, directors, shareholders, employees and representatives, from any and all claims, costs, losses, liabilities and damages of any sort, whether direct, indirect, special, consequential or otherwise, whether arising in tort (including active, passive or imputed negligence), contract, warranty, strict liability, reliance or under any other theory, and whether or not Alcon has been advised of the possibility of such damages, in each case arising out of or related to your Use of the App and/or or the Content and from any use that Alcon makes of information that you transmit to Alcon from or about the App.\n\n14.\tBecause Alcon has no control over and does not endorse the content of any websites or applications to which the Content may be linked, you agree that your access to such other websites or applications is at your own risk.\n\n15.\tProduct or other information in the Content is provided by Alcon for general information purposes only and does not constitute complete medical information. You should always obtain complete medical information and advice from a medical doctor or other healthcare provider. Should you have a medical condition, promptly see your medical doctor or healthcare provider. This App does not offer personalised medical diagnosis or patient-specific treatment advice.\n\n16.\tThese Terms of Use are effective until terminated. We may terminate or suspend your use of the App at any time and without prior notice, including if we believe that you have violated or acted inconsistently with the letter or spirit of these Terms of Use. Upon any such termination or suspension, your right to use the App will immediately cease, and we may, without liability to you or any third party, immediately deactivate or delete your user name, password and account, and all associated information and materials, without any obligation to provide any further access to such information or materials. The following sections shall survive any expiration or termination of these Terms of Use:  Jurisdictional Issues (Section 2); Disclaimer of Warranties (Section 6); Your Responsibilities (Section 8); Rules of Conduct (Section10); Information Submitted Through the App (Section 11); Indemnity (Section 12); Limitation of Liability (Section 13); Links (Section 14); Termination (Section 16); Miscellaneous (Section 17); and Apple-Specific Terms (Section 19); Governing Law and Jurisdiction (Section 20).\n\n17.\tIf any part or provision of these Terms of Use is found to be invalid, such invalidity shall not affect the enforceability or any other part or provision of these Terms of Use. These Terms of Use do not, and shall not be construed to, create any partnership, joint venture, employer-employee, agency or franchisor-franchisee relationship between you and Alcon. You must not assign, transfer or sublicense any or all of your rights or obligations under these Terms of Use without the express prior written consent of Alcon. Alcon may assign, transfer or sublicense any or all of its rights or obligations under these Terms of Use without restriction. No waiver by either party of any breach or default hereunder will be deemed to be a waiver of any preceding or subsequent breach or default. Any heading, caption or section title contained herein is for convenience only, and in no way defines or explains any section or provision. All terms defined in the singular shall have the same meanings when used in the plural, where appropriate and unless otherwise specified. Any use of the term “including” or variations thereof in these Terms of Use shall be construed as if followed by the phrase “without limitation”. These Terms of Use, including any terms and conditions incorporated herein, constitute the entire agreement between you and Alcon relating to the subject matter hereof and supersede any and all prior or contemporaneous written or oral agreements or understandings between you and Alcon relating to such subject matter. Notices to you (including notices of changes to these Terms of Use) may be made via posting to the App or by e-mail (including in each case via links), or by regular mail. Without limitation, a printed version of these Terms of Use and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to these Terms of Use to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form. Alcon will not be responsible for any failure to fulfil any obligation due to any cause beyond its control.\n\n18.\tIf you have a question or concern regarding the App, you may call Alcon on 1 800 671 203. \n\n19.\tIn addition to your agreement with the foregoing terms and conditions, and not withstanding anything to the contrary herein, the following provisions apply with respect to your use of any version of the App compatible with the iOS operating system of Apple Inc. (“Apple”). Apple is not a party to these Terms of Use and does not own and is not responsible for the App. Apple is not providing any warranty for the App except, if applicable, to refund the purchase price for it. Apple is not responsible for maintenance or other support services for the App and shall not be responsible for any other claims, losses, liabilities, damages, costs or expenses with respect to the App, including any third party product liability claims, claims that the App fails to conform to any applicable legal or regulatory requirement, claims arising under consumer protection or similar legislation, and claims with respect to intellectual property infringement. Any inquiries or complaints relating to the use of the App, including those pertaining to intellectual property rights, must be directed to Alcon in accordance with the Section 18 above. The licence you have been granted herein is limited to a non-transferable license to use the App on an Apple-branded product that runs Apple’s iOS operating system and is owned or controlled by you, or as otherwise permitted by the Usage Rules set forth in Apple’s App Store Terms of Service. In addition, you must comply with the terms of any third-party agreement applicable to you when using the App, such as your wireless data service agreement. Apple and Apple’s subsidiaries are third-party beneficiaries of these Terms of Use and, upon your acceptance of the terms and conditions of these Terms of Use, will have the right (and will be deemed to have accepted the right) to enforce these Terms of Use against you as a third-party beneficiary thereof; notwithstanding the foregoing, Alcon’ right to enter into, rescind or terminate any variation, waiver or settlement under these Terms of Use is not subject to the consent of any third party. \n\n20.\tYour Use of the App and these Terms of Use shall be governed, construed and interpreted in accordance with the laws of New South Wales, Australia. The parties submit to the non-exclusive jurisdiction of the courts in New South Wales, Australia."
    }
    
    struct storyboard
    {
        static let diagnosed = "DiagnosedFlow"
        static let nonDiagnosed = "NonDiagnosedFlow"
        static let main = "Main"
    }
    
    struct card
    {
        static let cardID = "id"
        static let retailerCopy = "retailer_copy"
        static let instructions = "instructions"
        static let discount = "discount"
        static let timeAvailable = "time_available"
        static let termsConditions = "terms_and_conditions"
        static let retailerImage = "retailer_image"
        static let barcodeImage = "barcode_image"
        static let status = "status"
        static let created = "created_at"
        static let updated = "updated_at"
        static let savingCardID = "savingCardID"
        //TEST
//        static let baseURL = "http://eyetopia.3pdevelopment.net"
        
        //DEVELOPMENT
        static let baseURL = httpStr+"www.eyetopiaadmin.com"

        static let isActivated = "is_activated"
        static let dateActivationCard = "date_activation_card"
    }
    
    struct pollen
    {
        //POLLEN
        
        static let low = "Low"
        static let moderate = "Moderate"
        static let high = "High"
        static let veryHigh = "Very High"
        static let extreme = "Extreme"
        
        static let pollenIndex = "pollen_index"
//        static let 
    }
    
    struct alert
    {
        static let deviceSubscribed = "Device subscribed"
        static let deviceUnsubscribed = "Device unsubscribed"
        static let noInternetConnection = "No Internet Connection"
        static let problemLoadingData = "Problem loading data"
        static let exceedErrorMessage = "You have exceeded your daily request quota for this API."
        static let noActiveBarcode = "No Active Barcode Found"
        
        //WEATHER
        static let dryIndexHighNotification = "Dry index in your area is high"
        static let pollenIndexHighNotification = "Pollen index in your area is high"
    }
    
    struct services
    {
        static let localyticsKey = "fe2e64fd2c2bd2ad5db09dd-b4034b1a-6dc9-11e8-283b-007c928ca240"
        
        //PLACE
        
        static let mainLocationTitle = "main_location_title"
        
        static let firstLaunchServiceNotification = "first_launch_service"
        static let placeID = "place_id"
        static let mainTextKeyPath = "structured_formatting.main_text"
        static let secondaryTextKeyPath = "structured_formatting.secondary_text"
        
        static let nextPageToken = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyB1yGxEoQnfY4_wEcrG68LLWvm0GWWQELg&pagetoken="
        
        static let doctorLocationsDistance = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyB1yGxEoQnfY4_wEcrG68LLWvm0GWWQELg&rankby=distance&type=doctor&keyword=optometrist"
        static let retailerlLocationsDistance = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyB1yGxEoQnfY4_wEcrG68LLWvm0GWWQELg&rankby=distance&type=pharmacy&keyword="
        static let doctorLocationsBestMatch = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyB1yGxEoQnfY4_wEcrG68LLWvm0GWWQELg&radius=40000&type=doctor&keyword=optometrist"
        static let retailerlLocationsBestMatch = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyB1yGxEoQnfY4_wEcrG68LLWvm0GWWQELg&radius=40000&type=pharmacy&keyword="
        
        static let doctorLocationsForSearch = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyB1yGxEoQnfY4_wEcrG68LLWvm0GWWQELg&rankby=distance&type=doctor&keyword=optometrist+"
        static let retailerlLocationsForSearch = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyB1yGxEoQnfY4_wEcrG68LLWvm0GWWQELg&rankby=distance&type=pharmacy&keyword="
        
        static let locationDistance = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyB1yGxEoQnfY4_wEcrG68LLWvm0GWWQELg&mode=drive&origins="
        
        
        //TEST
        
//        static let subscribePushNotification = "http://eyetopia.3pdevelopment.net/api/subscribe"
//        static let unsubscribePushNotification = "http://eyetopia.3pdevelopment.net/api/unsubscribe"
        
        //PRODUCTION
        
        static let subscribePushNotification = httpStr+"www.eyetopiaadmin.com/api/subscribe"
        static let unsubscribePushNotification = httpStr+"www.eyetopiaadmin.com/api/unsubscribe"
        
        
        //WEATHER
        
        //https://maps.googleapis.com/maps/api/distancematrix/json?origins=42.0032074,21.39511189999996&destinations=place_id:ChIJtdJ7MG37VRMRLE0yUWW9z2g&mode=drive&key=AIzaSyB1yGxEoQnfY4_wEcrG68LLWvm0GWWQELg
        
        //TEST
//        static let savingsCardUrl = "http://eyetopia.3pdevelopment.net/api/active-barcode"
        
        
        //PROD
//        static let savingsCardUrl = httpStr+"www.eyetopiaadmin.com/api/active-barcode"
        static let savingsCardUrl = "https://back.eyetopiaadmin.com/api/active-barcode"
        
        
        static let userWeatherID = "100168-1281s"
        static let passwordWeather = "pvSw7RDW"
        static let website = "http://ws.weatherzone.com.au/?"
        static let pollenForecastHeader = "&locdet=1&latlon=1&fc=1&pollen=1&format=json&u="
        static let weatherInfoHeader = "&locdet=1&latlon=1&fc=3&format=json&u="
        static let dryIndexForecastHeader = "&locdet=1&latlon=1&fc=3(index=dryskin)&format=json&u="
        //LOCATION
        
        static let googleMapsApiKey = "AIzaSyB1yGxEoQnfY4_wEcrG68LLWvm0GWWQELg"
        static let googleAutoComplete = "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyB1yGxEoQnfY4_wEcrG68LLWvm0GWWQELg&components=country:au&input="        
        static let googlePlaceId = "https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyB1yGxEoQnfY4_wEcrG68LLWvm0GWWQELg&placeid="
        
        static let allowLocationMessage = "Explanation for only while using the app: “EyeTopia” need to know your location to provide you with the Dry Eye Index rating in your area.\n\n Explanation for always: “EyeTopia” can warn you of abnormal changes to the Dry Eye Index depending where you are or travel to and will notify you."
        static let allowLocationTitle = "Allow “EyeTopia” to access your location?"
        static let onceAllowActionTitle = "Only While Using the App"
        static let alwaysAllowActionTitle = "Always Allow"
        static let dontAllowActionTitle = "Don’t Allow"
        static let latitudeDefaults = "latitude_defaults"
        static let longitudeDefaults = "longitude_defaults"
    }
    struct user
    {
        static let deviceToken = "device_token"
        static let eyeProblemId = "eye_problem_id"
        static let sufferFromAllergy = "suffer_from_allergy"
        static let dryEyesAtNight = "dry_eyes_at_night"
        static let airconEnvironmentProblem = "aircon_environment_problem"
        static let screenOrDrivingProblem = "screen_or_driving_problem"
        static let wearingContactLenses = "wearing_contact_lenses"
        static let blinkExamTime = "blink_exam_time"
        
        static let activeUltraUDSingleUse = "active_ultra_ud_single_use"
        static let activeBalance = "active_balance"
        static let activeUltraUltraUD = "active_ultra_ultraUD"
        static let activeUltraPrimary = "active_Ultra_primary"
    }
    
    static let FirstLaunch = "first_launch"
    static let nonDiagnosedJourneyCompletion = "non_diagnosed_completion"
    static let diagnosedJourneyCompletion = "diagnosed_completion"
    
    struct settings
    {
        static let notifications = "Notifications"
        static let backgroundLocation = "Background location"
        static let clearStoredData = "Clear stored data"
    }
    
    struct text
    {
        static let mustAgreeTerms = "You must agree to the terms of use to access the app"
        static let exitTitleMessage = "Are you sure you want to exit? You may continue later."
        static let clearProductMatchMessage = "Clear your product matches, exam data, and location?"
        static let burning = "Burning or stinging"
        static let sandy = "Sandy or gritty sensation"
        static let dry = "Dryness"
        static let fontSuper = "‰"
        static let null = "<null>"
        static let noLocationProvided = "No location provided"
        static let pollenForecastTitle = "4-Day Pollen Forecast"
        static let dryEyeForecastTitle = "4-Day Dry Eye Forecast"
        static let zeroResult = "ZERO_RESULTS"
        static let noSystaneCard = "No Active Barcode Found"
        static let currentLocation = "Current location"
        
    }
    
    struct productDescription {
        
        static let product1description = "PRODUCT FACTS\r\n\r\nKEY INGREDIENTS\t\r\nHYDROXYPROPYL GUAR 0.16-0.19%\r\nPOLYETHYLENE GLYCOL 400 0.40%\t\r\nPROPYLENE GLYCOL 0.30%\r\nSORBITOL 1.4%\r\n\r\nUSES\r\nFOR THE TEMPORARY RELIEF OF BURNING AND IRRITATION DUE TO DRYNESS OF THE EYE.\r\n\r\nWARNINGS\r\nOPHTHALMIC USE ONLY.\r\n\r\nDO NOT USE\r\n• IF THIS PRODUCT CHANGES COLOR OR     BECOMES CLOUDY\r\n• IF YOU ARE SENSITIVE TO ANY INGREDIENT IN     THIS PRODUCT\r\n\r\nWHEN USING THIS PRODUCT\r\n• DO NOT TOUCH TIP OF CONTAINER TO ANY     SURFACE TO AVOID CONTAMINATION\r\n• REPLACE CAP AFTER EACH USE\r\n\r\nSTOP USE AND ASK AN EYE CARE PROFESSIONAL IF\r\n• YOU FEEL EYE PAIN\r\n• CHANGES IN VISION OCCUR\r\n• REDNESS OR IRRITATION OF THE EYE(S) GETS     WORSE OR PERSISTS\r\n• YOU EXPERIENCE PERSISTENT EYE     DISCOMFORT\r\n\r\nDO NOT SWALLOW THE SOLUTION.\r\nKEEP OUT OF REACH OF CHILDREN\r\n\r\nDIRECTIONS\r\n1. SHAKE WELL BEFORE USING.\r\n2. INSTILL 1 OR 2 DROPS IN THE AFFECTED EYE(S)      AS NEEDED OR AS DIRECTED BY YOUR EYE      CARE PROFESSIONAL.\r\n\r\nOTHER INFORMATION\r\n• STERILE UNTIL OPENED\r\n• STORE BELOW 30◦C\r\n• DISCARD CONTAINER 6 MONTHS AFTER     OPENING. \r\n\r\nOTHER INGREDIENTS\r\nAMINOMETHYLPROPANOL, BORIC ACID, POTASSIUM CHLORIDE, SODIUM CHLORIDE, HYDROCHLORIC ACID and/or SODIUM HYDROXIDE, PURIFIED WATER AND POLYQUAD® (POLYQUATERNIUM-1) 0.001% PRESERVATIVE\r\n\r\nQUESTIONS?\r\nIN AUSTRALIA CALL 1800 224 153\n\nALWAYS READ THE LABEL. USE ONLY AS DIRECTED. IF SYMPTOMS PERSIST CONSULT YOUR HEALTHCARE PROFESSIONAL. SYSTANE® range of lubricant eye drops are intended for the temporary relief of burning and irritation due to dryness of the eye.\r\n"
        
        static  let product2description = "PRODUCT FACTS\r\n\r\nKEY INGREDIENTS\t\r\nHYDRPXYPROPYL GUAR 0.16-0.19 POLYETHYLENE GLYCOL 400 0.4%\nPROPYLENE GLYCOL 0.3%\nSORBITOL 1.4%\r\n\r\nUSES\r\nFOR THE TEMPORARY RELIEF OF BURNING AND IRRITATION DUE TO DRYNESS OF THE EYE.\r\n\r\nWARNINGS\r\nOPHTHALMIC USE ONLY.\r\n\r\nDO NOT USE\r\n• IF THIS PRODUCT CHANGES COLOR OR     BECOMES CLOUDY\r\n• IF YOU ARE SENSITIVE TO ANY INGREDIENT IN     THIS PRODUCT\n• IF PACKAGE OR UNIT DOSE VIALS ARE     DAMAGED OR OPEN.\r\n\r\nWHEN USING THIS PRODUCT\r\n• DO NOT TOUCH THE DROPPER TIP TO ANY     SURFACE \n\r\nSTOP USE AND ASK AN EYE CARE PROFESSIONAL IF\r\n• YOU FEEL EYE PAIN\r\n• CHANGES IN VISION OCCUR\r\n• REDNESS OR IRRITATION OF THE EYE(S) GETS     WORSE OR PERSISTS\r\n• YOU EXPERIENCE PERSISTENT EYE     DISCOMFORT OR EXCESSIVE TEARING\r\n\r\nDO NOT SWALLOW THE SOLUTION.\r\nKEEP OUT OF REACH OF CHILDREN\r\n\r\nDIRECTIONS\r\n1. TWIST OFF THE TOP OF THE SINGLE DOSE      VIAL.\n\r\n2. INSTILL 1 OR 2 DROPS IN THE AFFECTED EYE(S)      AS NEEDED OR AS DIRECTED BY YOUR EYE      CARE PROFESSIONAL.\n\nMAY BE USED TO TREAT DRY EYE ASSOCIATED WITH CONTACT LENS USAGE BY INSTILLING DROPS PRIOR TO INSERTING CONTACT LENSES AND AFTER THE REMOVAL OF CONTACT LENSES.\n\r\nOTHER INFORMATION\r\n• STERILE UNTIL OPENED\r\n• STORE BELOW 30◦C\r\n• DISCARD CONTAINER AFTER USE. \r\n\r\nOTHER INGREDIENTS\r\nAMINOMETHYLPROPANOL, BORIC ACID, POTASSIUM CHLORIDE, SODIUM CHLORIDE, HYDROCHLORIC ACID and/or SODIUM HYDROCHLORIDE, PURIFIED WATER.\r\n\r\nQUESTIONS?\r\nIN AUSTRALIA CALL 1 800 671 203\n\nALWAYS READ THE LABEL. USE ONLY AS DIRECTED. IF SYMPTOMS PERSIST CONSULT YOUR HEALTHCARE PROFESSIONAL. SYSTANE® range of lubricant eye drops are intended for the temporary relief of burning and irritation due to dryness of the eye."
        
        static let product3description = "PRODUCT FACTS\r\n\r\nKEY INGREDIENTS\t\r\nHYDROXYPROPYL GUAR 0.05% PROPYLENE GLYCOL 0.6%       \nMINERAL OIL 1% \nSORBITOL 0.7%\r\n\r\nUSES\r\nFOR THE TEMPORARY RELIEF OF BURNING AND IRRITATION DUE TO DRYNESS OF THE EYE.\r\n\r\nWARNINGS\r\nOPHTHALMIC USE ONLY.\r\n\r\nDO NOT USE\r\n• IF THIS PRODUCT CHANGES COLOR \n• IF YOU ARE SENSITIVE TO ANY INGREDIENT IN     THIS PRODUCT\n• IF PACKAGE IS DAMAGED OR OPEN.\n• IF TAMPER EVIDENT CAP IS DAMAGED\r\n\r\nWHEN USING THIS PRODUCT\r\n• DO NOT TOUCH THE DROPPER TIP TO ANY     SURFACE TO AVOID CONTAMINATION\n \nSTOP USE AND ASK AN EYE CARE PROFESSIONAL IF\r\n• YOU FEEL EYE PAIN\r\n• CHANGES IN VISION OCCUR\r\n• REDNESS OR IRRITATION OF THE EYE(S) GETS     WORSE OR PERSISTS\r\n• YOU EXPERIENCE PERSISTENT EYE     DISCOMFORT OR EXCESSIVE TEARING\r\n\r\nDO NOT SWALLOW THE SOLUTION.\r\nKEEP OUT OF REACH OF CHILDREN\r\n\r\nDIRECTIONS\r\n1. SHAKE WELL BEFORE USING.\r\n2. INSTILL 1 OR 2 DROPS IN THE AFFECTED EYE(S)      AS NEEDED OR AS DIRECTED BY YOUR EYE      CARE PROFESSIONAL.\n\nMAY BE USED TO TREAT DRY EYE ASSOCIATED WITH CONTACT LENS USAGE BY INSTILLING DROPS PRIOR TO INSERTING CONTACT LENSES AND AFTER THE REMOVAL OF CONTACT LENSES.\r\n\r\nOTHER INFORMATION\n• STERILE UNTIL OPENED\n• STORE BELOW 30◦C\n• DISCARD ANY REMAINING PRODUCT SIX     MONTHS AFTER FIRST OPENING\n\r\nOTHER INGREDIENTS\r\nDIMYRISTORYL PHOSPHATIDYDYLGLYCEROL, POLYOXYL 40 STEARATE, SORBITAN TRISTEARATE,  BORIC ACID,  EDETATE DISODIUM, HYDROCHLORIC ACID and/or SODIUM HYDROXIDE, PURIFIED WATER,AND POLYQUAD® (POLYQUATERNIUM-1) 0.001% PRESERVATIVE\n\r\nQUESTIONS?\r\nIN AUSTRALIA CALL 1800 224 153\n\nALWAYS READ THE LABEL. USE ONLY AS DIRECTED. IF SYMPTOMS PERSIST CONSULT YOUR HEALTHCARE PROFESSIONAL. SYSTANE® range of lubricant eye drops are intended for the temporary relief of burning and irritation due to dryness of the eye."
        
        static let product4description = "PRODUCT FACTS\r\n\r\nKEY INGREDIENTS\t\r\nHYDROXYPROPYL GUAR 0.7% POLYETHYLENE GLYCOL 400 0.4%\t\nPROPYLENE GLYCOL 0.3% SORBITOL 1.4%\r\n\r\nUSES\r\nDAY AND NIGHT PROTECTION AND INTENSIVE DRY EYE RELIEF OF BURNING AND IRRITATION DUE TO DRYNESS OF THE EYE.\r\n\r\nWARNINGS\r\nOPHTHALMIC USE ONLY\r\n\r\nDO NOT USE\r\n• IF THIS PRODUCT CHANGES COLOR \n• IF YOU ARE SENSITIVE TO ANY INGREDIENT IN     THIS PRODUCT\n• IF PACKAGE IS DAMAGED OR OPEN.\n• IF TAMPER EVIDENT CAP IS DAMAGED\r\n\r\nWHEN USING THIS PRODUCT\r\n• DO NOT TOUCH THE DROPPER TIP TO ANY     SURFACE TO AVOID CONTAMINATION\n \nSTOP USE AND ASK AN EYE CARE PROFESSIONAL IF\r\n• YOU FEEL EYE PAIN\r\n• CHANGES IN VISION OCCUR\r\n• REDNESS OR IRRITATION OF THE EYE(S) GETS     WORSE OR PERSISTS\r\n• YOU EXPERIENCE PERSISTENT EYE     DISCOMFORT OR EXCESSIVE TEARING\r\n\r\nDO NOT SWALLOW THE SOLUTION.\r\nKEEP OUT OF REACH OF CHILDREN\r\n\r\nDIRECTIONS\r\n1. SHAKE WELL BEFORE USING.\r\n2. INSTILL 1 OR 2 DROPS IN THE AFFECTED EYE(S)      AS NEEDED OR AS DIRECTED BY YOUR EYE      CARE PROFESSIONAL.\n\nMAY BE USED TO TREAT DRY EYE ASSOCIATED WITH CONTACT LENS USAGE BY INSTILLING DROPS PRIOR TO INSERTING CONTACT LENSES AND AFTER THE REMOVAL OF CONTACT LENSES.\n\r\nOTHER INFORMATION\r\n• STERILE UNTIL OPENED\n• STORE BELOW 30◦C\n• DISCARD ANY REMAINING PRODUCT THREE     MONTHS AFTER FIRST OPENING\n\r\nOTHER INGREDIENTS\r\nAMINOMETHYLPROPANOL, BORIC ACID, POTASSIUM CHLORIDE, SODIUM CHLORIDE, EDETATE DISODIUM, HYDROCHLORIC ACID and/or SODIUM HYDROXIDE, PURIFIED WATER  AND POLYQUAD® (POLYQUATERNIUM-1) 0.001% PRESERVATIVE\n\r\nQUESTIONS?\r\nIN AUSTRALIA CALL 1 800 224 153\n\nALWAYS READ THE LABEL. USE ONLY AS DIRECTED. IF SYMPTOMS PERSIST CONSULT YOUR HEALTHCARE PROFESSIONAL. SYSTANE® range of lubricant eye drops are intended for the temporary relief of burning and irritation due to dryness of the eye."
        
        static let product5description = "PRODUCT FACTS\r\n\r\nUSES\r\n• EYE-MAKEUP REMOVER\n• EYELID CLEANSER\r\n\r\nDIRECTIONS\r\n1. RUB THE CLOSED LID WIPE POUCH TO       DEVELOP LATHER, THEN OPEN.\n2. CLOSE EYE, THEN SWEEP LID WIPE GENTLY       ACROSS EYELID SEVERAL TIMES.\n3. USING A FRESH WIPE, REPEAT PROCESS ON      THE OTHER EYE.\n4. RINSE BOTH EYES WITH CLEAN, WARM WATER      AND PAT DRY\n5. DISCARD AFTER USE\n\r\nCAUTION\r\n• FOR EXTERNAL USE ONLY. \n• DO NOT APPLY DIRECTLY TO THE EYE.\n• DO NOT USE IF YOU HAVE KNOWN ALLERGIES        TO ANY OF THE INGREDIENTS.\n• REDUCE FREQUENCY OF USE IF IRRITATION OR     EXCESSIVE DRYNESS OCCURS.\n• DISCONTINUE USE AND CONSULT YOUR EYE     HEALTHCARE PROFESSIONAL IF YOU     EXPERIENCE EXCESSIVE ITCHING, REDNESS,     SWELLING OR PERSISTENT IRRITATION.\n• USE ONLY IF PACKETS ARE INTACT.\n• DO NOT USE IN CHILDREN\n• SINGLE-USE ONLY\n\nSTORE AT ROOM TEMPERATURE\n\nINGREDIENTS\r\nPURIFIED WATER USP, PEG-200 HYDROGENATED GLYCERYL PALMATE, DISODIUM LAURETH SULFOSUCCINATE, COCOAMIDOPROPYLAMINE OXIDE, PEG-80 GLYCERYL COCOATE, BENZYL ALCOHOL AND EDETATE DISODIUM.\n\r\nQUESTIONS?\r\nIN AUSTRALIA CALL 1 800 224 153\n\nALWAYS READ THE LABEL. USE ONLY AS DIRECTED. IF SYMPTOMS PERSIST CONSULT YOUR HEALTHCARE PROFESSIONAL."
        
        static let product6description = "PRODUCT FACTS\r\n\r\nUSES\r\n• EYE RINSE SOLUTION\n• CLEANS, REFRESHES AND SOOTHES SORE EYES\n• WASH AWAY AND CLEAN EYES FROM EVERYDAY IRRITANTS     SUCH AS DUST, SMOKE, GRIT AND POLLEN\n\r\nDIRECTIONS\r\n1. FLUSH THE AFFECTED EYE(S) AS NEEDED\n2. CONTROL FLOW RATE BY VARYING PRESSURE      ON THE BOTTLE\n\r\nCAUTION\r\n• FOR EXTERNAL USE ONLY. \n• DO NOT USE TO RINSE OR SOAK CONTACT     LENSES.\n• IF IRRITATION PERSISTS, SEEK MEDICAL ADVICE.\n• IF SOLUTION CHANGES COLOUR OR BECOMES     CLOUDY, DO NOT USE \n\nSTORE BELOW 25°C\n\nDISCARD CONTAINER 4 WEEKS AFTER OPENING \n\nINGREDIENTS\r\nPURIFIED WATERAQUA, SODIUM CHLORIDE, SODIUM ACETATE (TRIHYDRATE), SODIUM CITRATE (DIHYDRATE), POTASSIUM CHLORIDE, CALCIUM CHLORIDE (DIHYDRATE), MAGNESIUM CHLORIDE (HEXAHYDRATE), BENZALKONIUM CHLORIDE SOLUTION, SODIUM HYDROXIDE AND/OR HYDROCHLORIC ACID , PURIFIED WATER.\n\r\nQUESTIONS?\r\nIN AUSTRALIA CALL 1 800 224 153\n\nALWAYS READ THE LABEL. USE ONLY AS DIRECTED. IF SYMPTOMS PERSIST CONSULT YOUR HEALTHCARE PROFESSIONAL."
        
        static let product7description = "PRODUCT FACTS\r\n\r\nACTIVE INGREDIENTS\t\nNAPHAZOLINE HYDROCHLORIDE 0.1%\n\nUSES\r\nFOR THE RELIEF OF RED CONGESTED EYES.\n\r\nWARNINGS\nOPHTHALMIC USE ONLY.\nCONSULT YOUR DOCTOR OR PHARMACIST BEFORE USING IN CONJUNCTION WITH OTHER EYE DROPS\n\nDO NOT USE\n• IF YOU HAVE GLAUCOMA OR OTHER SERIOUS     EYE CONDITIONS\n• IF SEAL IS BROKEN\n• IN CHILDREN\n• WHILE WEARING SOFT CONTACT LENSES. SOFT     CONTACT LENSES CAN BE INSERTED 15     MINUTES AFTER USING THE PRODUCT.\n\nWHEN USING THIS PRODUCT\n• DO NOT TOUCH THE DROPPER TIP TO YOUR     EYE OR TO ANY OTHER SURFANCE \n• PROLONGED USE MAY BE HARMFUL\n\nSTOP USE AND ASK AN EYE CARE PROFESSIONAL IF SYMPTOMS PERSIST OR WORSEN\n\nKEEP OUT OF REACH OF CHILDREN\n\r\nDIRECTIONS\r\n1. INSTILL 1 OR 2 DROPS IN THE AFFECTED EYE(S)      EVERY 3 TO 4 HOURS AS REQUIRED, OR AS      DIRECTED BY YOUR DOCTOR OR PHARMACIST\n\nOTHER INFORMATION\nSTERILE UNTIL OPENED\nSTORE BELOW 25◦C\nDISCARD CONTAINER 4 WEEKS AFTER OPENING. \nPROTECT FROM EXCESSIVE HEAT\n\nOTHER INGREDIENTS\r\nBENZALKONIUM CHLORIDE 0.1mg/mL as preservative\n\r\nQUESTIONS?\r\nIN AUSTRALIA CALL 1 800 224 153\n\nSYSTANE® Red Eyes drops is indicated for use as a topical ocular vasoconstrictor – for relief of red congested eyes.\nALWAYS READ THE LABEL. USE ONLY AS DIRECTED. IF SYMPTOMS PERSIST CONSULT YOUR HEALTHCARE PROFESSIONAL.\n"
    }
    struct keysForUserDefaults
    {
        static let forBlinkExamResults = "results"
        static let journeyStation = "journey_station"
        static let journeyStationDictKey = "journey_station_dict_key"
        static let navigationJourney = "navigation_journey_key"
        
        static let journeyContinuationReminders = "journey_continuation_reminders_key"
        static let blinkExamReminders = "blink_exam_reminders_key"
        static let dryEyeIndexAndPollenForecastNotices = "dry_eye_index_and_pollen_forecast_notices"
        static let savingCardDiscountNotifications = "saving_card_discount_notifications"
    }
    
    static let presentHomeKey = "home_key"
    static let presentBlinkExamChartKey = "blink_exam_chart_key"
    static let presentLatestProductMatchKey = "latest_product_match_key"
    static let presentDiagnoseDryEyeKey = "diagnose_dry_eye_key"
    static let presentStartNewJourneyKey = "start_new_journey_key"
    static let presentContinueLeftJourneyKey = "continue_left_journey_key"
    static let presentFindRetailerKey = "find_retailer_key"
    static let presentLandingFindRetailerKey = "landing_find_retailer_key"
    static let presentEyeCareProffesionalKey = "eye_care_proffesional_key"
    static let presentCouponAndSavingCardKey = "saving_card_key"
    static let presentLocatorKey = "locator_key"
    static let presentProductsKey = "products_key"
    static let presentProductMatchKey = "product_match_key"
    static let presentEducationKey = "education_key"
    static let presentPollenForecastKey = "pollen_forecast_key"
    
    static let pushExpirienceDryEyeKey = "expirience_dry_eye_Key"
    static let pushContactLensKey = "contact_lens_key"
    static let pushReceiveGiftCardKey = "receive_gift_card_key"
    static let presentProductDetailsKey = "present_product_details_key"
    
    static let presentLandingProductMatchKey = "landing_product_match_key"
    static let presentLandingBlinkExamDashboardKey = "landing_blink_exam_key"
    static let presentLandingSavingsCardKey = "landing_savings_card_key"
    static let presentLandingProductDetailsKey = "present_landing_product_details_key"
    static let presentHomeSelectionKey = "present_home_selection_key"
    
    static let didChangeWindowOrientationKey = "did_change_window_orientation_key"
    
    static let checkNotificationAuthorizationKey = "notification_authorization_key"
    static let didSelectNotificationControllerKey = "did_select_notification_controller_key"
}
