//
//  Style.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/5/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

struct Style {
    static let navigationBottomBackgroundViewColor = UIColor(red:0.00, green:0.31, blue:0.32, alpha:1.0)
    static let buttonTextColor = UIColor.white
    static let secondaryColor = UIColor(red:0.01, green:0.29, blue:0.35, alpha:1.0)
    static let primaryColor = UIColor(red:0.00, green:0.56, blue:0.53, alpha:1.0)
    static let outterBackgroundColor = UIColor(red:0.03, green:0.23, blue:0.27, alpha:1.0)
    static let selectedCellBackgroundColor = UIColor(red:0.40, green:0.20, blue:0.60, alpha:1.0)
    static let nonSelectedCellBackgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0)
    static let navyDark = UIColor(red:0.29, green:0.29, blue:0.29, alpha:1.0)
    static let shadowNavyDark = UIColor(red:0.29, green:0.29, blue:0.29, alpha:0.4)
    static let textColor = UIColor(red:0.00, green:0.24, blue:0.22, alpha:1.0)
    static let noLocationColor = UIColor(red:0.50, green:0.59, blue:0.61, alpha:1.0)
    static let defaultBlue = UIColor(red:0.29, green:0.56, blue:0.89, alpha:1.0)
    
    struct switchThumb {
        static let on = UIColor.white
        static let off = UIColor(red:0.81, green:0.81, blue:0.81, alpha:1.0)
    }
    
    struct font {
        static let steelGrey = UIColor(red:0.50, green:0.55, blue:0.55, alpha:1.0)
        static let termsTextColor = UIColor(red: 70.0 / 255.0, green: 142.0 / 255.0, blue: 229.0 / 255.0, alpha: 1.0)
    }
    
    struct pollenColor {
        static let low = UIColor(red:0.22, green:0.79, blue:0.45, alpha:1.0)
        static let moderate = UIColor(red:0.95, green:0.77, blue:0.05, alpha:1.0)
        static let high = UIColor(red:0.90, green:0.49, blue:0.14, alpha:1.0)
        static let veryHigh = UIColor(red:0.91, green:0.30, blue:0.24, alpha:1.0)
        static let extreme = UIColor(red:0.46, green:0.07, blue:0.03, alpha:1.0)

    }
}
