//
//  RoundedButton.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 5/15/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {

    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 4
        self.clipsToBounds = true
    }
 

}
