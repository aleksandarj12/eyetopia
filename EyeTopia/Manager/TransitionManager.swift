//
//  TransitionManager.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/9/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class TransitionManager: NSObject {
    
    var didFinishTransition : Bool = false
    var storyboardID : String?
    var transitionLifeStyle : Int!
    var appDelegate : AppDelegate!
    var unfinishedJourney : String = ""
    var isSkipToProductMatch : Bool = false
    
    var productLearnMoreSelected : Product!
    
    var blinkExamRepeat : Bool = false
    
    var journeyAgain : Bool = false
    var diagnosedWithDryEyeButton : Bool = false     //from landing screen clicked "I've been Diagnosed with Dry eye"
//    var continueJourneyStateAnimation : Bool = false    //this represents if some of the screens has loaded animation
    
    static let sharedInstance : TransitionManager = {
        let instance = TransitionManager()
        return instance
    }()

    
    //MARK: Set Methods
    
    func setContinueJourneyDescription(controllerDescription : String)
    {
        UserDefaults.standard.setValue(controllerDescription, forKeyPath: k.keysForUserDefaults.journeyStation)
    }
    
    func setIsSkipToProductMatch(flag : Bool)
    {
        self.isSkipToProductMatch = flag
    }
    
    func setDidFinishTransition(flag : Bool)
    {
        self.didFinishTransition = flag
    }
    
    func setStoryboardID(storyboardID : String)
    {
        self.storyboardID = storyboardID
    }
    
    func setAppDelegate(delegate : AppDelegate)
    {
        self.appDelegate = delegate
    }
    
    func setUnfinishedJourney(storyboardID : String)
    {
        self.unfinishedJourney = storyboardID
    }
    
    func setFirstLaunchFlow(flag : Bool)
    {
        let userDefaults : UserDefaults = UserDefaults.standard
        userDefaults.set(flag, forKey: k.FirstLaunch)
    }
    
    
    //MARK: Get Methods
    
    func getContinueJourneyDescription() -> String
    {
        return UserDefaults.standard.value(forKey: k.keysForUserDefaults.journeyStation) as! String
    }
    
    func getFirstLaunchFlow() -> Bool
    {
        return UserDefaults.standard.bool(forKey: k.FirstLaunch)
    }

}
