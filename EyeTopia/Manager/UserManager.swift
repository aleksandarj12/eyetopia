//
//  UserManager.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/16/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import SwiftyUserDefaults
import UserNotifications

let controllersArray = DefaultsKey<[String]?>("controllersArray")
let savingCard = DefaultsKey<Bool?>("savingCard")
let savingCardID = DefaultsKey<Int?>("savingCardID")

class UserManager: NSObject {
    
    var selectedEyeSymptom : EyeProblem? = EyeProblem()
    var blinkExamTimeString : String = ""
    var searchOption : SearchOption?

    var isChangedLocation : Bool = false
    
    var isNotificationController : Bool = false
    
    var journeyStation : UINavigationController!
    
    var location : CLLocation?
    var locationDescription : String = ""
    
    var isCurrentLocationActive : Bool = false
    var mainLocationTitleOption : MainLocationTitleOption = .mainLocation
    
    var searchLocationResult : String = ""                  //LocationCustomViewController
    var searchLocation : LocationDetail = LocationDetail()
    
    

    
    static let sharedInstance : UserManager = {
        let instance = UserManager()
        return instance
    }()

    
    func checkSavingCardTimeForReset()
    {
        if let period = self.getNextCardPeriod() {
            if(period < Date())
            {
                self.setIsSavingCardActivated(false)
                self.setDateSavingCardActivated(nil)
            }
            else
            {
                
            }
        }
        else
        {
//            self.setIsSavingCardActivated(false)
//            self.setDateSavingCardActivated(nil)
        }
        
    }
    
    //MARK: Savings Card
    
    func setSavingCardRetailerCopy(_ text : String?)
    {
        UserDefaults.standard.set(text, forKey: k.card.retailerCopy)
        UserDefaults.standard.synchronize()
    }
    
    func setSavingCardDiscount(_ discount : Double?)
    {
        UserDefaults.standard.set(discount, forKey: k.card.discount)
        UserDefaults.standard.synchronize()
    }
    
    func setSavingCardTimeAvailable(_ days : Int?)
    {
        UserDefaults.standard.set(days, forKey: k.card.timeAvailable)
        UserDefaults.standard.synchronize()
    }
    
    func setSavingCardInstructions(_ text : String?)
    {
        UserDefaults.standard.set(text, forKey: k.card.instructions)
        UserDefaults.standard.synchronize()
    }
    
    func setSavingCardConditions(_ text : String?)
    {
        UserDefaults.standard.set(text, forKey: k.card.termsConditions)
        UserDefaults.standard.synchronize()
    }
    
    func setSavingCardBarcode(_ barcodeStringURL : String?)
    {
        UserDefaults.standard.set(barcodeStringURL, forKey: k.card.barcodeImage)
        UserDefaults.standard.synchronize()
        //        UserDefaults.standard.set(UIImagePNGRepresentation(barcode), forKey: k.card.barcodeImage)
    }
    
    func setSavingCardRetailerLogo(_ logoStringURL : String?)
    {
        UserDefaults.standard.set(logoStringURL, forKey: k.card.retailerImage)
        UserDefaults.standard.synchronize()
        //        UserDefaults.standard.set(UIImagePNGRepresentation(logo), forKey: k.card.retailerImage)
    }
    
    func setSavingCardID(cardID : Int?)
    {
        UserDefaults.standard.set(cardID, forKey: k.card.savingCardID)
        UserDefaults.standard.synchronize()
    }
    
    func setDateSavingCardActivated(_ date : Date?)
    {
        UserDefaults.standard.set(date, forKey: k.card.dateActivationCard)
        UserDefaults.standard.synchronize()
    }
    
    func setIsSavingCardActivated(_ flag : Bool)
    {
//        self.setDateSavingCardActivated(Date())
        UserDefaults.standard.set(flag, forKey: k.card.isActivated)
        UserDefaults.standard.synchronize()
    }
    
    
    //MARK: Set Methods
    
    func setMainLocationTitle(_ place : Place)
    {
        var string : String = place.mainText!
        
        let s1 : NSString = place.secondaryText! as NSString
        let splitRange = s1.range(of: ",")
        string.append(", ")
        if(s1.contains(","))
        {
            string.append(s1.substring(to: splitRange.location))
        }
        else
        {
            string.append(s1 as String)
        }
        
        
        UserDefaults.standard.set(string, forKey: k.services.mainLocationTitle)
        UserDefaults.standard.synchronize()
    }
    
    func setIsCurrentLocationActive(_ flag : Bool)
    {
        self.isCurrentLocationActive = flag
    }
    
    func setJourneyStation(_ navController : UINavigationController?, _ controller : UIViewController?)
    {
        if (navController == nil)
        {
            Defaults[controllersArray] = nil
        }
        else
        {
            var controllerStrings : [String] = [String]()
            
            for controller in (navController?.viewControllers)!
            {
                var controllerString : String = ""
                
                if(controller is TransitionViewController)
                {
                    controllerString = (controller as! TransitionViewController).storyboardTitle
                }
                else
                {
                    controllerString = String(describing: controller.self)
                }
                
                controllerStrings.append(controllerString)
            }
            
            Defaults[controllersArray] = controllerStrings
        }
        Defaults.synchronize()
    }
    
    func setSearchOption(searchOption : SearchOption)
    {
        self.searchOption = searchOption
    }
    
    func setIsChangedLocation(flag : Bool)
    {
        self.isChangedLocation = flag
    }
    
    func setLocationDescription(locationString : String)
    {
        self.locationDescription = locationString
    }
    
    func setLocationPlace(location : CLLocation?)
    {
        self.location = location
        UserDefaults.standard.set(location?.coordinate.latitude, forKey: k.services.latitudeDefaults)
        UserDefaults.standard.set(location?.coordinate.longitude, forKey: k.services.longitudeDefaults)
        UserDefaults.standard.synchronize()
    }
    
    func setIsActiveUltraPrimary(flag : Bool)
    {
        UserDefaults.standard.set(flag, forKey: k.user.activeUltraPrimary)
        UserDefaults.standard.synchronize()
    }
    
    func setIsActiveUltraUltraUD(flag : Bool)
    {
        UserDefaults.standard.set(flag, forKey: k.user.activeUltraUltraUD)
        UserDefaults.standard.synchronize()
    }
    
    func setIsActiveBalance(flag : Bool)
    {
        UserDefaults.standard.set(flag, forKey: k.user.activeBalance)
        UserDefaults.standard.synchronize()
    }
    
    func setIsActiveUltraUDSingleUse(flag : Bool)
    {
        UserDefaults.standard.set(flag, forKey: k.user.activeUltraUDSingleUse)
        UserDefaults.standard.synchronize()
    }
    
    func setIsSufferFromAllergy(flag : Bool)
    {
        UserDefaults.standard.set(flag, forKey: k.user.sufferFromAllergy)
        UserDefaults.standard.synchronize()
    }
    
    func setIsDryEyesAtNight(flag : Bool)
    {
        UserDefaults.standard.set(flag, forKey: k.user.dryEyesAtNight)
        UserDefaults.standard.synchronize()
    }
    
    func setIsAirconEnvironmentProblem(flag : Bool)
    {
        UserDefaults.standard.set(flag, forKey: k.user.airconEnvironmentProblem)
        UserDefaults.standard.synchronize()
    }
    
    func setIsScreenOrDrivingProblem(flag : Bool)
    {
        UserDefaults.standard.set(flag, forKey: k.user.screenOrDrivingProblem)
        UserDefaults.standard.synchronize()
    }
    
    func setSelectedEyeSymptom(eyeProblem : EyeProblem?)
    {
        if let problemId = eyeProblem?.id {
            UserDefaults.standard.set(problemId, forKey: k.user.eyeProblemId)
        }
        else
        {
            UserDefaults.standard.set(nil, forKey: k.user.eyeProblemId)
        }
        UserDefaults.standard.synchronize()
    }
    
    func setIsWearingContactLenses(flag : Bool)
    {
        UserDefaults.standard.set(flag, forKey: k.user.wearingContactLenses)
        UserDefaults.standard.synchronize()
    }
    
    func setBlinkExamTime(time : Float)
    {
        UserDefaults.standard.set(time, forKey: k.user.blinkExamTime)
        UserDefaults.standard.synchronize()
    }
    
    func setBlinkExamTimeString(timeString : String)
    {
        self.blinkExamTimeString = timeString
        UserDefaults.standard.synchronize()
    }
    
    func setDiagnosedJourneyCompetion(flag : Bool)
    {
        let userDefaults = UserDefaults.standard
        userDefaults.set(flag, forKey: k.diagnosedJourneyCompletion)
        UserDefaults.standard.synchronize()
    }
    
    func setNonDiagnosedJourneyCompetion(flag : Bool)
    {
        let userDefaults = UserDefaults.standard
        userDefaults.set(flag, forKey: k.nonDiagnosedJourneyCompletion)
        UserDefaults.standard.synchronize()
    }
    
    
    
    
    func removeProductMatchData()
    {
        self.setIsCurrentLocationActive(false)
        self.setLocationDescription(locationString: "")
        
        self.setIsWearingContactLenses(flag: false)
        self.setIsScreenOrDrivingProblem(flag: false)
        self.setIsAirconEnvironmentProblem(flag: false)
        self.setIsDryEyesAtNight(flag: false)
        self.setIsSufferFromAllergy(flag: false)
        
        self.setIsActiveUltraUDSingleUse(flag: false)
        self.setIsActiveUltraPrimary(flag: false)
        self.setIsActiveUltraUltraUD(flag: false)
        self.setIsActiveBalance(flag: false)
        self.setIsActiveUltraUDSingleUse(flag: false)
    }
    
    
    //MARK: Get Methods
    
    func getMainLocationTitle() -> String?
    {
        if let location = UserDefaults.standard.value(forKey: k.services.mainLocationTitle)
        {
            return location as! String
        }
        else
        {
            return nil
        }
    }
    
    
    func getLocation() -> CLLocation?
    {
        if let latitude = UserDefaults.standard.value(forKey: k.services.latitudeDefaults)
        {
            let longitude = UserDefaults.standard.value(forKey: k.services.longitudeDefaults) as! CLLocationDegrees
            return CLLocation(latitude: latitude as! CLLocationDegrees, longitude: longitude)
        }
        else
        {
            return nil
        }
    }
    
    func getExpireDateCard() -> Date
    {
        let time : Int = self.getSavingCardTime()!
        return Calendar.current.date(byAdding: .day, value: time, to: Date())!
    }
    
    func getNextCardPeriod() -> Date?
    {
        var newDate : Date?
//        if let date : Date = UserDefaults.standard.value(forKey: k.card.dateActivationCard) as? Date {
        if let date : Date = UserManager.sharedInstance.getDateActivationCard() {
            newDate = Calendar.current.date(byAdding: .day, value: 30, to: date)
            newDate = Calendar.current.date(byAdding: .day, value: self.getSavingCardTime()!, to: newDate!)
        }
        else
        {
            return nil
        }
        

        return newDate!
    }
    
    func getDateActivationCard() -> Date?
    {
        return UserDefaults.standard.value(forKey: k.card.dateActivationCard) as? Date
    }
    
    func getSavingCardRetailerCopy() -> String?
    {
        if let string = UserDefaults.standard.value(forKey: k.card.retailerCopy)
        {
            return string as? String
        }
        return nil
    }
    
    func getSavingCardDiscount() -> Float?
    {
        if let discount = UserDefaults.standard.value(forKey: k.card.discount)
        {
            return discount as? Float
        }
        return nil
    }
    
    func getSavingCardTime() -> Int?
    {
        if let time = UserDefaults.standard.value(forKey: k.card.timeAvailable)
        {
            return time as? Int
        }
        return nil
    }
    
    func getSavingCardInstructions() -> String?
    {
        if let instructions = UserDefaults.standard.value(forKey: k.card.instructions)
        {
            return instructions as? String
        }
        return nil
    }
    
    func getSavingCardConditions() -> String?
    {
        if let conditions = UserDefaults.standard.value(forKey: k.card.termsConditions)
        {
            return conditions as? String
        }
        return nil
    }
    
    
    func getSavingCardBarcode() -> String?
    {
//        let imageData : Data = UserDefaults.standard.value(forKey: k.card.barcodeImage) as! Data
//        let image : UIImage = UIImage(data: imageData)!
        if let string = UserDefaults.standard.value(forKey: k.card.barcodeImage)
        {
            return string as? String
        }
        return nil
    }
    
    func getSavingCardRetailer() -> String?
    {
//        let imageData : Data = UserDefaults.standard.value(forKey: k.card.retailerImage) as! Data
//        let image : UIImage = UIImage(data: imageData)!
        if let string = UserDefaults.standard.value(forKey: k.card.retailerImage)
        {
            return string as? String
        }
        return nil
    }
    
    func getSavingCardID() -> Int?
    {
        if(Defaults[savingCardID] == nil)
        {
            return nil
        }
        return Defaults[savingCardID]
    }
    
    func getIsSavingCardActivated() -> Bool
    {
        if let flag = UserDefaults.standard.value(forKey: k.card.isActivated)
        {
            return flag as! Bool
        }
        else
        {
            UserDefaults.standard.set(false, forKey: k.card.isActivated)
        }
        return false
    }
    
    func getClearProductMatch() -> Bool
    {
        var counter : Int = 0
        if(!self.getIsActiveBalance())
        {
            counter += 1
        }
        if(!self.getIsActiveUltraUD())
        {
            counter += 1
        }
        if(!self.getIsActiveUltraUDSingleUse())
        {
            counter += 1
        }
        if(!self.getIsActiveUltraUltraUD())
        {
            counter += 1
        }
        if(counter == 4)
        {
            return true
        }
        return false
    }
    
    func getController(_ controllerString : String) -> UIViewController?
    {
        let string = self.getIdentifier(controllerString)
        let diagnosedStoryboard : UIStoryboard = UIStoryboard(name: k.storyboard.diagnosed, bundle: nil)
        let nonDiagnosedStoryboard : UIStoryboard = UIStoryboard(name: k.storyboard.nonDiagnosed, bundle: nil)
        let mainStoryBoard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        if(string == "TransitionViewController11" || string == "ExpirienceDryEyeSymptomViewController" || string == "ReceiveGiftViewController")
        {
            return diagnosedStoryboard.instantiateViewController(withIdentifier: string)
        }
        if(string == "BlinkExamSurveyViewController")
        {
            return nonDiagnosedStoryboard.instantiateViewController(withIdentifier: string)
        }
        else
        {
            return mainStoryBoard.instantiateViewController(withIdentifier: string)
        }
        
    }
    
    func checkGuideMainViewController(_ controller : UIViewController) -> UIViewController
    {
        if (controller is GuideMainViewController)
        {
            (controller as! GuideMainViewController).isContinueJourney = true
        }
        
        return controller
    }
    
    func getJourneyStation() -> UINavigationController?
    {
        if let journey = Defaults[controllersArray]
        {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

            
            let identifier : String = self.getIdentifier(journey[0])
            var rootController : UIViewController!
            
            if(identifier == "MainViewController")
            {
                rootController = MainViewController(optionShown: .firstFlow)
                
            }
            else
            {
                 rootController = mainStoryboard.instantiateViewController(withIdentifier: self.getIdentifier(journey[0]))
            }
            rootController = self.checkGuideMainViewController(rootController)
            
            let navController : UINavigationController = UINavigationController(rootViewController: rootController)
            
            if(journey.count > 1)
            {
                for i in 1...journey.count-1
                {
                    var controller = self.getController(journey[i])
                    
                    controller = self.checkGuideMainViewController(controller!)
                    
                    navController.viewControllers.append(controller!)
                }
            }
            
            return navController
        }

        return nil
    }
    
    func getLocationPlace()
    {
        if let latitude = UserDefaults.standard.value(forKey: k.services.latitudeDefaults)
        {
            let longitude = UserDefaults.standard.value(forKey: k.services.longitudeDefaults) as! CLLocationDegrees
            let location : CLLocation = CLLocation(latitude: latitude as! CLLocationDegrees, longitude: longitude)
            self.setLocationPlace(location: location)
        }
        else
        {
            self.setLocationPlace(location: nil)
        }
    }
    
    func getIsActiveUltraUD() -> Bool
    {
        if let flag = UserDefaults.standard.value(forKey: k.user.activeUltraPrimary)
        {
            return flag as! Bool
        }
        else
        {
            return false
        }
    }
    
    func getIsActiveUltraUltraUD() -> Bool
    {
        if let flag = UserDefaults.standard.value(forKey: k.user.activeUltraUltraUD)
        {
            return flag as! Bool
        }
        else
        {
            return false
        }
    }
    
    func getIsActiveBalance() -> Bool
    {
        if let flag = UserDefaults.standard.value(forKey: k.user.activeBalance)
        {
            return flag as! Bool
        }
        else
        {
            return false
        }
    }
    
    func getIsActiveUltraUDSingleUse() -> Bool
    {
        if let flag = UserDefaults.standard.value(forKey: k.user.activeUltraUDSingleUse)
        {
            return flag as! Bool
        }
        else
        {
            return false
        }
    }
    
    func getSufferFromAllergy() -> Bool
    {
        if let flag = UserDefaults.standard.value(forKey: k.user.sufferFromAllergy)
        {
            return flag as! Bool
        }
        else
        {
            return false
        }
    }
    
    func getDryEyesAtNight() -> Bool
    {
        if let flag = UserDefaults.standard.value(forKey: k.user.dryEyesAtNight)
        {
            return flag as! Bool
        }
        else
        {
            return false
        }
    }
    
    func getAirconEnvironmentProblem() -> Bool
    {
        if let flag = UserDefaults.standard.value(forKey: k.user.airconEnvironmentProblem)
        {
            return flag as! Bool
        }
        else
        {
            return false
        }
    }
    
    func getScreenOrDrivingProblem() -> Bool
    {
        if let flag = UserDefaults.standard.value(forKey: k.user.screenOrDrivingProblem)
        {
            return flag as! Bool
        }
        else
        {
            return false
        }
    }
    
    func getWearingContactLenses() -> Bool
    {
        if let flag = UserDefaults.standard.value(forKey: k.user.wearingContactLenses)
        {
            return flag as! Bool
        }
        else
        {
            return false
        }
    }
    
    func getBlinkExamTime() -> Float
    {
        if let time = UserDefaults.standard.value(forKey: k.user.blinkExamTime)
        {
            return time as! Float
        }
        else
        {
            return 0.0
        }
    }
    
    func getNonDiagnosedJourneyCompletion() -> Bool
    {
        var flag = UserDefaults.standard.bool(forKey: k.nonDiagnosedJourneyCompletion)
        if(!flag)
        {
            let userDefaults = UserDefaults.standard
            userDefaults.set(false, forKey: k.nonDiagnosedJourneyCompletion)
            flag = false
        }
        return flag
    }
    
    func getDiagnosedJourneyCompletion() -> Bool
    {
        var flag = UserDefaults.standard.bool(forKey: k.diagnosedJourneyCompletion)
        if(!flag)
        {
            let userDefaults = UserDefaults.standard
            userDefaults.set(false, forKey: k.diagnosedJourneyCompletion)
            flag = false
        }
        return flag
    }
    
    func getSelectedEyeSymptom() -> EyeProblem?
    {
        if let symptomId = UserDefaults.standard.value(forKey: k.user.eyeProblemId) {
            
            if(symptomId is Int)
            {
                return DataManager.sharedInstance.getEyeSymptomsList()[symptomId as! Int]
            }
            return nil
        }
        
        return nil
    }
    
    //MARK: - Setup & Support Methods

    func setupDiagnosedNewJourney()
    {
//        self.setDiagnosedJourneyCompetion(flag: false)
        self.setIsSufferFromAllergy(flag: false)
        self.setIsDryEyesAtNight(flag: false)
        self.setIsAirconEnvironmentProblem(flag: false)
        self.setIsScreenOrDrivingProblem(flag: false)
        self.setIsWearingContactLenses(flag: false)
        self.setBlinkExamTime(time: 0.0)
    }
    
    
    //MARK: - Save and load blink exam results
    
    func saveBlinkExam(result: BlinkExamResult) {
        
        
        if let dataArray = UserDefaults.standard.object(forKey: k.keysForUserDefaults.forBlinkExamResults) as? [Data] {
            let oldBlinkExamResultArray = dataArray.map { NSKeyedUnarchiver.unarchiveObject(with: $0) as! BlinkExamResult}
            var newBlinkExamResultArray = [BlinkExamResult]()
            var flag : Bool = true
            for x in oldBlinkExamResultArray {
                if sameDay(newDate: result.date , oldDate: x.date) {
                    newBlinkExamResultArray.append(result)
                    flag = false
                } else {
                    newBlinkExamResultArray.append(x)
                }
            }
            if(flag)
            {
                newBlinkExamResultArray.append(result)
            }
            let newDataArray = newBlinkExamResultArray.map { NSKeyedArchiver.archivedData(withRootObject: $0)}
            UserDefaults.standard.set(newDataArray, forKey: k.keysForUserDefaults.forBlinkExamResults)
            
        } else {
            var newDataArray = [Data]()
            let newDataElement = NSKeyedArchiver.archivedData(withRootObject: result)
            newDataArray.append(newDataElement)
            UserDefaults.standard.set(newDataArray, forKey: k.keysForUserDefaults.forBlinkExamResults)
        }
    }
    
    func loadBlinkExamResults() -> [BlinkExamResult] {
        var results = [BlinkExamResult]()
        
        if let dataArray = UserDefaults.standard.object(forKey: k.keysForUserDefaults.forBlinkExamResults) as? [Data] {
            
            for d in dataArray {
                if let newElement = NSKeyedUnarchiver.unarchiveObject(with: d) as? BlinkExamResult {
                    results.append(newElement)
                }
            }
        }
        return results
    }
    
    func sameDay(newDate: Date, oldDate: Date) -> Bool {
        let yearCompare = Calendar.current.compare(newDate, to: oldDate, toGranularity: .year)
        let monthCompare = Calendar.current.compare(newDate, to: oldDate, toGranularity: .month)
        let dayCompare = Calendar.current.compare(newDate, to: oldDate, toGranularity: .day)
        let timeCompare = newDate > oldDate
        
        if yearCompare == .orderedSame && monthCompare == .orderedSame && dayCompare == .orderedSame && timeCompare == true {
            return true
        } else {
            return false
        }
    }
    
    
    func removeBlinkExamResults() -> Void
    {
        let newDataArray = [BlinkExamResult]()
        
        UserDefaults.standard.set(newDataArray, forKey: k.keysForUserDefaults.forBlinkExamResults)
    }
    
    
    
    func stringClassFromString(_ className: String) -> AnyClass! {
        
        /// get namespace
        let namespace = Bundle.main.infoDictionary!["CFBundleExecutable"] as! String;
        
        /// get 'anyClass' with classname and namespace
        let cls: AnyClass = NSClassFromString("\(namespace).\(className)")!;
        
        // return AnyClass!
        return cls;
    }
    
    
    
    func getIdentifier(_ identifier : String ) -> String
    {
        if(identifier.contains("."))
        {
            var newString : NSString = identifier as NSString
            let startRange : NSRange = newString.range(of: ".")
            
            
            newString = newString.substring(from: startRange.location + 1) as NSString
            let endRange : NSRange = newString.range(of: ":")
            newString = newString.substring(to: endRange.location) as NSString
            
            return newString as String
        }
        else if(identifier == "")
        {
            return "TransitionViewController11"
        }
        
        return identifier
    }
    
}

enum FlowStation {
    case diagnosedUser
    case nonDiagnoseduser
    case unfinishedFlow
}

enum MainLocationTitleOption {
    case mainLocation
    case locatorSearch
}

class JourneyStation : NSObject, NSCoding
{
    var navController: UINavigationController
    
    init(navController: UINavigationController) {
        self.navController = navController
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.navController, forKey: "navigation_controller")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let navController = aDecoder.decodeObject(forKey: "navigation_controller") as? UINavigationController else { return nil }
        self.init(navController: navController)
    }
    
}
