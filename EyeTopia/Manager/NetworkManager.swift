//
//  NetworkManager.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/23/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Reachability

class NetworkManager: NSObject {
    
    static let sharedInstance : NetworkManager = {
        let instance = NetworkManager()
        return instance
    }()
    
    func checkReachability() -> Bool
    {
        let reach : Reachability = Reachability(hostName: "www.google.com")
        
        if(reach.isReachable())
        {
            return true
        }
        else
        {
            return false
        }
    }
}
