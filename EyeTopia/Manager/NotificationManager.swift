//
//  File.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 6/14/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import UserNotifications

class NotificationManager: NSObject {
    
    var trackForJourneyContinuationNotification: Bool {
        if UserManager.sharedInstance.getDiagnosedJourneyCompletion() == false && UserManager.sharedInstance.getNonDiagnosedJourneyCompletion() == false {
            return true
        } else {
            return false
        }
    }
    
    static let sharedInstance : NotificationManager = {
        let instance = NotificationManager()
        return instance
    }()
    
    
    //MARK: Set Notifications
    
    func setJourneyContinuationReminders(flag: Bool) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(flag, forKey: k.keysForUserDefaults.journeyContinuationReminders)
    }
    
    func setBlinkExamReminders(flag: Bool) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(flag, forKey: k.keysForUserDefaults.blinkExamReminders)
    }
    
    func setDryEyeIndexAndPollenForecastNotices(flag: Bool) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(flag, forKey: k.keysForUserDefaults.dryEyeIndexAndPollenForecastNotices)
    }
    
    func setSavingCardDiscountNotifications(flag: Bool) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(flag, forKey: k.keysForUserDefaults.savingCardDiscountNotifications)
    }
    
    //MARK: Get Notifications
    
    func getJourneyContinuationReminders() -> Bool {
        let isSet = UserDefaults.standard.bool(forKey: k.keysForUserDefaults.journeyContinuationReminders)
        return isSet
    }
    
    func getBlinkExamReminders() -> Bool {
        let isSet = UserDefaults.standard.bool(forKey: k.keysForUserDefaults.blinkExamReminders)
        return isSet
    }
    
    func getDryEyeIndexAndPollenForecastNotices() -> Bool {
        let isSet = UserDefaults.standard.bool(forKey: k.keysForUserDefaults.dryEyeIndexAndPollenForecastNotices)
        return isSet
    }
    
    func getSavingCardDiscountNotifications() -> Bool {
        let isSet = UserDefaults.standard.bool(forKey: k.keysForUserDefaults.savingCardDiscountNotifications)
        return isSet
    }
    
    //MARK: Schedule Notifications
    
    func scheduleJourneyContinuationReminderNotification() {
        
        if getJourneyContinuationReminders() == true && trackForJourneyContinuationNotification == true {
            let content = UNMutableNotificationContent()
            content.body = "Don't forget to come back to complete your journey!"
            content.badge = 1
            content.sound = UNNotificationSound.default()
            //86400
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 86400, repeats: false)
            let request = UNNotificationRequest(identifier: "JourneyContinuationReminderNotification", content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        } else {
            cancelNotifictaion(withIndentifier: "JourneyContinuationReminderNotification")
        }
    }
    
    func scheduleBlinkExamReminders() {
        if getBlinkExamReminders() == true {
            let content = UNMutableNotificationContent()
            content.body = "Take the blink exam again and track your results over time."
            content.badge = 1
            content.sound = UNNotificationSound.default()
            //1814400
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1814400, repeats: false)
            let request = UNNotificationRequest(identifier: "BlinkExamReminders", content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        } else {
            cancelNotifictaion(withIndentifier: "BlinkExamReminders")
        }
    }
    
//    func scheduleJDryEyeIndexAndPollenForecastNotices() {
//        let content = UNMutableNotificationContent()
//        content.title = "hello test 3"
//        content.subtitle = "this is subtitle"
//        content.body = "this is body"
//        content.badge = 1
//        content.sound = UNNotificationSound.default()
//
//
//
//        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 600, repeats: true)
//        let request = UNNotificationRequest(identifier: "DryEyeIndexAndPollenForecastNotices", content: content, trigger: trigger)
//        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
//    }
    
    func scheduleSavingCardDiscountNotifications() {
        if getSavingCardDiscountNotifications() == true {
            if let time = UserManager.sharedInstance.getSavingCardTime() {
                let content = UNMutableNotificationContent()
                content.body = "You can now activate your next savings discount on SYSTANE® products!"
                content.badge = 1
                content.sound = UNNotificationSound.default()
                let interval : TimeInterval = TimeInterval((time + 30) * 86400)
                //(time + 30) * 86400
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval(interval), repeats: false)
                let request = UNNotificationRequest(identifier: "SavingCardDiscountNotifications", content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            }
        }
    }
    
    func scheduleTestPushNotification(_ message : String)
    {
        let content = UNMutableNotificationContent()
//        content.body = "Dry Index for today is "+index
        content.body = message
        content.badge = 0
        content.sound = UNNotificationSound.default()

        //(time + 30) * 86400
//        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval( 1 ), repeats: false)
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval(30), repeats: false)
        let request = UNNotificationRequest(identifier: "TestPushNotification", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func scheduleJourneyAgain() {
        let content = UNMutableNotificationContent()
        content.body = "Hope you are enjoying the Systane® product you were matched with! Come journey again if you want a product re-match."
        content.badge = 1
        content.sound = UNNotificationSound.default()

        //604800
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 604800, repeats: false)
        let request = UNNotificationRequest(identifier: "JourneyAgainRemainder", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func cancelNotifictaion(withIndentifier: String) {
        UNUserNotificationCenter.current().getPendingNotificationRequests { (notificationRequests) in
            var identifiers: [String] = []
            for notification:UNNotificationRequest in notificationRequests {
                if notification.identifier == withIndentifier {
                    identifiers.append(notification.identifier)
                }
            }
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: identifiers)
        }
    }
}
