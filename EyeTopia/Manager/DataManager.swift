//
//  DataManager.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/6/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class DataManager: NSObject
{    
    var symptomsEyeArray : [EyeProblem] = [EyeProblem]()
    var education : [Education] = [Education]()
    var products : [Product] = [Product]()
    var searchedProductArray : [Product] = [Product]()
    static let sharedInstance : DataManager = {
        let instance = DataManager()
        return instance
    }()
    
    
    //MARK: Set methods
    
    func setSearchTermProducts(array : [Product])
    {
        self.searchedProductArray = array
    }
    
    func setProductDetails()
    {
        // product 1
        
        let product1 : Product = Product()
        let filter1 : Filter = Filter()
        filter1.dryEye = true
        filter1.contactLenses = true
        product1.filter = filter1
        product1.title = "SYSTANE® ULTRA Lubricant Eye Drops"
        product1.subTitle = "If you’re looking for fast relief that can be used with contact lenses than look no further than one of our best-selling eye-care products, SYSTANE® ULTRA Lubricant Eye Drops. The fast-acting formula delivers long lasting dry eye symptom relief."
        product1.mainImageForDetail = UIImage(named: "productUltra280X280")
        product1.disclaimerFirst = "IMS Total Eye Care OTC. MAT Mar 2018\n Davitt WF, et al. J Ocul Pharmacol Ther. 2010;26(4):347-353"
        product1.mainImage = UIImage(named: "productUltra130X130")
        product1.externalLink = "https://www.chemistwarehouse.com.au/Buy/59023/Systane-Ultra-Lubricant-Eye-Drops-10ml"
        product1.zoomImage = UIImage(named: "PRODUCT_0000_ULTRA")
        product1.titleForDetails = "SYSTANE®‰ ULTRA Lubricant Eye Drops"
        

        
        product1.benefitsDetails = [ProductInfo]()
        
        let product1BenefitProductInfo1 = ProductInfo()
        product1BenefitProductInfo1.image = UIImage(named: "graphicFastActing")
        product1BenefitProductInfo1.details = "Fast-acting"
        product1.benefitsDetails?.append(product1BenefitProductInfo1)
        
        let product1BenefitProductInfo2 = ProductInfo()
        product1BenefitProductInfo2.image = UIImage(named: "graphicQol")
        product1BenefitProductInfo2.details = "Increased eye comfort for improved quality of life "
        product1.benefitsDetails?.append(product1BenefitProductInfo2)
        
        let product1BenefitProductInfo3 = ProductInfo()
        product1BenefitProductInfo3.image = UIImage(named: "graphicContactsUse")
        product1BenefitProductInfo3.details = "Suitable/compatible with contact lenses"
        product1.benefitsDetails?.append(product1BenefitProductInfo3)
            
            
            let product1Description = NSMutableAttributedString(string: "PRODUCT FACTS\r\n\r\nKEY INGREDIENTS\t\r\nHYDROXYPROPYL GUAR 0.16-0.19%\r\nPOLYETHYLENE GLYCOL 400 0.40%\t\r\nPROPYLENE GLYCOL 0.30%\r\nSORBITOL 1.4%\r\n\r\nUSES\r\nFOR THE TEMPORARY RELIEF OF BURNING AND IRRITATION DUE TO DRYNESS OF THE EYE.\r\n\r\nWARNINGS\r\nOPHTHALMIC USE ONLY.\r\n\r\nDO NOT USE\r\n• IF THIS PRODUCT CHANGES COLOR OR     BECOMES CLOUDY\r\n• IF YOU ARE SENSITIVE TO ANY INGREDIENT IN     THIS PRODUCT\r\n\r\nWHEN USING THIS PRODUCT\r\n• DO NOT TOUCH TIP OF CONTAINER TO ANY     SURFACE TO AVOID CONTAMINATION\r\n• REPLACE CAP AFTER EACH USE\r\n\r\nSTOP USE AND ASK AN EYE CARE PROFESSIONAL IF\r\n• YOU FEEL EYE PAIN\r\n• CHANGES IN VISION OCCUR\r\n• REDNESS OR IRRITATION OF THE EYE(S) GETS     WORSE OR PERSISTS\r\n• YOU EXPERIENCE PERSISTENT EYE     DISCOMFORT\r\n\r\nDO NOT SWALLOW THE SOLUTION.\r\nKEEP OUT OF REACH OF CHILDREN\r\n\r\nDIRECTIONS\r\n1. INSTILL 1 OR 2 DROPS IN THE AFFECTED EYE(S)      AS NEEDED OR AS DIRECTED BY YOUR EYE      CARE PROFESSIONAL.\r\n\r\nOTHER INFORMATION\r\n• STERILE UNTIL OPENED\r\n• STORE BELOW 30◦C\r\n• DISCARD CONTAINER 6 MONTHS AFTER     OPENING. \r\n\r\nOTHER INGREDIENTS\r\nAMINOMETHYLPROPANOL, BORIC ACID, POTASSIUM CHLORIDE, SODIUM CHLORIDE,  PURIFIED WATER AND POLYQUAD® (POLYQUATERNIUM-1) 0.001% PRESERVATIVE\r\n\r\nQUESTIONS?\r\nIN AUSTRALIA CALL 1800 224 153\n\nALWAYS READ THE LABEL. USE ONLY AS DIRECTED. IF SYMPTOMS PERSIST CONSULT YOUR HEALTHCARE PROFESSIONAL. SYSTANE® range of lubricant eye drops are intended for the temporary relief of burning and irritation due to dryness of the eye.\r\n", attributes: [
            .font: UIFont(name: "AvenirNext-Medium", size: 14.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0),
            .kern: 0.0
            ])
            product1Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 0, length: 32))
            product1Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 139, length: 4))
            product1Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 226, length: 8))
            product1Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 260, length: 10))
            product1Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 391, length: 23))
            product1Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 523, length: 44))
            product1Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 796, length: 10))
            product1Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 924, length: 17))
            product1Description.addAttribute(.font, value: UIFont(name: "LucidaGrande-Bold", size: 14.0)!, range: NSRange(location: 983, length: 1))
            product1Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 1039, length: 17))
            product1Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 1200, length: 10))
            
        
        product1.productDescription = product1Description
        
        //product 2
        
        let product2 : Product = Product()
        let filter2 : Filter = Filter()
        filter2.dryEye = true
        filter2.preservativeFree = true
        product2.filter = filter2
        product2.title = "SYSTANE® ULTRA UD Lubricant Eye Drops PRESERVATIVE-FREE"
        product2.subTitle = "Do you have sensitive eyes? Get the same fast dry eye relief of SYSTANE® ULTRA Lubricant Eye Drops in a preservative-free formula. Available in convenient, single-use vials that are perfect for relief on the go."
        product2.mainImage = UIImage(named: "productUltraUd130X130")
        product2.externalLink = "https://www.chemistwarehouse.com.au/Buy/66032/Systane-Ultra-Preservative-Free-Unit-Dose-24-x-0-4ml"
        product2.zoomImage = UIImage(named: "PRODUCT_0001_ULTRA-UD")
        product2.titleForDetails = "SYSTANE®‰ ULTRA UD Lubricant Eye Drops PRESERVATIVE-FREE"
        product2.mainImageForDetail = UIImage(named: "productUltraUd280X280")
        
    
        
        product2.benefitsDetails = [ProductInfo]()
        
        let product2BenefitProductInfo2 = ProductInfo()
        product2BenefitProductInfo2.image = UIImage(named: "graphicFastActing")
        product2BenefitProductInfo2.details = "Fast-acting"
        product2.benefitsDetails?.append(product2BenefitProductInfo2)
        
        let product2BenefitProductInfo1 = ProductInfo()
        product2BenefitProductInfo1.image = UIImage(named: "graphicSensitiveEyes")
        product2BenefitProductInfo1.details = "For sensitive eyes"
        product2.benefitsDetails?.append(product2BenefitProductInfo1)
        
        let product2BenefitProductInfo3 = ProductInfo()
        product2BenefitProductInfo3.image = UIImage(named: "graphicConvenient")
        product2BenefitProductInfo3.details = "Convenient on-the-go pack"
        product2.benefitsDetails?.append(product2BenefitProductInfo3)
        
        
        
        let product2Description = NSMutableAttributedString(string: "PRODUCT FACTS\r\n\r\nKEY INGREDIENTS\t\r\nHYDRPXYPROPYL GUAR 0.16-0.19% POLYETHYLENE GLYCOL 400 0.4%\nPROPYLENE GLYCOL 0.3%\nSORBITOL 1.4%\r\n\r\nUSES\r\nFOR THE TEMPORARY RELIEF OF BURNING AND IRRITATION DUE TO DRYNESS OF THE EYE.\r\n\r\nWARNINGS\r\nOPHTHALMIC USE ONLY.\r\n\r\nDO NOT USE\r\n• IF THIS PRODUCT CHANGES COLOR OR     BECOMES CLOUDY\r\n• IF YOU ARE SENSITIVE TO ANY INGREDIENT IN     THIS PRODUCT\n• IF PACKAGE OR UNIT DOSE VIALS ARE     DAMAGED OR OPEN.\r\n\r\nWHEN USING THIS PRODUCT\r\n• DO NOT TOUCH THE DROPPER TIP TO ANY     SURFACE \n\r\nSTOP USE AND ASK AN EYE CARE PROFESSIONAL IF\r\n• YOU FEEL EYE PAIN\r\n• CHANGES IN VISION OCCUR\r\n• REDNESS OR IRRITATION OF THE EYE(S) GETS     WORSE OR PERSISTS\r\n• YOU EXPERIENCE PERSISTENT EYE     DISCOMFORT OR EXCESSIVE TEARING\r\n\r\nDO NOT SWALLOW THE SOLUTION.\r\nKEEP OUT OF REACH OF CHILDREN\r\n\r\nDIRECTIONS\r\n1. TWIST OFF THE TOP OF THE SINGLE DOSE      VIAL.\n\r\n2. INSTILL 1 OR 2 DROPS IN THE AFFECTED EYE(S)      AS NEEDED OR AS DIRECTED BY YOUR EYE      CARE PROFESSIONAL.\n\nMAY BE USED TO TREAT DRY EYE ASSOCIATED WITH CONTACT LENS USAGE BY INSTILLING DROPS PRIOR TO INSERTING CONTACT LENSES AND AFTER THE REMOVAL OF CONTACT LENSES.\n\r\nOTHER INFORMATION\r\n• STERILE UNTIL OPENED\r\n• STORE BELOW 30◦C\r\n• DISCARD CONTAINER AFTER USE. \r\n\r\nOTHER INGREDIENTS\r\nAMINOMETHYLPROPANOL, BORIC ACID, POTASSIUM CHLORIDE, SODIUM CHLORIDE, PURIFIED WATER.\r\n\r\nQUESTIONS?\r\nIN AUSTRALIA CALL 1800 224 153\n\nALWAYS READ THE LABEL. USE ONLY AS DIRECTED. IF SYMPTOMS PERSIST CONSULT YOUR HEALTHCARE PROFESSIONAL. SYSTANE® range of lubricant eye drops are intended for the temporary relief of burning and irritation due to dryness of the eye.", attributes: [
            .font: UIFont(name: "AvenirNext-Medium", size: 14.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0),
            .kern: 0.0
            ])
        product2Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 0, length: 32))
        product2Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 132, length: 4))
        product2Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 219, length: 8))
        product2Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 253, length: 10))
        product2Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 441, length: 23))
        product2Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 519, length: 44))
        product2Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 813, length: 10))
        product2Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 1153, length: 17))
        product2Description.addAttribute(.font, value: UIFont(name: "LucidaGrande-Bold", size: 14.0)!, range: NSRange(location: 1212, length: 1))
        product2Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 1251, length: 17))
        product2Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 1359, length: 10))
                                                            
                                                            
                                                            
        product2.productDescription = product2Description
        
        
        // product 3
        
        let product3 : Product = Product()
        let filter3 : Filter = Filter()
        filter3.dryEye = true
        product3.filter = filter3
        product3.title = "SYSTANE® BALANCE Lubricant Eye Drops"
        product3.subTitle = "Get restoring dry eye relief with SYSTANE® BALANCE Lubricant Eye Drops unique* formula which locks in essential moisture and helps prevent tear evaporation for effective dry eye symptom relief."
        product3.mainImage = UIImage(named: "productBalance130X130")
        product3.externalLink = "https://www.chemistwarehouse.com.au/Buy/63622/Systane-Balance-Lubricant-Eye-Drop-10ml"
        product3.zoomImage = UIImage(named: "PRODUCT_0003_Balance")
        product3.titleForDetails = "SYSTANE®‰ BALANCE Lubricant Eye Drops"
        product3.mainImageForDetail = UIImage(named: "PRODUCTBALANCE")
        
        
        
        product3.benefitsDetails = [ProductInfo]()
        
        let product3BenefitProductInfo1 = ProductInfo()
        product3BenefitProductInfo1.image = UIImage(named: "graphicLipiTech")
        product3BenefitProductInfo1.details = "*Unique formula LipiTech™ system"
        product3.benefitsDetails?.append(product3BenefitProductInfo1)
        
        let product3BenefitProductInfo2 = ProductInfo()
        product3BenefitProductInfo2.image = UIImage(named: "graphicMoisture")
        product3BenefitProductInfo2.details = "Locks In moisture"
        product3.benefitsDetails?.append(product3BenefitProductInfo2)
        
        let product3BenefitProductInfo3 = ProductInfo()
        product3BenefitProductInfo3.image = UIImage(named: "graphicQol")
        product3BenefitProductInfo3.details = "Restoring relief"
        product3.benefitsDetails?.append(product3BenefitProductInfo3)
        
        let product3Description = NSMutableAttributedString(string: "PRODUCT FACTS\r\n\r\nKEY INGREDIENTS\t\r\nHYDROXYPROPYL GUAR 0.05% PROPYLENE GLYCOL 0.6%       \nMINERAL OIL 1% \nSORBITOL 0.7%\r\n\r\nUSES\r\nFOR THE TEMPORARY RELIEF OF BURNING AND IRRITATION DUE TO DRYNESS OF THE EYE.\r\n\r\nWARNINGS\r\nOPHTHALMIC USE ONLY.\r\n\r\nDO NOT USE\r\n• IF THIS PRODUCT CHANGES COLOR \n• IF YOU ARE SENSITIVE TO ANY INGREDIENT IN     THIS PRODUCT\n• IF PACKAGE IS DAMAGED OR OPEN.\n• IF TAMPER EVIDENT CAP IS DAMAGED\r\n\r\nWHEN USING THIS PRODUCT\r\n• DO NOT TOUCH THE DROPPER TIP TO ANY     SURFACE TO AVOID CONTAMINATION\n \nSTOP USE AND ASK AN EYE CARE PROFESSIONAL IF\r\n• YOU FEEL EYE PAIN\r\n• CHANGES IN VISION OCCUR\r\n• REDNESS OR IRRITATION OF THE EYE(S) GETS     WORSE OR PERSISTS\r\n• YOU EXPERIENCE PERSISTENT EYE     DISCOMFORT OR EXCESSIVE TEARING\r\n\r\nDO NOT SWALLOW THE SOLUTION.\r\nKEEP OUT OF REACH OF CHILDREN\r\n\r\nDIRECTIONS\r\n1. SHAKE WELL BEFORE USING.\r\n2. INSTILL 1 OR 2 DROPS IN THE AFFECTED EYE(S)      AS NEEDED OR AS DIRECTED BY YOUR EYE      CARE PROFESSIONAL.\n\nMAY BE USED TO TREAT DRY EYE ASSOCIATED WITH CONTACT LENS USAGE BY INSTILLING DROPS PRIOR TO INSERTING CONTACT LENSES AND AFTER THE REMOVAL OF CONTACT LENSES.\r\n\r\nOTHER INFORMATION\n• STERILE UNTIL OPENED\n• STORE BELOW 30◦C\n• DISCARD ANY REMAINING PRODUCT SIX     MONTHS AFTER FIRST OPENING\n\r\nOTHER INGREDIENTS\r\nDIMYRISTORYL PHOSPHATIDYDYLGLYCEROL, POLYOXYL 40 STEARATE, SORBITAN TRISTEARATE,  BORIC ACID,  EDETATE DISODIUM, PURIFIED WATER,AND POLYQUAD® (POLYQUATERNIUM-1) 0.001% PRESERVATIVE\n\r\nQUESTIONS?\r\nIN AUSTRALIA CALL 1800 224 153\n\nALWAYS READ THE LABEL. USE ONLY AS DIRECTED. IF SYMPTOMS PERSIST CONSULT YOUR HEALTHCARE PROFESSIONAL. SYSTANE® range of lubricant eye drops are intended for the temporary relief of burning and irritation due to dryness of the eye.", attributes: [
            .font: UIFont(name: "AvenirNext-Medium", size: 14.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0),
            .kern: 0.0
            ])
        product3Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 0, length: 32))
        product3Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 121, length: 4))
        product3Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 208, length: 8))
        product3Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 242, length: 10))
        product3Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 419, length: 23))
        product3Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 519, length: 44))
        product3Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 813, length: 10))
        product3Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 1130, length: 17))
        product3Description.addAttribute(.font, value: UIFont(name: "LucidaGrande-Bold", size: 14.0)!, range: NSRange(location: 1187, length: 1))
        product3Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 1259, length: 17))
        product3Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 1461, length: 10))
        
        product3.productDescription = product3Description
        
        // product 4
        
        let product4 : Product = Product()
        let filter4 : Filter = Filter()
        filter4.dryEye = true
        filter4.nightTime = true
        product4.filter = filter4
        product4.title = "SYSTANE® GEL DROPS"
        product4.subTitle = "Need a little extra protection from dry eye symptoms? Try SYSTANE® GEL DROPS for soothing, extended* relief from dry eye irritation. It creates a protective layer over your eyes for extra comfort day or night."
        product4.mainImage = UIImage(named: "productGelDrops130X130")
        product4.externalLink = "https://www.chemistwarehouse.com.au/Buy/67877/Systane-Gel-Lubricating-Drops-10ml"
        product4.zoomImage = UIImage(named: "PRODUCT_0004_Gel-Drops")
        product4.titleForDetails = "SYSTANE®‰ GEL DROPS"
        product4.mainImageForDetail = UIImage(named: "productGelDrops280X280")
        product4.extraDescription = "* vs Systane® Ultra lubricant eye drops (data on file)"
        
        product4.productInfo = [ProductInfo]()
        
      
        
        
        product4.benefitsDetails = [ProductInfo]()
        
        let product4BenefitProductInfo1 = ProductInfo()
        product4BenefitProductInfo1.image = UIImage(named: "graphicProtection")
        product4BenefitProductInfo1.details = "Extra protection*"
        product4.benefitsDetails?.append(product4BenefitProductInfo1)
        
        let product4BenefitProductInfo2 = ProductInfo()
        product4BenefitProductInfo2.image = UIImage(named: "graphicDrop")
        product4BenefitProductInfo2.details = "Gel thickness with the ease of a drop"
        product4.benefitsDetails?.append(product4BenefitProductInfo2)
        
        let product4BenefitProductInfo3 = ProductInfo()
        product4BenefitProductInfo3.image = UIImage(named: "graphicNightTime")
        product4BenefitProductInfo3.details = "Night time"
        
        product4.benefitsDetails?.append(product4BenefitProductInfo3)
        
        let product4Description = NSMutableAttributedString(string: "PRODUCT FACTS\r\n\r\nKEY INGREDIENTS\t\r\nHYDROXYPROPYL GUAR 0.7% POLYETHYLENE GLYCOL 400 0.4%\t\nPROPYLENE GLYCOL 0.3% SORBITOL 1.4%\r\n\r\nUSES\r\nDAY AND NIGHT PROTECTION FOR THE TEMPORARY RELIEF OF OCULAR SYMPTOMS DUE TO DRYNESS OF THE EYE, SUCH AS DISCOMFORT, BURNING AND IRRITATION.\r\n\r\nWARNINGS\r\nOPHTHALMIC USE ONLY\r\n\r\nDO NOT USE\r\n• IF THIS PRODUCT CHANGES COLOR \n• IF YOU ARE SENSITIVE TO ANY INGREDIENT IN     THIS PRODUCT\n• IF PACKAGE IS DAMAGED OR OPEN.\n• IF TAMPER EVIDENT CAP IS DAMAGED\r\n\r\nWHEN USING THIS PRODUCT\r\n• DO NOT TOUCH THE DROPPER TIP TO ANY     SURFACE TO AVOID CONTAMINATION\n \nSTOP USE AND ASK AN EYE CARE PROFESSIONAL IF\r\n• YOU FEEL EYE PAIN\r\n• CHANGES IN VISION OCCUR\r\n• REDNESS OR IRRITATION OF THE EYE(S) GETS     WORSE OR PERSISTS\r\n• YOU EXPERIENCE PERSISTENT EYE     DISCOMFORT OR EXCESSIVE TEARING\r\n\r\nDO NOT SWALLOW THE SOLUTION.\r\nKEEP OUT OF REACH OF CHILDREN\r\n\r\nDIRECTIONS\r\n1. SHAKE WELL BEFORE USING.\r\n2. INSTILL 1 OR 2 DROPS IN THE AFFECTED EYE(S)      AS NEEDED OR AS DIRECTED BY YOUR EYE      CARE PROFESSIONAL.\n\nMAY BE USED TO TREAT DRY EYE ASSOCIATED WITH CONTACT LENS USAGE BY INSTILLING DROPS PRIOR TO INSERTING CONTACT LENSES AND AFTER THE REMOVAL OF CONTACT LENSES.\n\r\nOTHER INFORMATION\r\n• STERILE UNTIL OPENED\n• STORE BELOW 30◦C\n• DISCARD ANY REMAINING PRODUCT THREE     MONTHS AFTER FIRST OPENING\n\r\nOTHER INGREDIENTS\r\nAMINOMETHYLPROPANOL, BORIC ACID, POTASSIUM CHLORIDE, SODIUM CHLORIDE, EDETATE DISODIUM, PURIFIED WATER AND POLYQUAD® (POLYQUATERNIUM-1) 0.001% PRESERVATIVE\n\r\nQUESTIONS?\r\nIN AUSTRALIA CALL 1800 224 153\n\nALWAYS READ THE LABEL. USE ONLY AS DIRECTED. IF SYMPTOMS PERSIST CONSULT YOUR HEALTHCARE PROFESSIONAL. SYSTANE® range of lubricant eye drops are intended for the temporary relief of burning and irritation due to dryness of the eye.", attributes: [
            .font: UIFont(name: "AvenirNext-Medium", size: 14.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0),
            .kern: 0.0
            ])
        product4Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 0, length: 32))
        product4Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 128, length: 4))
        product4Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 277, length: 8))
        product4Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 310, length: 10))
        product4Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 487, length: 23))
        product4Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 587, length: 44))
        product4Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 881, length: 10))
        product4Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 1197, length: 17))
        product4Description.addAttribute(.font, value: UIFont(name: "LucidaGrande-Bold", size: 14.0)!, range: NSRange(location: 1255, length: 1))
        product4Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 1329, length: 17))
        product4Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 1506, length: 10))
        
        product4.productDescription = product4Description
        
        // product 5
        
        let product5 : Product = Product()
        let filter5 : Filter = Filter()
        filter5.eyeHygiene = true
        product5.filter = filter5
        product5.title = "SYSTANE® LID WIPES"
        product5.subTitle = "SYSTANE® LID WIPES are hypoallergenic, premoistened eyelid cleansing wipes that can be used as part of a daily hygiene regimen. They remove debris and eye makeup that can cause irritation. Each wipe is individually wrapped so they are great on the go."
        product5.mainImage = UIImage(named: "productLidWipes130X130")
        product5.externalLink = "https://www.chemistwarehouse.com.au/Buy/73111/Systane-Lid-Wipes-30-Pack"
        product5.zoomImage = UIImage(named: "PRODUCT_0005_Lid-Wipes")
        product5.titleForDetails = "SYSTANE®‰ LID WIPES"
        product5.mainImageForDetail = UIImage(named: "productLidWipes280X280")
        

        
        product5.benefitsDetails = [ProductInfo]()
        
        let product5BenefitProductInfo1 = ProductInfo()
        product5BenefitProductInfo1.image = UIImage(named: "graphicHypoallergenic")
        product5BenefitProductInfo1.details = "Hypoallergenic"
        product5.benefitsDetails?.append(product5BenefitProductInfo1)
        
        let product5BenefitProductInfo2 = ProductInfo()
        product5BenefitProductInfo2.image = UIImage(named: "graphicRemoveDebris")
        product5BenefitProductInfo2.details = "Removes debris and make-up"
        product5.benefitsDetails?.append(product5BenefitProductInfo2)
        
        let product5BenefitProductInfo3 = ProductInfo()
        product5BenefitProductInfo3.image = UIImage(named: "graphicWrapped")
        product5BenefitProductInfo3.details = "Convenient, individually wrapped wipes"
        product5.benefitsDetails?.append(product5BenefitProductInfo3)
        
        let product5Description = NSMutableAttributedString(string: "PRODUCT FACTS\r\n\r\nUSES\r\n• EYE-MAKEUP REMOVER\n• EYELID CLEANSER\r\n\r\nDIRECTIONS\r\n1. RUB THE CLOSED LID WIPE POUCH TO       DEVELOP LATHER, THEN OPEN.\n2. CLOSE EYE, THEN SWEEP LID WIPE GENTLY       ACROSS EYELID SEVERAL TIMES.\n3. USING A FRESH WIPE, REPEAT PROCESS ON      THE OTHER EYE.\n4. RINSE BOTH EYES WITH CLEAN, WARM WATER      AND PAT DRY\n5. DISCARD AFTER USE\n\r\nCAUTION\r\n• FOR EXTERNAL USE ONLY. \n• DO NOT APPLY DIRECTLY TO THE EYE.\n• DO NOT USE IF YOU HAVE KNOWN ALLERGIES        TO ANY OF THE INGREDIENTS.\n• REDUCE FREQUENCY OF USE IF IRRITATION OR     EXCESSIVE DRYNESS OCCURS.\n• DISCONTINUE USE AND CONSULT YOUR EYE     HEALTHCARE PROFESSIONAL IF YOU     EXPERIENCE EXCESSIVE ITCHING, REDNESS,     SWELLING OR PERSISTENT IRRITATION.\n• USE ONLY IF PACKETS ARE INTACT.\n• DO NOT USE IN CHILDREN\n• SINGLE-USE ONLY\n\nSTORE AT ROOM TEMPERATURE\n\nINGREDIENTS\r\nPURIFIED WATER USP, PEG-200 HYDROGENATED GLYCERYL PALMATE, DISODIUM LAURETH SULFOSUCCINATE, COCAMIDOPROPYLAMINE OXIDE, PEG-80 GLYCERYL COCOATE, BENZYL ALCOHOL AND EDETATE DISODIUM.\n\r\nQUESTIONS?\r\nIN AUSTRALIA CALL 1800 224 153\n\nALWAYS READ THE LABEL. USE ONLY AS DIRECTED. IF SYMPTOMS PERSIST CONSULT YOUR HEALTHCARE PROFESSIONAL.", attributes: [
            .font: UIFont(name: "AvenirNext-Medium", size: 14.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0),
            .kern: 0.0
            ])
        product5Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 0, length: 21))
        product5Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 65, length: 10))
        product5Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 365, length: 7))
        product5Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 845, length: 11))
        product5Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 1041, length: 10))
        
        product5.productDescription = product5Description
        
        // product 6
        
        let product6 : Product = Product()
        product6.filter = filter5
        product6.title = "SYSTANE® EYE WASH"
        product6.subTitle = "Need a gentle cleansing solution for your irritated eyes? SYSTANE® EYE WASH can be used to wash away everyday irritants such as dust, smoke, grit and pollen."
        product6.mainImage = UIImage(named: "productEyeWash130X130")
        product6.externalLink = "https://www.chemistwarehouse.com.au/Buy/82464/Systane-Eye-Wash-120ml"
        product6.zoomImage = UIImage(named: "PRODUCT_0006_Eye-Wash")
        product6.titleForDetails = "SYSTANE®‰ EYE WASH"
        product6.mainImageForDetail = UIImage(named: "eyeWash")
        
        
        product6.benefitsDetails = [ProductInfo]()
        
        let product6BenefitProductInfo1 = ProductInfo()
        product6BenefitProductInfo1.image = UIImage(named: "graphicSoothe")
        product6BenefitProductInfo1.details = "Soothes uncomfortable eyes"
        product6.benefitsDetails?.append(product6BenefitProductInfo1)
        
        let product6BenefitProductInfo2 = ProductInfo()
        product6BenefitProductInfo2.image = UIImage(named: "graphicRefresh")
        product6BenefitProductInfo2.details = "Refreshes sore tired eyes"
        product6.benefitsDetails?.append(product6BenefitProductInfo2)
        
        let product6BenefitProductInfo3 = ProductInfo()
        product6BenefitProductInfo3.image = UIImage(named: "graphicBottle")
        product6BenefitProductInfo3.details = "Hygienic easy to use bottle"
        product6.benefitsDetails?.append(product6BenefitProductInfo3)
        
        
        let product6Description = NSMutableAttributedString(string: "PRODUCT FACTS\r\n\r\nUSES\r\n• Eye Rinse Solution\n• Cleans, refreshes and Soothes Sore Eyes\n• Wash away and clean eyes from everyday irritants     such as dust, smoke, grit and pollen\n\r\nDIRECTIONS\r\n1. FLUSH THE AFFECTED EYE(S) AS NEEDED\n2. CONTROL FLOW RATE BY VARYING PRESSURE      ON THE BOTTLE\n\r\nCAUTION\r\n• FOR EXTERNAL USE ONLY. \n• DO NOT USE TO RINSE OR SOAK CONTACT     LENSES.\n• IF IRRITATION PERSISTS, SEEK MEDICAL ADVICE.\n• IF SOLUTION CHANGES COLOUR OR BECOMES     CLOUDY, DO NOT USE \n\nSTORE BELOW 25°C\n\nDISCARD CONTAINER 4 WEEKS AFTER OPENING \n\nINGREDIENTS\r\nPURIFIED WATERAQUA, SODIUM CHLORIDE, SODIUM ACETATE (TRIHYDRATE), SODIUM CITRATE (DIHYDRATE), POTASSIUM CHLORIDE, CALCIUM CHLORIDE (DIHYDRATE), MAGNESIUM CHLORIDE (HEXAHYDRATE), BENZALKONIUM CHLORIDE SOLUTION, SODIUM HYDROXIDE AND/OR HYDROCHLORIC ACID.\n\r\nQUESTIONS?\r\nIN AUSTRALIA CALL 1800 224 153\n\nALWAYS READ THE LABEL. USE ONLY AS DIRECTED. IF SYMPTOMS PERSIST CONSULT YOUR HEALTHCARE PROFESSIONAL.", attributes: [
            .font: UIFont(name: "AvenirNext-Medium", size: 14.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0),
            .kern: 0.0
            ])
        product6Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 0, length: 21))
        product6Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 180, length: 10))
        product6Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 293, length: 7))
        product6Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 550, length: 11))
        product6Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 818, length: 10))
        
        product6.productDescription = product6Description
        
        
        
        // product 7
        
        let product7 : Product = Product()
        let filter7 : Filter = Filter()
        filter7.redEye = true
        product7.filter = filter7
        product7.title = "SYSTANE® RED EYE Drops"
        product7.subTitle = "“Red Eye” is one of the most common eye problems for which people visit a doctor1‰. Get relief of your red congested eyes for up to 8 hours with SYSTANE® RED EYES2‰,‰3‰."
        product7.mainImage = UIImage(named: "productRedEyes130X130")
        product7.externalLink = "https://www.chemistwarehouse.com.au/Buy/84218/Systane-Red-Eye-Drops-15ml"
        product7.zoomImage = UIImage(named: "PRODUCT_0007_Red-Eyes")
        product7.titleForDetails = "SYSTANE®‰ RED EYE Drops"
        product7.disclaimerSecond = "1. ASCIA, PCC Allergy Conjunctivitis 2015 \n2. Abelson MB et al. Tolerance and absence of rebound vasodilation      following topical ocular decongestant usage. Ophtha, 1984, Vol 91,      Issue 11, pp, 1364-7 \n3. Systane Red Eye Product Information \n4. IMS Total Eye Care OTC. MAT Mar 2018 \n\nINDICATED FOR USE AS A TOPICAL OCULAR VASOCONSTRICTOR - \nFOR RELIEF OF RED CONGESTED EYES."
        product7.mainImageForDetail = UIImage(named: "productRedEye")
        
        
        
        product7.benefitsDetails = [ProductInfo]()
        
        let product7BenefitProductInfo1 = ProductInfo()
        product7BenefitProductInfo1.image = UIImage(named: "graphicLongLasting")
        product7BenefitProductInfo1.details = "Up to 8 hours duration2‰"
        product7.benefitsDetails?.append(product7BenefitProductInfo1)
        
        let product7BenefitProductInfo2 = ProductInfo()
        product7BenefitProductInfo2.image = UIImage(named: "graphicRelief")
        product7BenefitProductInfo2.details = "Relief of red congested eyes"
        product7.benefitsDetails?.append(product7BenefitProductInfo2)
        
        let product7BenefitProductInfo3 = ProductInfo()
        product7BenefitProductInfo3.image = UIImage(named: "graphicNumber1")
        product7BenefitProductInfo3.details = "From the #1 OTC  Eye Care brand4‰"
        product7.benefitsDetails?.append(product7BenefitProductInfo3)
        
        let product7Description = NSMutableAttributedString(string: "PRODUCT FACTS\r\n\r\nACTIVE INGREDIENTS\t\nNAPHAZOLINE HYDROCHLORIDE 0.1%\n\nUSES\r\nFOR THE RELIEF OF RED CONGESTED EYES.\n\r\nWARNINGS\nOPHTHALMIC USE ONLY.\nCONSULT YOUR DOCTOR OR PHARMACIST BEFORE USING IN CONJUNCTION WITH OTHER EYE DROPS\n\nDO NOT USE\n• IF YOU HAVE GLAUCOMA OR OTHER SERIOUS     EYE CONDITIONS\n• IF SEAL IS BROKEN\n• IN CHILDREN\n• WHILE WEARING SOFT CONTACT LENSES. SOFT     CONTACT LENSES CAN BE INSERTED 15     MINUTES AFTER USING THE PRODUCT.\n\nWHEN USING THIS PRODUCT\n• DO NOT TOUCH THE DROPPER TIP TO YOUR     EYE OR TO ANY OTHER SURFANCE \n• PROLONGED USE MAY BE HARMFUL\n\nSTOP USE AND ASK AN EYE CARE PROFESSIONAL IF SYMPTOMS PERSIST OR WORSEN\n\nKEEP OUT OF REACH OF CHILDREN\n\r\nDIRECTIONS\r\n1. INSTILL 1 OR 2 DROPS IN THE AFFECTED EYE(S)      EVERY 3 TO 4 HOURS AS REQUIRED, OR AS      DIRECTED BY YOUR DOCTOR OR PHARMACIST\n\nOTHER INFORMATION\nSTERILE UNTIL OPENED\nSTORE BELOW 25◦C\nDISCARD CONTAINER 4 WEEKS AFTER OPENING. \nPROTECT FROM EXCESSIVE HEAT\n\nOTHER INGREDIENTS\r\nBENZALKONIUM CHLORIDE 0.1mg/mL AS PRESERVATIVE\n\r\nQUESTIONS?\r\nIN AUSTRALIA CALL 1800 224 153\n\nSYSTANE® Red Eyes drops is indicated for use as a topical ocular vasoconstrictor – for relief of red congested eyes.\nALWAYS READ THE LABEL. USE ONLY AS DIRECTED. IF SYMPTOMS PERSIST CONSULT YOUR HEALTHCARE PROFESSIONAL.\n", attributes: [
            .font: UIFont(name: "AvenirNext-Medium", size: 14.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0),
            .kern: 0.0
            ])
        product7Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 0, length: 35))
        product7Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 69, length: 4))
        product7Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 115, length: 8))
        product7Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 229, length: 10))
        product7Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 451, length: 23))
        product7Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 580, length: 71))
        product7Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 685, length: 10))
        product7Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 831, length: 17))
        product7Description.addAttribute(.font, value: UIFont(name: "LucidaGrande-Bold", size: 14.0)!, range: NSRange(location: 884, length: 1))
        product7Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 958, length: 17))
        product7Description.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 14.0)!, range: NSRange(location: 1026, length: 10))
        
        product7.productDescription = product7Description
        
//        
//        let tealColor = UIColor(red: 0/255.0, green: 143/255.0, blue: 136/255.0, alpha: 1)
//        
//        let product7additionalDescription =  NSMutableAttributedString(string: "For more information , you can download and read the Systane® Red Eyes drops Product Information \n\n1. ASCIA, PCC Allergy Conjunctivitis 2015\n2. Abelson MB et al. Tolerance and absence of rebound vasodilation  
//    following topical ocular decongestant usage. Ophtha,1984, Vol 91,  
//    Issue 11, pp, 1364-7\n3. IMS Total Eye Care OTC. MAT Sept 2017 in units", attributes: [
//            .font: UIFont(name: "AvenirNext-Medium", size: 10.0)!,
//            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0)
//            ])
        
        self.products = [product3, product6, product4, product5, product7, product1, product2]
        self.searchedProductArray = self.products
        
    }
    
    func setEyeSymptomsList()
    {
        let model1 = EyeProblem()
        model1.id = 0
        model1.title = "Redness"
        
        let model2 = EyeProblem()
        model2.id = 1
        model2.title = "Itchiness"
//        model2.symptoms = [1,4,5]

        let model3 = EyeProblem()
        model3.id = 2
        model3.title = "Discharge / excess mucous"
        
        let model4 = EyeProblem()
        model4.title = "Crusty eyes"
        model4.id = 3
        
        let model5 = EyeProblem()
        model5.title = "Burning or stinging"
        model5.image = UIImage(named: "icnBurning")
//        model5.symptoms = [1,4,5]
        model5.id = 0
        
        let model6 = EyeProblem()
        model6.title = "Sandy or gritty sensation"
        model6.image = UIImage(named: "icnSandy")
//        model6.symptoms = [1,4,5]
        model6.id = 1
        
        let model7 = EyeProblem()
        model7.title = "Dryness"
        model7.image = UIImage(named: "icnDryEye")
        model7.id = 2
        
        let model8 = EyeProblem()
        model8.title = "Pain"
        model8.id = 7
        
//        symptomsEyeArray.append(model1)
//        symptomsEyeArray.append(model2)
//        symptomsEyeArray.append(model3)
//        symptomsEyeArray.append(model4)
        symptomsEyeArray.append(model5)
        symptomsEyeArray.append(model6)
        symptomsEyeArray.append(model7)
//        symptomsEyeArray.append(model8)
        
    }
    
    func setEducationData()
    {
        let boldFont : UIFont = UIFont(name: "AvenirNext-Bold", size: 14.0)!
        //education 1
        
        let education1 : Education = Education()
        education1.mainImageView = UIImage(named: "graphicEduDryEyeLarge")
        education1.mainColor = UIColor(red:0.61, green:0.35, blue:0.71, alpha:1.0)
        education1.title = "What Dry Eye Feels Like"
        education1.descriptionString = "Dry eye symptoms can be irritating and affect your daily activities. Dry eye can cause a number of symptoms which may vary between people. Some common symptoms are:1‰"
        education1.referLinks = ["1. https://nei.nih.gov/health/dryeye/dryeye"]
        education1.tableImageView = UIImage(named: "graphicEduDryEyeSmall")
        
        var educationInfoArray1 : [EducationInfo] = [EducationInfo]()
        
        let educationInfo11 : EducationInfo = EducationInfo()
        educationInfo11.details = "Eye dryness or irritation"
        educationInfo11.font = boldFont
        educationInfo11.image = UIImage(named: "icnEyeDryness")
        
        let educationInfo12 : EducationInfo = EducationInfo()
        educationInfo12.details = "Burning or stinging eyes"
        educationInfo12.font = boldFont
        educationInfo12.image = UIImage(named: "icnBurning-1")
        
        let educationInfo13 : EducationInfo = EducationInfo()
        educationInfo13.details = "Sandy or gritty feeling in the eyes"
        educationInfo13.font = boldFont
        educationInfo13.image = UIImage(named: "icnSandy-1")
        
        let educationInfo14 : EducationInfo = EducationInfo()
        educationInfo14.details = "Tired eyes"
        educationInfo14.font = boldFont
        educationInfo14.image = UIImage(named: "icnTired")
        
        let educationInfo15 : EducationInfo = EducationInfo()
        educationInfo15.details = "Episodes of blurred vision"
        educationInfo15.font = boldFont
        educationInfo15.image = UIImage(named: "icnBlurred")
        
        let educationInfo16 : EducationInfo = EducationInfo()
        educationInfo16.details = "Excessive tearing"
        educationInfo16.font = boldFont
        educationInfo16.image = UIImage(named: "icnTearing")
        
        let educationInfo17 : EducationInfo = EducationInfo()
        educationInfo17.details = "Heavy eyelids"
        educationInfo17.font = boldFont
        educationInfo17.image = UIImage(named: "icnHeavyEyelids")
        
        let educationInfo18 : EducationInfo = EducationInfo()
        educationInfo18.details = "Decreased tolerance for reading or using digital screens"
        educationInfo18.font = boldFont
        educationInfo18.image = UIImage(named: "icnTolerance")
        
        educationInfoArray1 = [educationInfo11, educationInfo12, educationInfo13, educationInfo14, educationInfo15, educationInfo16, educationInfo17, educationInfo18]
        education1.educationInfo = educationInfoArray1
        
        
        //education 2
        
        let education2 : Education = Education()
        education2.tableImageView = UIImage(named: "graphicEduCauseSmall")
        education2.title = "What Causes Dry Eye"
        education2.mainImageView = UIImage(named: "graphicEduCauseLarge")
        education2.mainColor = UIColor(red:0.39, green:0.83, blue:0.85, alpha:1.0)
        education2.descriptionString = "Dry eye symptoms occur in one of three ways:1‰ \n\n1. Your tear glands stop making enough tears\n2. Your tear glands produce poor-quality tears \n3. Your tears evaporate too quickly\n\nAny one of these can cause your eyes to feel dry and irritated.\n\nDry eye symptoms can be triggered by these factors:2‰"
        
        var educationInfoArray2 : [EducationInfo] = [EducationInfo]()
        
        let educationInfo21 : EducationInfo = EducationInfo()
        educationInfo21.details = "Dry environments"
        educationInfo21.font = boldFont
        educationInfo21.image = UIImage(named: "graphicDry")
        
        let educationInfo22 : EducationInfo = EducationInfo()
        educationInfo22.details = "Overheated or overcooled air"
        educationInfo22.font = boldFont
        educationInfo22.image = UIImage(named: "graphicTemp")
        
        let educationInfo23 : EducationInfo = EducationInfo()
        educationInfo23.details = "Wind"
        educationInfo23.font = boldFont
        educationInfo23.image = UIImage(named: "graphicWind")
        
        let educationInfo24 : EducationInfo = EducationInfo()
        educationInfo24.details = "Sun"
        educationInfo24.font = boldFont
        educationInfo24.image = UIImage(named: "graphicSun")
        
        let educationInfo25 : EducationInfo = EducationInfo()
        educationInfo25.details = "Smoke"
        educationInfo25.font = boldFont
        educationInfo25.image = UIImage(named: "graphicSmoke")
        
        let educationInfo26 : EducationInfo = EducationInfo()
        educationInfo26.details = "Extended screen time"
        educationInfo26.font = boldFont
        educationInfo26.image = UIImage(named: "graphicScreens")
        
        educationInfoArray2 = [educationInfo21, educationInfo22, educationInfo23, educationInfo24, educationInfo25, educationInfo26]
        
        education2.referLinks = ["1. http://www.aoa.org/patients-and-public/eye-and-vision-problems/glossary-of-eye-and-vision-conditions/dry-eye?sso=y", "2. http://www.mayoclinic.org/diseases-conditions/dry-eyes/basics/prevention/con-2002412"]
        education2.educationInfo = educationInfoArray2
        
        
        //education 3
        
        let education3 : Education = Education()
        education3.tableImageView = UIImage(named: "graphicEduWhatCanIDoSmall")
        education3.title = "What You Can Do About Dry Eye"
        education3.mainColor = UIColor(red:0.90, green:0.45, blue:0.40, alpha:1.0)
        education3.mainImageView = UIImage(named: "graphicEduWhatCanIDoLarge")
        education3.descriptionString = "Help prevent and relieve dry eye symptoms with these lifestyle changes:1‰,‰2‰"
        education3.referLinks = ["1. http://www.mayoclinic.org/diseases-conditions/dry-eyes/basics/prevention/con-20024129", "2. https://www.mayoclinic.org/diseases-conditions/dry-eyes/basics/risk-factors/con-20024129"]
        
        
        var educationInfoArray3 : [EducationInfo] = [EducationInfo]()
        
        let educationInfo31 : EducationInfo = EducationInfo()
        educationInfo31.image = UIImage(named: "graphicScreens")
        educationInfo31.font = boldFont
        educationInfo31.details = "Take breaks from your digital screens"
        
        let educationInfo32 : EducationInfo = EducationInfo()
        educationInfo32.image = UIImage(named: "graphicFan")
        educationInfo32.font = boldFont
        educationInfo32.details = "Avoid air blowing in your eyes such as hair dryers, air conditioners, heaters, or fans"
        
        let educationInfo33 : EducationInfo = EducationInfo()
        educationInfo33.image = UIImage(named: "graphicOmega3")
        educationInfo33.font = boldFont
        educationInfo33.details = "Eat foods containing Omega-3 fatty acids"
        
        let educationInfo34 : EducationInfo = EducationInfo()
        educationInfo34.image = UIImage(named: "graphicGlasses")
        educationInfo34.font = boldFont
        educationInfo34.details = "Wear glasses on sunny or windy days"
        
        let educationInfo35 : EducationInfo = EducationInfo()
        educationInfo35.image = UIImage(named: "graphicHumidifier")
        educationInfo35.font = boldFont
        educationInfo35.details = "Use a humidifier indoors"
        
        let educationInfo36 : EducationInfo = EducationInfo()
        educationInfo36.image = UIImage(named: "graphicSmoking")
        educationInfo36.font = boldFont
        educationInfo36.details = "Quit smoking and avoid second-hand smoke"
        
        let educationInfo37 : EducationInfo = EducationInfo()
        educationInfo37.image = UIImage(named: "graphicOptometrist")
        educationInfo37.font = boldFont
        educationInfo37.details = "See your optometrist at least once a year"
        
        educationInfoArray3 = [educationInfo31, educationInfo32, educationInfo33, educationInfo34, educationInfo35, educationInfo36, educationInfo37]
        education3.educationInfo = educationInfoArray3
        
        
        //education 4
        
        let education4 : Education = Education()
        education4.title = "Who Suffers From Dry Eye"
        education4.tableImageView = UIImage(named: "graphicEduSufferSmall")
        education4.mainImageView = UIImage(named: "graphicEduSufferLarge")
        education4.mainColor = UIColor(red:0.95, green:0.76, blue:0.05, alpha:1.0)
        education4.descriptionString = "About 36% of Australian suffer from dry eye symptoms.1‰ But these factors can make you more likely to experience chronic dry eye symptoms.2‰"
        education4.referLinks = ["1. Market research conducted by Ipsos on behalf of Alcon Laboratories Australia Pty Ltd in 2016. Sample size = 836", "2. http://www.aoa.org/patients-and-public/eye-and-vision-problems/glossary-of-eye-and-vision-conditions/dry-eye?sso=y"]
        
        var educationInfoArray4 : [EducationInfo] = [EducationInfo]()
        
        let educationInfo41 : EducationInfo = EducationInfo()
        educationInfo41.image = UIImage(named: "graphicFemale")
        educationInfo41.font = boldFont
        educationInfo41.details = "Women are more likely to get dry eye"

        let educationInfo42 : EducationInfo = EducationInfo()
        educationInfo42.image = UIImage(named: "graphicHormone")
        educationInfo42.font = boldFont
        educationInfo42.details = "Hormonal changes, including menopause"
        
        let educationInfo43 : EducationInfo = EducationInfo()
        educationInfo43.image = UIImage(named: "graphicAging")
        educationInfo43.font = boldFont
        educationInfo43.details = "Aging"
        
        let educationInfo44 : EducationInfo = EducationInfo()
        educationInfo44.image = UIImage(named: "graphicContacts")
        educationInfo44.font = boldFont
        educationInfo44.details = "Long-term contact lens wear"
        
        let educationInfo45 : EducationInfo = EducationInfo()
        educationInfo45.image = UIImage(named: "graphicSurgery")
        educationInfo45.font = boldFont
        educationInfo45.details = "LASIK or cataract surgeries"
        
        let educationInfo46 : EducationInfo = EducationInfo()
        educationInfo46.image = UIImage(named: "graphicMeds")
        educationInfo46.font = boldFont
        educationInfo46.details = "Taking certain types of medications"
        
        let educationInfo47 : EducationInfo = EducationInfo()
        educationInfo47.image = UIImage(named: "graphicDiabetes")
        educationInfo47.font = boldFont
        educationInfo47.details = "Medical conditions such as arthritis, diabetes or thyroid problems"
        
        
        educationInfoArray4 = [educationInfo41, educationInfo42, educationInfo43, educationInfo44, educationInfo45, educationInfo46, educationInfo47]
        
        education4.educationInfo = educationInfoArray4
        
        
        //education 5
        
        let education5 : Education = Education()
        education5.title = "Dry Eye Quick Tips"
        education5.tableImageView = UIImage(named: "graphicEduDryEyeTipsSmall")
        education5.mainImageView = UIImage(named: "graphicEduDryEyeTipsLarge")
        education5.mainColor = UIColor(red:0.40, green:0.82, blue:0.83, alpha:1.0)
        education5.descriptionString = "These simple ideas can help ease bothersome dry eye symptoms."
        
        var educationInfoArray5 : [EducationInfo] = [EducationInfo]()
        
        let educationInfo51 : EducationInfo = EducationInfo()
        educationInfo51.image = UIImage(named: "graphicOmega3")
        educationInfo51.details = "Try incorporating fish into your diet. Fish contains omega-3 fatty acids which may help dry eye symptoms.1‰"
        
        let educationInfo52 : EducationInfo = EducationInfo()
        educationInfo52.image = UIImage(named: "graphicFoods")
        educationInfo52.details = "Eat foods like carrots, pumpkins, sweet potatoes or spinach to get vitamin A, which is important for good eyesight and vision.2‰"
        
        let educationInfo53 : EducationInfo = EducationInfo()
        educationInfo53.image = UIImage(named: "graphicGlasses2")
        educationInfo53.details = "Exposure to wind and sun can make tears evaporate faster. When spending time outside, wear protective eyewear like sunglasses and keep dry eye relief on hand.3‰"
        
        let educationInfo54 : EducationInfo = EducationInfo()
        educationInfo54.image = UIImage(named: "graphicAirplane")
        educationInfo54.details = "Dry air in airports and planes can make your eyes feel dry or irritated. Be sure to pack your go-to dry eye symptom relief.3‰"
        
        let educationInfo55 : EducationInfo = EducationInfo()
        educationInfo55.image = UIImage(named: "graphicHumidifier")
        educationInfo55.details = "When temps drop outside, heated air inside can make your eyes feel dry. Try running a humidifier and drinking more water.4‰"
        
        let educationInfo56 : EducationInfo = EducationInfo()
        educationInfo56.image = UIImage(named: "graphicTv")
        educationInfo56.details = "Failing to blink regularly, such as when you’re staring at a screen, can contribute to drying out your eyes3‰"
        
        let educationInfo57 : EducationInfo = EducationInfo()
        educationInfo57.image = UIImage(named: "graphicOffice")
        educationInfo57.details = "The work environment isn’t easy on your eyes. Dry air, digital screens and more can be a problem for people with dry eye. Try keeping dry eye symptom relief at your desk.3‰"
        
        let educationInfo58 : EducationInfo = EducationInfo()
        educationInfo58.image = UIImage(named: "graphicWater")
        educationInfo58.details = "Getting your 8 to 10 cups of water a day can help reduce symptoms of dry eye3‰, so drink up!"
        
        let educationInfo59 : EducationInfo = EducationInfo()
        educationInfo59.image = UIImage(named: "graphicExercise")
        educationInfo59.details = "To help keeping your eyes healthy, perform a few eye exercises anytime your eyes feel fatigued5‰"
/*
        let attributedInfo59 = NSMutableAttributedString(string: "To improve your vision naturally, perform a few eye exercises anytime your eyes feel fatigued.5\n View exercises", attributes: [
            .font: UIFont(name: "AvenirNext-Medium", size: 14.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0)
            ])
        attributedInfo59.addAttribute(.font, value: UIFont(name: "AvenirNext-Medium", size: 10.0)!, range: NSRange(location: 94, length: 1))
        attributedInfo59.addAttribute(.foregroundColor, value: UIColor(red: 74.0 / 255.0, green: 144.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0), range: NSRange(location: 97, length: 14))

        //attributedInfo59.addAttribute(.link, value: "https://www.webmd.com/eye-health/eye-exercises", range: NSRange(location: 97, length: 14))
        educationInfo59.link = "https://www.webmd.com/eye-health/eye-exercises"
  */
        let linkInfo : LinkInfo = LinkInfo()
        linkInfo.name = "View exercises"
        linkInfo.link = "https://www.webmd.com/eye-health/eye-exercises"
        
        
        //educationInfo59.detailsAttributed = attributedInfo59
        educationInfo59.link = linkInfo
        
            
        //let educationInfo50 : EducationInfo = EducationInfo()
        //educationInfo50.image = UIImage(named: "graphicGreens")
        //educationInfo50.details = "Eat greens; a diet filled with dark, leafy greens like kale and spinach has been shown to help support eye health.6‰"
        
        educationInfoArray5 = [educationInfo51, educationInfo52, educationInfo53, educationInfo54, educationInfo55, educationInfo56, educationInfo57, educationInfo58, educationInfo59]
        
        education5.educationInfo = educationInfoArray5
        education5.referLinks = ["1. https://www.mayoclinic.org/diseases-conditions/dry-eyes/basics/alternative-medicine/con-20024129", "2. http://www.fao.org/docrep/017/i3261e/i3261e06.pdf", "3. http://www.aoa.org/patients-and-public/eye-and-vision-problems/glossary-of-eye-and-vision-conditions/dry-eye?sso=y", "4. http://www.mayoclinic.org/diseases-conditions/dry-eyes/basics/prevention/con-20024129", "5. WebMD. http://www.webmd.com/eye-health/eye-exercises"]
        
        //education 6
        
        let education6 : Education = Education()
        education6.title = "Allergy Quick Tips"
        education6.tableImageView = UIImage(named: "graphicEduAllergyTipsSmall")
        education6.mainImageView = UIImage(named: "graphicEduAllergyTipsLarge")
        education6.mainColor = UIColor(red:0.11, green:0.74, blue:0.61, alpha:1.0)
        education6.descriptionString = ""
        
        
        
        var educationInfoArray6 : [EducationInfo] = [EducationInfo]()
        
        let educationInfo61 : EducationInfo = EducationInfo()
        educationInfo61.image = UIImage(named: "graphicGlasses")
        educationInfo61.details = "Wear wrap-around sunglasses to help protect your eyes from wind, dust, and allergens.1‰"
        
        
        let educationInfo62 : EducationInfo = EducationInfo()
        educationInfo62.image = UIImage(named: "graphicWindows")
        educationInfo62.details = "Keep windows closed on high pollen days.1‰"
        
        let educationInfo63 : EducationInfo = EducationInfo()
        educationInfo63.image = UIImage(named: "graphicShower")
        educationInfo63.details = "Shower at night to wash off pollen you have been exposed to outside during the day.1‰"
        
        //let educationInfo64 : EducationInfo = EducationInfo()
        //educationInfo64.image = UIImage(named: "graphicClothesline")
        //educationInfo64.details = "Don’t hang your washing to dry outside on high pollen days.1‰"
        
        educationInfoArray6 = [educationInfo61, educationInfo62, educationInfo63]
        
        education6.educationInfo = educationInfoArray6
        education6.referLinks = ["1. http://www.allergy.org.au/images/pcc/ASCIA_PCC_Pollen_allergy_2015.pdf.  Accessed 11 May 2016."]
        
        //education 7
        
        let education7 : Education = Education()
        education7.title = "Contact Lens Wearer Tips"
        education7.tableImageView = UIImage(named: "lenss")
        education7.mainImageView = UIImage(named: "bigLenss")
        education7.mainColor = UIColor(red:155/255.0, green:89/255.0, blue:182/255.0, alpha:1.0)
        education7.descriptionString = "Contact lenses are available in many different materials. Some new generation materials become less dry in challenging environments. A discussion with your optometrist to ensure you have the best contact lens for your lifestyle is advisable. \n \nSome modern Contact Lens care products can now assist in the wettability of your contacts lenses. Please ensure you have the best product for the contact lenses you are wearing and the environment in which you wear them. A review with your eye care provider is advised."
        
        
        
        
        education1.referedEducation = [education2, education3]
        education2.referedEducation = [education3, education4]
        education3.referedEducation = [education4, education5]
        education4.referedEducation = [education5, education6]
        education5.referedEducation = [education6, education7]
        education6.referedEducation = [education7, education5]
        education7.referedEducation = [education6, education5]
        
        self.education = [education1, education2, education3, education4, education5, education6, education7]
    }
    
    
    //MARK: Get Methods
    
    func getProductsArray() -> [Product]
    {
        return self.products
    }
    
    func getSearchedProductArray() -> [Product]
    {
        return self.searchedProductArray
    }
    
    func getEducationArray() -> [Education]
    {
        return self.education
    }
    
    func getEyeSymptomsList() -> [EyeProblem]
    {
        return self.symptomsEyeArray
    }
    
    
    //MARK: Update Methods
    
    func updateSearchTermProduct(searchTerm : String) -> [Product]
    {
        var newProduct : [Product] = [Product]()
        
        for product in self.searchedProductArray
        {
            if(searchTerm == "")
            {
                newProduct.append(product)
            }
            else
            {
                let lowerCaseSentence = product.title?.lowercased()
                let lowerCaseSearchTerm = searchTerm.lowercased()
                if(lowerCaseSentence?.contains(lowerCaseSearchTerm))!
                {
                    newProduct.append(product)
                }
            }
        }
        
        return newProduct
    }
    
    
    /*
    - (NSArray *)updateSearchTerm:(NSString *)searchTerm
    {
    NSMutableArray *mutableArray = [[NSMutableArray alloc] init];
    
    for (Activity *activity in [[DataManager sharedInstance] getAllActivities])
    {
    if ([searchTerm isEqualToString:@""])
    {
    [mutableArray addObject: activity];
    }
    else
    {
    NSString *lowerCaseSentence = [activity.activityName lowercaseString];
    NSString *lowerCaseSearchTerm = [searchTerm lowercaseString];
    
    if ([lowerCaseSentence containsString: lowerCaseSearchTerm])
    {
    [mutableArray addObject: activity];
    }
    }
    }
    
    return mutableArray;
    }
 */
    
}
