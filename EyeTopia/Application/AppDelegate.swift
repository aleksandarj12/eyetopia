//
//  AppDelegate.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/5/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import GoogleMaps
import GooglePlaces
import UserNotifications
import Alamofire
import Localytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var tabController : UITabBarController!
    var mainFlowCoordinator : MainFlowCoordinator!
    
//    var windowOrientation : WindowOrientation = .portrait
    
    var orientationLock = UIInterfaceOrientationMask.portrait
    
//    var appIsStarting : Bool = false
    
    
    //MARK: Setup & Support Methods
    
    func setupLoadAppLocalytics()
    {
        guard let load = UserDefaults.standard.value(forKey: trackingEvents.loadAppkey) else {
            UserDefaults.standard.set(true, forKey: trackingEvents.loadAppkey)
            UserDefaults.standard.synchronize()
            Localytics.tagEvent(trackingEvents.loadAppkey)
            return
        }
    }
    
    func setupUrl(_ header : String, _ location : CLLocation) -> String
    {
        var urlString : String = k.services.website
        urlString.append("lat=")
        urlString.append(String(location.coordinate.latitude))
        urlString.append("&lon=")
        urlString.append(String(location.coordinate.longitude))
        urlString.append(header)
        urlString.append(k.services.userWeatherID)
        urlString.append("&k=")
        
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        
        let year =  (components.year! - 2000) * 170000
        let month = components.month! * 300
        let day = components.day! * 2
        let keyString = String(day + month + year) + k.services.passwordWeather
        let digestString = NSString(string: keyString).md5Digest()
        
        urlString.append(digestString)
        
        return urlString
    }
    
    func setupSubscribe()
    {
        let header : HTTPHeaders = [
            "Accept" : "application/json"
        ]
        let deviceToken : String = UserDefaults.standard.value(forKey: k.user.deviceToken) as! String
        let params : [String: Any] = [
            "device_token": deviceToken
        ]
        
        Alamofire.request(k.services.subscribePushNotification, method: .post, parameters: params, headers: header)
            .responseJSON { response in
                if(response.error == nil)
                {
                    print("successful subscribed")
                    UserDefaults.standard.setValue(true, forKey: k.services.firstLaunchServiceNotification)
                    UserDefaults.standard.synchronize()
                
                }
                else
                {
                    print("Not subscribed")
                }
        }
        
    }
    
    func setupMainFlowCoordinator()
    {
        self.window = UIWindow()
        self.window?.makeKeyAndVisible()
        
        
        self.mainFlowCoordinator = MainFlowCoordinator(window: self.window!)
        self.mainFlowCoordinator.presentMain()
    }
    
    func setupData()
    {
        UserManager.sharedInstance.getLocationPlace()
        DataManager.sharedInstance.setEyeSymptomsList()
        DataManager.sharedInstance.setEducationData()
        DataManager.sharedInstance.setProductDetails()
    }
    
    func setupNotificationCenter()
    {
       NotificationCenter.default.addObserver(self, selector: #selector(self.presentHome), name: NSNotification.Name(rawValue: k.presentHomeKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentBlinkExamChart), name: NSNotification.Name(rawValue: k.presentBlinkExamChartKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentLatestProductMatch), name: NSNotification.Name(rawValue: k.presentLatestProductMatchKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentStartNewJourney), name: NSNotification.Name(rawValue: k.presentStartNewJourneyKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentContinueJourneyLeft), name: NSNotification.Name(rawValue: k.presentContinueLeftJourneyKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentFindRetailer), name: NSNotification.Name(rawValue: k.presentFindRetailerKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentSavingCard), name: NSNotification.Name(rawValue: k.presentCouponAndSavingCardKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentLocator), name: NSNotification.Name(rawValue: k.presentLocatorKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentProducts), name: NSNotification.Name(rawValue: k.presentProductsKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentEducation), name: NSNotification.Name(rawValue: k.presentEducationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentPollenForecast), name: NSNotification.Name(rawValue: k.presentPollenForecastKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentEyeCareProffesional), name: NSNotification.Name(rawValue: k.presentEyeCareProffesionalKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentDiagnosedFlow), name: NSNotification.Name(rawValue: k.presentDiagnoseDryEyeKey), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentLandingProductMatch), name: NSNotification.Name(rawValue: k.presentLandingProductMatchKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentLandingBlinkExamDashboard), name: NSNotification.Name(rawValue: k.presentLandingBlinkExamDashboardKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentLandingSavingsCard), name: NSNotification.Name(rawValue: k.presentLandingSavingsCardKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentLandingFindRetailer), name: NSNotification.Name(rawValue: k.presentLandingFindRetailerKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentProductDetails), name: NSNotification.Name(rawValue: k.presentProductDetailsKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentLandingProductDetails), name: NSNotification.Name(rawValue: k.presentLandingProductDetailsKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentHomeSelection), name: NSNotification.Name(rawValue: k.presentHomeSelectionKey), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.didChangeWindowOrientation), name: NSNotification.Name(rawValue: k.didChangeWindowOrientationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.didSelectNotification), name: NSNotification.Name(rawValue: k.didSelectNotificationControllerKey), object: nil)
        
        
//        NotificationCenter.default.addObserver(self, selector: #selector(, name: ), object: nil)
    }
    
   
    
    
    
    @objc func didChangeWindowOrientation()
    {
        if(self.orientationLock == .portrait)
        {
            self.orientationLock = .landscape
        }
        else
        {
            self.orientationLock = .portrait
        }
    }
    
    @objc func presentProductDetails()
    {
        self.mainFlowCoordinator.presentProductDetails()
    }
    
    @objc func presentHomeSelection()
    {
        self.mainFlowCoordinator.presentHomeSelection()
    }
    
    @objc func presentPollenForecast()
    {
        self.mainFlowCoordinator.presentPollenForecast()
    }
    
    @objc func presentEyeCareProffesional()
    {
        self.mainFlowCoordinator.presentEyeCareProffesional()
    }
    
    @objc func presentEducation()
    {
        self.mainFlowCoordinator.presentEducation()
    }
    
    @objc func presentLandingProductDetails()
    {
        self.mainFlowCoordinator.presentLandingProductDetails()
    }
    
    @objc func presentProducts()
    {
        self.mainFlowCoordinator.presentProducts()
    }
    
    @objc func didSelectNotification()
    {
        self.mainFlowCoordinator.selectNotification()
    }
    
    @objc func presentLocator()
    {
        self.mainFlowCoordinator.presentLocator()
    }
    
    @objc func presentHome()
    {
        self.mainFlowCoordinator.presentHomeViewController()
    }
    
    @objc func presentFindRetailer()
    {
       self.mainFlowCoordinator.presentFindRetailer()
    }
    
    @objc func presentBlinkExamChart()
    {
        self.mainFlowCoordinator.presentBlinkExamChartViewController()
    }
    
    @objc func presentLatestProductMatch()
    {
        self.mainFlowCoordinator.presentLatestProductMatchViewController()
    }
    
    @objc func presentStartNewJourney()
    {
        self.mainFlowCoordinator.presentStartNewJourneyViewController()
    }
    
    @objc func presentContinueJourneyLeft()
    {
        self.mainFlowCoordinator.presentContinueJourneyLeftViewController()
    }
    
    @objc func presentSavingCard()
    {
        self.mainFlowCoordinator.presentCouponSavingCards()
    }
    
    @objc func presentDiagnosedFlow() {
        self.mainFlowCoordinator.presentDiagnosedWithDryEyeJourney()
    }
    
    @objc func presentLandingBlinkExamDashboard()
    {
        self.mainFlowCoordinator.presentLandingBlinkExamDashboard()
    }
    
    @objc func presentLandingFindRetailer()
    {
        self.mainFlowCoordinator.presentLandingFindRetailer()
    }
    
    @objc func presentLandingProductMatch()
    {
        self.mainFlowCoordinator.presentLandingProductMatch()
    }
    
    @objc func presentLandingSavingsCard()
    {
        self.mainFlowCoordinator.presentLandingSavingsCard()
    }
    
    
    //MARK: App Workflow
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UNUserNotificationCenter.current().delegate = self
        
        if (UserDefaults.standard.value(forKey: k.services.firstLaunchServiceNotification) == nil) {
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        Localytics.autoIntegrate(k.services.localyticsKey, withLocalyticsOptions:[
            LOCALYTICS_WIFI_UPLOAD_INTERVAL_SECONDS: 5,
            LOCALYTICS_GREAT_NETWORK_UPLOAD_INTERVAL_SECONDS: 10,
            LOCALYTICS_DECENT_NETWORK_UPLOAD_INTERVAL_SECONDS: 30,
            LOCALYTICS_BAD_NETWORK_UPLOAD_INTERVAL_SECONDS: 90
            ], launchOptions: launchOptions)
        Localytics.tagEvent(trackingEvents.startedAppkey)
        UIApplication.shared.statusBarStyle = .lightContent
        UINavigationBar.appearance().tintColor = Style.primaryColor
        Fabric.with([Crashlytics.self])
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        
        GMSServices.provideAPIKey(k.services.googleMapsApiKey)
        GMSPlacesClient.provideAPIKey(k.services.googleMapsApiKey)
        self.setupData()
        TransitionManager.sharedInstance.setAppDelegate(delegate: self)
        self.setupMainFlowCoordinator()
        
        self.setupNotificationCenter()
        
        if #available(iOS 12.0, *), objc_getClass("UNUserNotificationCenter") != nil {
            let options: UNAuthorizationOptions = [.provisional]
            UNUserNotificationCenter.current().requestAuthorization(options: options) { (granted, error) in
                Localytics.didRequestUserNotificationAuthorization(withOptions: options.rawValue, granted: granted)
            }
        }
        
    
        
        self.setupLoadAppLocalytics()
        
        
        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        
//        let state : UIApplicationState = application.applicationState
        
//        if (state == UIApplicationState.background || (state == UIApplicationState.inactive && !self.appIsStarting))
//        {
            let aps = userInfo["aps"]
            
            if (aps != nil)
            {
                // perform the background fetch and
                // call completion handler
                self.fetchWeatherZoneDataFor(k.services.dryIndexForecastHeader, .drySkin) { (flag) in
                    if(flag)!
                    {
                        NotificationManager.sharedInstance.scheduleTestPushNotification(k.alert.dryIndexHighNotification)
                        completionHandler(UIBackgroundFetchResult.newData)
                    }
                    else
                    {
                        completionHandler(UIBackgroundFetchResult.noData)
                    }
                }
                
                self.fetchWeatherZoneDataFor(k.services.pollenForecastHeader, .pollen) { (flag) in
                    if(flag)!
                    {
                        NotificationManager.sharedInstance.scheduleTestPushNotification(k.alert.pollenIndexHighNotification)
                        completionHandler(UIBackgroundFetchResult.newData)
                    }
                    else
                    {
                        completionHandler(UIBackgroundFetchResult.noData)
                    }
                }
                
            }
//        }
//        else if (state == UIApplicationState.inactive && self.appIsStarting)
//        {
            // user tapped notification
            
//        }
//        else
//        {
            // app is active
            
//        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        
        
        if(UserDefaults.standard.value(forKey: k.user.deviceToken) == nil)
        {
            UserDefaults.standard.set(token, forKey: k.user.deviceToken)
            UserDefaults.standard.synchronize()
        }
        
        if(UserDefaults.standard.value(forKey: k.services.firstLaunchServiceNotification) == nil)
        {
            let userNotification = UNUserNotificationCenter.current()
            userNotification.getNotificationSettings { (settings) in
                if(settings.authorizationStatus == .authorized)
                {
                    self.setupSubscribe()
                }
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("i am not available in simulator \(error)")
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if response.notification.request.identifier == "JourneyContinuationReminderNotification" {
            UIApplication.shared.applicationIconBadgeNumber = 0
            self.mainFlowCoordinator.presentHomeSelection()
        }
        if response.notification.request.identifier == "BlinkExamReminders" {
            UIApplication.shared.applicationIconBadgeNumber = 0
            self.presentLandingBlinkExamDashboard()
        }
        if response.notification.request.identifier == "SavingCardDiscountNotifications"{
            UIApplication.shared.applicationIconBadgeNumber = 0
            self.presentLandingSavingsCard()
        }
        
        if response.notification.request.identifier == "JourneyAgainRemainder" {
            UIApplication.shared.applicationIconBadgeNumber = 0
            self.mainFlowCoordinator.presentHomeSelection()
        }
        
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("HELP 3")
        completionHandler([.alert, .badge, .sound])
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
//        self.appIsStarting = false
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
//        self.appIsStarting = false
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
//        self.appIsStarting = true
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
//        self.appIsStarting = false
        UIApplication.shared.applicationIconBadgeNumber = 0
        let userNotification = UNUserNotificationCenter.current()
        userNotification.getNotificationSettings { (settings) in
            if settings.authorizationStatus != .authorized {
                
            NotificationManager.sharedInstance.cancelNotifictaion(withIndentifier: "JourneyContinuationReminderNotification")
            NotificationManager.sharedInstance.setJourneyContinuationReminders(flag: false)
                
            NotificationManager.sharedInstance.cancelNotifictaion(withIndentifier: "BlinkExamReminders")
            NotificationManager.sharedInstance.setBlinkExamReminders(flag: false)
                
            NotificationManager.sharedInstance.cancelNotifictaion(withIndentifier: "SavingCardDiscountNotifications")
            NotificationManager.sharedInstance.setSavingCardDiscountNotifications(flag: false)
                
            //no toggle for JourneyAgainRemainder
            NotificationManager.sharedInstance.cancelNotifictaion(withIndentifier: "JourneyAgainRemainder")
            
            NotificationManager.sharedInstance.cancelNotifictaion(withIndentifier: "DryEyeIndexAndPollenForecastNotices")
            NotificationManager.sharedInstance.setDryEyeIndexAndPollenForecastNotices(flag: false)
            }
        }
    }

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask
    {
        return self.orientationLock
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
//        DispatchQueue.global(qos: .background).async {
            Localytics.tagEvent(trackingEvents.closedAppKey)
//        }
        sleep(3)
//        DispatchQueue.main.async {
//            Localytics.tagEvent(trackingEvents.closedAppKey)
//        }
    }
    
    
    //MARK: WeatherZone Service
    
    func fetchWeatherZoneDataFor(_ forecast : String, _ option : ForecastOption, completion: @escaping (Bool?) -> Void)
    {
        if(NetworkManager.sharedInstance.checkReachability())
        {
            var url : String = ""
            if let location = UserManager.sharedInstance.getLocation()
            {
                url = self.setupUrl(forecast, location)
            }
            else
            {
                url = ""
            }
            
            if(url != "")
            {
                DispatchQueue.main.async {
                    Alamofire.request(url)
                        .responseJSON { response in
                            
                            if(response.error == nil)
                            {
                                let json = response.result.value
                                let forecasts = (json as! NSDictionary).value(forKeyPath: "countries.locations.local_forecasts.forecasts") as AnyObject
                                
                                if(((forecasts.object(at: 0) as AnyObject).object(at: 0) as AnyObject).description != k.text.null)
                                {
                                    var index : Int?
                                    if(option == .drySkin)
                                    {
                                        if let dryIndex = (((forecasts.object(at: 0) as AnyObject).object(at: 0) as AnyObject).object(at: 0) as AnyObject).value(forKeyPath: "indices.value")
                                        {
                                            index = (dryIndex as AnyObject).object(at: 0) as? Int
                                        }
                                        else
                                        {
                                            index = 0
                                        }
                                        
                                    }
                                    else if(option == .pollen)
                                    {
                                        if let pollenIndex = (((forecasts.object(at: 0) as AnyObject).object(at: 0) as AnyObject).object(at: 0) as AnyObject).value(forKeyPath: "pollen_index")
                                        {
                                            index = (pollenIndex as AnyObject) as? Int
                                            
                                        }
                                        else
                                        {
                                            index = 0
                                        }
                                    }
                                    
                                    
                                    if(index! >= 5)
                                    {
                                        completion(true)
                                    }
                                    else
                                    {
                                        completion(false)
                                    }
                                }
                                else
                                {
                                    completion(false)
                                }
                                
                                
                            }
                            else
                            {
                                completion(false)
                            }
                    }
                    
                }
                
            }
        }
        else
        {
            completion(false)
        }
        
    }
}



enum WindowOrientation {
    case portrait
    case landscape
}

