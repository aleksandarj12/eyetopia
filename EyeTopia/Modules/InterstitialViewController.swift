//
//  InterstitialViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 5/18/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

class InterstitialViewController: UIViewController {

    @IBOutlet weak var holderView: UIView!
    
    var link: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        holderView.layer.cornerRadius = 7
    }

    
    

    @IBAction func continueButtonTapped(_ sender: UIButton) {
        Localytics.tagEvent(trackingEvents.buyNowOptionChemistWarehouseKey)
        self.dismiss(animated: true) {
            if let linkURL = self.link {
                UIApplication.shared.open(linkURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func stayInEyeTopiaTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
