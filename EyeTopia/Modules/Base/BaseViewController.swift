//
//  BaseViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/7/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    var skipButton : UIButton!
    var backButton : UIButton!
    var exitButton : UIButton!
    var fullView : UIView!
    var holderView : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getHolderView() -> UIView
    {
        return self.holderView
    }
    
    
    // MARK: - Setup & Support Methods
    
    func setupTopHolderView(backgroundColor : UIColor, hasCancelButton : Bool, hasSkipButton : Bool, progressBarLoaded : Int, hasBackButton : Int, hasExitButton : Bool)
    {
        self.holderView = UIView(frame: CGRect(x: 0, y: 44, width: UIScreen.main.bounds.size.width, height: 44))
        self.holderView.backgroundColor = backgroundColor
        
        if(hasCancelButton)
        {
            let cancelButton : UIButton = UIButton(frame: CGRect(x: 15, y: 2, width: 60, height: 40))
            cancelButton.setTitleColor(Style.primaryColor, for: .normal)
            cancelButton.setTitle("Cancel", for: .normal)
            cancelButton.addTarget(self, action: #selector(backButtonTapped(_:)), for: UIControlEvents.touchUpInside)
            self.holderView.addSubview(cancelButton)
        }
        
        if(hasSkipButton)
        {
            self.skipButton = UIButton(frame: CGRect(x: UIScreen.main.bounds.size.width - 75, y: 2, width: 60, height: 40))
            skipButton.setTitleColor(Style.primaryColor, for: .normal)
            skipButton.setTitle("Skip", for: .normal)
            skipButton.addTarget(self, action: #selector(skipButtonTapped(_:)), for: UIControlEvents.touchUpInside)
            
            self.holderView.addSubview(skipButton)
        }
        
        if(progressBarLoaded != 0)
        {
            self.fullView = UIView(frame: CGRect(x: 0, y: 40, width: self.holderView.frame.size.width, height: 4))
            fullView.backgroundColor = UIColor.black
            
            let progressView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: progressBarLoaded, height: 4))
            progressView.backgroundColor = Style.primaryColor
            fullView.addSubview(progressView)
            
            self.holderView.addSubview(fullView)
        }
        
        if(hasBackButton == 1)
        {
            self.backButton = UIButton(frame: CGRect(x: 15, y: 2, width: 60, height: 40))
            backButton.setTitleColor(UIColor.white, for: .normal)
            backButton.setTitle("< Back", for: .normal)
            backButton.addTarget(self, action: #selector(backButtonTapped(_:)), for: UIControlEvents.touchUpInside)
            self.holderView.addSubview(backButton)
        }
        else if(hasBackButton == 2)
        {
            self.backButton = UIButton(frame: CGRect(x: 15, y: 2, width: 60, height: 40))
            backButton.setTitleColor(UIColor.white, for: .normal)
            backButton.setTitle("< Back", for: .normal)
            backButton.addTarget(self, action: #selector(dismissViewController(_:)), for: UIControlEvents.touchUpInside)
            self.holderView.addSubview(backButton)
        }
        
        if(hasExitButton)
        {
            self.exitButton = UIButton(frame: CGRect(x: UIScreen.main.bounds.size.width - 75, y: 2, width: 60, height: 40))
            exitButton.setTitleColor(UIColor.white, for: .normal)
            exitButton.setTitle("Exit", for: .normal)
            //            exitButton.addTarget(self, action: #selector(skipButtonTapped(_:)), for: UIControlEvents.touchUpInside)
            
            self.holderView.addSubview(exitButton)
        }
        
        
    }
    
    
    //MARK: Action Methods
    
    @objc func dismissViewController(_ sender: AnyObject)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func backButtonTapped(_ sender: AnyObject)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func skipButtonTapped(_ sender: AnyObject)
    {
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        let controller : GuideMainViewController = storyboard.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }

}
