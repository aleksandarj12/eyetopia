//
//  LifestyleSurveyViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/13/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

class LifestyleSurveyViewController: UIViewController {

    @IBOutlet weak var noAllergyButton: RoundedButton!
    @IBOutlet weak var yesAllergyButton: RoundedButton!
    @IBOutlet weak var yesNightButton: RoundedButton!
    @IBOutlet weak var noNightButton: RoundedButton!
    @IBOutlet weak var yesAirconButton: RoundedButton!
    @IBOutlet weak var noAirconButton: RoundedButton!
    @IBOutlet weak var noDriveButton: RoundedButton!
    @IBOutlet weak var yesDriveButton: RoundedButton!
    
    @IBOutlet weak var holderView1: UIView!
    @IBOutlet weak var holderView2: UIView!
    @IBOutlet weak var holderView3: UIView!
    @IBOutlet weak var holderView4: UIView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
        
        if let view = self.holderView1 {
            view.layer.cornerRadius = 4
        }
        
        if let view = self.holderView2 {
            view.layer.cornerRadius = 4
        }
        
        if let view = self.holderView3 {
            view.layer.cornerRadius = 4
        }
        
        if let view = self.holderView4 {
            view.layer.cornerRadius = 4
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        self.setupTransitionLifestyleSurver()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(self.restorationIdentifier == "LifestyleSurveyViewController1")
        {
//            Localytics.tagScreen("Lifestyle survey driving")
        }
        else if(self.restorationIdentifier == "LifestyleSurveyViewController2")
        {
//            Localytics.tagScreen("Lifestyle survey air conditioned environment")
        }
        else if(self.restorationIdentifier == "LifestyleSurveyViewController3")
        {
//            Localytics.tagScreen("Lifestyle survey at night")
        }
        else if(self.restorationIdentifier == "LifestyleSurveyViewController4")
        {
//            Localytics.tagScreen("Lifestyle survey hayfever allergies")
        }
    }
    
    
    @IBAction func cancelButtonTapped(_ sender: Any)
    {
        self.setupExitCancel()
    }
    
    @IBAction func backButtonTapped(_ sender: Any)
    {
        if(TransitionManager.sharedInstance.transitionLifeStyle == 1)
        {
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    //MARK: Setup & Support methods
    
    func setupExitCancel()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to cancel?", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes", style: .destructive) { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func setupTransitionLifestyleSurver()
    {
        if(TransitionManager.sharedInstance.transitionLifeStyle == 1)
        {
            self.descriptionLabel.text = "Do you notice your symptoms when concentrating on screens or driving for long periods of time?"
            self.mainImageView.image = UIImage(named: "graphicScreensDriving")
        }
        else if(TransitionManager.sharedInstance.transitionLifeStyle == 1)
        {
           self.descriptionLabel.text = "Do you find your symptoms are worse when you are in an air conditioned environment?"
            self.mainImageView.image = UIImage(named: "graphicAc")
        }
        else if(TransitionManager.sharedInstance.transitionLifeStyle == 1)
        {
            self.descriptionLabel.text = "Do you have any dry eye symptoms at night?"
            self.mainImageView.image = UIImage(named: "graphicNight")
        }
        else if(TransitionManager.sharedInstance.transitionLifeStyle == 1)
        {
            self.descriptionLabel.text = "Do you suffer from hayfever allergies?"
            self.mainImageView.image = UIImage(named: "graphicAllergies")
        }
    }

    
    @IBAction func didFinishScreen(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: k.pushReceiveGiftCardKey), object: self)
    }
    
    @IBAction func noButtonTapped(_ sender: Any)
    {
        if(self.restorationIdentifier == "LifestyleSurveyViewController1")
        {
            UserManager.sharedInstance.setIsScreenOrDrivingProblem(flag: false)
        }
        else if(self.restorationIdentifier == "LifestyleSurveyViewController2")
        {
            UserManager.sharedInstance.setIsAirconEnvironmentProblem(flag: false)
        }
        
//        self.continueButtonTapped()
    }
    
    @IBAction func yesSufferFromAllergyButtonTapped(_ sender: Any)
    {
        UserManager.sharedInstance.setIsSufferFromAllergy(flag: true)
    }
    
    @IBAction func yesSymptomsAtNightButtonTapped(_ sender: Any)
    {
        UserManager.sharedInstance.setIsDryEyesAtNight(flag: true)
    }
    
    @IBAction func yesButtonTapped(_ sender: Any)
    {
        if(self.restorationIdentifier == "LifestyleSurveyViewController1")
        {
            UserManager.sharedInstance.setIsScreenOrDrivingProblem(flag: true)
        }
        else if(self.restorationIdentifier == "LifestyleSurveyViewController2")
        {
            UserManager.sharedInstance.setIsAirconEnvironmentProblem(flag: true)
        }
        
//        self.continueButtonTapped()
    }
    
    func continueButtonTapped()
    {
//        TransitionManager.sharedInstance.transitionLifeStyle = TransitionManager.sharedInstance.transitionLifeStyle + 1
        
        
        
        if(TransitionManager.sharedInstance.transitionLifeStyle == 5)
        {
            TransitionManager.sharedInstance.transitionLifeStyle = 0
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            let controller : LifestyleSurveyViewController = self.storyboard?.instantiateViewController(withIdentifier: "LifestyleSurveyViewController" + String(TransitionManager.sharedInstance.transitionLifeStyle)) as! LifestyleSurveyViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
}
