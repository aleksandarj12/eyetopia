//
//  WelcomeBackToEyeTopiaViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 4/20/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

protocol WelcomeBackDelegate {
    func presentDiagnosedWithDryEyeJourney()
}

class WelcomeBackToEyeTopiaViewController: UIViewController {

    @IBOutlet weak var holderView: UIView!
    
    var delegate : WelcomeBackDelegate!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        holderView.layer.cornerRadius = 5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func dismissButtonTapped(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func continueButtonTapped(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
        UserManager.sharedInstance.removeProductMatchData()
        UserManager.sharedInstance.setDiagnosedJourneyCompetion(flag: false)
        UserManager.sharedInstance.setNonDiagnosedJourneyCompetion(flag: false)
        NotificationManager.sharedInstance.scheduleJourneyContinuationReminderNotification()
//        if let delegateMethod = self.delegate {
//            delegateMethod.presentDiagnosedWithDryEyeJourney()
//        }
//        else
//        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: k.presentDiagnoseDryEyeKey), object: self)
//        }
//        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentHomeSelectionKey), object: self)
//        let controller : ContactLensIntroductionViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactLensIntroductionViewController") as! ContactLensIntroductionViewController
//
//        let navController : UINavigationController = UINavigationController(rootViewController: controller)
//        navController.setNavigationBarHidden(true, animated: false)
//        self.present(navController, animated: true, completion: nil)
//        appDelegate?.window?.rootViewController = controller
    }
    
    

}
