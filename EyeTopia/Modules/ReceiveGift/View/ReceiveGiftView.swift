//
//  ReceiveGiftView.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/4/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

protocol ReceiveGiftView {
    func presentSecondScreen()
    func presentThirdScreen()
    func setTransitionOtherWorld()
    func setSystaneCard(_ flag : Bool, _ cardID : Int?)
    func didSetAlert(_ message : String)
}
