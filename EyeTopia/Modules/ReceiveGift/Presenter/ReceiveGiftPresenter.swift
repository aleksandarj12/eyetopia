//
//  ReceiveGiftPresenter.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/4/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Alamofire

class ReceiveGiftPresenter: NSObject {
    
    var view : ReceiveGiftView!
    var isReceivedGift : Bool = false
    
    var cardID : Int?
    
    func attachView(view: ReceiveGiftView)
    {
        self.view = view
    }
    
    func detachView() -> Void
    {
        self.view = nil
    }
    
    func setIsReceivedReGift()
    {
       self.isReceivedGift = true
    }
    
    func loadSystaneCard(_ storeID : Bool)
    {
        Alamofire.request(k.services.savingsCardUrl, headers: nil)
            .responseJSON { response in
                
                if(response.error == nil)
                {
                    let json = response.result.value
                    let barcode = (json as AnyObject).value(forKey: k.card.status) as AnyObject
                    
                    if(barcode as! String == k.alert.noActiveBarcode)
                    {
                        self.view.setSystaneCard(false, nil)
                    }
                    else
                    {
                        let cardId = (json as AnyObject).value(forKeyPath: "barcode.id") as AnyObject
                        
                        UserManager.sharedInstance.checkSavingCardTimeForReset()
                        
                        if(!UserManager.sharedInstance.getIsSavingCardActivated())
                        {
                            let retailerImageStringURL = ((json as AnyObject).value(forKeyPath: "barcode.retailer_image") as AnyObject).description
                            let barcodeImageStringURL = ((json as AnyObject).value(forKeyPath: "barcode.barcode_image") as AnyObject).description
                            let retailerCopy = ((json as AnyObject).value(forKeyPath: "barcode.retailer_copy") as AnyObject).description
                            let discount = ((json as AnyObject).value(forKeyPath: "barcode.discount") as AnyObject) as! Double
                            let time = ((json as AnyObject).value(forKeyPath: "barcode.time_available") as AnyObject) as! Int
                            let instructions = ((json as AnyObject).value(forKeyPath: "barcode.instructions") as AnyObject).description
                            let conditions = ((json as AnyObject).value(forKeyPath: "barcode.terms_and_conditions") as AnyObject).description
                            
                            
                            UserManager.sharedInstance.setSavingCardRetailerCopy(retailerCopy)
                            UserManager.sharedInstance.setSavingCardDiscount(discount)
                            UserManager.sharedInstance.setSavingCardTimeAvailable(time)
                            UserManager.sharedInstance.setSavingCardBarcode(barcodeImageStringURL!)
                            UserManager.sharedInstance.setSavingCardRetailerLogo(retailerImageStringURL!)
                            UserManager.sharedInstance.setSavingCardInstructions(instructions)
                            UserManager.sharedInstance.setSavingCardConditions(conditions)
                        }
                        
                        if(barcode.description == k.text.noSystaneCard || UserManager.sharedInstance.getSavingCardID() == cardId as? Int)
                        {
                            self.view.setSystaneCard(false, nil)
                        }
                        else
                        {
                            UserManager.sharedInstance.checkSavingCardTimeForReset()
                            
                            if(UserManager.sharedInstance.getIsSavingCardActivated())
                            {
                                self.view.setSystaneCard(false, nil)
                            }
                            else
                            {
                                self.view.setSystaneCard(true, cardId as? Int)
                            }
                        }
                        self.cardID = (cardId as? Int)
                        if(storeID)
                        {
                            UserManager.sharedInstance.setSavingCardID(cardID: self.cardID)
                        }
                        
                    }
                }
                else
                {
                    self.view.didSetAlert("Cannot retrieve data from server")
                    self.view.setSystaneCard(false, nil)
                }
        }
    }
    
    func getTransitionToOtherWorld()
    {
        self.view.setTransitionOtherWorld()
    }
    
    func getNextScreen()
    {
        self.view.presentThirdScreen()
    }
}
