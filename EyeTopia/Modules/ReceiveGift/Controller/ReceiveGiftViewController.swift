//
//  ReceiveGiftViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/4/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

class ReceiveGiftViewController: UIViewController, ReceiveGiftView, SystaneCardDelegate {
    
    @IBOutlet weak var progressBarTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var bottomHolderView: UIView!
    @IBOutlet weak var topHolderView1: UIView!
    @IBOutlet weak var topHolderView2: UIView!
    @IBOutlet weak var topHolderView3: UIView!
    @IBOutlet weak var topHolderView5: UIView!
    @IBOutlet weak var topHolderView4: UIView!
    
    @IBOutlet weak var skipProductMatchButton: UIButton!
    @IBOutlet weak var bottomHolderView1: UIView!
    @IBOutlet weak var bottomHolderView2: UIView!
    @IBOutlet weak var bottomHolderView3: UIView!
    @IBOutlet weak var bottomHolderView5: UIView!
    @IBOutlet weak var bottomHolderView4: UIView!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    var finishJourney : Bool = false
    var cardID : Int?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var presenter : ReceiveGiftPresenter = ReceiveGiftPresenter()
    var isTransitioned : Bool = false
    var isLocationProvided : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        skipProductMatchButton.layer.cornerRadius = 4
        skipProductMatchButton.clipsToBounds = true
        
        
        if let bottom = self.bottomHolderView1 {
            bottom.backgroundColor = Style.primaryColor
        }
        
        if let bottom = self.bottomHolderView2 {
            bottom.backgroundColor = Style.primaryColor
        }
        
        if let bottom = self.bottomHolderView3 {
            bottom.backgroundColor = Style.primaryColor
        }
        
        if let bottom = self.bottomHolderView4 {
            bottom.backgroundColor = Style.primaryColor
        }
        
        self.bottomHolderView.backgroundColor = Style.primaryColor
        if let bottom = self.bottomHolderView5 {
            bottom.backgroundColor = Style.primaryColor
        }
        self.presenter.attachView(view: self)
//        self.setupFirstTopHolderView()
        self.skipProductMatchButton.alpha = 0
        self.presenter.loadSystaneCard(false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(TransitionManager.sharedInstance.storyboardID == "didFinishWithLocationSuccessfully")
        {
            TransitionManager.sharedInstance.storyboardID = ""
            self.isLocationProvided = true
            self.setupLastTransitionFlowView()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Localytics.tagScreen("Receive gift promo code")
    }
    
    
    //MARK: - SystaneCardDelegate
    
    func didFinishSystaneCard()
    {
        self.presentSecondScreen()
    }
    

    //MARK: - Setup & Support Methods
    
    func setup4Holders()
    {
        self.topHolderView4.alpha = 1
        self.bottomHolderView4.alpha = 1
        self.bottomHolderView.alpha = 1
    }
    
    func setupAnimations(views : [UIView], alpha : CGFloat)
    {
        UIView.animate(withDuration: 0.7) {
            for view in views
            {
                view.alpha = alpha
            }
            self.bottomHolderView.alpha = alpha
        }
    }
    
    func setupResultScreen()
    {
        let navController : UINavigationController = self.storyboard?.instantiateViewController(withIdentifier: "ResultBalanceUltraPrimaryNavigationController") as! UINavigationController
        self.present(navController, animated: true, completion: nil)
    }
    
    func setupJourneyStation()
    {
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
        UserManager.sharedInstance.setJourneyStation(self.navigationController!, controller)
    }
    
    func setupExitView()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to exit? You may continue later.", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes, exit", style: .destructive) { (action) -> Void in
            self.setupJourneyStation()
            NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentHomeKey), object: self)
            UserManager.sharedInstance.setDiagnosedJourneyCompetion(flag: false)
            UserManager.sharedInstance.setNonDiagnosedJourneyCompetion(flag: false)
            NotificationManager.sharedInstance.scheduleJourneyContinuationReminderNotification()
            Localytics.closeSession()
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func setupLastTransitionFlowView()
    {
        UIView.animate(withDuration: 0.7, animations: {
            self.topHolderView3.alpha = 0
            self.skipProductMatchButton.alpha = 0
            self.bottomHolderView2.alpha = 0
            self.bottomHolderView.alpha = 0
        }) { (flag : Bool) in
            UIView.animate(withDuration: 0.7) {
                self.topHolderView5.alpha = 1
                self.bottomHolderView3.alpha = 1
                self.bottomHolderView.alpha = 1
            }
        }
        progressBarTrailing.constant = 70
    }
    
    func setupFirstTopHolderView()
    {
        UIView.animate(withDuration: 0.7) {
            self.topHolderView1.alpha = 1
        }
    }
    
    
    //MARK: - ReceiveGiftView
    
    func setSystaneCard(_ flag: Bool, _ cardID : Int?)
    {
        //tuka false nil
        if(cardID != nil)
        {
            self.cardID = cardID
        }
        if(!flag)
        {
            self.setupAnimations(views: [self.topHolderView4, self.bottomHolderView4], alpha: 1)
        }
        else
        {
            self.setupAnimations(views: [self.topHolderView1, self.bottomHolderView1], alpha: 1)
        }
        
    }
    
    func didSetAlert(_ message: String)
    {
        let controller : UIAlertController = UIAlertController(title: "WARNING", message: k.alert.noInternetConnection, preferredStyle: .alert)
        let cancelAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func setTransitionOtherWorld()
    {
        UIView.animate(withDuration: 0.7, animations: {
            self.topHolderView3.alpha = 0
            self.skipProductMatchButton.alpha = 0
            self.bottomHolderView2.alpha = 0
            self.bottomHolderView.alpha = 0
        }) { (flag : Bool) in
            UIView.animate(withDuration: 4, animations: {
                self.backgroundImageView.frame.origin.x = -3215
            }, completion: { (flag : Bool) in
                UIView.animate(withDuration: 0.7, animations: {
//                    self.topHolderView4.alpha = 1
                    self.bottomHolderView3.alpha = 1
                    self.bottomHolderView.alpha = 1
                }, completion: { (flag : Bool) in
                    
                })
                
            })
        }
        progressBarTrailing.constant = 30
    }
    
    func presentSecondScreen()
    {
        if(!self.finishJourney)
        {
            UIView.animate(withDuration: 0.7, animations: {
                self.topHolderView1.alpha = 0
                self.bottomHolderView1.alpha = 0
                self.bottomHolderView.alpha = 0
                self.topHolderView4.alpha = 0
                self.bottomHolderView4.alpha = 0
            }) { (flag : Bool) in
                
                UIView.animate(withDuration: 4, animations: {
                    self.backgroundImageView.frame.origin.x = -2520.0
                }, completion: { (flag : Bool) in
                    
                    UIView.animate(withDuration: 0.7, delay: 2, options: UIViewAnimationOptions.allowAnimatedContent, animations: {
                        self.topHolderView2.alpha = 1
                        self.bottomHolderView5.alpha = 1
                        self.bottomHolderView.alpha = 1
                    }, completion: { (flag : Bool) in
                        
                    })
                })
            }
            
            progressBarTrailing.constant = 60
        }
        
    }
    
    func presentThirdScreen()
    {
        UIView.animate(withDuration: 0.7, animations: {
            self.topHolderView2.alpha = 0
            self.bottomHolderView5.alpha = 0
            self.bottomHolderView.alpha = 0
        }) { (flag : Bool) in
            UIView.animate(withDuration: 0.7, animations: {
                self.topHolderView3.alpha = 1
                self.skipProductMatchButton.alpha = 1
                self.bottomHolderView2.alpha = 1
                self.bottomHolderView.alpha = 1
            })
        }
        progressBarTrailing.constant = 60
    }
    
    
    // MARK: - Action Methods
    
    @IBAction func continueSkipCardButtonTapped(_ sender: Any)
    {
        self.presentSecondScreen()
    }
    
    func continueWithJourney()
    {
        UIView.animate(withDuration: 0.7, animations: {
            self.topHolderView5.alpha = 0
            self.bottomHolderView3.alpha = 0
            self.bottomHolderView.alpha = 0
        }) { (flag : Bool) in
            UIView.animate(withDuration: 4, animations: {
                self.backgroundImageView.frame.origin.x = -3215
            }, completion: { (flag : Bool) in
                UIView.animate(withDuration: 0.7, animations: {
                    //                    self.topHolderView4.alpha = 1
                    self.bottomHolderView3.alpha = 1
                    self.bottomHolderView.alpha = 1
                }, completion: { (flag : Bool) in
                    
                })
                
            })
        }
        progressBarTrailing.constant = 30
    }
    
    @IBAction func continueButtonTapped(_ sender: Any)
    {
        self.finishJourney = true
        self.continueWithJourney()
    }
    
    @IBAction func seeResultsButtonTapped(_ sender: Any)
    {
        self.setupResultScreen()
    }
    
    @IBAction func previousButtonTapped(_ sender: Any)
    {
        UIView.animate(withDuration: 0.7, animations: {
            self.topHolderView3.alpha = 0
            self.skipProductMatchButton.alpha = 0
        }) { (flag : Bool) in
            UIView.animate(withDuration: 0.7, animations: {
                self.topHolderView2.alpha = 1
                self.bottomHolderView5.alpha = 1
                self.bottomHolderView.alpha = 1
                self.bottomHolderView2.alpha = 0
            })
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any)
    {
        if(self.topHolderView1.alpha == 1)
        {
            self.navigationController?.popViewController(animated: false)
        }
        
        else if(self.topHolderView5.alpha == 1)
        {
            UIView.animate(withDuration: 0.7, animations: {
                self.topHolderView5.alpha = 0
                self.bottomHolderView3.alpha = 0
                self.bottomHolderView.alpha = 0
            }) { (flag : Bool) in
                self.setupAnimations(views: [self.topHolderView3, self.skipProductMatchButton, self.bottomHolderView2], alpha: 1)
            }
            progressBarTrailing.constant = 60
        }
        
        else if(self.topHolderView4.alpha == 1 && self.bottomHolderView4.alpha == 1)
        {
            self.navigationController?.popViewController(animated: false)
        }
        
        else if(self.topHolderView3.alpha == 1 && self.bottomHolderView2.alpha == 1)
        {
            self.setupAnimations(views: [self.topHolderView3, self.bottomHolderView2, self.skipProductMatchButton], alpha: 0)
            self.setupAnimations(views: [self.topHolderView2, self.bottomHolderView5], alpha: 1)
        }
        
        else if(self.topHolderView2.alpha == 1 && self.bottomHolderView5.alpha == 1)
        {
            UIView.animate(withDuration: 0.7, animations: {
                self.topHolderView3.alpha = 0
                self.topHolderView2.alpha = 0
                self.skipProductMatchButton.alpha = 0
                self.bottomHolderView5.alpha = 0
                self.bottomHolderView2.alpha = 0
                self.bottomHolderView.alpha = 0
            }) { (flag : Bool) in
                UIView.animate(withDuration: 4, animations: {
                    self.backgroundImageView.frame.origin.x = -1750
                }, completion: { (flag : Bool) in
                    UIView.animate(withDuration: 0.7, animations: {
                        //tuka
                        
                        if(self.presenter.cardID == nil)
                        {
                            self.setup4Holders()
                        }
                        else if(UserManager.sharedInstance.getSavingCardID() == nil)
                        {
                            self.topHolderView1.alpha = 1
                            self.bottomHolderView1.alpha = 1
                            self.bottomHolderView.alpha = 1
                        }
                        else
                        {
                            self.setup4Holders()
                        }
                    })
                })
            }
            progressBarTrailing.constant = 80
        }
    }
    
    @IBAction func exitButtonTapped(_ sender: Any)
    {
        self.setupExitView()
//        let controller : ExitViewController = self.storyboard?.instantiateViewController(withIdentifier: "ExitViewController") as! ExitViewController
//        TransitionManager.sharedInstance.setContinueJourneyDescription(controllerDescription: "ReceiveGiftViewController")
//
//        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func receiveGiftButtonTapped(_ sender: Any)
    {
        UserManager.sharedInstance.setSavingCardID(cardID: self.presenter.cardID)
        self.isTransitioned = true
        self.presenter.setIsReceivedReGift()
        let storyBoard : UIStoryboard = UIStoryboard(name: k.storyboard.diagnosed, bundle: nil)
        let controller : SystaneCardViewController = storyBoard.instantiateViewController(withIdentifier: "SystaneCardViewController") as! SystaneCardViewController
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func nextButtonTapped(_ sender: Any)
    {
        self.presenter.getNextScreen()
    }
    
    @IBAction func skipButtonTapped(_ sender: Any)
    {
//        self.presenter.getTransitionToOtherWorld()
        self.setupResultScreen()
    }
    
    @IBAction func provideLocationButtonTapped(_ sender: Any)
    {
        let controller : ProvideLocationViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProvideLocationViewController") as! ProvideLocationViewController
        let navController : UINavigationController = UINavigationController(rootViewController: controller)
        navController.setNavigationBarHidden(true, animated: false)
        self.present(navController, animated: true, completion: nil)
    }
    

}
