//
//  PollenForecastViewCell.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/17/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class PollenForecastViewCell: UITableViewCell {

    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var mainProgressView: CornerCustomView!
    @IBOutlet weak var loadingProgressView: CornerCustomView!
    
    @IBOutlet weak var progressWidth: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func checkPollenStyle(status : String) -> UIColor
    {
        
        if(status == k.pollen.low)
        {
            return Style.pollenColor.low
        }
        else if(status == k.pollen.moderate)
        {
            return Style.pollenColor.moderate
        }
        else if(status == k.pollen.high)
        {
            return Style.pollenColor.high
        }
        else if(status == k.pollen.veryHigh)
        {
            return Style.pollenColor.veryHigh
        }
        else if(status == k.pollen.extreme)
        {
            return Style.pollenColor.extreme
        }
        
        return UIColor.lightGray
    }
    
    func setProgressBar(status : String) -> CGFloat
    {
        if(status == k.pollen.low)
        {
            return (self.mainProgressView.frame.width / 5) * 1
        }
        else if(status == k.pollen.moderate)
        {
            return (self.mainProgressView.frame.width / 5) * 2
        }
        else if(status == k.pollen.high)
        {
            return (self.mainProgressView.frame.width / 5) * 3
        }
        else if(status == k.pollen.veryHigh)
        {
            return (self.mainProgressView.frame.width / 5) * 4
        }
        else if(status == k.pollen.extreme)
        {
            return self.mainProgressView.frame.width
        }
        
        return 0.0
    }
    
}
