//
//  Polen.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/17/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

enum ForecastOption {
    case pollen
    case drySkin
}

class ForecastIndex: NSObject {
    var day : String?
    var index : String?
    var status : String?
}
