//
//  PollenForecastViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/17/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import GooglePlaces
import Alamofire
import Localytics

class PollenForecastViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, SearchPlaceDelegate {

    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var changeLocation: UIButton!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyDataLabel: UILabel!
    @IBOutlet weak var mainTitleLabel: UILabel!
    
    var forecastDays : [ForecastIndex] = [ForecastIndex]()
    var mainTitle : String!
    var isLocationChanged : Bool = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    var locationManager : CLLocationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkForEmptyData()
        self.tableView.register(UINib(nibName: "PollenForecastViewCell", bundle: nil), forCellReuseIdentifier: "PollenForecastViewCell")
        
        self.setupMainTitle()
        
        self.tableView.bounces = false
        self.tableView.isScrollEnabled = false
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        self.locationManager.delegate = self
        if(UserManager.sharedInstance.location != nil)
        {
            self.checkForForecast()
            self.getCity()
            self.changeLocation.setTitle("Change location", for: .normal)
        }
        else
        {
            self.setupDefaults()
            self.changeLocation.setTitle("Set location", for: .normal)
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
    }
    

    //MARK: SearchPlaceDelegate
    
    func didFindPlace(_ location: LocationDetail)
    {
        let locationC : CLLocation = CLLocation(latitude: (location.coordinates?.latitude)!, longitude: (location.coordinates?.longitude)!)
        UserManager.sharedInstance.setLocationPlace(location: locationC)
        if(self.isLocationChanged)
        {
            self.isLocationChanged = false
            
            
            if(UserManager.sharedInstance.isCurrentLocationActive)
            {
                UserManager.sharedInstance.setIsCurrentLocationActive(false)
                UserManager.sharedInstance.setLocationPlace(location: nil)
                self.locationManager.startUpdatingLocation()
            }
            else
            {
                if(UserManager.sharedInstance.location != nil)
                {
                    self.getCity()
//                    self.getCity(location: UserManager.sharedInstance.location!)
                    self.checkForForecast()
                }
                else
                {
                    if(CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse)
                    {
                        self.locationManager.requestWhenInUseAuthorization()
                        self.locationManager.startUpdatingLocation()
                    }
                }
            }
            
            
        }
    }
    
    func didSelectCurrentLocation()
    {
        UserManager.sharedInstance.setLocationPlace(location: nil)
        if(CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse)
        {
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.startUpdatingLocation()
        }
    }
    
    
    //UITableView DataSource & Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(self.forecastDays.count > 0)
        {
            return self.forecastDays.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : PollenForecastViewCell = self.tableView.dequeueReusableCell(withIdentifier: "PollenForecastViewCell") as! PollenForecastViewCell
        
        let pollen : ForecastIndex = self.forecastDays[indexPath.row]
        cell.dayLabel.text = pollen.day
        cell.descriptionLabel.text = pollen.index! + ", " + pollen.status!
        cell.descriptionLabel.textColor = cell.checkPollenStyle(status: pollen.status!)
        cell.loadingProgressView.backgroundColor = cell.checkPollenStyle(status: pollen.status!)
        cell.progressWidth.constant = cell.setProgressBar(status: pollen.status!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 90.5
    }
    
    
    //MARK: Action Methods
    
    @IBAction func closeButtonTapped(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeLocationButtonTapped(_ sender: Any)
    {
//        let autocompleteController = GMSAutocompleteViewController()
//        autocompleteController.delegate = self
//        autocompleteController.navigationController?.navigationBar.tintColor = Style.primaryColor
//        present(autocompleteController, animated: true, completion: nil)
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        let controller : SearchPlaceViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlaceViewController") as! SearchPlaceViewController
        controller.delegate = self
        self.isLocationChanged = true
        self.present(controller, animated: true, completion: nil)
        
    }
    
    
//    //MARK: GMSAutocompleteViewControllerDelegate
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace)
//    {
//        let location : CLLocation = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
//        self.getCity(location: location)
//        UserManager.sharedInstance.setIsChangedLocation(flag: true)
//        UserManager.sharedInstance.setLocationPlace(location: location)
//        self.dismiss(animated: true, completion: nil)
//    }
//    
//    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error)
//    {
//        self.dismiss(animated: true, completion: nil)
//    }
//
//    func wasCancelled(_ viewController: GMSAutocompleteViewController)
//    {
//        self.dismiss(animated: true, completion: nil)
//    }
    
    
    //MARK: CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        if (status == .authorizedWhenInUse || status == .authorizedAlways)
        {
            self.locationLabel.text = ""
            if let mainTitle = UserManager.sharedInstance.getMainLocationTitle()
            {
                self.locationLabel.text = mainTitle
            }
            locationManager.startUpdatingLocation()
        }
        else if(status == .denied)
        {
            if let location = UserManager.sharedInstance.location
            {
                self.setupChangedLocation()
            }
            else
            {
                self.setupDefaults()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        locationManager.stopUpdatingLocation()
        
        if let newLocation = UserManager.sharedInstance.location
        {

        }
        else
        {
            UserManager.sharedInstance.setLocationPlace(location: location)
        }
        
        
        self.setupChangedLocation()
        
    }
    
    
    //MARK: Setup & Support Methods
    
    func setupMainTitle()
    {
        self.mainTitleLabel.text = self.mainTitle
        let font : UIFont!
        if(UIScreen.main.bounds.size.width == 320.0)
        {
            font = UIFont(name: "AvenirNext-Bold", size: 20)
        }
        else
        {
            font = UIFont(name: "AvenirNext-Bold", size: 24)
        }
        self.mainTitleLabel.font = font
    }
    
    func checkForForecast()
    {
        if(self.mainTitle == k.text.pollenForecastTitle)
        {
            Localytics.tagEvent(trackingEvents.pollenForecastkey)
            self.getPollenForecast(location: UserManager.sharedInstance.location!)
        }
        else
        {
            Localytics.tagEvent(trackingEvents.dryEyeForecastKey)
            self.getDryEyeForecast(location: UserManager.sharedInstance.location!)
        }
        
    }
    
    func setupChangedLocation()
    {
        self.locationImageView.isHidden = false
        self.getCity(location: UserManager.sharedInstance.location!)
        self.checkForForecast()
    }
    
    func setupDefaults()
    {
        self.locationImageView.isHidden = true
        self.locationLabel.text = ""
    }
    
    func setupUrl(_ header : String, _ location : CLLocation) -> String
    {
        var urlString : String = k.services.website
        urlString.append("lat=")
        urlString.append(String(location.coordinate.latitude))
        urlString.append("&lon=")
        urlString.append(String(location.coordinate.longitude))
        urlString.append(header)
        urlString.append(k.services.userWeatherID)
        urlString.append("&k=")
        
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        
        let year =  (components.year! - 2000) * 170000
        let month = components.month! * 300
        let day = components.day! * 2
        let keyString = String(day + month + year) + k.services.passwordWeather
        let digestString = NSString(string: keyString).md5Digest()
        
        urlString.append(digestString)
        
        return urlString
    }
    
    func getDryEyeForecast(location : CLLocation)
    {
        let url = self.setupUrl(k.services.dryIndexForecastHeader, location)
        
        if(NetworkManager.sharedInstance.checkReachability())
        {
            Alamofire.request(url, headers: nil)
                .responseJSON { response in
                    
                    
                    if(response.error == nil)
                    {
                        let json = response.result.value
                        
                        let firstDay = ((json as AnyObject).value(forKeyPath: "countries.locations.local_forecasts.forecasts") as AnyObject)
                        
                        if((((firstDay ).object(at: 0) as AnyObject).object(at: 0) as AnyObject).description != k.text.null)
                        {
                            let numberOfItems : Int = (((firstDay ).object(at: 0) as AnyObject).object(at: 0) as! NSArray).count
                            
                            self.forecastDays.removeAll()
                            
                            for i in 0...numberOfItems - 1
                            {
                                let dryEyeIndex : ForecastIndex = ForecastIndex()
                                let day = (((firstDay.object(at: 0) as AnyObject).object(at: 0) as AnyObject).object(at: i) as AnyObject).value(forKey: "indices")
                                
                                
                                if let dryIndex = day
                                {
                                    dryEyeIndex.index = (((dryIndex as AnyObject).value(forKey: "value") as AnyObject).object(at: 0) as AnyObject).description
//                                    pollen.index = String(pollenIndex as! Int)
                                }
                                else
                                {
                                    break
                                }
                                
                                if let dryIndexText = day
                                {
                                    dryEyeIndex.status = ((dryIndexText as AnyObject).value(forKey: "text") as AnyObject).object(at: 0) as? String
                                }
                                
                                let dayName = (((firstDay.object(at: 0) as AnyObject).object(at: 0) as AnyObject).object(at: i) as AnyObject).value(forKey: "day_name")
                                
                                if let pollenDay = dayName
                                {
                                    dryEyeIndex.day = pollenDay as? String
                                }
                                
                                
                                self.forecastDays.append(dryEyeIndex)
                            }
                            
                            
//                            let day = (((((firstDay ).value(forKey: "indices") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as AnyObject).object(at: 0) as AnyObject).description
//                            if(day != "<null>")
//                            {
//                                let dryIndexText = ((((firstDay.value(forKeyPath: "indices.text") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as AnyObject).objectAt(0) ).object(at: 0) as! String
//                                let dryIndexValue : Float = ((((firstDay.value(forKeyPath: "indices.value") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as AnyObject).objectAt(0) ).object(at: 0) as! Float
//                                self.dryIndex = dryIndexValue
//                                self.dryIndexText = dryIndexText
//
//                                self.checkDryIndex()
//
//
//                                self.view.setDryIndex(self.dryIndex, self.dryIndexText)
//                                self.view.setProgressBarDryIndex(self.setupProgressBar(status: self.dryIndexText))
//
//                            }
//                            else
//                            {
//                                self.view.setProgressBarDryIndex(self.setupProgressBar(status: ""))
//                                self.view.setDryIndex(0, "")
//                                self.view.setDryIndexShown(false)
//                            }
                        }
//                        else
//                        {
//                            self.view.setProgressBarDryIndex(self.setupProgressBar(status: ""))
//                            self.view.setDryIndex(0, "")
//                            self.view.setDryIndexShown(false)
//                        }
                    }
                        
                    self.tableView.reloadData()
                    self.checkForEmptyData()
                        
                        
//                    else
//                    {
//                        self.setAlertView(k.alert.problemLoadingData)
//                    }
            }
        }
        else
        {
            self.setAlertView(k.alert.noInternetConnection)
        }
    }
    
    func getPollenForecast(location : CLLocation)
    {
        let url = self.setupUrl(k.services.pollenForecastHeader, location)
        
        if(NetworkManager.sharedInstance.checkReachability())
        {
            Alamofire.request(url, headers: nil)
                .responseJSON { response in
                    
                    
                    if(response.error == nil)
                    {
                        let json = response.result.value
                        let forecasts = (json as! NSDictionary).value(forKeyPath: "countries.locations.local_forecasts.forecasts") as AnyObject
                        
                        self.forecastDays.removeAll()

                        if(((forecasts.object(at: 0) as AnyObject).object(at: 0) as AnyObject).description != k.text.null)
                        {
                            let numberOfItems : Int = ((forecasts.object(at: 0) as AnyObject).object(at: 0) as! NSArray).count
                            for i in 0...numberOfItems - 1
                            {
                                let pollen : ForecastIndex = ForecastIndex()
                                let day = ((forecasts.object(at: 0) as AnyObject).object(at: 0) as AnyObject).object(at: i) as AnyObject
                                
                                
                                if let pollenIndex = day.value(forKey: k.pollen.pollenIndex)
                                {
                                    pollen.index = String(pollenIndex as! Int)
                                }
                                else
                                {
                                    break
                                }
                                
                                if let pollenText = day.value(forKey: "pollen_text")
                                {
                                    pollen.status = pollenText as? String
                                }
                                if let pollenDay  = day.value(forKey: "day_name")
                                {
                                    pollen.day = pollenDay as? String
                                }
                                
                                
                                self.forecastDays.append(pollen)
                            }
                        }
                        
                        self.tableView.reloadData()
                        self.checkForEmptyData()
                    }
                    else
                    {
                        self.setAlertView(k.alert.problemLoadingData)
                    }
            }
        }
        else
        {
            self.setAlertView(k.alert.noInternetConnection)
        }
    }
    
    func setAlertView(_ message : String)
    {
        let controller : UIAlertController = UIAlertController(title: "WARNING", message: message, preferredStyle: .alert)
        let cancelAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func getCity()
    {
        self.locationLabel.text = UserManager.sharedInstance.getMainLocationTitle()
    }
    
    func getCity(location : CLLocation)
    {
        let geocoder = CLGeocoder()
        
        
        geocoder.reverseGeocodeLocation(location,
                                        completionHandler: { (placemarks, error) in
                                            
                                            if(error == nil)
                                            {
                                                if let city = placemarks?.first?.locality {
                                                    self.locationLabel.text = city + ", "
                                                }
                                                else if let thoroughfare = placemarks?.first?.thoroughfare
                                                {
                                                    self.locationLabel.text = thoroughfare + ", "
                                                }
                                                else if let adressName = (placemarks?.first?.addressDictionary as AnyObject).object(forKey: "Name")
                                                {
                                                    self.locationLabel.text = (adressName as! String) + ", "
                                                }
                                                
                                                if let area = placemarks?.first?.administrativeArea
                                                {
                                                    self.locationLabel.text?.append(area)
                                                }
                                                else
                                                {
                                                    self.locationLabel.text?.append("AU")
                                                }
                                                
                                            }
                                                
                                            else
                                            {
                                                self.locationLabel.text = k.text.noLocationProvided
                                            }
                                            UserManager.sharedInstance.setLocationDescription(locationString: self.locationLabel.text!)
                                            UserDefaults.standard.setValue(self.locationLabel.text, forKey: k.services.mainLocationTitle)
                                            
                                            
        })
    }
    
    private func checkForEmptyData() {
        if forecastDays.count == 0 {
            emptyDataLabel.isHidden = false
        } else {
            emptyDataLabel.isHidden = true
        }
    }
    
}
