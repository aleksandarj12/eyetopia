//
//  PollenCustomView.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/17/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class CornerCustomView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        
    }

}
