//
//  HomePresenter.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/23/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps
import MD5Digest

class HomePresenter: NSObject {
    var view : HomeView!
    
    var dryIndex : Float = 0.0
    var dryIndexText : String = ""
    var pollenIndex : Float = 0.0
    var pollenIndexText : String = ""
    var isPollenForecastWarningHidden : Bool = false
    
    func attachView(view: HomeView)
    {
        self.view = view
    }
    
    func detachView() -> Void
    {
        self.view = nil
    }
    
    func getDryIndex()
    {
        
        let url = self.setupUrl(k.services.dryIndexForecastHeader, UserManager.sharedInstance.location!)
        if(NetworkManager.sharedInstance.checkReachability())
        {
            Alamofire.request(url, headers: nil)
                .responseJSON { response in
                    
                    if(response.error == nil)
                    {
                        let json = response.result.value
                        
                        let firstDay = ((json as AnyObject).value(forKeyPath: "countries.locations.local_forecasts.forecasts") as AnyObject)

                        if((((firstDay ).object(at: 0) as AnyObject).object(at: 0) as AnyObject).description != k.text.null)
                        {
                          let day = (((((firstDay ).value(forKey: "indices") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as AnyObject).object(at: 0) as AnyObject).description
                            if(day != "<null>")
                            {
                                let dryIndexText = ((((firstDay.value(forKeyPath: "indices.text") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as AnyObject).objectAt(0) ).object(at: 0) as! String
                                let dryIndexValue : Float = ((((firstDay.value(forKeyPath: "indices.value") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as AnyObject).objectAt(0) ).object(at: 0) as! Float
                                self.dryIndex = dryIndexValue
                                self.dryIndexText = dryIndexText
                                
                                self.checkDryIndex()
                                
                                
                                self.view.setDryIndex(self.dryIndex, self.dryIndexText)
                                self.view.setProgressBarDryIndex(self.setupProgressBar(status: self.dryIndexText))
                                
                            }
                            else
                            {
                                self.view.setProgressBarDryIndex(self.setupProgressBar(status: ""))
                                self.view.setDryIndex(0, "")
                                self.view.setDryIndexShown(false)
                            }
                        }
                        else
                        {
                            self.view.setProgressBarDryIndex(self.setupProgressBar(status: ""))
                            self.view.setDryIndex(0, "")
                            self.view.setDryIndexShown(false)
                        }
                    }
                    else
                    {
                        self.view.setAlertView(k.alert.problemLoadingData)
                    }
            }
        }
        else
        {
            self.view.setAlertView(k.alert.noInternetConnection)
        }
    }

    func getPollenForecast()
    {
        let url = self.setupUrl(k.services.pollenForecastHeader, UserManager.sharedInstance.location!)
        if(NetworkManager.sharedInstance.checkReachability())
        {
            Alamofire.request(url, headers: nil)
                .responseJSON { response in
                    
                    if(response.error == nil)
                    {
                        let json = response.result.value
                        
                        let forecasts = (json as! NSDictionary).value(forKeyPath: "countries.locations.local_forecasts.forecasts") as AnyObject
                        if(((forecasts.object(at: 0) as AnyObject).object(at: 0) as AnyObject).description != k.text.null)
                        {
                            let firstDay = ((forecasts.object(at: 0) as AnyObject).object(at: 0) as AnyObject).object(at: 0) as AnyObject
                            if let pollenIndex = firstDay.value(forKey: "pollen_index")
                            {
                                self.pollenIndex = pollenIndex as! Float
                                self.pollenIndexText  = firstDay.value(forKey: "pollen_text") as! String
                                self.view.setPollenForecast(pollenIndex as! Float, String(self.pollenIndexText))
                                self.view.setProgressBarPollenForecast(self.setupProgressBar(status: self.pollenIndexText))
                                
                                self.checkPollenIndex()
                                
                            }
                            else
                            {
                                self.setupDefaultPollenForecast()
                            }
                        }
                            
                        else
                        {
                            self.setupDefaultPollenForecast()
                        }
                    }
                    else
                    {
                        self.view.setAlertView(k.alert.problemLoadingData)
                    }
            }
        }
        else
        {
            self.view.setAlertView(k.alert.noInternetConnection)
        }
    }
    
    func getWeatherInfo()
    {
        let url = self.setupUrl(k.services.weatherInfoHeader, UserManager.sharedInstance.location!)
        if(NetworkManager.sharedInstance.checkReachability())
        {
            Alamofire.request(url, headers: nil)
                .responseJSON { response in
                    
                    if(response.error == nil)
                    {
                        let json = response.result.value
                        let forecasts = (json as! NSDictionary).value(forKeyPath: "countries.locations.local_forecasts.forecasts") as AnyObject
                        
                        if((((forecasts ).object(at: 0) as AnyObject).object(at: 0) as AnyObject).description != k.text.null)
                        {
                            let points : AnyObject = (((((forecasts.object(at: 0) as AnyObject).object(at: 0) as AnyObject).object(at: 0) as AnyObject).value(forKey: "point_forecasts") as AnyObject).object(at: 1) as AnyObject)
                            
                            let checkHumidity : String = (points.value(forKey: "relative_humidity") as AnyObject).description
                            
                            let humidity : Int?
                            
                            if(checkHumidity == k.text.null)
                            {
                                humidity = nil
                            }
                            else
                            {
                                humidity = points.value(forKey: "relative_humidity") as? Int
                            }
                            
                            let checkWind : String = (points.value(forKey: "wind_speed") as AnyObject).description
                            
                            let wind : Int?
                            
                            if(checkWind == k.text.null)
                            {
                                wind = nil
                            }
                            else
                            {
                                wind = points.value(forKey: "wind_speed") as? Int
                            }
                            
                            let checkTemp : String = ((((forecasts.object(at: 0) as AnyObject).object(at: 0) as AnyObject).object(at: 0) as AnyObject).value(forKey: "max") as AnyObject).description
                            
                            let temperature : Int?
                            
                            if(checkTemp == k.text.null)
                            {
                                temperature = nil
                            }
                            else
                            {
                                temperature = (((forecasts.object(at: 0) as AnyObject).object(at: 0) as AnyObject).object(at: 0) as AnyObject).value(forKey: "max") as? Int
                            }
                            
                            self.view.setWeatherInfo(temperature, humidity, wind)
                        }
                        else
                        {
                            self.view.setWeatherInfo(nil, nil, nil)
                        }
                    }
                    else
                    {
                        self.view.setAlertView(k.alert.problemLoadingData)
                    }
            }
        }
        else
        {
            self.view.setAlertView(k.alert.noInternetConnection)
        }
    }
    
    func checkDryIndex()
    {
        if(self.dryIndexText == "High" || self.dryIndexText == "Very High" || self.dryIndexText == "Extreme")
        {
            self.view.setDryIndexAlert(true)
        }
        else
        {
            self.view.setDryIndexAlert(false)
        }
    }
    
    func checkPollenIndex()
    {
        if(self.pollenIndexText == "High" || self.pollenIndexText == "Very High" || self.pollenIndexText == "Extreme")
        {
            self.view.setPollenWarningButtonShown(true)
            if(!self.isPollenForecastWarningHidden)
            {
                self.view.setPollenIndexAlert(true)
            }
            else
            {
                self.view.setPollenIndexAlert(false)
            }
            
        }
        else
        {
            self.view.setPollenIndexAlert(false)
        }
    }
    
    func getHomeSetup()
    {
        if(UserManager.sharedInstance.getDiagnosedJourneyCompletion())
        {
            self.view.setDiagnoseHolderView(flag: true)
            self.view.setContinueHolderView(flag: true)
            self.view.setMultipleOptionHolderView(flag: false)
            self.view.setNewJourneyHolderView(flag: true)
        }
        else if(UserManager.sharedInstance.getNonDiagnosedJourneyCompletion())
        {
            self.view.setDiagnoseHolderView(flag: false)
            self.view.setContinueHolderView(flag: true)
            self.view.setNewJourneyHolderView(flag: false)
            self.view.setMultipleOptionHolderView(flag: true)
        }
        else
        {
            self.view.setDiagnoseHolderView(flag: true)
            self.view.setContinueHolderView(flag: false)
            self.view.setNewJourneyHolderView(flag: false)
            self.view.setMultipleOptionHolderView(flag: true)
        }
    }
    
    func checkContinueJourneyLeftOff()
    {
        
    }
    
    func checkBlinExam()
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentBlinkExamChartKey), object: self)
    }
    
    func getBlinkExamDashboard()
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentLandingBlinkExamDashboardKey), object: self)
    }
    
    func getLandingProductMatch()
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentLandingProductMatchKey), object: self)
    }
    
    func getLandingSavingsCard()
    {
       NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentLandingSavingsCardKey), object: self)
    }
    
    func checkProductMatch()
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentLatestProductMatchKey), object: self)
//        let appDelegate = TransitionManager.sharedInstance.appDelegate
//        let controllers = appDelegate?.tabController.viewControllers
//        var flag : Bool = false
//        var index : Int = 0
//        for controller in controllers!
//        {
//            if (controller is LatestProductMatchViewController)
//            {
//                flag = false
//                break
//            }
//            else
//            {
//                flag = true
//            }
//            index = index + 1
//        }
//
//        if(flag)
//        {
//            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//
//
//            let controller : LatestProductMatchViewController = mainStoryboard.instantiateViewController(withIdentifier: "LatestProductMatchViewController") as! LatestProductMatchViewController
//            appDelegate?.tabController.addChildViewController(controller)
//
//        }
//        self.view.presentProductMatchViewController(index)
    }
    
    func checkSavingsCard()
    {
            
        
//        let appDelegate = TransitionManager.sharedInstance.appDelegate
//        let controllers = appDelegate?.tabController.viewControllers
//        var flag : Bool = false
//        var index : Int = 0
//        for controller in controllers!
//        {
//            if (controller is CouponViewController)
//            {
//                flag = false
//                break
//            }
//            else
//            {
//                flag = true
//            }
//            index = index + 1
//        }
//        if(flag)
//        {
//            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            
//
//            let controller : CouponViewController = mainStoryboard.instantiateViewController(withIdentifier: "CouponVC") as! CouponViewController
//            appDelegate?.tabController.addChildViewController(controller)
//        }
//        
//        
//        self.view.presentSavingsCardViewController(index)
        
    }
    
    func setupUrl(_ header : String, _ location : CLLocation) -> String
    {
        var urlString : String = k.services.website
        urlString.append("lat=")
        urlString.append(String(location.coordinate.latitude))
        urlString.append("&lon=")
        urlString.append(String(location.coordinate.longitude))
        urlString.append(header)
        urlString.append(k.services.userWeatherID)
        urlString.append("&k=")
        
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        
        let year =  (components.year! - 2000) * 170000
        let month = components.month! * 300
        let day = components.day! * 2
        let keyString = String(day + month + year) + k.services.passwordWeather
        let digestString = NSString(string: keyString).md5Digest()
        
        urlString.append(digestString)
        
        return urlString
    }
    
    func setupProgressBar(status : String) -> ProgressBar
    {
        let progressBar : ProgressBar = ProgressBar()
        
        if(status == k.pollen.low)
        {
            progressBar.color = Style.pollenColor.low
            progressBar.multiply = 1
        }
        else if(status == k.pollen.moderate)
        {
            progressBar.color = Style.pollenColor.moderate
            progressBar.multiply = 2
        }
        else if(status == k.pollen.high)
        {
            progressBar.color = Style.pollenColor.high
            progressBar.multiply = 3
        }
        else if(status == k.pollen.veryHigh)
        {
            progressBar.color = Style.pollenColor.veryHigh
            progressBar.multiply = 4
        }
        else if(status == k.pollen.extreme)
        {
            progressBar.color = Style.pollenColor.extreme
            progressBar.multiply = 5
        }
        else
        {
            progressBar.multiply = 0
        }
        
        return progressBar

    }
    
    func setupDefaultPollenForecast()
    {
        self.view.setProgressBarPollenForecast(self.setupProgressBar(status: ""))
        self.view.setPollenForecast(0, "")
        self.view.setPollenForecastShown(false)
    }
    
}

class ProgressBar
{
    var color : UIColor = UIColor.clear
    var multiply : Int = 0
}
