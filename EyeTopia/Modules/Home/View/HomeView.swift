//
//  HomeView.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/23/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

protocol HomeView {
    func setContinueHolderView(flag : Bool)
    func setDiagnoseHolderView(flag : Bool)
    func setNewJourneyHolderView(flag : Bool)
    func setMultipleOptionHolderView(flag : Bool)
    func presentSavingsCardViewController(_ index : Int)
    func presentBlinkExamResultsViewController(_ index : Int)
    func presentProductMatchViewController(_ index : Int)
    func presentLatestProductMatchViewController(index : Int)
    func setPollenForecast(_ index : Float, _ text : String)
    func setDryIndex(_ index : Float, _ text : String)
    func setWeatherInfo(_ temperature : Int?, _ humidity : Int?, _ wind : Int?)
    func setProgressBarPollenForecast(_ progressBar : ProgressBar)
    func setProgressBarDryIndex(_ progressBar : ProgressBar)
    func setDryIndexAlert(_ flag : Bool)
    func setPollenIndexAlert(_ flag : Bool)
    func setAlertView(_ message : String)
    func setDryIndexShown(_ flag : Bool)
    func setPollenForecastShown(_ flag : Bool)
    func setPollenWarningButtonShown(_ flag : Bool)


}
