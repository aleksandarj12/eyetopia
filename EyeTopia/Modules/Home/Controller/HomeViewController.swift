//
//  HomeViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/16/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import GooglePlaces
import Localytics

protocol HomeViewDelegate {
    func presentJourneyAgain()
    func presentDiagnosedWithDryEyeJourney()
}

class HomeViewController: UIViewController, HomeView, CLLocationManagerDelegate, WelcomeBackDelegate {
    
    @IBOutlet weak var pollenHolderFullView: CornerCustomView!
    
    @IBOutlet weak var viewDryEyeForecastButton: UIButton!
    @IBOutlet weak var viewPollenForecastButton: UIButton!
    @IBOutlet weak var pollenProgressHolderView: CornerCustomView!
    
    @IBOutlet weak var dryIndexFullHolderView: CornerCustomView!
    @IBOutlet weak var dryIndexProgressHolderView: CornerCustomView!
    @IBOutlet weak var pollenForecastWarningHolderView: UIView!
    
    @IBOutlet weak var noDataDryIndexLabel: UILabel!
    @IBOutlet weak var noPollenForecastLabel: UILabel!
    
    @IBOutlet weak var dryIndexWarningButton: UIButton!
    @IBOutlet weak var pollenForecastWarningButton: UIButton!
    @IBOutlet weak var diagnoseHolderView: UIView!
    @IBOutlet weak var continueHolderView: UIView!
    @IBOutlet weak var newJourneyHolderView: UIView!
    @IBOutlet weak var diagnoseButton: UIButton!
    @IBOutlet weak var multipleOptionHolderView: UIView!
    @IBOutlet weak var locationHolderView: UIView!
    
    @IBOutlet weak var mainHolderView: OptionalView!
    @IBOutlet weak var locationImageView: UIImageView!
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var changeLocationButton: UIButton!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    
    var presenter : HomePresenter = HomePresenter()
    @IBOutlet weak var dryIndexLabel: UILabel!
    @IBOutlet weak var pollenForecastLabel: UILabel!
    
    @IBOutlet weak var dryIndexWarningHolderView: UIView!
    @IBOutlet weak var eyeIndexProgressWidthLayoutConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var pollenProgressWidthLayoutConstraint: NSLayoutConstraint!
    
    private let locationManager = CLLocationManager()
    
    var location : CLLocation?
    
    var isClosedWarningsForecast : Bool = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var delegate : HomeViewDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserManager.sharedInstance.mainLocationTitleOption = .mainLocation
        self.presenter.attachView(view: self)
        self.setupDefaultInfo()
        self.locationManager.delegate = self
        self.setupWeatherInfoLabels()
        if(UserManager.sharedInstance.getDiagnosedJourneyCompletion() || UserManager.sharedInstance.getNonDiagnosedJourneyCompletion())
        {
            
        }
        UserManager.sharedInstance.getLocationPlace()
        if let location = UserManager.sharedInstance.location
        {
            self.changeLocationButton.setTitle("Change location", for: .normal)
            self.viewPollenForecastButton.isHidden = false
            self.viewDryEyeForecastButton.isHidden = false
            self.setupNewChanges()
        }
        else
        {
            if(UserManager.sharedInstance.isCurrentLocationActive)
            {
                locationManager.startUpdatingLocation()
            }
            else
            {
                self.setupDefaultInfo()
            }
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if(TransitionManager.sharedInstance.storyboardID == "PollenForecastViewController")
        {
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
            let controller : PollenForecastViewController = self.storyboard!.instantiateViewController(withIdentifier: "PollenForecastViewController") as! PollenForecastViewController
            controller.mainTitle = k.text.pollenForecastTitle
            self.present(controller, animated: false, completion: nil)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UserManager.sharedInstance.mainLocationTitleOption = .mainLocation
        self.presenter.getHomeSetup()

        if(UserManager.sharedInstance.isChangedLocation)
        {
            UserManager.sharedInstance.setIsChangedLocation(flag: false)
            self.setupNewChanges()
        }
        else if(TransitionManager.sharedInstance.storyboardID == "LocationViewController")
        {
            self.isClosedWarningsForecast = false
            self.presenter.isPollenForecastWarningHidden = false
            if(UserManager.sharedInstance.isCurrentLocationActive)
            {
                UserManager.sharedInstance.setLocationPlace(location: nil)
                self.locationManager.requestWhenInUseAuthorization()
                self.locationManager.startUpdatingLocation()
            }
            else
            {
                self.setupNewChanges()
            }
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
        }
        else if(UserManager.sharedInstance.location == nil)
        {
            self.setupDefaultInfo()
        }
        else
        {
            self.setupNewChanges()
        }
    }
    
    
//    //MARK: SearchPlaceDelegate
//
//    func didFindPlace(_ location: LocationDetail)
//    {
//
//    }
//
//    func didSelectCurrentLocation()
//    {
//    
//    }
    
    
    //MARK: WelcomeBack Delegate
    
    func presentDiagnosedWithDryEyeJourney()
    {
        self.delegate.presentDiagnosedWithDryEyeJourney()
    }
    
    
    //MARK: HomeView
    
    func setPollenForecastShown(_ flag: Bool)
    {
        self.pollenForecastWarningButton.isHidden = !flag
        
        if(self.isClosedWarningsForecast)
        {
            self.pollenForecastWarningHolderView.isHidden = true
        }
        else
        {
            self.pollenForecastWarningHolderView.isHidden = !flag
        }
    }
    
    func setDryIndexShown(_ flag: Bool)
    {
        self.dryIndexWarningButton.isHidden = !flag

        if(self.isClosedWarningsForecast)
        {
            self.dryIndexWarningHolderView.isHidden = true
        }
        else
        {
            self.dryIndexWarningHolderView.isHidden = !flag
        }
    }
    
    func setDryIndex(_ index: Float, _ text: String)
    {
        if(index == 0)
        {
            self.dryIndexLabel.text = ""
            self.noDataDryIndexLabel.isHidden = false
        }
        else
        {
            self.noDataDryIndexLabel.isHidden = true
            self.dryIndexLabel.text = String(format: index == floor(index) ? "%.0f" : "%.1f", index) + ", " + text
        }
    }
    
    func setProgressBarDryIndex(_ progressBar : ProgressBar)
    {
        self.dryIndexProgressHolderView.backgroundColor = progressBar.color
        self.dryIndexLabel.textColor = progressBar.color
        self.eyeIndexProgressWidthLayoutConstraint.constant = (self.pollenHolderFullView.frame.width / 5) * CGFloat(progressBar.multiply)
    }
    
    func presentBlinkExamResultsViewController(_ index : Int)
    {
        let appDelegate = TransitionManager.sharedInstance.appDelegate
        appDelegate?.tabController.selectedIndex = index
    }
    
    func setAlertView(_ message : String)
    {
        let controller : UIAlertController = UIAlertController(title: "WARNING", message: k.alert.noInternetConnection, preferredStyle: .alert)
        let cancelAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func setPollenWarningButtonShown(_ flag : Bool)
    {
        self.pollenForecastWarningButton.isHidden = !flag
    }
    
    func setPollenIndexAlert(_ flag : Bool)
    {
        self.pollenForecastWarningHolderView.isHidden = !flag
    }
    
    func setDryIndexAlert(_ flag : Bool)
    {
        self.dryIndexWarningButton.isHidden = !flag

        if(self.isClosedWarningsForecast)
        {
            self.dryIndexWarningHolderView.isHidden = true
        }
        else
        {
            self.dryIndexWarningHolderView.isHidden = !flag
        }
        
    }
    
    func setProgressBarPollenForecast(_ progressBar : ProgressBar)
    {
        self.pollenProgressHolderView.backgroundColor = progressBar.color
        self.pollenForecastLabel.textColor = progressBar.color
        self.pollenProgressWidthLayoutConstraint.constant = (self.pollenHolderFullView.frame.width / 5) * CGFloat(progressBar.multiply)
    }
    
    func setWeatherInfo(_ temperature : Int?, _ humidity : Int?, _ wind : Int?)
    {
        if let temp = temperature
        {
            self.temperatureLabel.text = String(temp) + "°C"
            self.temperatureLabel.textColor = .white
        }
        else
        {
            self.temperatureLabel.text = "0°C"
            self.temperatureLabel.textColor = Style.noLocationColor
        }
        
        if let h = humidity {
            self.humidityLabel.text = "Humidity: " + String(h) + "%"
            self.humidityLabel.textColor = .white
        }
        else
        {
            self.humidityLabel.text = "Humidity: 0%"
            self.humidityLabel.textColor = Style.noLocationColor
        }
        
        if let w = wind {
            self.windLabel.text = "Wind: " + String(w) + " km/h"
            self.windLabel.textColor = .white
        }
        else
        {
            self.windLabel.text = "Wind: 0 km/h"
            self.windLabel.textColor = Style.noLocationColor
        }

    }
    
    func setPollenForecast(_ index : Float, _ text : String)
    {
        if(index == 0)
        {
            self.pollenForecastLabel.text = ""
            self.noPollenForecastLabel.isHidden = false
            self.pollenForecastWarningButton.isHidden = true
        }
        else
        {
            self.pollenForecastWarningButton.isHidden = index >= 3 ? false : true
//            index > 3 ? self.pollenForecastWarningButton.isHidden = false : self.pollenForecastWarningButton.isHidden = true
            self.noPollenForecastLabel.isHidden = true
            self.pollenForecastLabel.text = String(format: index == floor(index) ? "%.0f" : "%.1f", index) + ", " + text
        }
        
    }
    
    func presentLatestProductMatchViewController(index : Int)
    {
        let appDelegate = TransitionManager.sharedInstance.appDelegate
        appDelegate?.tabController.selectedIndex = index
    }
    
    func presentSavingsCardViewController(_ index : Int)
    {
        let appDelegate = TransitionManager.sharedInstance.appDelegate
        appDelegate?.tabController.selectedIndex = index
    }
    
    func presentProductMatchViewController(_ index: Int)
    {
        let appDelegate = TransitionManager.sharedInstance.appDelegate
        appDelegate?.tabController.selectedIndex = index
    }
    
    func setContinueHolderView(flag : Bool)
    {
       self.continueHolderView.isHidden = flag
    }   
    
    func setDiagnoseHolderView(flag : Bool)
    {
        self.diagnoseHolderView.isHidden = flag
    }
    
    func setNewJourneyHolderView(flag : Bool)
    {
        self.newJourneyHolderView.isHidden = flag
    }
    
    func setMultipleOptionHolderView(flag : Bool)
    {
        self.multipleOptionHolderView.isHidden = flag
    }
    
    
    //MARK: CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        if (status == .authorizedWhenInUse || status == .authorizedAlways)
        {
            self.setupNoLocationProvided()
            if(self.changeLocationButton.titleLabel?.text != "Set location")
            {
                locationManager.startUpdatingLocation()
            }
        }
        else if(status == .denied)
        {
            if UserManager.sharedInstance.location != nil
            {
                self.setupNewChanges()
                self.changeLocationButton.setTitle("Change location", for: .normal)
                self.viewPollenForecastButton.isHidden = false
                self.viewDryEyeForecastButton.isHidden = false
            }
            else
            {
                self.setupDefaultInfo()
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        locationManager.stopUpdatingLocation()

        if let newLocation = UserManager.sharedInstance.location
        {
            self.changeLocationButton.setTitle("Change location", for: .normal)
            self.viewPollenForecastButton.isHidden = false
            self.viewDryEyeForecastButton.isHidden = false
            self.getCity()
        }
        else
        {
            self.location = location
            UserManager.sharedInstance.setLocationPlace(location: location)
            self.getCity()
        }
        if(UserManager.sharedInstance.isCurrentLocationActive)
        {
            self.getCity(location: location)
            UserManager.sharedInstance.setIsCurrentLocationActive(false)
        }
        self.setupChangedLocation()

        
    }

    
    //MARK: Action Methodsop
    
    @IBAction func startNewJourneyStationButtonTapped(_ sender: Any)
    {
//        if let station = UserManager.sharedInstance.getJourneyStation() {
            let controller : StartANewJourneyViewController = self.storyboard?.instantiateViewController(withIdentifier: "StartANewJourneyViewController") as! StartANewJourneyViewController
            self.present(controller, animated: true, completion: nil)
//        }
//        else
//        {
//            TransitionManager.sharedInstance.storyboardID = ""
//            NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentStartNewJourneyKey), object: self)
//        }
    }
    
    @IBAction func continueJourneyButtonTapped(_ sender: Any)
    {
        UserManager.sharedInstance.setNonDiagnosedJourneyCompetion(flag: false)
        UserManager.sharedInstance.setDiagnosedJourneyCompetion(flag: false)
        NotificationManager.sharedInstance.scheduleJourneyContinuationReminderNotification()
        UserManager.sharedInstance.removeProductMatchData()
        TransitionManager.sharedInstance.storyboardID = ""
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentContinueLeftJourneyKey), object: self)
    }
    
    @IBAction func pollenForecastWarningButtonTapped(_ sender: Any)
    {
        self.isClosedWarningsForecast = false
        self.pollenForecastWarningHolderView.isHidden = false
        self.dryIndexWarningHolderView.isHidden = true
    }
    
    @IBAction func dryEyeIndexWarningButtonTapped(_ sender: Any)
    {
        self.isClosedWarningsForecast = false
        self.dryIndexWarningHolderView.isHidden = false
        self.pollenForecastWarningHolderView.isHidden = true

    }
    
    @IBAction func changeLocationButtonTapped(_ sender: Any)
    {
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        let controller : SearchPlaceViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlaceViewController") as! SearchPlaceViewController
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func diagnoseButtonTapped(_ sender: Any)
    {
        let controller : WelcomeBackToEyeTopiaViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeBackToEyeTopiaViewController") as! WelcomeBackToEyeTopiaViewController
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func blinkExamButtonTapped(_ sender: Any)
    {
        self.presenter.getBlinkExamDashboard()
    }
    
    @IBAction func productMatchButtonTapped(_ sender: Any)
    {
        self.presenter.getLandingProductMatch()
    }
    
    @IBAction func savingsCardButtonTapped(_ sender: Any)
    {
        self.presenter.getLandingSavingsCard()
    }
    
    @IBAction func startNewJourneyButtonTapped(_ sender: Any)
    {
        UserManager.sharedInstance.setSelectedEyeSymptom(eyeProblem: nil)
        UserManager.sharedInstance.removeProductMatchData()
        UserManager.sharedInstance.setNonDiagnosedJourneyCompetion(flag: false)
        UserManager.sharedInstance.setDiagnosedJourneyCompetion(flag: false)
        NotificationManager.sharedInstance.scheduleJourneyContinuationReminderNotification()
        Localytics.tagEvent(trackingEvents.journeyAgainKey)
        self.delegate.presentJourneyAgain()
        
//        let controller : GuideMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
//        let navController : UINavigationController = UINavigationController(rootViewController: controller)
//        navController.setNavigationBarHidden(true, animated: false)
//        self.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func historyDryEyeForecastButtonTapped(_ sender: Any)
    {
        let controller : PollenForecastViewController = self.storyboard?.instantiateViewController(withIdentifier: "PollenForecastViewController") as! PollenForecastViewController
        controller.mainTitle = k.text.dryEyeForecastTitle
        self.present(controller, animated: true, completion: nil)

    }
    
    @IBAction func historyForecastButtonTapped(_ sender: Any)
    {
        let controller : PollenForecastViewController = self.storyboard?.instantiateViewController(withIdentifier: "PollenForecastViewController") as! PollenForecastViewController
        controller.mainTitle = k.text.pollenForecastTitle
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func closeWarningViewButtonTapped(_ sender: Any)
    {
        self.presenter.isPollenForecastWarningHidden = true
        self.isClosedWarningsForecast = true
        self.dryIndexWarningHolderView.isHidden = true
        self.pollenForecastWarningHolderView.isHidden = true
    }
    
   
    
    
    
//        let location : CLLocation = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
//        self.location = location
//        UserManager.sharedInstance.setLocationPlace(location: location)
//        self.setupChangedLocation()
    
        
  
    
    
    //MARK: Setup & Support Methods
    
    func setupWeatherInfoLabels()
    {
        let font : UIFont!
        if(UIScreen.main.bounds.size.width == 320.0)
        {
            font = UIFont(name: "AvenirNext-Bold", size: 12.0)
        }
        else
        {
            font = UIFont(name: "AvenirNext-Bold", size: 14.0)
        }
        self.temperatureLabel.font = font
        self.humidityLabel.font = font
        self.windLabel.font = font
    }
    
    func setupNoLocationProvided()
    {
        if let locationString = UserManager.sharedInstance.getMainLocationTitle() {
            self.locationLabel.text = locationString
            self.changeLocationButton.setTitle("Change location", for: .normal)

        }
        else
        {
            self.locationLabel.text = k.text.noLocationProvided
            self.changeLocationButton.setTitle("Set location", for: .normal)
        }
        
        self.temperatureLabel.text = "0°C"
        self.humidityLabel.text = "Humidity: 0%"
        self.windLabel.text = "Wind: 0 km/h"
        self.temperatureLabel.textColor = Style.noLocationColor
        self.humidityLabel.textColor = Style.noLocationColor
        self.windLabel.textColor = Style.noLocationColor
        self.viewPollenForecastButton.isHidden = true
        self.viewDryEyeForecastButton.isHidden = true
        self.noDataDryIndexLabel.isHidden = true
        self.noPollenForecastLabel.isHidden = true
    }
    
    func setupNewChanges()
    {
//        let location : CLLocation = UserManager.sharedInstance.location!
        if let location = UserManager.sharedInstance.location {
            self.location = location
        }
//        self.location = location
        self.setupChangedLocation()
    }
    
    func setupDefaultInfo()
    {
        self.setupNoLocationProvided()
        self.dryIndexLabel.text = ""
        self.pollenForecastLabel.text = ""
        self.locationImageView.alpha = 0
        self.dryIndexWarningButton.isHidden = true
        self.pollenForecastWarningButton.isHidden = true
        self.pollenProgressWidthLayoutConstraint.constant = 0.0
        self.eyeIndexProgressWidthLayoutConstraint.constant = 0.0
        self.dryIndexWarningHolderView.layer.cornerRadius = 5
        self.dryIndexWarningHolderView.clipsToBounds = true
        self.dryIndexWarningHolderView.isHidden = true
        
    }
    
    func setupChangedLocation()
    {
        self.locationImageView.alpha = 1
        self.getCity()
        self.presenter.getPollenForecast()
        self.presenter.getWeatherInfo()
        self.presenter.getDryIndex()
    }
   

    
    func getCity(location : CLLocation)
    {
        let geocoder = CLGeocoder()
        
        
        geocoder.reverseGeocodeLocation(location,
                                        completionHandler: { (placemarks, error) in
                                            
                                            if(error == nil)
                                            {
                                                if let city = placemarks?.first?.locality {
                                                    self.locationLabel.text = city + ", "
                                                }
                                                else if let thoroughfare = placemarks?.first?.thoroughfare
                                                {
                                                    self.locationLabel.text = thoroughfare + ", "
                                                }
                                                else if let adressName = (placemarks?.first?.addressDictionary as AnyObject).object(forKey: "Name")
                                                {
                                                    self.locationLabel.text = (adressName as! String) + ", "
                                                }
                                                
                                                if let area = placemarks?.first?.administrativeArea
                                                {
                                                    self.locationLabel.text?.append(area)
                                                }
                                                else
                                                {
                                                    self.locationLabel.text?.append("AU")
                                                }
                                                self.viewPollenForecastButton.isHidden = false
                                                self.viewDryEyeForecastButton.isHidden = false
                                                self.changeLocationButton.setTitle("Change location", for: .normal)
                                            }
                                                
                                            else
                                            {
                                                self.locationLabel.text = k.text.noLocationProvided
                                            }
                                            
                                            UserDefaults.standard.setValue(self.locationLabel.text, forKey: k.services.mainLocationTitle)
                                            UserDefaults.standard.synchronize()
        })
    }
    
    
    func getCity()
    {
        if let locationString = UserManager.sharedInstance.getMainLocationTitle()
        {
            self.locationLabel.text = locationString
            self.viewPollenForecastButton.isHidden = false
            self.viewDryEyeForecastButton.isHidden = false
            self.changeLocationButton.setTitle("Change location", for: .normal)
        }
        else
        {
            self.locationLabel.text = k.text.noLocationProvided
        }
        
        UserManager.sharedInstance.setLocationDescription(locationString: self.locationLabel.text!)
    }
    
}
