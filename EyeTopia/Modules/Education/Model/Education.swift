//
//  Education.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/20/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class Education: NSObject {
    var tableImageView : UIImage?
    var mainImageView : UIImage?
    var mainColor : UIColor?
    var title : String?
    var descriptionString : String?
    var educationInfo : [EducationInfo]?
    var referLinks : [String]?
    var referedEducation : [Education]?
    
}

class EducationInfo: NSObject {
    var image : UIImage?
    var details : String?
    var link : LinkInfo?
    var font : UIFont = UIFont(name: "AvenirNext-Medium", size: 14.0)!
}


class LinkInfo : NSObject {
    var name : String?
    var link : String?
}
