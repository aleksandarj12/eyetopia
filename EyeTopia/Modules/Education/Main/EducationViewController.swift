//
//  EducationViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/20/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

class EducationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: "EducationTableViewCell", bundle: nil), forCellReuseIdentifier: "EducationTableViewCell")
        self.tableView.backgroundColor = UIColor(red: 249/255.0, green: 249/255.0, blue: 249/255.0, alpha: 1)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Localytics.tagEvent(trackingEvents.dryEyeInformationKey)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableView DataSource & Delegate
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return DataManager.sharedInstance.getEducationArray().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : EducationTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "EducationTableViewCell") as! EducationTableViewCell
        
        let education : Education = DataManager.sharedInstance.getEducationArray()[indexPath.row]
        
        if let tableImageView = education.tableImageView {
            cell.tableImageView.image = tableImageView
        }
        
        if let titleLabel = education.title {
            cell.titleLabel.text = titleLabel
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "EducationDetail") as? EducationDetailsViewController {
            let object = DataManager.sharedInstance.getEducationArray()[indexPath.row]
            destinationVC.educationObject = object
            Localytics.tagEvent(trackingEvents.dryEyeInformationKey, attributes: ["Information" : object.title!])
            
            self.navigationController?.pushViewController(destinationVC, animated: true)
        }
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let identifier = segue.identifier {
//            switch identifier {
//            case "showEducationDetail":
//                if let destinationVC = segue.destination as? EducationDetailsViewController, let indexPath = tableView.indexPathForSelectedRow {
//                    destinationVC.educationObject = DataManager.sharedInstance.getEducationArray()[indexPath.row]
//                }
//            default: break
//            }
//        }
//    }

}
