//
//  EducationDetailsViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 3/21/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit


class EducationDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

  
    @IBOutlet weak var heightHeaderHolderView: NSLayoutConstraint!
    @IBOutlet weak var educationDetailsTableView: UITableView!
    @IBOutlet weak var myHeaderView: UIView!
    @IBOutlet weak var imageHeader: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    
    var educationObject = Education()
    var imageWidth : CGFloat = 0.0
    var imageHeight : CGFloat = 0.0
    var imageAspect : CGFloat = 0.0
   
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.layer.cornerRadius = 4
        educationDetailsTableView.estimatedRowHeight = 40
        educationDetailsTableView.rowHeight = UITableViewAutomaticDimension
        educationDetailsTableView.bounces = false
        
        educationDetailsTableView.separatorStyle = .none
        
        if let headerColor = educationObject.mainColor {
            myHeaderView.backgroundColor = headerColor
            self.view.backgroundColor = headerColor
            backButton.tintColor = headerColor
        }
        
        if let headerImage = educationObject.mainImageView {
            imageHeader.image = headerImage
       
            
//            self.imageWidth = headerImage.size.width
//            self.imageHeight = headerImage.size.height
//            self.imageAspect = self.imageWidth / self.imageHeight
//            imageHeader.contentMode = .center
        }
        
    }
    
    
    //MARK: UITableView DataSource & Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            if let edInfo = educationObject.educationInfo
            {
                return edInfo.count
            }
            
        }
        else if section == 2
        {
            if let links = educationObject.referLinks
            {
                return 1
//                return links.count
            }
            
        }
        else if section == 3
        {
            if let refArticle = educationObject.referedEducation
            {
                return refArticle.count
            }
        }
        
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let offSet = 200 - scrollView.contentOffset.y
        let h = max(60, offSet)
        heightHeaderHolderView.constant = h
        
//        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0)
//            {
//                //UP
//                if(heightHeaderHolderView.constant < 200)
//                {
//                    heightHeaderHolderView.constant += 2
//                    let height = (imageHeader.image?.size.height)! + 2
//                    let width = height * imageAspect
//                    if(height < 140)
//                    {
//                        imageHeader.image = self.imageWIthImage(image: educationObject.mainImageView!, newSize: CGSize(width: width, height: height))
//                    }
//                }
//            }
//            else
//            {
//                //DOWN
//                if(heightHeaderHolderView.constant > 80)
//                {
//                    heightHeaderHolderView.constant -= 2
//                    let height = (imageHeader.image?.size.height)! - 2
//                    let width = height * imageAspect
//                    if(height > 60)
//                    {
//                        imageHeader.image = self.imageWIthImage(image: educationObject.mainImageView!, newSize: CGSize(width: width, height: height))
//                    }
//                }
//
//            }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dequeuedCell0 = tableView.dequeueReusableCell(withIdentifier: "titleAndSubtitle", for: indexPath)
        let dequeuedCell1 = tableView.dequeueReusableCell(withIdentifier: "imageAndDetails", for: indexPath)
        let dequeuedCell2 = tableView.dequeueReusableCell(withIdentifier: "links", for: indexPath)
        let dequeuedCell3 = tableView.dequeueReusableCell(withIdentifier: "otherArticles", for: indexPath)

        
        if indexPath.section == 0 {
            
            if let cell0 = dequeuedCell0 as? TitleAndSubtitleCell {
                if let edTitle = educationObject.title, let edDesc = educationObject.descriptionString {
                    cell0.title.text = edTitle
                    cell0.setupSubtitle(edDesc)
                    cell0.selectionStyle = UITableViewCellSelectionStyle.none
                }
            }
            return dequeuedCell0
            
        } else if indexPath.section == 1 {
            
            if let cell1 = dequeuedCell1 as? ImageAndDetailsCell {
                if let edInf = educationObject.educationInfo, let edImage = edInf[indexPath.row].image {
                    cell1.myImage.image = edImage

                    cell1.selectionStyle = UITableViewCellSelectionStyle.none
                }
                
                cell1.myDetail.font = self.educationObject.educationInfo![indexPath.row].font
                cell1.setupSubtitle(self.educationObject.educationInfo![indexPath.row].details!)
                
                //First if remain only if they change thier mind for button view exercesis
                if let link = self.educationObject.educationInfo![indexPath.row].link
                {
                    cell1.linkButton.isHidden = true
                    cell1.linkButtonHeight.constant = 0
                    cell1.link = link.link!
                }
                else
                {
                    cell1.linkButton.isHidden = true
                    cell1.linkButtonHeight.constant = 0
                }
            }
        return dequeuedCell1
        } else if indexPath.section == 2 {
            
            if let cell2 = dequeuedCell2 as? LinksCell {
                if let edLinks = educationObject.referLinks {
                    
                    
                    var newString : String = ""
                    for link in edLinks
                    {
                        newString.append(link)
                        newString.append("\n")
                    }
                    
                    cell2.myLink.text = newString
                    cell2.myLink.lineBreakMode = .byCharWrapping
                    cell2.selectionStyle = UITableViewCellSelectionStyle.none

                }
            }
            return dequeuedCell2

        } else if indexPath.section == 3 {
            
            if let cell3 = dequeuedCell3 as? OtherArticlesCell {
                if let edRefArtArray = educationObject.referedEducation, let tableImage = edRefArtArray[indexPath.row].tableImageView, let titleRef = edRefArtArray[indexPath.row].title {
                    cell3.myTitle.text = titleRef
                    cell3.myImage.image = tableImage
                    cell3.selectionStyle = UITableViewCellSelectionStyle.none

                }
            }
            
            return dequeuedCell3
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2 {
            let sec2HeaderView = UIView()
            sec2HeaderView.backgroundColor = UIColor(red: 249/255.0, green: 249/255.0, blue: 249/255.0, alpha: 1)
            return sec2HeaderView
        }
        
        if section == 3 {
            let sec3HeaderView = UIView()
            sec3HeaderView.backgroundColor = UIColor(red: 249/255.0, green: 249/255.0, blue: 249/255.0, alpha: 1)
            let headerLabel = UILabel()
            headerLabel.text = "Other articles you may be interested in"
            headerLabel.textAlignment = .center
            headerLabel.font = UIFont(name: "AvenirNext-Medium", size: 14)
            headerLabel.textColor = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1)
            headerLabel.sizeToFit()
            let centerForLabel = CGPoint(x: self.view.bounds.size.width / 2, y: 20)
            headerLabel.center = centerForLabel
            sec3HeaderView.addSubview(headerLabel)
            return sec3HeaderView
        }
        return nil
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            return 40
        } else if section == 3 {
            return 40
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 3 {
            if let nextEducation = educationObject.referedEducation?[indexPath.row] {
                educationObject = nextEducation
                if let nextColor = nextEducation.mainColor, let nextHeaderImage = nextEducation.mainImageView {
                    myHeaderView.backgroundColor = nextColor
                    self.view.backgroundColor = nextColor
                    backButton.tintColor = nextColor
                    imageHeader.image = nextHeaderImage
                }
                educationDetailsTableView.reloadData()
                let newOffSet = IndexPath(item: 0, section: 0)
                educationDetailsTableView.scrollToRow(at: newOffSet, at: .top, animated: false)
            }
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    func imageWIthImage(image : UIImage, newSize : CGSize) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }

}
