//
//  OtherArticlesCell.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 3/21/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class OtherArticlesCell: UITableViewCell {

    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var myTitle: UILabel!
    @IBOutlet weak var holderView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        holderView.layer.borderWidth = 1
        let myCGColor = UIColor(red: 206/255.0, green: 206/255.0, blue: 206/255.0, alpha: 1).cgColor
        holderView.layer.borderColor = myCGColor
        
        holderView.layer.cornerRadius = 4
        holderView.clipsToBounds = true
            
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
