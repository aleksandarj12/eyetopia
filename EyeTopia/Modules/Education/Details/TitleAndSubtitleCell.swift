//
//  TitleAndSubtitleCell.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 3/21/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class TitleAndSubtitleCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupSubtitle(_ text : String)
    {

        let fontSuper : UIFont? = UIFont(name: "AvenirNext-Medium", size:9)
        let font : UIFont = UIFont(name: "AvenirNext-Medium", size: 14)!
        
        var ranges : [NSRange] = [NSRange]()
        
        var string : NSString = text as NSString
        
        while (string.contains(k.text.fontSuper))
        {
            let range : NSRange = string.range(of: k.text.fontSuper)
            string = string.replacingCharacters(in: range, with: "") as NSString
            
            let newRange : NSRange = NSRange(location: range.location - 1, length: 1)
            ranges.append(newRange)
        }
        
        let attString : NSMutableAttributedString = NSMutableAttributedString(string: string as String, attributes: [.font:font])
        
        for range in ranges
        {
           attString.setAttributes([.font:fontSuper!,.baselineOffset:10], range: range)
        }
        
        self.subtitle.attributedText = attString
    }

}
