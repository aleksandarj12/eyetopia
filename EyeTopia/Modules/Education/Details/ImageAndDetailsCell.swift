//
//  ImageAndDetailsCell.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 3/21/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class ImageAndDetailsCell: UITableViewCell {

    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var myDetail: UILabel!
    @IBOutlet weak var linkButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var linkButton: UIButton!
    
    var link : String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func linkButtonTapped(_ sender: Any)
    {
        UIApplication.shared.open(URL(string: self.link)!, options: [:], completionHandler: nil)
    }
    
    func setupSubtitle(_ text : String)
    {
        
        let fontSuper : UIFont? = UIFont(name: "AvenirNext-Bold", size:9)
        let font : UIFont = UIFont(name: "AvenirNext-Bold", size: 14)!
        
        var ranges : [NSRange] = [NSRange]()
        
        var string : NSString = text as NSString
        
        while (string.contains(k.text.fontSuper))
        {
            let range : NSRange = string.range(of: k.text.fontSuper)
            string = string.replacingCharacters(in: range, with: "") as NSString
            
            let newRange : NSRange = NSRange(location: range.location - 1, length: 1)
            ranges.append(newRange)
        }
        
        let attString : NSMutableAttributedString = NSMutableAttributedString(string: string as String, attributes: [.font:font])
        
        for range in ranges
        {
            attString.setAttributes([.font:fontSuper!,.baselineOffset:10], range: range)
        }
        
        self.myDetail.attributedText = attString
    }

}
