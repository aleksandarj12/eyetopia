//
//  BlinkExamContactLensViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/15/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

class BlinkExamContactLensViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var noCheckImageView: UIImageView!
    @IBOutlet weak var yesCheckImageView: UIImageView!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var yesLabel: UILabel!
    @IBOutlet weak var noLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    var storyboardTitle : String = ""
    @IBOutlet weak var backButtonBlinkInstructions: UIButton!
    @IBOutlet weak var backButtonContactLens: UIButton!
    @IBOutlet weak var additionalHolderView: UIView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(TransitionManager.sharedInstance.blinkExamRepeat)
        {
            if let backButton = self.backButton {
                backButton.setImage(nil, for: .normal)
            }
            
            if let contactLens = self.backButtonContactLens {
                contactLens.setImage(nil, for: .normal)
            }
            
            if let blinkExamInstructions = self.backButtonBlinkInstructions {
                blinkExamInstructions.setImage(nil, for: .normal)
            }
            
            if let holder = self.additionalHolderView {
                holder.alpha = 1
            }
            if let title = self.titleLabel {
                title.isHidden = true
            }
            
            
        }
        else
        {
            if let backButton = self.backButton {
                backButton.setImage(UIImage(named: "icnBackGreen"), for: .normal)
            }
            
            if let backButton = self.backButtonBlinkInstructions {
                backButton.setImage(UIImage(named: "icnBackGreen"), for: .normal)
            }
            
            if let backButton = self.backButtonContactLens {
                backButton.setImage(UIImage(named: "icnBackGreen"), for: .normal)
            }
            
            if let holder = self.additionalHolderView {
                holder.alpha = 0
            }
            if let title = self.titleLabel {
                title.isHidden = false
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(TransitionManager.sharedInstance.blinkExamRepeat) {
            if UserManager.sharedInstance.getWearingContactLenses() == true && self.additionalHolderView != nil {
                self.yesLabel.textColor = Style.primaryColor
                self.yesCheckImageView.alpha = 1
                self.noCheckImageView.alpha = 0
                self.noLabel.textColor = Style.navyDark
            }
            if UserManager.sharedInstance.getWearingContactLenses() == false && self.additionalHolderView != nil {
                self.yesLabel.textColor = Style.navyDark
                self.noLabel.textColor = Style.primaryColor
                self.yesCheckImageView.alpha = 0
                self.noCheckImageView.alpha = 1
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        if(self.storyboardTitle == "BlinkExamContactLensViewController1")
        {
//            Localytics.tagScreen("Do you wear contact lens?")
        }
        else if(self.storyboardTitle == "BlinkExamContactLensViewController2")
        {
//            Localytics.tagScreen("Contact lens removal")
        }
        else if(self.storyboardTitle == "BlinkExamContactLensViewController3")
        {
//            Localytics.tagScreen("Blink Exam Instruction")
        }
        
    }
    
    func setupExitCancel()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to cancel?", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes", style: .destructive) { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
            TransitionManager.sharedInstance.blinkExamRepeat = false
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func continueButtonTapped(_ sender: Any)
    {
        if(self.yesCheckImageView.alpha == 1)
        {
            let controller : BlinkExamContactLensViewController = self.storyboard?.instantiateViewController(withIdentifier: "BlinkExamContactLensViewController2") as! BlinkExamContactLensViewController
            controller.storyboardTitle = "BlinkExamContactLensViewController2"
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else
        {
            let controller : BlinkExamContactLensViewController = self.storyboard?.instantiateViewController(withIdentifier: "BlinkExamContactLensViewController3") as! BlinkExamContactLensViewController
            controller.storyboardTitle = "BlinkExamContactLensViewController3"
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func yesButtonTapped(_ sender: Any)
    {
        //self.continueButton.isEnabled = true
        self.yesLabel.textColor = Style.primaryColor
        self.yesCheckImageView.alpha = 1
        self.noCheckImageView.alpha = 0
        self.noLabel.textColor = Style.navyDark
        UserManager.sharedInstance.setIsWearingContactLenses(flag: true)
    }
    @IBAction func noButtonTapped(_ sender: Any)
    {
        //self.continueButton.isEnabled = true
        self.yesLabel.textColor = Style.navyDark
        self.noLabel.textColor = Style.primaryColor
        self.yesCheckImageView.alpha = 0
        self.noCheckImageView.alpha = 1
        UserManager.sharedInstance.setIsWearingContactLenses(flag: false)
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any)
    {
       setupExitCancel()
    }
    
    @IBAction func backButtonTapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func yesTappedFromNonDiagnosed(_ sender: UIButton) {
        UserManager.sharedInstance.setIsWearingContactLenses(flag: true)
        let controller : BlinkExamContactLensViewController = self.storyboard?.instantiateViewController(withIdentifier: "BlinkExamContactLensViewController2") as! BlinkExamContactLensViewController
        controller.storyboardTitle = "BlinkExamContactLensViewController2"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func noTappedFromNonDiagnosed(_ sender: UIButton) {
        UserManager.sharedInstance.setIsWearingContactLenses(flag: false)
        let controller : BlinkExamContactLensViewController = self.storyboard?.instantiateViewController(withIdentifier: "BlinkExamContactLensViewController3") as! BlinkExamContactLensViewController
        controller.storyboardTitle = "BlinkExamContactLensViewController3"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
