//
//  BlinkExamSurveyView.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/15/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

protocol BlinkExamSurveyView
{
    func setBackButton(flag : Bool)
    func setProgressView(progress : CGFloat)
    func setMainButtonTitle(title : String)
//    func setTransitionToScreen(screenID : Int)
//    func setCharacter(characterID : Int)
    
}
