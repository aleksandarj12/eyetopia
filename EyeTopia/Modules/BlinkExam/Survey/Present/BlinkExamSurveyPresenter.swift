//
//  BlinkExamSurveyPresenter.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/15/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class BlinkExamSurveyPresenter: NSObject
{
    var view : BlinkExamSurveyView!
    var screenId : Int = 1
    
    func attachView(view: BlinkExamSurveyView){
        self.view = view
    }
    
    func detachView() -> Void {
        self.view = nil
    }
    
//    func makeTransition()
//    {
//        if(self.screenId == 1)
//        {
//            self.screenId = 2
//            self.view.setBackButton(flag: false)
//
//            self.view.setProgressView(progress: 150)
////            self.view.setTransitionToScreen(screenID: 2)
//        }
//        else
//        {
//            self.screenId = 1
//            self.view.setBackButton(flag: true)
//            self.view.setProgressView(progress: 0)
////            self.view.setTransitionToScreen(screenID: 1)
//        }
//
//    }
    
//    func didFinishTransition(transitionID : Int)
//    {
//
//        if(transitionID == 2)
//        {
//            self.view.setCharacter(characterID: 2)
////            self.view.setMainButtonTitle(title: "Take blink exam")
//        }
//        else if(transitionID == 1)
//        {
////            self.view.setMainButtonTitle(title: "Continue")
//            self.view.setCharacter(characterID: 1)
//        }
//
//    }
    
    
    
}
