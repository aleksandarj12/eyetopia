//
//  BlinkExamSurveyViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/15/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

class BlinkExamSurveyViewController: UIViewController, BlinkExamSurveyView {
    

    @IBOutlet weak var bottomHomeIndicatorView: UIView!
    @IBOutlet weak var skyImageView: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var topHolderView2: UIView!
    @IBOutlet weak var bottomHolderView2: UIView!
    @IBOutlet weak var firstCubeImageView: UIImageView!
    @IBOutlet weak var progressBarView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var transitionButton: UIButton!
    @IBOutlet weak var progressBarTrailing: NSLayoutConstraint!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var isTransitioned : Bool = false
    
    var presenter : BlinkExamSurveyPresenter = BlinkExamSurveyPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backButton.isHidden = false
        self.presenter.attachView(view: self)
        self.bottomHolderView2.backgroundColor = UIColor(red:0.00, green:0.56, blue:0.53, alpha:1.0)
        self.bottomHomeIndicatorView.backgroundColor = UIColor(red:0.00, green:0.56, blue:0.53, alpha:1.0)

        UIView.animate(withDuration: 0.7) {
            self.topHolderView2.alpha = 1
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Localytics.openSession()
//        Localytics.tagScreen("Non-Diagnosed journey begin")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        
        if(TransitionManager.sharedInstance.journeyAgain)
        {
            TransitionManager.sharedInstance.journeyAgain = false
            self.dismiss(animated: false, completion: nil)
        }
        
        
        
    }
    
    //MARK: Setup & Support Methods
    
    func setupJourneyStation()
    {
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
        UserManager.sharedInstance.setJourneyStation(self.navigationController!, controller)
    }
    
    func setupExitView()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to exit? You may continue later.", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes, exit", style: .destructive) { (action) -> Void in
            self.setupJourneyStation()
                NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentHomeKey), object: self)
            UserManager.sharedInstance.setDiagnosedJourneyCompetion(flag: false)
            UserManager.sharedInstance.setNonDiagnosedJourneyCompetion(flag: false)
            NotificationManager.sharedInstance.scheduleJourneyContinuationReminderNotification()
            Localytics.closeSession()
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func setupBlinkExam()
    {
        let controller : TransitionViewController = (self.storyboard?.instantiateViewController(withIdentifier: "BlinkExam2")) as! TransitionViewController
        controller.storyboardTitle = "BlinkExam2"
        
        let navController : UINavigationController = UINavigationController(rootViewController: controller)
        navController.setNavigationBarHidden(true, animated: false)
        self.present(navController, animated: true, completion: nil)
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK: Action Methods
    
//    @IBAction func continueButtonTapped(_ sender: Any)
//    {
//        self.isTransitioned = true
//        UIView.animate(withDuration: 0.7, animations: {
//            self.topHolderView1.alpha = 0
//        }) { (flag : Bool) in
//
//            UIView.animate(withDuration: 4, delay: 1, options: .allowAnimatedContent, animations: {
//                self.skyImageView.frame.origin.x = -1095.0
//            }, completion: { (flag : Bool) in
//                self.bottomHolderView2.alpha = 1
//                self.topHolderView1.isHidden = true
//            })
//
//            UIView.animate(withDuration: 4, animations: {
//                self.backgroundImageView.frame.origin.x = -880.0
//            }, completion: { (flag : Bool) in
//                UIView.animate(withDuration: 0.7, animations: {
//                    self.topHolderView2.alpha = 1
//                    self.bottomHolderView1.alpha = 0
//                }, completion: { (flag : Bool) in
//                    self.topHolderView2.isHidden = false
//                })
//            })
//        }
//        progressBarTrailing.constant = 60
//    }
    
    @IBAction func exitButtonTapped(_ sender: Any)
    {
        self.setupExitView()
        
//        let controller : ExitViewController = self.storyboard?.instantiateViewController(withIdentifier: "ExitViewController") as! ExitViewController
//        TransitionManager.sharedInstance.setContinueJourneyDescription(controllerDescription: "BlinkExamSurveyViewController")
        
//        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func takeBlinkExamButtonTapped(_ sender: Any)
    {
        self.setupBlinkExam()
    }
    
//    @IBAction func transitionButtonTapped(_ sender: Any)
//    {
//        self.presenter.makeTransition()
//    }
    @IBAction func backButtonTapped(_ sender: Any)
    {
//        TransitionManager.sharedInstance.storyboardID = ""
//        if(self.isTransitioned)
//        {
//            self.isTransitioned = false
//
//            UIView.animate(withDuration: 0.7, animations: {
//                self.topHolderView2.alpha = 0
//            }) { (flag : Bool) in
//
//                UIView.animate(withDuration: 4, delay: 1, options: .allowAnimatedContent, animations: {
//                    self.skyImageView.frame.origin.x = -605.0
//                }, completion: { (flag : Bool) in
//                    self.bottomHolderView2.alpha = 0
//                    self.topHolderView2.isHidden = true
//
//                })
//
//                UIView.animate(withDuration: 4, animations: {
//                    self.backgroundImageView.frame.origin.x = -385.0
//                }, completion: { (flag : Bool) in
//                    UIView.animate(withDuration: 0.7, animations: {
//                        self.topHolderView1.alpha = 1
//                        self.bottomHolderView1.alpha = 1
//                    }, completion: { (flag : Bool) in
//                        self.topHolderView1.isHidden = false
//                    })
//                })
//            }
//            progressBarTrailing.constant = 240
//        }
//        else
//        {
            self.navigationController?.popViewController(animated: false)
//        }
    }
    
    
    //MARK: BlinkExamSurveyView
    
//    func setCharacter(characterID: Int)
//    {
//        if(characterID == 1)
//        {
//            self.firstCubeImageView.alpha = 1
//            self.secondCubeImageView.alpha = 0
//        }
//        else
//        {
//            self.firstCubeImageView.alpha = 0
//            self.secondCubeImageView.alpha = 1
//        }
//    }
    
    func setProgressView(progress: CGFloat)
    {
        
    }
    
    func setBackButton(flag: Bool)
    {
        self.backButton.isHidden = flag
    }
    
    func setProgressView(progress: Float)
    {
        
    }
    
    func setMainButtonTitle(title: String)
    {
        self.transitionButton.setTitle(title, for: .normal)
    }
    
//    func setTransitionToScreen(screenID: Int)
//    {
//        if(screenID == 1)
//        {
//            UIView.animate(withDuration: 0.7, animations: {
//                self.firstMainView.frame.origin.x = 0
//                self.secondMainView.frame.origin.x = UIScreen.main.bounds.width
//            }, completion: { (flag : Bool) in
//
//                self.presenter.didFinishTransition(transitionID: screenID)
//
//                UIView.animate(withDuration: 0.7)
//                {
//                    self.firstBubbleView.alpha = 1
//                    self.secondBubbleView.alpha = 0
//                }
//            })
//        }
//        else if(screenID == 2)
//        {
//            UIView.animate(withDuration: 0.7, animations: {
//                self.firstMainView.frame.origin.x = UIScreen.main.bounds.width - (2 * UIScreen.main.bounds.width)
//                self.secondMainView.frame.origin.x = 0
//            }, completion: { (flag : Bool) in
//
//                self.presenter.didFinishTransition(transitionID: screenID)
//
//                UIView.animate(withDuration: 0.7)
//                {
//                    self.firstBubbleView.alpha = 0
//                    self.secondBubbleView.alpha = 1
//                }
//            })
//        }
//    }

}
