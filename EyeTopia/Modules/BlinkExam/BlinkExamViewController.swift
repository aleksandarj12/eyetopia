//
//  BlinkExamViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 3/7/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

class BlinkExamViewController: UIViewController, RedoExamDelegate {
    
    private var timer = Timer()
    private var minutes = 0
    private var seconds = 0
    private var fractions = 0
    private var timerIsOn = true
    private var stopWatchString = ""
    private var stopWatchAllInSecondsString = ""
    private var resultInDouble: Double {
        return Double(fractions) + Double((seconds * 100)) + Double((minutes * 60 * 100))
    }

    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var dotlbl: UILabel!
    @IBOutlet weak var stopWatchlbl: UILabel!
    @IBOutlet weak var startTimerBtn: UIButton!
    @IBOutlet weak var examCompletelbl: UILabel!
    @IBOutlet weak var continuebtn: UIButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet weak var resultlbl: UILabel!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var viewresult: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startTimerBtn .setTitle("Start Timer", for: UIControlState.normal)
        continuebtn.isHidden = true
        examCompletelbl.isHidden = true
        resultlbl.isHidden = true
        startTimerBtn.layer.borderWidth = 2
        startTimerBtn.layer.cornerRadius = 3
        startTimerBtn.layer.borderColor = UIColor(red:0.19, green:0.68, blue:0.38, alpha:1.0).cgColor
        startTimerBtn.backgroundColor = UIColor.clear
        
        dotlbl.layer.masksToBounds = true
        dotlbl.layer.cornerRadius = 23.0
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Localytics.tagScreen("Blink Exam Timer")
    }

    @IBAction func startTimer(_ sender: UIButton) {
        
        
        if sender.titleLabel?.text == "Start Timer" || sender.titleLabel?.text == "Stop Timer" {
            if timerIsOn == true {
                timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(upDateStopWatch), userInfo: nil, repeats: true)
                timerIsOn = false
                startTimerBtn.setTitle("Stop Timer", for: UIControlState.normal)
                startTimerBtn.layer.borderColor = UIColor(red:0.90, green:0.30, blue:0.26, alpha:1.0).cgColor
                titlelbl.isHidden = true
                
            } else {
                timer.invalidate()
                timerIsOn = true
                startTimerBtn .setTitle("Redo exam", for: UIControlState.normal)
                startTimerBtn.layer.borderColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0).cgColor
                continuebtn.isHidden = false
                UserManager.sharedInstance.setBlinkExamTime(time: Float(stopWatchAllInSecondsString)!)
                UserManager.sharedInstance.setBlinkExamTimeString(timeString: stopWatchString)
                stopWatchlbl.isHidden = true
                dotlbl.isHidden = true
                examCompletelbl.isHidden = false
                resultlbl.text = stopWatchString
                resultlbl.isHidden = false
            }
        } else if sender.titleLabel?.text == "Redo exam" {
            self.setupResetBlinkExam()
        }
    }
    
    @objc func upDateStopWatch() {
        
        fractions += 1
        if fractions == 100 {
            seconds += 1
            fractions = 0
        }
        
        if seconds == 60 {
            minutes += 1
            seconds = 0
        }
        
        let fractionsInString = fractions > 9 ? "\(fractions)" : "0\(fractions)"
        let secondsInString = seconds > 9 ? "\(seconds)" : "0\(seconds)"
        let minutesInString = minutes > 9 ? "\(minutes)" : "0\(minutes)"
        var totalSecondsWithMinutesInString: String {
            if minutes > 0 {
                return String((minutes * 60) + seconds)
            } else {
                return String(seconds)
            }
        }
        
        if minutes == 0 {
            stopWatchString = "\(secondsInString).\(fractionsInString)"
            stopWatchAllInSecondsString = "\(totalSecondsWithMinutesInString).\(fractionsInString)"
        } else {
        stopWatchString = "\(minutesInString).\(secondsInString).\(fractionsInString)"
        stopWatchAllInSecondsString = "\(totalSecondsWithMinutesInString).\(fractionsInString)"
        }
        stopWatchlbl.text = stopWatchString
        
    }
    
    
    //MARK: RedoExamDelegate
    
    func didSelectRedoExam()
    {
        self.setupResetBlinkExam()
    }
    
    
    // MARK: - Action Methods

    @IBAction func continueButtonTapped(_ sender: Any)
    {
        let result = createBlinkExamResult(time: resultInDouble)
        UserManager.sharedInstance.saveBlinkExam(result: result)
        NotificationManager.sharedInstance.scheduleBlinkExamReminders()
        NotificationManager.sharedInstance.scheduleJourneyContinuationReminderNotification()
        
        if(TransitionManager.sharedInstance.blinkExamRepeat)
        {
            Localytics.tagEvent(trackingEvents.blinkExamCompletedKey)
            TransitionManager.sharedInstance.blinkExamRepeat = false
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            UserManager.sharedInstance.setNonDiagnosedJourneyCompetion(flag: true)
            NotificationManager.sharedInstance.scheduleJourneyContinuationReminderNotification()
            if TransitionManager.sharedInstance.didFinishTransition == true {
                let controller : PivotNotDiagnosedViewController = self.storyboard?.instantiateViewController(withIdentifier: "PivotNotDiagnosedViewController") as! PivotNotDiagnosedViewController
                controller.delegate = self
                TransitionManager.sharedInstance.setDidFinishTransition(flag: false)
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
                let controller : BlinkExamDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "BlinkExamDetailsViewController") as! BlinkExamDetailsViewController
                self.navigationController?.pushViewController(controller, animated: true)
                
            }
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any)
    {
        setupExitCancel()
    }
    
    
    //MARK: Setup & Support Methods
    
    func setupResetBlinkExam()
    {
        fractions = 0
        seconds = 0
        minutes = 0
        
        startTimerBtn .setTitle("Start Timer", for: UIControlState.normal)
        startTimerBtn.layer.borderColor = UIColor(red:0.19, green:0.68, blue:0.38, alpha:1.0).cgColor
        stopWatchlbl.text = "00.00"
        titlelbl.isHidden = false
        stopWatchlbl.isHidden = false
        dotlbl.isHidden = false
        startTimerBtn.isHidden = false
        continuebtn.isHidden = true
        examCompletelbl.isHidden = true
        resultlbl.isHidden = true
    }
    
    private func createBlinkExamResult(time: Double) -> BlinkExamResult {
        let date = Date()
        let result = BlinkExamResult(date: date, time: time)
        return result
    }
    
    
    func setupExitCancel()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to cancel?", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes", style: .destructive) { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }

}
