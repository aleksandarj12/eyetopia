//
//  BlinkExamResult.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 4/18/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class BlinkExamResult: NSObject, NSCoding {
 
    var date: Date
    var time: Double
    
    init(date: Date, time: Double) {
        self.date = date
        self.time = time
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.date, forKey: "date")
        aCoder.encode(self.time, forKey: "time")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let date = aDecoder.decodeObject(forKey: "date") as? Date else { return nil }
        self.init(date: date, time: aDecoder.decodeDouble(forKey: "time"))
    }
    
    
}
