//
//  BlinkExamDetailsViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/29/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class BlinkExamDetailsViewController: UIViewController  {
    
    @IBOutlet weak var blinkExamEmptyLabel: UILabel!
    @IBOutlet weak var w2: UIButton!
    @IBOutlet weak var m1: UIButton!
    @IBOutlet weak var m3: UIButton!
    @IBOutlet weak var m6: UIButton!
    @IBOutlet weak var all: UIButton!
    
    
    
    
    @IBOutlet weak var holderViewForChary: UIView!
    @IBOutlet weak var chartView: ChartGraphicView!
    @IBOutlet weak var pastDateLabel: UILabel!
    
    private var results = [BlinkExamResult]()
    private var daysForUnits: CGFloat = 0
    
    var isMain : Bool = false

    //var myarray = [BlinkExamResult]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        w2.layer.cornerRadius = 10
        m1.layer.cornerRadius = 10
        m3.layer.cornerRadius = 10
        m6.layer.cornerRadius = 10
        all.layer.cornerRadius = 10
        
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: self.holderViewForChary.frame.size.height)
        gradient.colors = [firstColor.cgColor,middleColor.cgColor, middleColor2.cgColor, secondColor.cgColor]
        self.holderViewForChary.layer.insertSublayer(gradient, at: 0)
        
        //for testing dates
        
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy/MM/dd HH:mm"
//
//        let date1 = formatter.date(from: "2018/07/18 05:00")
//        let test1 = BlinkExamResult(date: date1!, time: 100)
//
//
//        let date2 = formatter.date(from: "2017/12/11 06:00")
//        let test2 = BlinkExamResult(date: date2!, time: 900)
//
//
//        let date3 = formatter.date(from: "2018/07/08 05:00")
//        let test3 = BlinkExamResult(date: date3!, time: 100)
//
//
//        let date4 = formatter.date(from: "2018/06/06 05:00")
//        let test4 = BlinkExamResult(date: date4!, time: 400)
//
//
//
//        let date5 = formatter.date(from: "2018/03/13 05:00")
//        let test5 = BlinkExamResult(date: date5!, time: 1500)
//
//        let date6 = formatter.date(from: "2018/03/10 05:00")
//        let test6 = BlinkExamResult(date: date6!, time: 700)
//
//        let date7 = formatter.date(from: "2017/11/10 05:00")
//        let test7 = BlinkExamResult(date: date7!, time: 1200)
//
//        myarray = [test1,test2, test3,test4,test5,test6,test7]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(self.isMain)
        {
            self.isMain = false
        }
        else
        {
            self.navigationController?.popViewController(animated: false)
        }
        
        let loadedResults = UserManager.sharedInstance.loadBlinkExamResults()
        self.results = loadedResults
        
        m1.setTitleColor(UIColor(red: 2/255.0, green: 47/255.0, blue: 57/255.0, alpha: 1), for: UIControlState.normal)
        m1.backgroundColor = .white
        w2.backgroundColor = .clear
        w2.setTitleColor(.white, for: UIControlState.normal)
        m3.backgroundColor = .clear
        m3.setTitleColor(.white, for: UIControlState.normal)
        m6.backgroundColor = .clear
        m6.setTitleColor(.white, for: UIControlState.normal)
        all.backgroundColor = .clear
        all.setTitleColor(.white, for: UIControlState.normal)
        
        let date = getPast(month: -1)
        pastDateLabel.text = date.0
        let newResultOfTime = getValueForChartFrom(array: results, fromDay: nil, fromMonth: -1)
        chartView.dates = daysForUnits
        chartView.results = newResultOfTime
        checkingFor(emptyResults: self.results)
    }


    
    @IBAction func blinkExamJourneyButtonTapped(_ sender: Any)
    {
        TransitionManager.sharedInstance.journeyAgain = true
        TransitionManager.sharedInstance.blinkExamRepeat = true
        self.isMain = true
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.nonDiagnosed, bundle: nil)
        let controller : TransitionViewController = (storyboard.instantiateViewController(withIdentifier: "BlinkExam2")) as! TransitionViewController
        controller.storyboardTitle = "BlinkExam2"
        let navController : UINavigationController = UINavigationController(rootViewController: controller)
        navController.setNavigationBarHidden(true, animated: false)
        self.present(navController, animated: true, completion: nil)
    }
    
   
    @IBAction func showChartFor2Weeks(_ sender: UIButton) {
        
        w2.setTitleColor(UIColor(red: 2/255.0, green: 47/255.0, blue: 57/255.0, alpha: 1), for: UIControlState.normal)
        w2.backgroundColor = .white
        m1.backgroundColor = .clear
        m1.setTitleColor(.white, for: UIControlState.normal)
        m3.backgroundColor = .clear
        m3.setTitleColor(.white, for: UIControlState.normal)
        m6.backgroundColor = .clear
        m6.setTitleColor(.white, for: UIControlState.normal)
        all.backgroundColor = .clear
        all.setTitleColor(.white, for: UIControlState.normal)
        
        let date = getPast(day: -15)
        pastDateLabel.text = date.0
        
        let newResultOfTime = getValueForChartFrom(array: results, fromDay: -15, fromMonth: nil)
        chartView.dates = daysForUnits
        chartView.results = newResultOfTime
    }
    
    @IBAction func showChartFor1Months(_ sender: UIButton) {
     
        m1.setTitleColor(UIColor(red: 2/255.0, green: 47/255.0, blue: 57/255.0, alpha: 1), for: UIControlState.normal)
        m1.backgroundColor = .white
        w2.backgroundColor = .clear
        w2.setTitleColor(.white, for: UIControlState.normal)
        m3.backgroundColor = .clear
        m3.setTitleColor(.white, for: UIControlState.normal)
        m6.backgroundColor = .clear
        m6.setTitleColor(.white, for: UIControlState.normal)
        all.backgroundColor = .clear
        all.setTitleColor(.white, for: UIControlState.normal)
        
        let date = getPast(month: -1)
        pastDateLabel.text = date.0
        let newResultOfTime = getValueForChartFrom(array: results, fromDay: nil, fromMonth: -1)
        chartView.dates = daysForUnits
        chartView.results = newResultOfTime
    }
    
    @IBAction func showChartFor3Months(_ sender: UIButton) {

        m3.setTitleColor(UIColor(red: 2/255.0, green: 47/255.0, blue: 57/255.0, alpha: 1), for: UIControlState.normal)
        m3.backgroundColor = .white
        w2.backgroundColor = .clear
        w2.setTitleColor(.white, for: UIControlState.normal)
        m1.backgroundColor = .clear
        m1.setTitleColor(.white, for: UIControlState.normal)
        m6.backgroundColor = .clear
        m6.setTitleColor(.white, for: UIControlState.normal)
        all.backgroundColor = .clear
        all.setTitleColor(.white, for: UIControlState.normal)
        
        let date = getPast(month: -3)
        pastDateLabel.text = date.0
       
        let newResultOfTime = getValueForChartFrom(array: results, fromDay: nil, fromMonth: -3)
        chartView.dates = daysForUnits
        chartView.results = newResultOfTime
    }
    
    @IBAction func showChartFor6Months(_ sender: UIButton) {
       
        m6.setTitleColor(UIColor(red: 2/255.0, green: 47/255.0, blue: 57/255.0, alpha: 1), for: UIControlState.normal)
        m6.backgroundColor = .white
        w2.backgroundColor = .clear
        w2.setTitleColor(.white, for: UIControlState.normal)
        m1.backgroundColor = .clear
        m1.setTitleColor(.white, for: UIControlState.normal)
        m3.backgroundColor = .clear
        m3.setTitleColor(.white, for: UIControlState.normal)
        all.backgroundColor = .clear
        all.setTitleColor(.white, for: UIControlState.normal)
        
        let date = getPast(month: -6)
        pastDateLabel.text = date.0
        
        let newResultOfTime = getValueForChartFrom(array: results, fromDay: nil, fromMonth: -6)
        chartView.dates = daysForUnits
        chartView.results = newResultOfTime
    }
    
    @IBAction func showChartForAll(_ sender: Any) {
        if self.results.count == 0 {
            blinkExamEmptyLabel.isHidden = false
            
            all.setTitleColor(UIColor(red: 2/255.0, green: 47/255.0, blue: 57/255.0, alpha: 1), for: UIControlState.normal)
            all.backgroundColor = .white
            w2.backgroundColor = .clear
            w2.setTitleColor(.white, for: UIControlState.normal)
            m1.backgroundColor = .clear
            m1.setTitleColor(.white, for: UIControlState.normal)
            m6.backgroundColor = .clear
            m6.setTitleColor(.white, for: UIControlState.normal)
            m3.backgroundColor = .clear
            m3.setTitleColor(.white, for: UIControlState.normal)
            
            pastDateLabel.text = "All"
        } else {
        blinkExamEmptyLabel.isHidden = true
        all.setTitleColor(UIColor(red: 2/255.0, green: 47/255.0, blue: 57/255.0, alpha: 1), for: UIControlState.normal)
        all.backgroundColor = .white
        w2.backgroundColor = .clear
        w2.setTitleColor(.white, for: UIControlState.normal)
        m1.backgroundColor = .clear
        m1.setTitleColor(.white, for: UIControlState.normal)
        m6.backgroundColor = .clear
        m6.setTitleColor(.white, for: UIControlState.normal)
        m3.backgroundColor = .clear
        m3.setTitleColor(.white, for: UIControlState.normal)
        
        pastDateLabel.text = "All"
        
        let newResultOfTime = getAllValueForChartFrom(array: results)
        chartView.results = newResultOfTime
        chartView.dates = self.daysForUnits
        }
    }
    
    private func getPast(month: Int) -> (String, Date) {
        var dateInString = ""
        var date = Date()
        if let previousMonth = Calendar.current.date(byAdding: .month, value: month, to: Date()) {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM d"
            dateInString = formatter.string(from: previousMonth).uppercased()
            date = previousMonth
        }
        
        let tuple = (dateInString, date)
        return tuple
    }
    
    private func getPast(day: Int) -> (String, Date) {
        var dateInString = ""
        var date = Date()
        if let previousDays = Calendar.current.date(byAdding: .day, value: day, to: Date()) {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM d"
            dateInString = formatter.string(from: previousDays).uppercased()
            date = previousDays
        }
        let tuple = (dateInString, date)
        return tuple
    }
    
    
    
    
    private func getValueForChartFrom(array: [BlinkExamResult], fromDay: Int?, fromMonth: Int?) -> [(CGFloat, CGFloat)]{
        var resultsOfTimeAndDate = [(CGFloat, CGFloat)]()
        
        let currentDate = Date()
        var sortedArray = array
        sortedArray.sort(by: {$0.date.timeIntervalSinceNow < $1.date.timeIntervalSinceNow})
        
        if let range = fromMonth, let requiredDate = Calendar.current.date(byAdding: .month,  value: range, to: Date()), let requiredDatePastOneDay = Calendar.current.date(byAdding: .day,  value: -1, to: requiredDate){
            let filteredArrayByRequiredDate = sortedArray.filter({return $0.date >= requiredDatePastOneDay})
            let filteredArrayByCurrentDate = filteredArrayByRequiredDate.filter({return $0.date <= currentDate})
            let mappedArrayTime = filteredArrayByCurrentDate.map({CGFloat($0.time)})
            let mappedArrayDatesByDay = filteredArrayByCurrentDate.map({self.getUnitsInDays(startDate: requiredDatePastOneDay, endDate: $0.date)})
            let finalPointsvalue = self.mergeTimeAndDate(time: mappedArrayTime, date: mappedArrayDatesByDay)
            self.daysForUnits = getUnitsInDays(startDate: requiredDate, endDate: Date())
            resultsOfTimeAndDate = finalPointsvalue
            
        }
        if let range = fromDay, let requiredDate = Calendar.current.date(byAdding: .day, value: range, to: Date()), let requiredDatePastOneDay = Calendar.current.date(byAdding: .day,  value: -1, to: requiredDate) {
            let filteredArrayByRequiredDate = sortedArray.filter({return $0.date >= requiredDatePastOneDay})
            let filteredArrayByCurrentDate = filteredArrayByRequiredDate.filter({return $0.date < currentDate})
            let mappedArrayTime = filteredArrayByCurrentDate.map({CGFloat($0.time)})
            let mappedArrayDatesByDay = filteredArrayByCurrentDate.map({self.getUnitsInDays(startDate: requiredDatePastOneDay, endDate: $0.date)})
            let finalPointsvalue = self.mergeTimeAndDate(time: mappedArrayTime, date: mappedArrayDatesByDay)
            self.daysForUnits = getUnitsInDays(startDate: requiredDate, endDate: Date())
            resultsOfTimeAndDate = finalPointsvalue
        }
        return resultsOfTimeAndDate
    }
    
    private func getAllValueForChartFrom(array: [BlinkExamResult]) -> [(CGFloat, CGFloat)] {
        var resultsOfTime = [(CGFloat, CGFloat)]()
        
        var sortedArray = array
        sortedArray.sort(by: {$0.date.timeIntervalSinceNow < $1.date.timeIntervalSinceNow})
        let mappedArrayTime = sortedArray.map({CGFloat($0.time)})
        let firstWithOneDayPlus = Calendar.current.date(byAdding: .day,  value: 0, to: (sortedArray.first?.date)!)
        let first =  sortedArray.first?.date
        let mappedArrayDatesByDay = sortedArray.map({self.getUnitsInDays(startDate:first!, endDate: $0.date)})
        let finalPointsvalue = self.mergeTimeAndDate(time: mappedArrayTime, date: mappedArrayDatesByDay)
        self.daysForUnits = getUnitsInDays(startDate: firstWithOneDayPlus!, endDate: Date())
        resultsOfTime = finalPointsvalue
        
        
        
        return resultsOfTime
    }
    
    private func getDayOfDateInCGfloat(date: Date) -> CGFloat {
        let day =  Calendar.current.component(.day, from: date)
        let CGDay = CGFloat(day)
        return CGDay
    }
    
  
    
    private func getUnitsInDays(startDate: Date, endDate: Date) -> CGFloat {
        let dateComponents = Calendar.current.dateComponents([.day], from: startDate, to: endDate)
        let days = dateComponents.day
        let daysInCGfloat = CGFloat(days!)
        return daysInCGfloat
    }
    
    private func mergeTimeAndDate(time: [CGFloat], date: [CGFloat]) -> [(CGFloat, CGFloat)] {
        var newArray = [(CGFloat, CGFloat)]()
        
        for (index, element) in time.enumerated() {
            var tuple: (CGFloat, CGFloat) = (0,0)
            tuple.0 = element
            tuple.1 = date[index]
            newArray.append(tuple)
        }
        
        return newArray
    }
    
    private func checkingFor(emptyResults: [BlinkExamResult]) {
        if emptyResults.isEmpty {
            blinkExamEmptyLabel.isHidden = false
        } else {
            blinkExamEmptyLabel.isHidden = true
        }
    }
    
    // constants for gradient color on chart
    //UIColor(red: 1/255.0, green: 52/255.0, blue: 63/255.0, alpha: 1)
    let firstColor = UIColor(red: 3/255.0, green: 48/255.0, blue: 57/255.0, alpha: 1)
    let middleColor = UIColor(red: 1/255.0, green: 51/255.0, blue: 62/255.0, alpha: 1)
    let middleColor2 = UIColor(red: 1/255.0, green: 66/255.0, blue: 82/255.0, alpha: 1)
    let secondColor = UIColor(red: 2/255.0, green: 73/255.0, blue: 89/255.0, alpha: 1)
    
}
