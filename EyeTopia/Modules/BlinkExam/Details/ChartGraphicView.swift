//
//  ChartGraphicView.swift
//  ChartGraphic
//
//  Created by NIKOLA RUSEV on 4/16/18.
//  Copyright © 2018 NIKOLA RUSEV. All rights reserved.
//

import UIKit

class ChartGraphicView: UIView {
    

    
    var results:[(CGFloat, CGFloat)] = [] { didSet {setNeedsDisplay() }}
    var dates: CGFloat = 31
    
    private var fractionPerHeight: CGFloat {
        return bounds.size.height / 1500
    }
    private var secondPerHeight: CGFloat {
        return bounds.size.height / 15
    }
    
    private var xAxisWidth: CGFloat {
       return (bounds.size.width - 85)
    }
    
    private var unitOfDates: CGFloat {
       return xAxisWidth / dates
    }
    
    private var pointsOfDots: [(CGFloat, CGFloat)] {
        return makingPointsForLineOfChanges(y: self.results)
    }
    
    
    
    override func draw(_ rect: CGRect) {
        setUpChart()
        setUpShadow()
        setUpDots()
        setUpLine()
    }
    
    
    //MARK: Helper methods
    
    private func pathForLine(startPoint: CGPoint, endPoint: CGPoint) -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: startPoint)
        path.addLine(to: endPoint)
        
        return path
    }
    
    private func styleForLine(path: UIBezierPath, width: CGFloat, color: UIColor) {
        path.lineWidth = width
        color.setStroke()
        path.stroke()
        
    }
    
    private func makingPointsForLineOfChanges(y: [(CGFloat, CGFloat)]) -> [(CGFloat, CGFloat)] {
        var dotsForLineOfChanges = [(CGFloat, CGFloat)]()
        if y .isEmpty {
            return dotsForLineOfChanges
        } else {
            var chekedArray = [(CGFloat, CGFloat)]()
            var newElement: (CGFloat, CGFloat) = (0,0)
            for element in y {
                if element.0 > 1450 {
                    newElement.0 = 1450
                    newElement.1 = element.1
                    chekedArray.append(newElement)
                    
                } else {
                    chekedArray.append(element)
                }
            }
            let yDotsForLineOfChanges = chekedArray.map({return ((bounds.size.height - ($0.0 * fractionPerHeight)), $0.1 ) })
            
            let startPoint: CGFloat = 60
            
            for p in yDotsForLineOfChanges {
                var tupleForLineOfChanges: (x: CGFloat, y: CGFloat)
                tupleForLineOfChanges.x = p.0
                if dates == 0 {
                    //this means that there is no range of days 
                   tupleForLineOfChanges.y = bounds.size.width - 25
                }else {
                  tupleForLineOfChanges.y = startPoint + (p.1 * unitOfDates)
                }
                
                dotsForLineOfChanges.append(tupleForLineOfChanges)
                
            }
            return dotsForLineOfChanges
        }
        
        
    }
            


    
    private func makingPointsForShadow() -> [(CGFloat, CGFloat)] {
        
        var dotsForShadow = [(CGFloat, CGFloat)]()
        
        var firstPoint: (CGFloat, CGFloat) = (0,0)
        let middlePoints = makingPointsForLineOfChanges(y: results)
        
        var firstAfterMiddlePoint: (CGFloat, CGFloat) = (0,0)
        var secondAfterMiddlePoint: (CGFloat, CGFloat) = (0,0)
        var thirdAfterMiddlePoint: (CGFloat, CGFloat) = (0,0)
        var fourthAfterMiddlePoint: (CGFloat, CGFloat) = (0,0)
        var fifthAfterMiddlePoint: (CGFloat, CGFloat) = (0,0)
        var sixthAfterMiddlePoint: (CGFloat, CGFloat) = (0,0)
        
        if middlePoints.count > 1 {
            if let first = middlePoints.first, let last = middlePoints.last {
                firstPoint.0 = first.1
                firstPoint.1 = bounds.size.height
                dotsForShadow.append(firstPoint)
                
                for p in middlePoints {
                    var tuple : (CGFloat, CGFloat) = (0,0)
                    tuple.0 = p.1
                    tuple.1 = p.0
                    dotsForShadow.append(tuple)
                }
                
                firstAfterMiddlePoint.0 = last.1
                firstAfterMiddlePoint.1 = bounds.size.height
                dotsForShadow.append(firstAfterMiddlePoint)
                
                secondAfterMiddlePoint.0 = bounds.size.width
                secondAfterMiddlePoint.1 = bounds.size.height
                dotsForShadow.append(secondAfterMiddlePoint)
                
                thirdAfterMiddlePoint.0 = bounds.size.width
                thirdAfterMiddlePoint.1 = 0
                dotsForShadow.append(thirdAfterMiddlePoint)
                
                fourthAfterMiddlePoint.0 = 0 + GraphStyle.yAxisWidth
                fourthAfterMiddlePoint.1 = 0
                dotsForShadow.append(fourthAfterMiddlePoint)
                
                fifthAfterMiddlePoint.0 = 0 + GraphStyle.yAxisWidth
                fifthAfterMiddlePoint.1 = bounds.size.height
                dotsForShadow.append(fifthAfterMiddlePoint)
                
                sixthAfterMiddlePoint.0 = first.1
                sixthAfterMiddlePoint.1 = bounds.size.height
                dotsForShadow.append(sixthAfterMiddlePoint)
                
            }
        }
        return dotsForShadow
    }
 

    private func setUpChart() {
        
        let xAxis0 = pathForLine(startPoint: CGPoint(x: 0, y: bounds.size.height), endPoint: CGPoint(x: bounds.size.width, y: bounds.size.height))
        styleForLine(path: xAxis0, width: GraphStyle.xAxisWidth, color: GraphStyle.xAxisColor)
        
        let horizontal8 = pathForLine(startPoint: CGPoint(x: 0, y: bounds.size.height - (secondPerHeight * 8)), endPoint: CGPoint(x: bounds.size.width, y: bounds.size.height - (secondPerHeight * 8)))
        styleForLine(path: horizontal8, width: GraphStyle.xAxisWidth, color: GraphStyle.xAxisColor)
        
        let horizontal12 = pathForLine(startPoint: CGPoint(x: 0, y: bounds.size.height - (secondPerHeight * 12)), endPoint:CGPoint(x: bounds.size.width, y: bounds.size.height - (secondPerHeight * 12)))
        styleForLine(path: horizontal12, width: GraphStyle.xAxisWidth, color: GraphStyle.xAxisColor)
        
        let horizontal12Plus = pathForLine(startPoint: CGPoint(x: 0, y: 0), endPoint:CGPoint(x: bounds.size.width, y: 0))
        styleForLine(path: horizontal12Plus, width: GraphStyle.xAxisWidth, color: GraphStyle.xAxisColor)
        
        let yAxis8s = pathForLine(startPoint: CGPoint(x: 0, y: bounds.size.height), endPoint: CGPoint(x: 0, y: bounds.size.height - (secondPerHeight * 8)))
        styleForLine(path: yAxis8s, width: GraphStyle.yAxisWidth, color: GraphStyle.yAxisColor8)
        
        let yAxis12s = pathForLine(startPoint: CGPoint(x: 0, y: bounds.size.height - (secondPerHeight * 8)), endPoint: CGPoint(x: 0, y: bounds.size.height - (secondPerHeight * 12)))
        styleForLine(path: yAxis12s, width: GraphStyle.yAxisWidth, color: GraphStyle.yAxisColor12)
        
        let yAxis12Plus = pathForLine(startPoint: CGPoint(x: 0, y: bounds.size.height - (secondPerHeight * 12)), endPoint: CGPoint(x: 0, y: 0))
        styleForLine(path: yAxis12Plus, width: GraphStyle.yAxisWidth, color: GraphStyle.yAxisColor12Plus)
    }
    
    
    private func setUpDots() {


        for p in self.pointsOfDots {

            if self.pointsOfDots.count > 0 {
                let xOrigin = p.1 - (8/2)
                let yOrigin = p.0 - (8/2)
                
                let pathForDot = UIBezierPath(ovalIn: CGRect(x: xOrigin, y: yOrigin, width: 8, height: 8))
                pathForDot.lineWidth = 1
                GraphStyle.dotColor.setStroke()
                GraphStyle.dotColor.setFill()
                pathForDot.stroke()
                pathForDot.fill()
            }
        }
    }
    
    private func setUpLine() {
        let lineOfChanges = UIBezierPath()
        
        for (index,p) in self.pointsOfDots.enumerated() {
            if self.pointsOfDots.count > 1 {
                if index == 0 {
                    lineOfChanges.move(to: CGPoint(x: p.1, y: p.0))
                } else {
                    lineOfChanges.addLine(to: CGPoint(x: p.1, y: p.0))
                }
                GraphStyle.dotColor.setStroke()
                lineOfChanges.lineWidth = 2
                lineOfChanges.stroke()
            }
        }
    }

    
    

    
    private func setUpShadow() {
        
        let pathForShadow = UIBezierPath()
        
        let shadowPoint = makingPointsForShadow()
        
        if shadowPoint.count > 0 {
            for (index, points) in shadowPoint.enumerated() {
                if index == 0 {
                    pathForShadow.move(to: CGPoint(x: points.0, y: points.1))
                } else {
                    pathForShadow.addLine(to: CGPoint(x: points.0, y: points.1))
                }
            }
            
            pathForShadow.close()
            GraphStyle.shadowColor.setFill()
            pathForShadow.fill()
            
            
        }
        
    }


    let firstColor = UIColor(red: 1/255.0, green: 52/255.0, blue: 63/255.0, alpha: 1)
    let middleColor = UIColor(red: 1/255.0, green: 60/255.0, blue: 73/255.0, alpha: 1)
    let middleColor2 = UIColor(red: 1/255.0, green: 66/255.0, blue: 82/255.0, alpha: 1)
    let secondColor = UIColor(red: 2/255.0, green: 73/255.0, blue: 89/255.0, alpha: 1)
    

    
    
}

struct GraphStyle {
    
    // color
    static let xAxisColor = UIColor(red: 17/255.0, green: 103/255.0, blue: 122/255.0, alpha: 1)
    static let yAxisColor8 = UIColor(red: 229/255.0, green: 77/255.0, blue: 66/255.0, alpha: 1)
    static let yAxisColor12 = UIColor(red: 254/255.0, green: 205/255.0, blue: 94/255.0, alpha: 1)
    static let yAxisColor12Plus = UIColor(red: 57/255.0, green: 202/255.0, blue: 116/255.0, alpha: 1)
    static let dotColor = UIColor(red: 0/255.0, green: 143/255.0, blue: 136/255.0, alpha: 1)
    //static let shadowColor = UIColor(red: 2/255.0, green: 73/255.0, blue: 89/255.0, alpha: 0.6)
    static let shadowColor = UIColor(red: 250/255.0, green: 250/255.0, blue: 250/255.0, alpha: 0.1)
    //width
    static let yAxisWidth: CGFloat = 8
    static let xAxisWidth: CGFloat = 1
}












