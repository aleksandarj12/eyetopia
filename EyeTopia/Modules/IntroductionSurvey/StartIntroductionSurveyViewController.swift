//
//  StartIntroductionSurveyViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/14/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

protocol StartIntroductionSurveyDelegate {
    func didSelectNonDiagnosedFlow()
}

class StartIntroductionSurveyViewController: UIViewController {

    @IBOutlet weak var mainHolderView: UIView!
    
    var delegate : StartIntroductionSurveyDelegate!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
        mainHolderView.layer.cornerRadius = 4
        
        if let mainView = self.mainHolderView
        {
            mainView.layer.cornerRadius = 4
            mainView.clipsToBounds = true
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.diagnosed, bundle: nil)
        
        if(TransitionManager.sharedInstance.diagnosedWithDryEyeButton)
        {
            let controller : ContactLensIntroductionViewController = storyboard.instantiateViewController(withIdentifier: "ContactLensIntroductionViewController") as! ContactLensIntroductionViewController
            self.navigationController?.pushViewController(controller, animated: false)
            TransitionManager.sharedInstance.diagnosedWithDryEyeButton = false
        }
        
        
        if(TransitionManager.sharedInstance.didFinishTransition && TransitionManager.sharedInstance.storyboardID == "ExpirienceDryEyeSymptomViewController")
        {
            let controller : ExpirienceDryEyeSymptomViewController = storyboard.instantiateViewController(withIdentifier: "ExpirienceDryEyeSymptomViewController") as! ExpirienceDryEyeSymptomViewController
            TransitionManager.sharedInstance.setDidFinishTransition(flag: false)
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
            self.navigationController?.pushViewController(controller, animated: false)
        }
        if(TransitionManager.sharedInstance.didFinishTransition && TransitionManager.sharedInstance.storyboardID == "BlinkExamSurveyViewController")
        {
            let controller : BlinkExamSurveyViewController = self.storyboard?.instantiateViewController(withIdentifier: "BlinkExamSurveyViewController") as! BlinkExamSurveyViewController
            TransitionManager.sharedInstance.setDidFinishTransition(flag: false)
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
            self.navigationController?.pushViewController(controller, animated: false)
        }
    }
    
    
    //MARK: Setup Methods
    
    func setupExitCancel()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to cancel?", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes", style: .destructive) { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func setupJourneyStation()
    {
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
        UserManager.sharedInstance.setJourneyStation(self.navigationController!, controller)
    }
    
    func setupExitView()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to exit? You may continue later.", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes, exit", style: .destructive) { (action) -> Void in
            self.setupJourneyStation()
                NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentHomeKey), object: self)
            UserManager.sharedInstance.setDiagnosedJourneyCompetion(flag: false)
            UserManager.sharedInstance.setNonDiagnosedJourneyCompetion(flag: false)
            NotificationManager.sharedInstance.scheduleJourneyContinuationReminderNotification()
            Localytics.closeSession()
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    //MARK: Action Methods

    @IBAction func exitButtonTapped(_ sender: Any)
    {
        self.setupExitView()
//        let controller : ExitViewController = self.storyboard?.instantiateViewController(withIdentifier: "ExitViewController") as! ExitViewController
//        TransitionManager.sharedInstance.setContinueJourneyDescription(controllerDescription: "StartIntroductionSurveyViewController")
//
//        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any)
    {
        self.setupExitCancel()
    }
    
    @IBAction func backButtonTapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func noButtonTapped(_ sender: Any)
    {
//        TransitionManager.sharedInstance.setStoryboardID(storyboardID: "BlinkExamSurveyViewController")
//        TransitionManager.sharedInstance.setDidFinishTransition(flag: true)
        self.dismiss(animated: true, completion: nil)
        TransitionManager.sharedInstance.setDidFinishTransition(flag: true)
        self.delegate.didSelectNonDiagnosedFlow()
    }
    @IBAction func yesButtonTapped(_ sender: UIButton)
    {
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.diagnosed, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ContactLensIntroductionViewController") as? ContactLensIntroductionViewController
        
//        var nextView : UIView = UIView()
//        UIView.beginAnimations(nil, context: nil)
//        UIView.setAnimationCurve(UIViewAnimationCurve.linear)
//        UIView.setAnimationDuration(0.75)
//        self.navigationController?.pushViewController(controller!, animated: false)
//
//        UIView.setAnimationTransition(.curlDown, for: (self.navigationController?.view)!, cache: false)
//        UIView.commitAnimations()
//        nextView = nil
        
        
        
        
//
//        let transition : CATransition = CATransition()
//        transition.duration = 0.3
//
//        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
//        transition.type = kCATransitionPush
//        transition.subtype = kCATransitionFromRight
//        self.view.window?.layer.add(transition, forKey: nil)
        self.navigationController?.pushViewController(controller!, animated: false)
        
        
        
        
    }
    
}
