//
//  ContactLensIntroductionViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/14/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

class ContactLensIntroductionViewController: UIViewController {

    @IBOutlet weak var mainHolderViewContactLens: UIView!
    
    var isPushed : Bool = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)

        mainHolderViewContactLens.layer.cornerRadius = 4
        mainHolderViewContactLens.clipsToBounds = true
        UserManager.sharedInstance.setupDiagnosedNewJourney()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        Localytics.openSession()
//        Localytics.tagScreen("Contact Lenses")
    }

    @IBAction func cancelButtonTapped(_ sender: Any)
    {
        self.setupExitCancel()
    }
    
    func setupExitCancel()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to cancel?", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes", style: .destructive) { (action) -> Void in
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "DidCanceledSurvey")
            
            if(self.isPushed)
            {
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                self.dismiss(animated: false, completion: nil)
            }
            
            
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func setupDidFinishWithScreen()
    {
        if(self.isPushed)
        {
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "ExpirienceDryEyeSymptomViewController")

            self.navigationController?.popViewController(animated: false)
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: k.pushExpirienceDryEyeKey), object: self)
        }

    }
    
    @IBAction func yesButtonTapped(_ sender: Any)
    {

        if(self.checkTransition())
        {
        self.setupExpiriencedScreen()
        }
        else
        {
            self.setupDidFinishWithScreen()
        }
        
        UserManager.sharedInstance.setIsWearingContactLenses(flag: true)
    }
    
    @IBAction func noButtonTapped(_ sender: Any)
    {
        if(self.checkTransition())
        {
            self.setupExpiriencedScreen()
        }
        else
        {
            self.setupDidFinishWithScreen()
        }
        
        UserManager.sharedInstance.setIsWearingContactLenses(flag: false)
    }
    
    
    func checkTransition() -> Bool
    {
        if(TransitionManager.sharedInstance.storyboardID == "DiagnosedJourneyAgain")
        {
            return true
        }
        else
        {
            return false
        }
    }

    func setupExpiriencedScreen()
    {
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.diagnosed, bundle: nil)
        let controller : ExpirienceDryEyeSymptomViewController = storyboard.instantiateViewController(withIdentifier: "ExpirienceDryEyeSymptomViewController") as! ExpirienceDryEyeSymptomViewController
        let navController : UINavigationController = UINavigationController(rootViewController: controller)
        navController.setNavigationBarHidden(true, animated: false)
        self.present(navController, animated: false, completion: nil)
    }
    
}

