//
//  BackgroundLocationViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/26/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

protocol BackgroundLocationDelegate {
    func didNavigationBackgroundBack()
}

class BackgroundLocationViewController: UIViewController {

    var isMain : Bool = false
    var delegate : BackgroundLocationDelegate!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func backgroundLocationButtonTapped(_ sender: Any)
    {
        let option : [String : Any] = [String : Any]()
        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: option, completionHandler: nil)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        if(self.isMain)
        {
            self.isMain = false
        }
        else
        {
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backButtonTapped(_ sender: Any)
    {
        self.delegate.didNavigationBackgroundBack()
      self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
