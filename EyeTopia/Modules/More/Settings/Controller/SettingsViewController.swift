//
//  SettingsViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/26/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NotificationViewDelegate, BackgroundLocationDelegate {

    @IBOutlet weak var termsOfUseLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    var isMain : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: "SettingsTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingsTableViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didNavigationBackgroundBack()
    {
        self.isMain = true
    }
    
    func didNavigationBack()
    {
        self.isMain = true
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        if(self.isMain)
        {
            self.isMain = false
        }
        else
        {
            self.navigationController?.popViewController(animated: false)
        }
        if(TransitionManager.sharedInstance.storyboardID == "Notification")
        {
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
            let controller : NotificationViewController = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            controller.isMain = true
            controller.delegate = self
            UserManager.sharedInstance.isNotificationController = true
            self.navigationController?.pushViewController(controller, animated: false)
        }
        
        print("tuka")
    }
    
    //MARK: UITableView DataSource & Delegate
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : SettingsTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell") as! SettingsTableViewCell
        cell.setupCell(indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(indexPath.row == 0)
        {
            let controller : NotificationViewController = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            controller.isMain = true
            controller.delegate = self
            UserManager.sharedInstance.isNotificationController = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if(indexPath.row == 1)
        {
            let controller : BackgroundLocationViewController = self.storyboard?.instantiateViewController(withIdentifier: "BackgroundLocationViewController") as! BackgroundLocationViewController
            controller.isMain = true
            controller.delegate = self
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else
        {
            let alert : UIAlertController = UIAlertController(title: "", message: k.text.clearProductMatchMessage, preferredStyle: .alert)
            
            let noAction = UIAlertAction(title: "No", style: .cancel) { action in
                
            }
            
            let yesAction = UIAlertAction(title: "Yes", style: .default) { action in
                UserManager.sharedInstance.setLocationPlace(location: nil)
                UserManager.sharedInstance.removeBlinkExamResults()
                UserManager.sharedInstance.setSelectedEyeSymptom(eyeProblem: nil)
                UserManager.sharedInstance.removeProductMatchData()
                UserDefaults.standard.setValue(nil, forKey: k.services.mainLocationTitle)
            }
            
            alert.addAction(yesAction)
            alert.addAction(noAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func privacyNoticeButtonTapped(_ sender: Any)
    {
        let controller : MainViewController = MainViewController(optionShown: .privacyNotice)
        self.isMain = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func termsUseButtonTapped(_ sender: Any)
    {
        let controller : MainViewController = MainViewController(optionShown: .termsConditions)
        self.isMain = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
