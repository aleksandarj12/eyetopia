//
//  SettingsTableViewCell.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/26/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var bottomSeparatorHolderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(indexPath : IndexPath)
    {
        if(indexPath.row == 0)
        {
            self.titleLabel.text = k.settings.notifications
            self.imageView?.isHidden = false
            self.bottomSeparatorHolderView.isHidden = false
        }
        else if(indexPath.row == 1)
        {
            self.titleLabel.text = k.settings.backgroundLocation
            self.imageView?.isHidden = false
            self.bottomSeparatorHolderView.isHidden = false
        }
        else if(indexPath.row == 2)
        {
            self.titleLabel.text = k.settings.clearStoredData
            self.arrowImageView?.isHidden = true
            self.bottomSeparatorHolderView.isHidden = true
        }
    }
    
}
