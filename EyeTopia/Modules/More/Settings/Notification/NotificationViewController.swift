//
//  NotificationViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/26/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import PWSwitch
import UserNotifications
import MBProgressHUD
import Alamofire

protocol NotificationViewDelegate {
    func didNavigationBack()
}

@IBDesignable

class NotificationViewController: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var notificationScrollView: UIScrollView!
    
    @IBOutlet weak var journeyContinuationSwitch: PWSwitch!
    @IBOutlet weak var blinkExamRemainderSwitch: PWSwitch!
    @IBOutlet weak var dryEyePolenSwitch: PWSwitch!
    @IBOutlet weak var savingCardSwitch: PWSwitch!
    
    @IBOutlet weak var notGrantedTitle: UILabel!
    @IBOutlet weak var notGrantedMessage: UILabel!
    @IBOutlet weak var goToSettingsButton: UIButton!
    
    @IBOutlet weak var journeyContinuationRemainderLabel: UILabel!
    @IBOutlet weak var blinkExamRemainderLabel: UILabel!
    @IBOutlet weak var dryEyeIndexRemainderLabel: UILabel!
    @IBOutlet weak var savingCardRemainderLabel: UILabel!
    
    
    private var acessStatus = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    var isMain : Bool = false
    
    var delegate : NotificationViewDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addTargetsOnSwitch()
        

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        if(self.isMain)
        {
            self.isMain = false
        }
        else
        {
            self.navigationController?.popViewController(animated: false)
        }
        
        self.checkForAllowedAcessForNotification()
    }
    
    
    //MARK: Action Methods
    
    @IBAction func backButtonTapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
        self.delegate.didNavigationBack()
    }
    
    @IBAction func goToSettingsTapped(_ sender: UIButton) {
        let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
        UIApplication.shared.open(settingsUrl!, options: [:], completionHandler: nil)
        self.navigationController?.popViewController(animated: false)
    }
    
    
    //MARK: Setup methods
    

    
  
    
    private func checkForAllowedAcessForNotification () {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let userNotification = UNUserNotificationCenter.current()
        
        userNotification.getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                DispatchQueue.main.async {
                    self.acessStatus = true
                    self.setAllNotification()
                    self.hideNotGranted()
                    self.notificationScrollView.isScrollEnabled = false
                    self.changeTextColorOnNotificationLabels(enabled: true)
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
               
            } else {
                DispatchQueue.main.async {
                    self.acessStatus = false
                    self.setAllNotificationOff()
                    self.showNotGranted()
                    self.notificationScrollView.isScrollEnabled = true
                    self.changeTextColorOnNotificationLabels(enabled: false)
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }
        }
    }
    
    private func setAllNotificationOff() {
        
        journeyContinuationSwitch.setOn(false, animated: true)
        journeyContinuationSwitch.isEnabled = false
        
        blinkExamRemainderSwitch.setOn(false, animated: true)
        blinkExamRemainderSwitch.isEnabled = false

        savingCardSwitch.setOn(false, animated: true)
        savingCardSwitch.isEnabled = false
        
        dryEyePolenSwitch.setOn(false, animated: true)
        dryEyePolenSwitch.isEnabled = false
        
        
    }
    private func setAllNotification() {
        
        if NotificationManager.sharedInstance.getJourneyContinuationReminders() == true {
            journeyContinuationSwitch.setOn(true, animated: true)
        } else {
            journeyContinuationSwitch.setOn(false, animated: true)
        }
        
        if NotificationManager.sharedInstance.getBlinkExamReminders() == true {
            blinkExamRemainderSwitch.setOn(true, animated: true)
        } else {
            blinkExamRemainderSwitch.setOn(false, animated: true)
        }
        
        if NotificationManager.sharedInstance.getSavingCardDiscountNotifications() == true {
            savingCardSwitch.setOn(true, animated: true)
        } else {
            savingCardSwitch.setOn(false, animated: true)
        }
        
        if NotificationManager.sharedInstance.getDryEyeIndexAndPollenForecastNotices() == true {
            dryEyePolenSwitch.setOn(true, animated: true)
        } else {
            dryEyePolenSwitch.setOn(false, animated: true)
        }

      

    }
    
    private func hideNotGranted() {
        notGrantedTitle.isHidden = true
        notGrantedMessage.isHidden = true
        goToSettingsButton.isHidden = true
    }
    private func showNotGranted() {
        notGrantedTitle.isHidden = false
        notGrantedMessage.isHidden = false
        goToSettingsButton.isHidden = false
    }
    
    private func changeTextColorOnNotificationLabels(enabled: Bool) {
        if enabled == true {
            self.journeyContinuationRemainderLabel.textColor = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1)
            self.blinkExamRemainderLabel.textColor = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1)
            self.savingCardRemainderLabel.textColor = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1)
            self.dryEyeIndexRemainderLabel.textColor = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1)
        } else {
            self.journeyContinuationRemainderLabel.textColor = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 0.5)
            self.blinkExamRemainderLabel.textColor = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 0.5)
            self.savingCardRemainderLabel.textColor = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 0.5)
            self.dryEyeIndexRemainderLabel.textColor = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 0.5)
        }
    }
    
    //Action methods
    
    private func addTargetsOnSwitch() {
        journeyContinuationSwitch.addTarget(self, action: #selector(tappedJourneyContinuationSwitch), for: .valueChanged)
        blinkExamRemainderSwitch.addTarget(self, action: #selector(tappedBlinkExamRemainderSwitch), for: .valueChanged)
        savingCardSwitch.addTarget(self, action: #selector(tappedSavingCardSwitch), for: .valueChanged)
        dryEyePolenSwitch.addTarget(self, action: #selector(dryEyePolenSwitchTapped), for: .valueChanged)
    }
    
    @objc func dryEyePolenSwitchTapped()
    {
        var url : String = ""
        if(dryEyePolenSwitch.on)
        {
            NotificationManager.sharedInstance.setDryEyeIndexAndPollenForecastNotices(flag: true)
            url = k.services.subscribePushNotification
        }
        else
        {
            NotificationManager.sharedInstance.setDryEyeIndexAndPollenForecastNotices(flag: false)
            url = k.services.unsubscribePushNotification
        }
        

        let header : HTTPHeaders = [
            "Accept" : "application/json"
        ]
        let deviceToken : String = UserDefaults.standard.value(forKey: k.user.deviceToken) as! String
        let params : [String: Any] = [
            "device_token": deviceToken
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: header)
            .responseJSON { response in
                if(response.error == nil)
                {
                    print("SUCCESSFUL")
                    
                }
                else
                {
                    print("FAILED")
                }
        }
    }
    
    @objc  func tappedJourneyContinuationSwitch() {
        if journeyContinuationSwitch.on == true {
            NotificationManager.sharedInstance.setJourneyContinuationReminders(flag: true)
           
        } else {
            NotificationManager.sharedInstance.cancelNotifictaion(withIndentifier: "JourneyContinuationReminderNotification")
            NotificationManager.sharedInstance.setJourneyContinuationReminders(flag: false)
        }
    }
    
    @objc func tappedBlinkExamRemainderSwitch() {
        if blinkExamRemainderSwitch.on == true {
            NotificationManager.sharedInstance.setBlinkExamReminders(flag: true)
        } else {
            NotificationManager.sharedInstance.cancelNotifictaion(withIndentifier: "BlinkExamReminders")
            NotificationManager.sharedInstance.setBlinkExamReminders(flag: false)
        }
    }
    @objc func tappedSavingCardSwitch() {
        if savingCardSwitch.on == true {
            NotificationManager.sharedInstance.setSavingCardDiscountNotifications(flag: true)
        } else {
            NotificationManager.sharedInstance.cancelNotifictaion(withIndentifier: "SavingCardDiscountNotifications")
            NotificationManager.sharedInstance.setSavingCardDiscountNotifications(flag: false)
        }
    }
    
    
    
    
}







