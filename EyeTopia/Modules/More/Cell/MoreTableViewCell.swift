//
//  MoreTableViewCell.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/19/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class MoreTableViewCell: UITableViewCell {

    @IBOutlet weak var footerHolderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    
    func setupCell(indexPath : IndexPath)
    {
        if(indexPath.row == 0)
        {
            self.mainImageView.image = UIImage(named: "icnProductMatchSmall")
            self.titleLabel.text = "Product Match"
            self.footerHolderView.isHidden = false
        }
        else if(indexPath.row == 1)
        {
            self.mainImageView.image = UIImage(named: "graphicCubeBlinkingSmall")
            self.titleLabel.text = "Blink Exam"
            self.footerHolderView.isHidden = false
        }
        else if(indexPath.row == 2)
        {
            self.mainImageView.image = UIImage(named: "icnSettingsSmall")
            self.titleLabel.text = "Settings"
            self.footerHolderView.isHidden = true
        }
    }
}
