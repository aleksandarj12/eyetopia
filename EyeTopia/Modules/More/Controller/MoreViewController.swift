//
//  MoreViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/19/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var isMain : Bool = false
    var presenter = HomePresenter()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: "MoreTableViewCell", bundle: nil), forCellReuseIdentifier: "MoreTableViewCell")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        if(self.isMain)
        {
            self.navigationController?.popViewController(animated: false)
            self.isMain = false
        }
        if(TransitionManager.sharedInstance.storyboardID == "Notification")
        {
            let controller : SettingsViewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            controller.isMain = true
            self.navigationController?.pushViewController(controller, animated: false)
        }
    }
    

    // MARK: - UITableView DataSource & Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
         let cell : MoreTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "MoreTableViewCell") as! MoreTableViewCell
        
        cell.setupCell(indexPath: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(indexPath.row == 0)
        {
            let controller : LatestProductMatchViewController = self.storyboard?.instantiateViewController(withIdentifier: "LatestProductMatchViewController") as! LatestProductMatchViewController
            controller.isMain = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if(indexPath.row == 1)
        {
            let controller : BlinkExamDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "BlinkExamDetailsViewController") as! BlinkExamDetailsViewController
            controller.isMain = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else
        {
            let controller : SettingsViewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            controller.isMain = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }

}
