//
//  TermsOfUseVCViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 6/11/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class TermsOfUseVCViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var myTableView: UITableView!
  
    private var termsOfUseLabel = UILabel()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myTableView.delegate = self
        myTableView.dataSource = self
        
        
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.estimatedRowHeight = 120
    }

   
 

   
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    // Settup methods
    
//    private func setUpLabel () {
//    self.termsOfUseLabel = UILabel(frame: CGRect(x: 20, y: 20, width: self.view.bounds.size.width - 40, height: self.view.bounds.height))
//        self.termsOfUseLabel.numberOfLines = 0
//        let fontColor = UIColor(red: 74, green: 74, blue: 74, alpha: 1)
//        let atributedTerms = NSAttributedString(string: k.Terms.terms, attributes: [
//            .font: UIFont(name: "AvenirNext-Medium", size: 14.0)!,
//            .foregroundColor:fontColor,
//            .kern: 0.0
//            ])
//        self.termsOfUseLabel.text = k.Terms.test
//        self.termsOfUseLabel.sizeToFit()
//        myScrollView.addSubview(self.termsOfUseLabel)
//        myScrollView.contentSize = CGSize(width: 0, height: (self.termsOfUseLabel.bounds.height))
//    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      let cell = tableView.dequeueReusableCell(withIdentifier: "termsAndNotice") as! TermsAndNoticeCellTableViewCell
        
       cell.myLabel.text = k.Terms.terms
            
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    
    
    
    
    
    
    
}
