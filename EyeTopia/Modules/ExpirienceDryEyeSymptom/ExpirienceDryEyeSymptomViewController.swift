//
//  ExpirienceDryEyeSymptomViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/3/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

class ExpirienceDryEyeSymptomViewController: UIViewController, SurveyMainDelegate {
    
    var isTransitioned : Bool = false
    
    @IBOutlet weak var bottomHolderView: UIView!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var progressBarTrailing: NSLayoutConstraint!
    @IBOutlet weak var skyImageView: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var topHolderView2: UIView!
    @IBOutlet weak var bottomHolderView1: UIView!
    @IBOutlet weak var bottomHolderView2: UIView!
    @IBOutlet weak var topHolderView1: UIView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
        TransitionManager.sharedInstance.setDidFinishTransition(flag: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

        
        self.setupFirstDialogHolderView()
        
        if(TransitionManager.sharedInstance.didFinishTransition && TransitionManager.sharedInstance.storyboardID == "TransitionViewController11")
        {
            TransitionManager.sharedInstance.setDidFinishTransition(flag: false)
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
            let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.diagnosed, bundle: nil)
            let controller : TransitionViewController = storyboard.instantiateViewController(withIdentifier: "TransitionViewController11") as! TransitionViewController
            controller.storyboardTitle = "TransitionViewController11"
            self.navigationController?.pushViewController(controller, animated: false)
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Localytics.tagScreen("Expirience Dry Eye Symptom")
    }
    
    
    //MARK: SurveyMainDelegate
    
    func didFinishSurveyMain()
    {
        let controller : TransitionViewController = self.storyboard?.instantiateViewController(withIdentifier: "TransitionViewController11") as! TransitionViewController
        controller.storyboardTitle = "TransitionViewController11"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    //MARK: Setup & Support Methods
    
    func setupJourneyStation()
    {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        let controller = mainStoryboard.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
        UserManager.sharedInstance.setJourneyStation(self.navigationController!, controller)
    }
    
    func setupExitView()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to exit? You may continue later.", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes, exit", style: .destructive) { (action) -> Void in
            self.setupJourneyStation()
                NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentHomeKey), object: self)
            UserManager.sharedInstance.setDiagnosedJourneyCompetion(flag: false)
            UserManager.sharedInstance.setNonDiagnosedJourneyCompetion(flag: false)
            NotificationManager.sharedInstance.scheduleJourneyContinuationReminderNotification()
            Localytics.closeSession()
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func setupFirstDialogHolderView()
    {
        if let firstHolderView = self.topHolderView1 {
            UIView.animate(withDuration: 0.7, animations: {
                firstHolderView.alpha = 1
            })
        }
    }
    
    
    //MARK: Action Methods
    
    @IBAction func exitButtonTapped(_ sender: Any)
    {
//        let controller : ExitViewController = self.storyboard?.instantiateViewController(withIdentifier: "ExitViewController") as! ExitViewController
//        TransitionManager.sharedInstance.setContinueJourneyDescription(controllerDescription: "ExpirienceDryEyeSymptomViewController")
//        self.present(controller, animated: true, completion: nil)
        
        
        self.setupExitView()
        
    }
    
    @IBAction func backButtonTapped(_ sender: Any)
    {
        if(self.isTransitioned)
        {
            self.isTransitioned = false
            
            UIView.animate(withDuration: 0.7, animations: {
                self.topHolderView2.alpha = 0
                
            }) { (flag : Bool) in
                
                
                
                UIView.animate(withDuration: 4, delay: 1, options: .allowAnimatedContent, animations: {
                    self.skyImageView.frame.origin.x = 0.0
                }) { (flag : Bool) in
                    UIView.animate(withDuration: 0.7, animations: {
                        self.topHolderView1.alpha = 1
                    })
                }
                UIView.animate(withDuration: 4, animations: {
                    self.backgroundImageView.frame.origin.x = -390.0
                }, completion: { (flag : Bool) in
                    self.bottomHolderView1.alpha = 1
                    
                    
                })
                
            }
            
            
        }
        else
        {
            self.navigationController?.popViewController(animated: false)
            
        }
        progressBarTrailing.constant = 240
    }
    
    @IBAction func continueButtonTapped(_ sender: Any)
    {
        self.isTransitioned = true
        
        
        UIView.animate(withDuration: 0.7, animations: {
            self.topHolderView1.alpha = 0
            
        }) { (flag : Bool) in
            
            
            
            UIView.animate(withDuration: 4, delay: 1, options: .allowAnimatedContent, animations: {
                self.skyImageView.frame.origin.x = -590.0
            }) { (flag : Bool) in
                UIView.animate(withDuration: 0.7, animations: {
                    self.topHolderView2.alpha = 1
                    //                    self.topHolderView2.alpha = 1
                })
            }
            UIView.animate(withDuration: 4, animations: {
                self.backgroundImageView.frame.origin.x = -980.0
            }, completion: { (flag : Bool) in
                self.bottomHolderView1.alpha = 0
                
            })
            
        }
        progressBarTrailing.constant = 140
    }
    
    @IBAction func startSymptomSurveyViewController(_ sender: Any)
    {
        UserManager.sharedInstance.setSelectedEyeSymptom(eyeProblem: nil)
        let storyboard : UIStoryboard = UIStoryboard(name: "DiagnosedFlow", bundle: nil)
        let controller : SymptomSurveyViewController = storyboard.instantiateViewController(withIdentifier: "SurveyMainViewController") as! SymptomSurveyViewController
//        let navController : UINavigationController = UINavigationController(rootViewController: controller)
//        navController.setNavigationBarHidden(true, animated: false)
        controller.modalPresentationStyle = .overCurrentContext
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }

}
