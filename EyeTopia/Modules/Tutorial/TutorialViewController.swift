//
//  TutorialViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/5/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController, UIScrollViewDelegate {
    
    
    private var skipButton = UIButton(type: UIButtonType.custom)
    private var cancelButton = UIButton(type: UIButtonType.custom)
    
    private var imageON = UIImage(named: "Ellipse 2")
    private var imageOFF = UIImage(named: "Ellipse 1")
    
    
    private var dot1 = UIImageView(image: UIImage(named: "Ellipse 2"))
    private var dot2 = UIImageView(image: UIImage(named: "Ellipse 1"))
    private var dot3 = UIImageView(image: UIImage(named: "Ellipse 1"))
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        let centarForDot2 = CGPoint(x:self.view.center.x, y:self.view.center.y + 150)
        dot2.center = centarForDot2
        dot2.contentMode = .center
        self.view.addSubview(dot2)

        let centarForDot1 = CGPoint(x:self.view.center.x - 30, y:self.view.center.y + 150)
        dot1.center = centarForDot1
        dot1.contentMode = .center
        self.view.addSubview(dot1)

        let centarForDot3 = CGPoint(x:self.view.center.x + 30, y:self.view.center.y + 150)
        dot3.center = centarForDot3
        dot3.contentMode = .center
        self.view.addSubview(dot3)
        
        let scrollViewForTutorial = UIScrollView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height))
        scrollViewForTutorial.contentSize = CGSize(width: self.view.bounds.size.width * 3, height: self.view.bounds.size.height)
        scrollViewForTutorial.bounces = false
        scrollViewForTutorial.showsHorizontalScrollIndicator = false
        scrollViewForTutorial.showsVerticalScrollIndicator = false
        scrollViewForTutorial.isPagingEnabled = true
        self.view .addSubview(scrollViewForTutorial)
        if #available(iOS 11.0, *) {
            scrollViewForTutorial.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.never
        } else {
            // Fallback on earlier versions
        }
        scrollViewForTutorial.delegate = self
        
        //FORM 1
        let form1 = UIView(frame: CGRect(x: self.view.bounds.origin.x, y: self.view.bounds.origin.y, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        scrollViewForTutorial .addSubview(form1)
        
        
        // welcome label
        let welcomeLabel = UILabel()
        welcomeLabel.text = "Welcome traveller"
        welcomeLabel.textAlignment = .center
        welcomeLabel.font = UIFont(name: "Nunito-ExtraBold", size: 24)
        welcomeLabel.textColor = UIColor(red: 0/255.0, green: 143/255.0, blue: 136/255.0, alpha: 1)
        let center = CGPoint(x: self.view.center.x, y: self.view.center.y - 160)
        welcomeLabel.sizeToFit()
        welcomeLabel.center = center
        form1.addSubview(welcomeLabel)
        
        //Message label
        let messagelabel = UILabel(frame: CGRect(x:10, y: 100, width: 300, height: 100))
        messagelabel.text = "You are here because you have  been diagnosed with dry eye or are experiencing dry eye discomfort. You may have tried various eye solutions in the past, but have not found an answer for your dry eye problems."
        messagelabel.textAlignment = .center
        messagelabel.font = UIFont(name: "AvenirNext-Medium", size: 16)
        messagelabel.numberOfLines = 0
        messagelabel.sizeToFit()
        messagelabel.center = CGPoint(x: self.view.center.x, y: welcomeLabel.center.y + (welcomeLabel.bounds.size.height / 2) + (messagelabel.bounds.size.height / 2) + 10)
        form1 .addSubview(messagelabel)
        
        // swipe label
        
        let swipeLabel = UILabel()
        swipeLabel.text = "Swipe to continue"
        swipeLabel.textAlignment = .center
        swipeLabel.font = UIFont(name: "AvenirNext-Medium", size: 14)
        swipeLabel.textColor = UIColor(red: 127/255.0, green: 140/255.0, blue: 141/255.0, alpha: 1)
        let swipeLabelCenter = CGPoint(x:self.view.center.x, y: dot2.center.y + 40)
        swipeLabel.sizeToFit()
        swipeLabel.center = swipeLabelCenter
        form1.addSubview(swipeLabel)
        
        
        //FORM 2
        let form2 = UIView(frame: CGRect(x: self.view.frame.origin.x + self.view.frame.size.width, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.height))
        scrollViewForTutorial .addSubview(form2)
        
        // Seeking answer label
        let seekingAnswerlbl = UILabel()
        seekingAnswerlbl.text = "Seeking answers"
        seekingAnswerlbl.font = UIFont(name: "Nunito-ExtraBold", size: 24)
        seekingAnswerlbl.textColor = UIColor(red: 0/255.0, green: 143/255.0, blue: 136/255.0, alpha: 1)
        seekingAnswerlbl.sizeToFit()
        let centerForSALabel = CGPoint(x: self.view.center.x, y: self.view.center.y - 160)
        seekingAnswerlbl.center = centerForSALabel
        form2 .addSubview(seekingAnswerlbl)
        
        //Message seeking answer
        let messageSeekingAnswerLabel = UILabel(frame: CGRect(x: 30, y: 30, width: 300, height: 100))
        messageSeekingAnswerLabel.text = "You will embark on a journey to reveal the most about your dry eye condition and  the SYSTANE® product that best fits your needs."
        messageSeekingAnswerLabel.font = UIFont(name: "AvenirNext-Medium", size: 16)
        messageSeekingAnswerLabel.textAlignment = .center
        messageSeekingAnswerLabel.numberOfLines = 0
        messageSeekingAnswerLabel.sizeToFit()
        let centerForMSAlabel = CGPoint(x: self.view.center.x, y: seekingAnswerlbl.center.y + (seekingAnswerlbl.bounds.size.height / 2) + (messageSeekingAnswerLabel.bounds.size.height / 2) + 10)
        messageSeekingAnswerLabel.center = centerForMSAlabel
        form2.addSubview(messageSeekingAnswerLabel)
        
        
        // image for seeking answer
        let cubeImage = UIImageView(image: UIImage(named: "graphicCubeIntro"))
        let cubeImageCenter = CGPoint(x: messageSeekingAnswerLabel.center.x + 50, y: messageSeekingAnswerLabel.frame.origin.y + messageSeekingAnswerLabel.bounds.size.height + (cubeImage.bounds.size.height / 2) + 20)
        cubeImage.center = cubeImageCenter
        form2.addSubview(cubeImage)

        
        
        //FORM 3
        let form3 = UIView(frame: CGRect(x: self.view.frame.origin.x + (self.view.frame.size.width * 2), y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.height))
        scrollViewForTutorial .addSubview(form3)
        
        // Eye opening label
        let eyeOpeningLabel = UILabel()
        eyeOpeningLabel.text = "Eye opening"
        eyeOpeningLabel.font = UIFont(name: "Nunito-ExtraBold", size: 24)
        eyeOpeningLabel.textColor = UIColor(red: 0/255.0, green: 143/255.0, blue: 136/255.0, alpha: 1)
        eyeOpeningLabel.sizeToFit()
        let centerForEyeOpeninglabel = CGPoint(x: self.view.center.x, y: self.view.center.y - 160)
        eyeOpeningLabel.center = centerForEyeOpeninglabel
        form3.addSubview(eyeOpeningLabel)
        
        // eyeopening label
        let eyeOpeningMessage = UILabel(frame: CGRect(x: 10, y: 10, width: 300, height: 100))
        eyeOpeningMessage.text = "Along the way, you will encounter various pit stops that will ask you to answer questions and perform tasks. Your answers to the questions and results of the tasks will help uncover if and what SYSTANE® product would suit you best. \n \n \nAre you ready to begin? "
        eyeOpeningMessage.textAlignment = .center
        eyeOpeningMessage.font = UIFont(name: "AvenirNext-Medium", size: 16)
        eyeOpeningMessage.textColor = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1)
        eyeOpeningMessage.numberOfLines = 0
        eyeOpeningMessage.sizeToFit()
        let eyeOpeningCenter = CGPoint(x: self.view.center.x, y:eyeOpeningLabel.center.y + (eyeOpeningLabel.bounds.size.height / 2) + (eyeOpeningMessage.bounds.size.height / 2) + 10)
        eyeOpeningMessage.center = eyeOpeningCenter
        form3 .addSubview(eyeOpeningMessage)
        
        
 
        //begin jurney button
        
        let beginJourneyButton = UIButton(type: UIButtonType.custom)
        beginJourneyButton.frame.size = CGSize(width: 250, height: 60)
        beginJourneyButton.setBackgroundImage(UIImage(named: "btnDefaultActive"), for: UIControlState.normal)
        
        beginJourneyButton.center = CGPoint(x: self.view.center.x, y: self.view.bounds.size.height - ((beginJourneyButton.bounds.size.height / 2) + 50))
        beginJourneyButton .setTitle("Begin your journey", for: UIControlState.normal)
        beginJourneyButton.titleLabel?.font = UIFont(name: "AvenirNext-Bold", size: 18)
        beginJourneyButton.titleLabel?.textAlignment = .center
        beginJourneyButton .setTitleColor(UIColor .white, for: UIControlState.normal)
        beginJourneyButton .addTarget(self, action: #selector(beginJurney), for: UIControlEvents.touchUpInside)
        beginJourneyButton.contentEdgeInsets.bottom = 3
        form3.addSubview(beginJourneyButton)
        
        
        //FORM4 VIEW FOR BUTTONS SKIP AND CANCEL
        let form4 = UIView(frame: CGRect(x: 0, y: 20, width: self.view.bounds.size.width, height: self.view.bounds.size.height / 8))
        self.view .addSubview(form4)
        
        cancelButton.frame = CGRect(x: 20, y: 20, width: 45, height: 20)
        cancelButton .setTitle("Cancel", for: UIControlState.normal)
        cancelButton.titleLabel?.font = UIFont(name: "AvenirNext-Medium", size: 14)
        cancelButton .setTitleColor(UIColor(red: 0/255.0, green: 143/255.0, blue: 136/255.0, alpha: 1), for: UIControlState.normal)
        cancelButton .addTarget(self, action: #selector(cancelButtonPressed), for: UIControlEvents.touchUpInside)
        form4.addSubview(cancelButton)
        
        skipButton.frame = CGRect(x: form4.frame.size.width - 65, y: 20, width: 45, height: 20)
        skipButton .setTitle("Skip", for: UIControlState.normal)
        skipButton.titleLabel?.font = UIFont(name: "AvenirNext-Medium", size: 14)
        skipButton .setTitleColor(UIColor(red: 0/255.0, green: 143/255.0, blue: 136/255.0, alpha: 1), for: UIControlState.normal)
        skipButton .addTarget(self, action: #selector(beginJurney), for: UIControlEvents.touchUpInside)
        form4.addSubview(skipButton)
        
        self.setupStyle()
        
    }
    
    @objc func cancelButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func beginJurney()
    {   NotificationManager.sharedInstance.scheduleJourneyContinuationReminderNotification()
        TransitionManager.sharedInstance.setDidFinishTransition(flag: true)
        TransitionManager.sharedInstance.setStoryboardID(storyboardID: "GuideMainViewController")
        self.dismiss(animated: true, completion: nil)
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        let controller : GuideMainViewController = storyboard.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset == CGPoint(x: self.view.frame.origin.x, y: self.view.frame.origin.y) {
            dot1.image = imageON
            dot2.image = imageOFF
            dot3.image = imageOFF
            skipButton.isHidden = false
        } else if scrollView.contentOffset == CGPoint(x: self.view.bounds.size.width, y: self.view.frame.origin.y) {
            dot1.image = imageOFF
            dot2.image = imageON
            dot3.image = imageOFF
            skipButton.isHidden = false
        } else if scrollView.contentOffset == CGPoint(x: self.view.bounds.size.width * 2, y: self.view.frame.origin.y) {
            dot1.image = imageOFF
            dot2.image = imageOFF
            dot3.image = imageON
            skipButton.isHidden = true
        }
       
        print(scrollView.contentOffset)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    
    // MARK: - Setup & Support Methods
    
    func setupStyle()
    {
        //        self.cancelButton.setTitleColor(Style.primaryColor, for: .normal)
        //        self.skipButton.setTitleColor(Style.primaryColor, for: .normal)
    }
    
    
}
