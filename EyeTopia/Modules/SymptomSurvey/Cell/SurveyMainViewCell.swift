//
//  SurveyMainViewCell.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/7/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class SurveyMainViewCell: UITableViewCell {

    
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var symptomImage: UIImageView!
    @IBOutlet weak var symptomTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }
}
