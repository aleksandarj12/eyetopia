//
//  SurveyMainViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/6/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

protocol SurveyMainDelegate {
    func didFinishSurveyMain()
}

class SymptomSurveyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var continueButton: RoundedButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    var selectedIndexPath : IndexPath!
    var delegate : SurveyMainDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        holderView.layer.cornerRadius = 4
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.continueButton.isEnabled = false
        
        
        self.tableView.register(UINib(nibName: "SurveyMainViewCell", bundle: nil), forCellReuseIdentifier: "SurveyMainViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Localytics.tagScreen("Symptom Survey")
    }
    
    //MARK: UITableView DataSource & Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return DataManager.sharedInstance.getEyeSymptomsList().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : SurveyMainViewCell = self.tableView.dequeueReusableCell(withIdentifier: "SurveyMainViewCell") as! SurveyMainViewCell
        let eyeProblem : EyeProblem = DataManager.sharedInstance.getEyeSymptomsList()[indexPath.row]
        
        cell.symptomTitle.text = eyeProblem.title
        cell.symptomImage.image = eyeProblem.image
        
        
        if(UserManager.sharedInstance.getSelectedEyeSymptom()?.id == eyeProblem.id)
        {
            cell.backImageView.image = UIImage(named: "btnSymptomOn")
            cell.symptomTitle.textColor = UIColor.white
        }
        else
        {
            cell.backImageView.image = UIImage(named: "btnSymptomOff")
            cell.symptomTitle.textColor = Style.navyDark
        }
        
        
//        if(self.selectedEyeProblems.count > 0)
//        {
//            for eyeIndex : EyeProblem in self.selectedEyeProblems
//            {
//                if(eyeIndex.id == indexPath.row)
//                {
//                    cell.backgroundColor = UIColor.purple
//                    self.selectedEyeProblems.remove(at: 0)
//                    break
//                }
//                else
//                {
//                    cell.backgroundColor = UIColor.white
//                    break
//                }
//            }
//        }
//        else
//        {
//            cell.backgroundColor = UIColor.white
//        }
        
            
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
//        self.setupSelectedEyeProblems(index: indexPath.row)
//        let cell : SurveyMainViewCell = tableView.cellForRow(at: indexPath) as! SurveyMainViewCell
//        cell.backgroundColor = Style.selectedCellBackgroundColor
        
        let eyeProblem : EyeProblem = DataManager.sharedInstance.getEyeSymptomsList()[indexPath.row]
        self.selectedIndexPath = indexPath
        UserManager.sharedInstance.setSelectedEyeSymptom(eyeProblem: eyeProblem)
        self.tableView.reloadData()
        self.continueButton.isEnabled = true
        
    }
    
    
    //MARK: Setup & Support Methods
    
    func setupExitCancel()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to cancel?", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes", style: .destructive) { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    
    //MARK: Action Methods
    
    @IBAction func cancelButtonTapped(_ sender: Any)
    {
        self.setupExitCancel()
    }
    
    @IBAction func continueButtonTapped(_ sender: Any)
    {
        self.continueButton.isEnabled = false
        self.dismiss(animated: true, completion: nil)
        self.delegate.didFinishSurveyMain()
    }
}
