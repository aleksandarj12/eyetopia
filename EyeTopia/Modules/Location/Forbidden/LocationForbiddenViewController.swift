//
//  LocationForbiddenViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/13/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import GoogleMaps

class LocationForbiddenViewController: UIViewController, CLLocationManagerDelegate {

    var locationManager : CLLocationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.locationManager.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(self.checkAuthorization())
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        if(self.checkAuthorization())
        {
            self.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func cancelButtonTapped(_ sender: Any)
    {
        self.setupExitCancel()
    }
    
    @IBAction func settingsButtonTapped(_ sender: Any)
    {
        let option : [String : Any] = [String : Any]()
        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: option, completionHandler: nil)
        
        
//        UIApplication.shared.open(URL(string: "App-Prefs:root=Privacy&path=LOCATION")!, options: option, completionHandler: nil)

    }
    
    func setupExitCancel()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to cancel?", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes", style: .destructive) { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "ForbiddenScreenFail")
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func checkAuthorization() -> Bool
    {
        if(CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse)
        {
            return true
        }
        else
        {
            return false
        }
    }
}
