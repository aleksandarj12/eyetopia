//
//  SearchLocationViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 4/12/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class SearchLocationViewController: UIViewController {

    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchLocationTextField: UITextField!
    @IBOutlet weak var holderViewForSearchTextField: UIView!
    @IBOutlet weak var holderViewForSearchLocationTextField: UIView!
    @IBOutlet weak var currnetLocationButton: UIButton!
    @IBOutlet weak var arrowImg: UIImageView!
    
    var presenter : LocationDetailsPresenter = LocationDetailsPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.delegate = self
        searchLocationTextField.delegate = self
        
        holderView.layer.cornerRadius = 4
        self.view.backgroundColor = UIColor(red:0.02, green:0.02, blue:0.06, alpha:0.4)
        
        
        holderViewForSearchTextField.backgroundColor = UIColor(red: 243/255.0, green: 243/255.0, blue: 243/255.0, alpha: 1)
        holderViewForSearchTextField.layer.borderWidth = 0
        holderViewForSearchTextField.layer.cornerRadius = 15
        holderViewForSearchTextField.clipsToBounds = true
        searchTextField.backgroundColor = UIColor.clear
        
        holderViewForSearchLocationTextField.backgroundColor = UIColor(red: 243/255.0, green: 243/255.0, blue: 243/255.0, alpha: 1)
        holderViewForSearchLocationTextField.layer.borderWidth = 0
        holderViewForSearchLocationTextField.layer.cornerRadius = 15
        holderViewForSearchLocationTextField.clipsToBounds = true
        searchLocationTextField.backgroundColor = UIColor.clear
        
        searchTextField.becomeFirstResponder()
    }

   
    // Actions
    @IBAction func deleteButttonPressed(_ sender: UIButton) {
        searchTextField.text = ""
    }
    
    
    @IBAction func useCurrentLocationPressed(_ sender: UIButton) {
        
            searchLocationTextField.text = ""
            searchLocationTextField.text = "Current location"
            searchLocationTextField.textColor = UIColor(red: 74/255.0, green: 144/255.0, blue: 226/255.0, alpha: 1)
            arrowImg.isHidden = true
            currnetLocationButton.isHidden = true
            UserManager.sharedInstance.setIsCurrentLocationActive(false)
        
    }
    
    @IBAction func searchButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    

}

extension SearchLocationViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var newString = textField.text
        
        if(string == "")
        {
            textField.text?.removeLast()
        }
        else
        {
            newString?.append(string)
        }
        
        if(textField == searchTextField)
        {
            self.presenter.getSearch(newString!)
        }
        else if (textField == searchLocationTextField && searchLocationTextField.text == "Current location")
        {
            searchLocationTextField.text = ""
            arrowImg.isHidden = false
            currnetLocationButton.isHidden = false
            searchLocationTextField.textColor = .black
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        if textField == searchLocationTextField {
            arrowImg.isHidden = false
            currnetLocationButton.isHidden = false
            searchLocationTextField.textColor = .black
        }
        return true
    }
    
}

















