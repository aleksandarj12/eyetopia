//
//  LocatorOptionViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/18/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

enum SearchOption {
    case retailer
    case eyeCareProfessionals
}

class LocatorOptionViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserManager.sharedInstance.searchLocationResult = ""
        if(TransitionManager.sharedInstance.storyboardID == "LocationDetailsViewController")
        {
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
            self.setupLocationDetailsViewController()
        }
    }
    
    
    //MARK: Setup & Support Methods
    
    func setupLocationDetailsViewController()
    {
        let controller : LocationDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "LocationDetailsViewController") as! LocationDetailsViewController
        controller.isMain = true
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    //MARK: Action Methods
    
    @IBAction func systaneRetailerButtonTapped(_ sender: Any)
    {
        self.setupLocationDetailsViewController()
        UserManager.sharedInstance.setSearchOption(searchOption: .retailer)
    }
    
    @IBAction func eyeCareProfessionalButtonTapped(_ sender: Any)
    {
        self.setupLocationDetailsViewController()
        UserManager.sharedInstance.setSearchOption(searchOption: .eyeCareProfessionals)
    }
    
   

}
