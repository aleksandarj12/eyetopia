//
//  ProvideLocationViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/12/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import GooglePlaces
import Localytics

class ProvideLocationViewController: UIViewController, SearchPlaceDelegate {

    @IBOutlet weak var automaticLocateButton: UIButton!
    @IBOutlet weak var cityZipCodeButton: UIButton!
    
    
//    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cityZipCodeButton.layer.borderWidth = 1
        self.cityZipCodeButton.layer.cornerRadius = 2
        self.cityZipCodeButton.clipsToBounds = true
        self.cityZipCodeButton.layer.borderColor = Style.switchThumb.off.cgColor
        
        
       
        
//        searchController = UISearchController(searchResultsController: resultsViewController)
//        searchController?.searchResultsUpdater = resultsViewController
        
        // Put the search bar in the navigation bar.
        searchController?.searchBar.sizeToFit()
        navigationItem.titleView = searchController?.searchBar
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        // Prevent the navigation bar from being hidden when searching.
        searchController?.hidesNavigationBarDuringPresentation = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(TransitionManager.sharedInstance.storyboardID == "LocationViewController")
        {
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
            let controller : LocationViewController = self.storyboard?.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
            controller.isLocationProvided = true
            self.navigationController?.pushViewController(controller, animated: false)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Localytics.tagScreen("Provide Location")
    }
    
    
    //MARK: SearchPlaceDelegate
    
    func didFindPlace(_ location: LocationDetail)
    {
        let controller : LocationViewController = self.storyboard?.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
        controller.isLocationProvided = true
        controller.locationDetail = location
        let locationC : CLLocation = CLLocation(latitude: (location.coordinates?.latitude)!, longitude: (location.coordinates?.longitude)!)
        UserManager.sharedInstance.setLocationPlace(location: locationC)
        UserManager.sharedInstance.isCurrentLocationActive = false
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func didSelectCurrentLocation()
    {
        UserManager.sharedInstance.setIsCurrentLocationActive(true)
        self.setupLocationViewController()
    }
    
    
    //MARK: GMSAutocompleteResultsViewControllerDelegate
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
        
    }
    
    
    //MARK: Action Methods
    
    @IBAction func automaticLocateButtonTapped(_ sender: Any)
    {
        UserManager.sharedInstance.setIsCurrentLocationActive(true)
        self.setupLocationViewController()
        
//        let alertController : UIAlertController = UIAlertController(title: k.services.allowLocationTitle, message: k.services.allowLocationMessage, preferredStyle: .alert)
//        let onceAction : UIAlertAction = UIAlertAction(title: k.services.onceAllowActionTitle, style: .default) { (action : UIAlertAction) in
//            self.setupLocationViewController()
//        }
//
//        let alwaysAllowAction : UIAlertAction = UIAlertAction(title: k.services.alwaysAllowActionTitle, style: .default) { (action : UIAlertAction) in
//            self.setupLocationViewController()
//        }
//
//        let dontAllowAction : UIAlertAction = UIAlertAction(title: k.services.dontAllowActionTitle, style: .cancel) { (action : UIAlertAction) in
//            let controller : LocationForbiddenViewController = self.storyboard?.instantiateViewController(withIdentifier: "LocationForbiddenViewController") as! LocationForbiddenViewController
//            self.navigationController?.pushViewController(controller, animated: true)
//        }
//
//        alertController.addAction(onceAction)
//        alertController.addAction(alwaysAllowAction)
//        alertController.addAction(dontAllowAction)
//
//        self.present(alertController, animated: false, completion: nil)
    }
    
    func setupExitCancel()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to cancel?", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes", style: .destructive) { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
            TransitionManager.sharedInstance.storyboardID = ""
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func setupLocationViewController()
    {
        let controller : LocationViewController = self.storyboard?.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func enterDetailsButtonTapped(_ sender: Any)
    {
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        let controller : SearchPlaceViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlaceViewController") as! SearchPlaceViewController
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any)
    {
        self.setupExitCancel()
    }
    

}
