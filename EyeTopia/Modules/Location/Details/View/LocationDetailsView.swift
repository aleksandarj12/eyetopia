//
//  LocationDetailsView.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/29/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import GoogleMaps

protocol LocationDetailsView {
    
    func setSortHolderView()
    func setSearchResults()
    func setAlertView(_ message : String)
    func setSortLocations(_ locations : [LocationDetail])
    func showMapView(_ mapView : GMSMapView)
    func showMapView()
    func hideMapView()
    func setDidFinishedNextPageToken()
    func hideProgressHud()
    func setMapView(_ mapView : GMSMapView)
    func setTableViewToTop()
}
