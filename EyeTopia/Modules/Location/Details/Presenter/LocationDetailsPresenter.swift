//
//  LocationDetailsPresenter.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/29/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps

enum SortLocation {
    case BestMatch
    case Distance
}

class LocationDetailsPresenter: NSObject {
    
    var view : LocationDetailsView!
    
    var mainLocation : CLLocation?
    
    var locationsCounter : Int = 0
    var placeCounter : Int = 0
    
    var isSortChanged : Bool = false
    
    var locations : [LocationDetail] = [LocationDetail]()
    
    var isOpenStoreSwitch : Bool = false
    
    var controllerArray : [LocationDetail] = [LocationDetail]()
    
    //THIS 3 vars comes out from LocationCustomViewController
    var keywords : String = ""
    var locationDetailSearch : LocationDetail?
    var currentLocationForSearch : Bool = false
    
//    var searchResults : SearchResult?
    
    var markers : [GMSMarker] = [GMSMarker]()
    
//    var locationArray : [LocationDetail]!
    var locationArrayBestMatch : [LocationDetail]!
    
    var sortLocation : SortLocation = .BestMatch
    
    var nextPageToken : String?
    
    func attachView(view: LocationDetailsView){
        self.view = view
    }
    
    func detachView() -> Void {
        self.view = nil
    }
    
    func getSearchResult(text : String)
    {
        let newLocations : [LocationDetail] = self.updateSearchTermProduct(searchTerm: text)
        self.view.setSortLocations(newLocations)
    }
    
    func setupMarkerStyle(_ selectedMarker : GMSMarker?, _ flag : Bool) -> UIImageView
    {
        var counter : Int = 1
        for marker in self.markers
        {
            if(selectedMarker == marker)
            {
                break
            }
            else
            {
                counter += 1
            }
        }
        
        if(flag)
        {
            let imageView : UIImageView = UIImageView(image: UIImage(named: "icnDropPinOn"))
            let label : UILabel = UILabel(frame: CGRect(x: 0, y: -3, width: imageView.frame.size.width, height: imageView.frame.size.height))
            let font : UIFont = UIFont(name: "AvenirNext-Bold", size: 14.0)!
            label.font = font
            label.textAlignment = .center
            label.textColor = Style.selectedCellBackgroundColor
            label.text = String(counter)
            imageView.addSubview(label)
            
            return imageView
        }
        else
        {
            let imageView : UIImageView = UIImageView(image: UIImage(named: "icnDropPinOff"))
            let label : UILabel = UILabel(frame: CGRect(x: 0, y: -3, width: imageView.frame.size.width, height: imageView.frame.size.height))
            let font : UIFont = UIFont(name: "AvenirNext-Bold", size: 14.0)!
            label.font = font
            label.textAlignment = .center
            label.textColor = UIColor.white
            label.text = String(counter)
            imageView.addSubview(label)
            
            return imageView
        }
        
    }
    
    func drawMapView(_ mapView : GMSMapView, _ locations : [LocationDetail], _ coordinates : CLLocationCoordinate2D)
    {
        mapView.camera = GMSCameraPosition(target: coordinates, zoom: 15, bearing: 0, viewingAngle: 0)
        var counter : Int = 1
        self.markers.removeAll()
        mapView.clear()
        
        var newLocations : [LocationDetail] = [LocationDetail]()
        if(self.sortLocation == .Distance)
        {
            newLocations = locations.sorted(by: { $0.distanceValue! < $1.distanceValue! })
        }
        else
        {
            newLocations = locations
        }
        
        for location in newLocations
        {
            let marker = GMSMarker(position: location.coordinates!)
            
            marker.iconView = self.setupMarkerStyle(marker, false)
            //                marker.zIndex = Int32(counter)
            self.markers.append(marker)
            marker.map = mapView
            marker.tracksViewChanges = true
//            self.markers[self.markers.count - 1].map = mapView
            counter += 1
        }
        
        self.view.setMapView(mapView)
    }
    
    func presentMapView(_ showMap : Bool, _ mapView : GMSMapView?)
    {
        if(showMap)
        {
            self.drawMapView(mapView!, self.locations, (self.mainLocation?.coordinate)!)
            self.view.showMapView()
        }
        else
        {
            self.view.hideMapView()
        }
        
    }
    
    func getLocationsForSearchResults(_ keyword : String?, _ location : LocationDetail?, _ currentLocation : Bool)
    {
        self.locationsCounter = 0
        self.placeCounter = 0
        if(self.locationArrayBestMatch != nil)
        {
            self.locationArrayBestMatch.removeAll()
        }
        else
        {
            self.locationArrayBestMatch = [LocationDetail]()
        }
        
        self.keywords = keyword!
        
        self.locationDetailSearch = location
        self.currentLocationForSearch = currentLocation
        self.mainLocation = CLLocation(latitude: (location?.coordinates?.latitude)!, longitude: (location?.coordinates?.longitude)!)
        var urlString : String = ""
        
        switch UserManager.sharedInstance.searchOption {
        case .retailer?:
            
            urlString = k.services.retailerlLocationsForSearch+self.setupKeywords()
            break
            
        case .eyeCareProfessionals?:
            
            urlString = k.services.doctorLocationsForSearch+self.setupKeywords()
            break
            
        default:
            break
        }
        
        if(self.currentLocationForSearch)
        {
            
            urlString.append("&location=")
            urlString.append((UserManager.sharedInstance.searchLocation.coordinates?.latitude.description)!)
            urlString.append(",")
            urlString.append((UserManager.sharedInstance.searchLocation.coordinates?.longitude.description)!)
        }
        else
        {
            urlString.append("&location=")
            if let coordinate = location {
                if let crd = coordinate.coordinates {
                    urlString.append((crd.latitude.description))
                    urlString.append(",")
                    urlString.append((crd.longitude.description))
                }
                else
                {
                    //tuka
                    print("AUTHORIZATION ALLOWED")
                    print("NO LOCATION SIMULATED")
                }
                
            }
            else
            {
                //tuka
                urlString.append("25.2744")
                urlString.append(",")
                urlString.append("133.7751")
            }
        }
//        if(CLLocationManager.locationServicesEnabled())
//        {
            self.fetchResults(urlString, true)
//        }
//        else
//        {
//            self.view.hideProgressHud()
//        }
    }
    
    func getLocations()
    {
        switch UserManager.sharedInstance.searchOption {
        case .retailer?:
            if(self.sortLocation == .Distance)
            {
                self.fetchResults(k.services.retailerlLocationsDistance+self.setupKeywords(), false)
            }
            else
            {
                self.fetchResults(k.services.retailerlLocationsBestMatch+self.setupKeywords(), false)
            }
            break
            
        case .eyeCareProfessionals?:
            
            if(self.sortLocation == .Distance)
            {
                self.fetchResults(k.services.doctorLocationsDistance+self.setupKeywords(), false)
            }
            else
            {
                self.fetchResults(k.services.doctorLocationsBestMatch+self.setupKeywords(), false)
            }
            
            break
        default:
            break
        }
    }
    
    func didChangedSort() -> Void
    {
        if(self.sortLocation == .Distance)
        {
            self.sortByDistance()
        }
        else
        {
            self.sortByBestMatch()
        }
    }
    
    func sortByBestMatch() -> Void
    {
        self.getLocations()
    }
    
    func sortByDistance() -> Void
    {
        self.getLocations()
    }
    
    func sortByOpenStore(_ flag : Bool)
    {
        var openStoreArray : [LocationDetail] = [LocationDetail]()
        if let array = self.locationArrayBestMatch
        {
            if(flag)
            {
                for location in array
                {
                    if let isOpen = location.open
                    {
                        if(isOpen)
                        {
                            openStoreArray.append(location)
                        }
                    }
                }
                self.view.setSortLocations(openStoreArray)
            }
            else
            {
             self.view.setSortLocations(self.locationArrayBestMatch)
            }
        }
    }
    
    func getSortHolderView()
    {
        self.view.setSortHolderView()
    }
    
    func getSearch(_ string : String)
    {
        if let location = UserManager.sharedInstance.location
        {
            //&type=restaurant
            var urlString : String = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="
            urlString.append(String(location.coordinate.latitude))
            urlString.append(",")
            urlString.append(String(location.coordinate.longitude))
            urlString.append("&rankby=distance&keyword=")
            urlString.append(string)
            urlString.append("&key="+k.services.googleMapsApiKey)
            
//            self.fetchResults(urlString)
        }
        
        
    }
    
    func fetchResults(_ url : String?, _ locationSearchResult : Bool)
    {
        var returnValue : Bool = true
        
        if(self.mainLocation != nil)
        {
            
            var urlString : String = ""
            
            if(NetworkManager.sharedInstance.checkReachability())
            {
                if(url == nil)
                {
                    if let pageToken = self.nextPageToken {
                        urlString = k.services.nextPageToken + pageToken
                    }
                    else
                    {
                        returnValue = false
                    }
                    
                    self.nextPageToken = nil
                }
                else
                {
                    self.view.setTableViewToTop()
                    if(!locationSearchResult)
                    {
                        self.placeCounter = 0
                        if(self.locationArrayBestMatch != nil) {
                            self.locationArrayBestMatch.removeAll()
                        }
                        
                        urlString = url!
                        urlString.append("&location=")
                        urlString.append((self.mainLocation?.coordinate.latitude.description)!)
                        urlString.append(",")
                        urlString.append((self.mainLocation?.coordinate.longitude.description)!)
                        self.currentLocationForSearch = false
                    }
                    else
                    {
                        self.currentLocationForSearch = true
                        urlString = url!
                    }
                    
                }

                
                if(returnValue)
                {
                    Alamofire.request(urlString)
                        .responseJSON { response in
                            
                            
                            if(response.error == nil)
                            {
                                
                                if let nextPage = (response.value as AnyObject).value(forKey: "next_page_token")
                                {
                                    self.nextPageToken = nextPage as? String
                                }
                                else
                                {
                                    self.nextPageToken = nil
                                }
                                
                                let json = response.result.value
                                if (((json as AnyObject).value(forKey: "error_message") as AnyObject).description != k.alert.exceedErrorMessage)
                                {
                                    let results = (json as AnyObject).value(forKey: "results") as AnyObject
                                    
                                    var placeIDsArray : [String] = [String]()
                                    var coordinations : [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
                                    if(self.isSortChanged)
                                    {
                                        self.isSortChanged = false
                                        self.locationsCounter = results.count
                                    }
                                    else
                                    {
                                        self.locationsCounter += results.count
                                    }
                                    
                                    if(results.count() != 0)
                                    {
                                        for i in 0...results.count - 1
                                        {
                                            //coordinates
                                            let lat : CLLocationDegrees = ((results.object(at: i) as AnyObject).value(forKeyPath: "geometry.location.lat") as AnyObject) as! CLLocationDegrees
                                            let lon : CLLocationDegrees = ((results.object(at: i) as AnyObject).value(forKeyPath: "geometry.location.lng") as AnyObject) as! CLLocationDegrees
                                            let coordinates : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                                            coordinations.append(coordinates)
                                            
                                            //placeId
                                            let placeId : String = (results.object(at: i) as AnyObject).value(forKey: "place_id") as! String
                                            placeIDsArray.append(placeId)
                                        }
                                    }
                                    
                                    self.fetchPlacesUsingPlaceIDs(placeIDsArray, coordinations)
                                }
                                else
                                {
                                    self.view.setAlertView(k.alert.exceedErrorMessage)
                                }
                                
                            }
                            else
                            {
                                self.view.setAlertView(k.alert.problemLoadingData)
                                self.view.hideProgressHud()
                            }
                    }
                }
                else
                {
                    self.view.setDidFinishedNextPageToken()
                    self.view.hideProgressHud()
                }
                
            }
            else
            {
                self.view.setAlertView(k.alert.noInternetConnection)
                self.view.hideProgressHud()
            }
            
        }
    }
    
    
    func fetchPlacesUsingPlaceIDs(_ placeIDs : [String], _ coordinates : [CLLocationCoordinate2D])
    {
        var locationsArray : [LocationDetail] = [LocationDetail]()
        if(NetworkManager.sharedInstance.checkReachability())
        {
            var coordinatesCounter : Int = 0
            if(placeIDs.count != 0)
            {
            for placeID in placeIDs
            {
                let url : String = k.services.googlePlaceId+placeID
                let locationDetail : LocationDetail = LocationDetail()
                locationDetail.placeId = placeID
                locationDetail.coordinates = coordinates[coordinatesCounter]
                coordinatesCounter += 1
                
                Alamofire.request(url, headers: nil)
                    .responseJSON { response in
                        
                        
                        if(response.error == nil)
                        {
                            let json = response.result.value
                            let results = (json as AnyObject).value(forKey: "result") as AnyObject
                    
                            if (((json as AnyObject).value(forKey: "error_message") as AnyObject).description != k.alert.exceedErrorMessage)
                            {
                                
                                //MAin Info
                                
                                locationDetail.address = results.value(forKey: "vicinity") as? String
                                locationDetail.title =  results.value(forKey: "name") as? String
                                locationDetail.open = results.value(forKeyPath: "opening_hours.open_now") as? Bool
                                locationDetail.phoneNumber = results.value(forKey: "international_phone_number") as? String
                                locationDetail.website = results.value(forKey: "website") as? String
                                
                                
                                if let openingHour = (results.value(forKeyPath: "opening_hours"))
                                {
                                    if((openingHour as AnyObject).description != k.text.null)
                                    {
                                        let workTime = ((openingHour as AnyObject).value(forKey: "weekday_text") as AnyObject).object(at: self.checkNumberDate())
                                        locationDetail.workTime = workTime as? String
                                    }
                                    else
                                    {
                                        locationDetail.workTime = nil
                                    }
                                }
                               
                                //Distance
                                
                                var urlDestination : String = k.services.locationDistance
                                if(self.currentLocationForSearch)
                                {
                                    urlDestination.append((self.locationDetailSearch?.coordinates?.latitude.description)!)
                                    urlDestination.append(",")
                                    urlDestination.append((self.locationDetailSearch?.coordinates?.longitude.description)!)
                                    
                                }
                                else
                                {
                                    urlDestination.append((self.mainLocation?.coordinate.latitude.description)!)
                                    urlDestination.append(",")
                                    urlDestination.append((self.mainLocation?.coordinate.longitude.description)!)
                                }

                                
                                urlDestination.append("&destinations=place_id:")
                                urlDestination.append(placeID)
                                
                                Alamofire.request(urlDestination, headers: nil)
                                    .responseJSON { response in
                                        
                                        
                                        if(response.error == nil)
                                        {
                                            let json = response.result.value
                                            
                                            if(!self.checkZeroResults(result: json))
                                            {
                                                let distanceString = (((json as AnyObject).value(forKeyPath: "rows.elements.distance.text") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as! String
                                                let distanceValue = (((json as AnyObject).value(forKeyPath: "rows.elements.distance.value") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as! Int
                                                locationDetail.distanceString = distanceString
                                                locationDetail.distanceValue = distanceValue
                                                locationsArray.append(locationDetail)
                                                self.placeCounter += 1
                                                if(self.checkCounter())
                                                {
                                                    if(self.locationArrayBestMatch == nil)
                                                    {
                                                        self.locationArrayBestMatch = [LocationDetail]()
                                                    }
                                                    for location in locationsArray
                                                    {
                                                        self.locationArrayBestMatch.append(location)
                                                    }
                                                    self.view.setSortLocations(self.checkOpenStore())
                                                    self.view.hideProgressHud()
                                                }
                                            }
                                            
                                        }
                                        else
                                        {
                                            self.view.setAlertView(k.alert.problemLoadingData)
                                            self.view.hideProgressHud()
                                        }
                                }
                                
                            }
                            else
                            {
                               self.view.setAlertView(k.alert.exceedErrorMessage)
                            }
                            
                        }
                        else
                        {
                            self.view.setAlertView(k.alert.problemLoadingData)
                            self.view.hideProgressHud()
                        }
                }
            }
            }
            else
            {
                self.view.setSortLocations([LocationDetail]())
            }
            }
        else
        {
            self.view.setAlertView(k.alert.noInternetConnection)
            self.view.hideProgressHud()
        }
        
    }
    
    func sortBySelection() -> Void
    {
        switch self.sortLocation {
        case .BestMatch:
            self.sortByBestMatch()
            break
        case .Distance:
            self.sortByDistance()
            break
        }
    }
    
    func checkCounter() -> Bool
    {
        if(self.placeCounter == self.locationsCounter)
        {
            return true
        }
        else
        {
            return false
        }
        
    }
    
    func checkOpenStore() -> [LocationDetail]
    {
        var newArray : [LocationDetail] = [LocationDetail]()
        if(self.isOpenStoreSwitch)
        {
            for location in self.locationArrayBestMatch
            {
                if let isOpen = location.open
                {
                    if(isOpen)
                    {
                        newArray.append(location)
                    }
                }
            }
            return newArray
        }
        else
        {
            return self.locationArrayBestMatch
        }
    }
    
    func checkNumberDate() -> Int
    {
        switch Calendar.current.component(.weekday, from: Date()) {
        case 1:
            return 6
        case 2:
            return 0
        case 3:
            return 1
        case 4:
            return 2
        case 5:
            return 3
        case 6:
            return 4
        case 7:
            return 5
        default:
            print("Error fetching days")
            return 0
        }
    }
    
    func updateSearchTermProduct(searchTerm : String) -> [LocationDetail]
    {
        var newLocation : [LocationDetail] = [LocationDetail]()
        
        for location in self.locationArrayBestMatch
        {
            if(searchTerm == "")
            {
                newLocation.append(location)
            }
            else
            {
                let lowerCaseSentence = location.title?.lowercased()
                let lowerCaseSearchTerm = searchTerm.lowercased()
                if(lowerCaseSentence?.contains(lowerCaseSearchTerm))!
                {
                    newLocation.append(location)
                }
            }
        }
        
        return newLocation
    }
    
    
//    func fetchDestination(_ placeID : String)
//    {
//        var url : String = k.services.locationDistance
//        url.append((UserManager.sharedInstance.location?.coordinate.latitude.description)!)
//        url.append(",")
//        url.append((UserManager.sharedInstance.location?.coordinate.longitude.description)!)
//        url.append("&destinations=place_id:")
//        url.append(placeID)
//
//        Alamofire.request(url, headers: nil)
//            .responseJSON { response in
//
//
//                if(response.error == nil)
//                {
//                    let json = response.result.value
//                    let distance = (json as AnyObject).value(forKeyPath: "rows.elements.distance.text") as AnyObject
//
//                }
//                else
//                {
//                    self.view.setAlertView(k.alert.problemLoadingData)
//                }
//        }
//    }
    
    
    func checkZeroResults(result : Any?) -> Bool
    {
        let string : String = (((result as AnyObject).value(forKeyPath: "rows.elements.status") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as! String
        if(string == k.text.zeroResult)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func setupKeywords() -> String
    {
        var newKeyWord = ""
        
        for char in self.keywords
        {
            if(char == " ")
            {
                newKeyWord.append("+")
            }
            else
            {
                newKeyWord.append(char)
            }
        }
        
        return "+"+newKeyWord
    }
    
}
