//
//  LocationDetailsViewCell.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/29/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class LocationDetailsViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var websiteButton: UIButton!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var bottomBorderHolderView: UIView!
    var websiteUrl : String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func websiteButtonTapped(_ sender: Any)
    {
        UIApplication.shared.open(URL(string: self.websiteUrl)!, options: [:], completionHandler: nil)
    }
    
    func checkLastBorder(_ index : Int, _ locationCount : Int)
    {
        if(index == locationCount - 1)
        {
            self.bottomBorderHolderView.backgroundColor = UIColor.clear
        }
        else
        {
            self.bottomBorderHolderView.backgroundColor = Style.switchThumb.off
        }
    }
}
