//
//  SortLocationsViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 4/10/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class SortLocationsViewController: UIViewController {

    var presenter = LocationDetailsPresenter()
    
    @IBOutlet weak var sortHolderView: UIView!
    @IBOutlet weak var distanceCheckBox: UIButton!
    @IBOutlet weak var bestMatchCheckbox: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(red:0.02, green:0.02, blue:0.06, alpha:0.4)
        sortHolderView.layer.cornerRadius = 3
        self.setSort()
    }

   
    func setSort()
    {
        if(self.presenter.sortLocation == .BestMatch)
        {
            self.bestMatchCheckbox.setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
            self.distanceCheckBox.setImage(UIImage(named: "check_box_off"), for: UIControlState.normal)
        }
        else
        {
            self.bestMatchCheckbox.setImage(UIImage(named: "check_box_off"), for: UIControlState.normal)
            self.distanceCheckBox.setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
        }
    }

    @IBAction func distanceCheckBoxPressed(_ sender: UIButton)
    {
        self.presenter.sortLocation = .Distance
        self.setSort()
    }
    
    @IBAction func bestMatchCheckBoxPressed(_ sender: UIButton)
    {
        self.presenter.sortLocation = .BestMatch
        self.setSort()
    }
    
    @IBAction func applyPressed(_ sender: UIButton)
    {
        self.dismiss(animated: false, completion: {
            self.presenter.isSortChanged = true
            self.presenter.didChangedSort()
        })
    }
    
    @IBAction func clearSelectionPressed(_ sender: UIButton)
    {
        self.dismiss(animated: false, completion: {
            self.presenter.isSortChanged = true
            self.presenter.sortLocation = .BestMatch
            self.presenter.locationsCounter = 0
            self.presenter.didChangedSort()
        })
    }
    
    @IBAction func hideSortHolderViewPressed(_ sender: UIButton)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    
}
