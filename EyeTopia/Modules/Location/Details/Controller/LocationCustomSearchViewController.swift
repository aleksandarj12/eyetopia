//
//  LocationCustomSearchViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 5/16/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import GoogleMaps

protocol LocationCustomSearchDelegate {
    func didSetSearchResults(_ keyword : String?, _ location : LocationDetail?, _ currentLocation : Bool)
    func didSendAlertMessage(_ message : String?)
}

class LocationCustomSearchViewController: UIViewController, UITextFieldDelegate, SearchPlaceDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var searchKeywordTextField: UITextField!
    @IBOutlet weak var mainHolderView: UIView!
    @IBOutlet weak var searchPlaceTextField: UITextField!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    
    var searchResult : String = ""
//    var searchLocationPlaceResult : String = ""
    
    var delegate : LocationCustomSearchDelegate!
    var isCurrentLocationSelected : Bool = false
    
    var locationManager : CLLocationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTextFields()
        self.setupCurrentLocationOption()
        self.locationManager.delegate = self
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        
        if(UserManager.sharedInstance.searchLocationResult == "")
        {
            UserManager.sharedInstance.searchLocationResult = k.text.currentLocation
        }
        else
        {
            if(UserManager.sharedInstance.searchLocationResult != k.text.currentLocation)
            {
                self.searchPlaceTextField.text = UserManager.sharedInstance.searchLocationResult
            }
            else
            {
                UserManager.sharedInstance.searchLocationResult = k.text.currentLocation
            }
            
        }
//        UserManager.sharedInstance.searchLocationResult = self.searchPlaceTextField
        self.searchKeywordTextField.text = self.searchResult
        self.setupSearchPlaceTextField()
        
        if searchKeywordTextField.text == "" {
            self.deleteButton.isHidden = true
        } else {
            self.deleteButton.isHidden = false
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
//        UserManager.sharedInstance.setIsCurrentLocationActive(true)
//        TransitionManager.sharedInstance.setStoryboardID(storyboardID: "LocationViewController")
    
    }
    
    
    //MARK: CLLocationManager Delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        UserManager.sharedInstance.searchLocation.coordinates = locations[0].coordinate
        self.locationManager.stopUpdatingLocation()
    }
    
    //MARK: SearchPlaceDelegate
    
    func didSelectCurrentLocation()
    {
        self.currentLocationButton.isHidden = true
        self.setupCurrentLocation()
    }
    
    func didFindPlace(_ location: LocationDetail)
    {
        UserManager.sharedInstance.searchLocation = location
        UserManager.sharedInstance.searchLocationResult = location.title!

        self.searchPlaceTextField.text = location.title
        self.searchPlaceTextField.textColor = Style.navyDark
        self.setupSearchPlaceTextField()
    }
    
    //MARK: Action Methods
    
    @IBAction func locationButtonTapped(_ sender: Any)
    {
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        let controller : SearchPlaceViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlaceViewController") as! SearchPlaceViewController
        controller.delegate = self
        UserManager.sharedInstance.mainLocationTitleOption = .locatorSearch
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
//        self.delegate.didSetSearchResults(nil, nil, false)
    }
    
    func setupSearchResult()
    {
        self.dismiss(animated: false, completion: {
            if(self.searchPlaceTextField.text != "")
            {
                self.delegate.didSetSearchResults(self.searchKeywordTextField.text, UserManager.sharedInstance.searchLocation, self.isCurrentLocationSelected)
            }
            else if(self.searchKeywordTextField.text != "")
            {
               self.delegate.didSetSearchResults(self.searchKeywordTextField.text, nil, self.isCurrentLocationSelected)
            }
        })
    }
    
    @IBAction func searchButtonTapped(_ sender: Any)
    {
//        let searchOption : SearchResult = SearchResult()
//        searchOption.currentLocation = self.isCurrentLocationSelected
//        searchOption.customLocation = self.searchPlaceTextField.text
//        searchOption.keywords = self.searchKeywordTextField.text
//        if(CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse)
//        {
//            UserManager.sharedInstance.searchLocationResult = k.text.currentLocation
//        }
        
            if(self.searchPlaceTextField.text == "")
            {
                UserManager.sharedInstance.searchLocationResult = ""
                self.dismiss(animated: false, completion: {
                    self.delegate.didSendAlertMessage("Location field is required")
                })
                
            }
            else
            {
                UserManager.sharedInstance.searchLocationResult = self.searchPlaceTextField.text!
//                self.setupSearchResult()
            }
        
        if(self.searchPlaceTextField.text != "")
        {
            self.setupSearchResult()
        }
    }
    
    @IBAction func currentLocationButtonTapped(_ sender: Any)
    {
        self.currentLocationButton.isHidden = true
        self.setupCurrentLocation()
    }
    
    @objc func doneKeyboardButtonTapped()
    {
        self.searchPlaceTextField.resignFirstResponder()
        self.searchKeywordTextField.resignFirstResponder()
    }
    
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        self.searchKeywordTextField.text = ""
        self.deleteButton.isHidden = true
    }
    
    //MARK: UITextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(textField == self.searchKeywordTextField)
        {
            self.setupSearchResult()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if(textField == self.searchPlaceTextField)
        {
            if(self.isCurrentLocationSelected)
            {
                if(string == "")
                {
                    UserManager.sharedInstance.searchLocationResult = ""
                    self.searchPlaceTextField.text = ""
                    self.searchPlaceTextField.textColor = Style.navyDark
                    self.isCurrentLocationSelected = false
                }
                else
                {
                    return false
                }
            }
        }
        
        if(textField == self.searchKeywordTextField)
        {
            if(textField.text == "") {
                deleteButton.isHidden = true
            } else {
                deleteButton.isHidden = false
            }
        }
        
        return true
    }
    
    
    //MARK: Setup & Support Methods
    
    func setupSearchPlaceTextField()
    {
        if(self.searchPlaceTextField.text == k.text.currentLocation)
        {
            self.currentLocationButton.isHidden = true
            self.searchPlaceTextField.textColor = Style.defaultBlue
        }
        else
        {
            if(CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined)
            {
                self.currentLocationButton.isHidden = true
            }
            else
            {
                if(self.searchPlaceTextField.text != k.text.currentLocation)
                {
                    self.currentLocationButton.isHidden = false
                }
                
            }
            self.searchPlaceTextField.textColor = Style.navyDark
            
        }
    }
    
    func setupCurrentLocation()
    {
        self.locationManager.startUpdatingLocation()
        self.isCurrentLocationSelected = true
        UserManager.sharedInstance.searchLocationResult = k.text.currentLocation
        self.searchPlaceTextField.text = k.text.currentLocation
        self.searchPlaceTextField.textColor = Style.defaultBlue
    }
    
    func setupCurrentLocationOption()
    {
        if(CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse)
        {
            self.currentLocationButton.isHidden = false
        }
        else
        {
            self.searchPlaceTextField.text = UserManager.sharedInstance.searchLocationResult
            self.currentLocationButton.isHidden = true
        }
        self.setupTextFields()
    }
    
    func setupTextFields()
    {
        self.searchKeywordTextField.delegate = self
        self.searchPlaceTextField.delegate = self
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneKeyboardButtonTapped))
        
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        
        self.searchKeywordTextField.layer.cornerRadius = 15
        self.searchKeywordTextField.clipsToBounds = true
        self.searchPlaceTextField.layer.cornerRadius = 15
        self.searchPlaceTextField.clipsToBounds = true
        
        self.searchPlaceTextField.leftViewMode = .always
        self.searchKeywordTextField.leftViewMode = .always
        
        let viewPlace : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: self.searchPlaceTextField.frame.size.height))
        self.searchPlaceTextField.leftView = viewPlace
        
        let viewKeyword : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: self.searchKeywordTextField.frame.size.height))
        self.searchKeywordTextField.leftView = viewKeyword
        
        if(CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined)
        {
            self.searchPlaceTextField.text = UserManager.sharedInstance.searchLocationResult
            self.searchPlaceTextField.textColor = Style.navyDark
        }
        else
        {
            self.searchPlaceTextField.text = UserManager.sharedInstance.searchLocationResult
            if(self.searchPlaceTextField.text == k.text.currentLocation)
            {
                self.searchPlaceTextField.textColor = Style.defaultBlue
            }
            else
            {
                self.searchPlaceTextField.textColor = Style.navyDark
            }
            
        }
        self.setupSearchPlaceTextField()
        
        //        self.searchTextField.inputAccessoryView = keyboardToolbar
    }
    
}


//class SearchResult: NSObject {
//    var keywords : String?
//    var currentLocation : Bool = false
//    var customLocation : String?
//}
