//
//  LocationDetail.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/18/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import GoogleMaps

class LocationDetail: NSObject {
    var placeId : String?
    var coordinates : CLLocationCoordinate2D?
    var title : String?
    var address : String?
    var phoneNumber : String?
    var workTime : String?
    var distanceString : String?
    var distanceValue : Int?
    var website : String?
    var open : Bool?
}
