//
//  Place.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/19/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class Place: NSObject {
    var placeID : String?
    var mainText : String?
    var secondaryText : String?
}
