//
//  SearchPlaceViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/16/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import GooglePlaces

protocol SearchPlaceDelegate {
    func didFindPlace(_ location : LocationDetail)
    func didSelectCurrentLocation()
}

class SearchPlaceViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, SearchPlaceView {
    
    @IBOutlet weak var searchTextHolderView: UIView!
    @IBOutlet weak var clearTextButton: UIButton!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var emptyResultLabel: UILabel!
    
    var delegate : SearchPlaceDelegate!
    
    private var wrongText = ""
    private var messageForWrongText: String {
        
        return "No results for \"\(wrongText)\""
    }
    
    var places : [Place] = [Place]()
    
    var presenter = SearchPlacePresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emptyResultLabel.isHidden = true
        self.setupSearchText()
        self.clearTextButton.isHidden = true
        self.presenter.attachView(view: self)
        self.setupCurrentLocationOption()
        self.tableView.separatorStyle = .none
        self.tableView.register(UINib(nibName: "SearchPlaceViewCell", bundle: nil), forCellReuseIdentifier: "SearchPlaceViewCell")

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        var newString : String = textField.text!
        if(string == "")
        {
            newString.removeLast()
        }
        else
        {
            newString.append(string)
        }
        wrongText = newString

        if(textField == searchTextField && newString.count > 2)
        {
            var string : String = ""
            for character in newString
            {
                if(character == " ")
                {
                    string.append("%22")
                }
                else
                {
                    string.append(character)
                }
            }
            self.presenter.getSearch(string)
        }
        
        if(newString.count == 0)
        {
            self.clearTextButton.isHidden = true
        }
        else
        {
            self.clearTextButton.isHidden = false
        }
        
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.searchTextField.resignFirstResponder()
        return true
    }
    
    @IBAction func deleteTextButtonPressed(_ sender: UIButton) {
        searchTextField.text = ""
        self.clearTextButton.isHidden = true
        emptyResultLabel.text = ""
        emptyResultLabel.isHidden = true
    }
    
    //MARK: Action Methods
    
    @IBAction func currentLocationButtonTapped(_ sender: Any)
    {
        if(self.delegate != nil)
        {
            self.delegate.didSelectCurrentLocation()
        }
        else
        {
            UserManager.sharedInstance.setIsCurrentLocationActive(true)
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "LocationViewController")
        }
        self.dismiss(animated: true, completion: nil)        
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }

    //MARK: SearchPlaceView
    
    func didSetSearchLocationDetails(_ location: LocationDetail) {
        self.dismiss(animated: false, completion: {
            self.delegate.didFindPlace(location)
        })
        
    }
    
    func setAlertView(_ message : String)
    {
        let controller : UIAlertController = UIAlertController(title: "WARNING", message: message, preferredStyle: .alert)
        let cancelAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func setSearchResults(_ places: [Place])
    {
        self.places = places
        self.tableView.reloadData()
        checkForWrongLetters()
    }
    
    func didSetLocation()
    {
        TransitionManager.sharedInstance.setStoryboardID(storyboardID: "LocationViewController")
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: UITableView DataSource & Delegate
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : SearchPlaceViewCell = self.tableView.dequeueReusableCell(withIdentifier: "SearchPlaceViewCell") as! SearchPlaceViewCell
        
        
        if let secondaryText = self.places[indexPath.row].secondaryText
        {
            cell.mainTitleLabel.text = self.places[indexPath.row].mainText! + ", " + secondaryText
        }
        else
        {
            cell.mainTitleLabel.text = self.places[indexPath.row].mainText
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.places.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let urlString : String = k.services.googlePlaceId + self.places[indexPath.row].placeID!
        self.searchTextField.resignFirstResponder()
        if(self.delegate != nil)
        {
            self.presenter.getLocationWithPlace(urlString, true, self.places[indexPath.row])
        }
        else
        {
            self.presenter.getLocationWithPlace(urlString, false, self.places[indexPath.row])
        }
    }
    

    //MARK: Setup & Support Methods
    
    func setupSearchText()
    {
        self.searchTextField.delegate = self
        self.searchTextHolderView.clipsToBounds = true
        self.searchTextHolderView.layer.cornerRadius = 15

    }
    
    func setupCurrentLocationOption()
    {
        if(CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .notDetermined)
        {
            self.currentLocationButton.isHidden = false
        }
        else
        {
            self.currentLocationButton.isHidden = true
        }
    }
    
    private func checkForWrongLetters() {
        if places.count == 0 {
            emptyResultLabel.isHidden = false
            emptyResultLabel.text = messageForWrongText
        } else {
            emptyResultLabel.isHidden = true
        }
    }
    
}
