//
//  SearchPlacePresenter.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/18/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps
import Reachability

class SearchPlacePresenter: NSObject {
    
    var view : SearchPlaceView!
    
    
    func attachView(view: SearchPlaceView){
        self.view = view
    }
    
    func detachView() -> Void {
        self.view = nil
    }
    
    func getSearch(_ string : String)
    {
        var urlString : String = k.services.googleAutoComplete
        urlString.append(string)
        
        self.fetchResults(urlString)
    }
    
    func getLocationWithPlace(_ urlString : String, _ isLocationDetailsSearch : Bool, _ place : Place)
    {
        if(NetworkManager.sharedInstance.checkReachability())
        {
            Alamofire.request(urlString, headers: nil)
                .responseJSON { response in
                    
                    if(response.error == nil)
                    {
                        let json = response.result.value
                        if(json as AnyObject).value(forKey: "error_message") != nil
                        {
                            self.view.setAlertView(k.alert.exceedErrorMessage)
                        }
                        else
                        {
                            let jsonArray = ((json as AnyObject).value(forKeyPath: "result.geometry.location") as AnyObject)
                            
                            let lat : CLLocationDegrees = jsonArray.value(forKey: "lat") as! CLLocationDegrees
                            let lon : CLLocationDegrees = jsonArray.value(forKey: "lng") as! CLLocationDegrees
                            
                            let location : CLLocation = CLLocation(latitude: lat, longitude: lon)
                            if(isLocationDetailsSearch)
                            {
                                let locationDetails : LocationDetail = LocationDetail()
                                locationDetails.coordinates = location.coordinate
                                locationDetails.title = place.mainText! + ", " + place.secondaryText!
                                if(UserManager.sharedInstance.mainLocationTitleOption == .mainLocation)
                                {
                                    UserManager.sharedInstance.setMainLocationTitle(place)
                                }
                                self.view.didSetSearchLocationDetails(locationDetails)
                            }
                            else
                            {
                                UserManager.sharedInstance.setLocationPlace(location: location)
                                UserManager.sharedInstance.setMainLocationTitle(place)
                                UserManager.sharedInstance.setIsCurrentLocationActive(false)
                                self.view.didSetLocation()
                            }
                        }
                        
                       
                        
                    }
                    else
                    {
                        self.view.setAlertView(k.alert.problemLoadingData)
                    }
            }
        }
        else
        {
            self.view.setAlertView(k.alert.noInternetConnection)
        }
    }
    
    func fetchResults(_ url : String)
    {
        if(NetworkManager.sharedInstance.checkReachability())
        {
            Alamofire.request(url, headers: nil)
                .responseJSON { response in
                    
                    if(response.error == nil)
                    {
                        let json = response.result.value
                        
                        var places : [Place] = [Place]()
                        
                        let jsonArray = (json as AnyObject).value(forKey: "predictions") as AnyObject
                        
                        if(jsonArray.count != 0)
                        {
                            for i in 0...jsonArray.count - 1
                            {
                                let place : Place = Place()
                                place.placeID = (jsonArray.object(at: i) as AnyObject).value(forKey: k.services.placeID) as? String
                                place.mainText = (jsonArray.object(at: i) as AnyObject).value(forKeyPath: k.services.mainTextKeyPath) as? String
                                place.secondaryText = (jsonArray.object(at: i) as AnyObject).value(forKeyPath: k.services.secondaryTextKeyPath) as? String
                                if let secondaryText = place.secondaryText {
                                    places.append(place)
                                }
                            }
                        }
                        
                        
                        self.view.setSearchResults(places)
                    }
                    else
                    {
                        self.view.setAlertView(k.alert.problemLoadingData)
                    }
                   
            }
        }
        else
        {
            self.view.setAlertView(k.alert.noInternetConnection)
        }
        
    }
}
