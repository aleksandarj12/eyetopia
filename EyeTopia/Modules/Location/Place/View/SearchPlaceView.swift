//
//  SearchPlaceView.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/18/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

protocol SearchPlaceView {
    func setSearchResults(_ places : [Place])
    func didSetLocation()
    func setAlertView(_ message : String)
    func didSetSearchLocationDetails(_ location : LocationDetail)
}
