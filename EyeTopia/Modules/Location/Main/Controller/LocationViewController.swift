 //
//  LocationViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/8/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import GoogleMaps
import DGActivityIndicatorView
import Alamofire

class LocationViewController: UIViewController, CLLocationManagerDelegate, UIAlertViewDelegate, GMSMapViewDelegate {

    @IBOutlet weak var loadingHolderView: UIView!
    @IBOutlet weak var locationTitleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var resultView: UIView!
    @IBOutlet weak var mapHolderView: GMSMapView!
    @IBOutlet weak var dotsHolderView: UIView!
    private let locationManager = CLLocationManager()
    @IBOutlet weak var forbiddenLocationHolderView: UIView!
    
    var activityView : DGActivityIndicatorView!
    var longitude : String = ""
    var latitude : String = ""
    var locationDetail : LocationDetail = LocationDetail()
    var isLocationProvided : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapHolderView.delegate = self
        self.checkForLocationService()
        self.setupAnimation()
        self.backButton.isHidden = false
        if(self.isLocationProvided)
        {
            self.isLocationProvided = false
            self.setupLocation()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(TransitionManager.sharedInstance.storyboardID == "SettingsScreen")
        {
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
        }
        else if(TransitionManager.sharedInstance.storyboardID == "ForbiddenScreenFail")
        {
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
            self.navigationController?.popViewController(animated: false)
        }
//        else if(TransitionManager.sharedInstance.storyboardID == "LocationViewController")
//        {
//            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
//            self.setupLocation()
//        }
    }
    
    
    /*
    if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse)
    {
    
    }
    else if(CLLocationManager.authorizationStatus() == .authorizedAlways)
    {
    
    }
    else
    {
    self.setupForbiddenLocationViewController()
    }
    */
    
    
    //MARK: GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition)
    {
        print("TUKA 2")
    }
    
    func mapView(_ mapView: GMSMapView, didTapMyLocation location: CLLocationCoordinate2D)
    {
        print("TUKA 1")
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool
    {
        mapView.camera = GMSCameraPosition(target: (mapView.myLocation?.coordinate)!, zoom: 15, bearing: 0, viewingAngle: 0)
        return true
    }
    
    
    //MARK: Setup & Support Methods
    
    func setupLocation()
    {
        if let place = UserManager.sharedInstance.location {
            mapHolderView.camera = GMSCameraPosition(target: place.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            self.getCity()
        }
    }
    
    func setupExitCancel()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to cancel?", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes", style: .destructive) { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func checkForLocationService()
    {
        locationManager.delegate = self
        
        if(!(CLLocationManager.authorizationStatus() == .notDetermined && self.isLocationProvided) && UserManager.sharedInstance.isCurrentLocationActive)
        {
            locationManager.startUpdatingLocation()
            locationManager.requestAlwaysAuthorization()
            UserManager.sharedInstance.setLocationPlace(location: nil)
        }
        else if(UserManager.sharedInstance.isCurrentLocationActive)
        {
            UserManager.sharedInstance.setLocationPlace(location: nil)
            self.locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
    }
    
    func setupForbiddenLocationViewController()
    {
        let controller : LocationForbiddenViewController = self.storyboard?.instantiateViewController(withIdentifier: "LocationForbiddenViewController") as! LocationForbiddenViewController
        self.present(controller, animated: true, completion: nil)
    }
    
    func setupForbiddenLocationHolderView()
    {
        self.activityView.stopAnimating()
        self.loadingHolderView.alpha = 0
        self.forbiddenLocationHolderView.alpha = 1
    }
    
    func setupMainInfo()
    {
        self.activityView.stopAnimating()
        self.loadingHolderView.alpha = 0
        self.resultView.alpha = 1
        self.backButton.isHidden = true
    }
    
    func setupAnimation()
    {
        self.activityView = DGActivityIndicatorView(type: .ballPulse, tintColor: Style.textColor, size: 18.0)
        self.activityView.frame = CGRect(x: 5, y: 0, width: 20, height: 20)
        self.dotsHolderView.addSubview(self.activityView)
        self.activityView.startAnimating()
    }
    
    //MARK: CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        if (status == .authorizedWhenInUse || status == .authorizedAlways)
        {
            locationManager.startUpdatingLocation()
            mapHolderView.isMyLocationEnabled = true
//            mapHolderView.settings.myLocationButton = true

        }
        else if(status == .denied)
        {
            if(self.isLocationProvided || (UserManager.sharedInstance.location != nil))
            {
            }
            else
            {
                self.setupForbiddenLocationViewController()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let newLocation : CLLocation!
        
        guard let location = locations.first else {
            return
        }
        if(!self.isLocationProvided && UserManager.sharedInstance.isCurrentLocationActive)
        {
            UserManager.sharedInstance.setIsCurrentLocationActive(false)
            mapHolderView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            newLocation = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            self.longitude = (manager.location?.coordinate.longitude.description)!
            self.latitude = (manager.location?.coordinate.latitude.description)!
            self.getCityWith(location: location)
        }
        else
        {
            let coordinates = UserManager.sharedInstance.location
            mapHolderView.camera = GMSCameraPosition(target: (coordinates?.coordinate)!, zoom: 15, bearing: 0, viewingAngle: 0)
            
            self.longitude = (coordinates!.coordinate.longitude.description)
            self.latitude = (coordinates!.coordinate.latitude.description)
//            self.getCity()
            newLocation = CLLocation(latitude: (coordinates?.coordinate.latitude)!, longitude: (coordinates?.coordinate.longitude)!)
        }
        UserManager.sharedInstance.setLocationPlace(location: newLocation)
        
        
        locationManager.stopUpdatingLocation()
//        self.getLocationInfo()
        
        
        
        
       
//        [geoCoder reverseGeocodeLocation:newLocation
//        completionHandler:^(NSArray *placemarks, NSError *error) {
//        for (CLPlacemark *placemark in placemarks) {
//
//        NSLog(@"%@",[placemark locality]);
//
//        CLPlacemark *placemark = [placemarks objectAtIndex:0];
//
//        NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
//        NSLog(@"placemark.country %@",placemark.country);
//        NSLog(@"placemark.postalCode %@",placemark.postalCode);
//        NSLog(@"placemark.administrativeArea %@",placemark.administrativeArea);
//        NSLog(@"placemark.locality %@",placemark.locality);
//        NSLog(@"placemark.subLocality %@",placemark.subLocality);
//        NSLog(@"placemark.subThoroughfare %@",placemark.subThoroughfare);
//
//        }
//        }];
        
    }

    func getCityWith(location : CLLocation)
    {
        let geocoder = CLGeocoder()
        
        geocoder.reverseGeocodeLocation(location,
                                        completionHandler: { (placemarks, error) in
                                            
                                            
                                            if(error == nil)
                                            {
                                                if let city = placemarks?.first?.locality {
                                                    self.locationTitleLabel.text = city + ", "
                                                }
                                                else
                                                {
                                                    if let thoroughfare = placemarks?.first?.thoroughfare
                                                    {
                                                        self.locationTitleLabel.text = thoroughfare + ", "
                                                    }
                                                    else
                                                    {
                                                        self.locationTitleLabel.text = "Australia, "
                                                    }
                                                }
                                                
                                                if let area = placemarks?.first?.administrativeArea
                                                {
                                                    self.locationTitleLabel.text?.append(area)
                                                }
                                                else
                                                {
                                                    self.locationTitleLabel.text?.append("AU")
                                                }
                                            }
                                            else
                                            {
                                                self.locationTitleLabel.text = k.text.noLocationProvided
                                                self.forbiddenLocationHolderView.alpha = 1
                                                self.loadingHolderView.alpha = 0
                                            }
                                            
                                            UserManager.sharedInstance.setLocationDescription(locationString: self.locationTitleLabel.text!)
                                            UserDefaults.standard.setValue(self.locationTitleLabel.text, forKey: k.services.mainLocationTitle)
                                            UserDefaults.standard.synchronize()
                                            self.setupMainInfo()
        })
    }
    
    
    func getCity()
    {
        self.locationTitleLabel.text = self.locationDetail.title
        UserManager.sharedInstance.setLocationDescription(locationString: self.locationTitleLabel.text!)
        self.setupMainInfo()
    }
    
    
//    func getLocationInfo()
//    {
//        let url : String = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+self.latitude+","+self.longitude+"&key="+k.services.googleMapsApiKey
//
//        Alamofire.request(url, headers: nil)
//            .responseJSON { response in
//
//                let json = response.result.value
//                var jsonResult = json as! NSDictionary
//
//                let arrayLong = jsonResult.value(forKeyPath: "results.address_components.long_name")
//                //(arrayLong as! AnyObject).object(at: 0)
//
//
//
//                let city = ((jsonResult.value(forKeyPath: "results.address_components.long_name") as AnyObject).object(at: 0) as AnyObject).object(at: 2)
//                let code = ((jsonResult.value(forKeyPath: "results.address_components.short_name") as! AnyObject).object(at: 0) as! AnyObject).object(at: 4)
//
//                print(jsonResult)
//
//
//               self.setupMainInfo()
//        }
    

//    }
    
    
    //MARK: Action Methods
    
    @IBAction func continueButtonTapped(_ sender: Any)
    {
        TransitionManager.sharedInstance.setStoryboardID(storyboardID: "didFinishWithLocationSuccessfully")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func enterLocationButtonTapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: false)

    }
    
    @IBAction func tryAgainButtonTapped(_ sender: Any)
    {
        self.loadingHolderView.alpha = 1
        self.forbiddenLocationHolderView.alpha = 0
        locationManager.startUpdatingLocation()
    }
    
    @IBAction func backButtonTapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any)
    {
        self.setupExitCancel()
    }
    
    @IBAction func searchAgainButtonTapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

}
