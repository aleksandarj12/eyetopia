//
//  SystaneCardViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/13/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

protocol SystaneCardDelegate {
    func didFinishSystaneCard()
}

class SystaneCardViewController: UIViewController, ReceiveGiftView {

    @IBOutlet weak var continueButton: RoundedButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleIntro: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    var delegate : SystaneCardDelegate!
    var presenter : ReceiveGiftPresenter = ReceiveGiftPresenter()

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainView.layer.cornerRadius = 8
        self.mainView.clipsToBounds = true
        self.presenter.attachView(view: self)
        self.descriptionLabel.text = ""
        self.descriptionLabel.attributedText = self.setupSubtitle()
        
        self.titleIntro.attributedText = setupSubtitleTwoFonts("You received a  SYSTANE®‰ Savings Card", withFont: UIFont(name: "Nunito-ExtraBold", size: 18.0)!, withSuperFont: UIFont(name: "Nunito-ExtraBold", size: 10.0)!)

        
    }

    func setupSubtitle() -> NSMutableAttributedString
    {
        let text = "With this card, you will receive exclusive savings on select SYSTANE®‰ products at participating stores. \n\nPlease, complete your journey to see details."
        
        let fontSuper : UIFont? = UIFont(name: "AvenirNext-Medium", size:12)
        let font : UIFont = UIFont(name: "AvenirNext-Medium", size: 16)!
        
        var ranges : [NSRange] = [NSRange]()
        
        var string : NSString = text as NSString
        
        while (string.contains(k.text.fontSuper))
        {
            let range : NSRange = string.range(of: k.text.fontSuper)
            string = string.replacingCharacters(in: range, with: "") as NSString
            
            let newRange : NSRange = NSRange(location: range.location - 1, length: 1)
            ranges.append(newRange)
        }
        
        let attString : NSMutableAttributedString = NSMutableAttributedString(string: string as String, attributes: [.font:font])
        
        for range in ranges
        {
            attString.setAttributes([.font:fontSuper!,.baselineOffset:8], range: range)
        }
        
        return attString
    }
    func setupSubtitleTwoFonts(_ text : String, withFont: UIFont, withSuperFont: UIFont) -> NSAttributedString
    {
        
        let fontSuper : UIFont? = withSuperFont
        let font : UIFont = withFont
        
        var ranges : [NSRange] = [NSRange]()
        
        var string : NSString = text as NSString
        
        while (string.contains(k.text.fontSuper))
        {
            let range : NSRange = string.range(of: k.text.fontSuper)
            string = string.replacingCharacters(in: range, with: "") as NSString
            
            let newRange : NSRange = NSRange(location: range.location - 1, length: 1)
            ranges.append(newRange)
        }
        
        let attString : NSMutableAttributedString = NSMutableAttributedString(string: string as String, attributes: [.font:font])
        
        for range in ranges
        {
            attString.setAttributes([.font:fontSuper!,.baselineOffset:10], range: range)
        }
        
        return attString
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Action Methods
    
    @IBAction func continueButtonTapped(_ sender: Any)
    {
        self.presenter.loadSystaneCard(true)
        //tuka
//        UserManager.sharedInstance.setSavingCardID(cardID: )
        //        if(TransitionManager.sharedInstance.isSkipToProductMatch)
        //        {
        //            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "TransitionViewControllerKomilia")
        //        }
        //        else
        //        {
        //            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "TransitionViewController13")
        //        }
        //        TransitionManager.sharedInstance.setDidFinishTransition(flag: true)
        self.dismiss(animated: true, completion: {
            self.delegate.didFinishSystaneCard()
        })
    }
    
    
    //MARK: ReceiveGiftView
    
    func presentSecondScreen()
    {
        
    }
    
    func presentThirdScreen()
    {
        
    }
    
    func setTransitionOtherWorld()
    {
        
    }
    
    func setSystaneCard(_ flag : Bool, _ cardID : Int?)
    {
        
    }
    
    func didSetAlert(_ message : String)
    {
        let controller : UIAlertController = UIAlertController(title: "WARNING", message: k.alert.noInternetConnection, preferredStyle: .alert)
        let cancelAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }

}
