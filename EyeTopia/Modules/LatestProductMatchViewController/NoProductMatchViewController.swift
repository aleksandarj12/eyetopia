//
//  NoProductMatchViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 5/15/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class NoProductMatchViewController: UIViewController {

    var buttonTitle: String?
    var message: String?
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var variableButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        if (UserManager.sharedInstance.getNonDiagnosedJourneyCompletion() && !UserManager.sharedInstance.getDiagnosedJourneyCompletion()) {
            variableButton.titleLabel?.lineBreakMode = .byWordWrapping
            variableButton.titleLabel?.textAlignment = .center
            variableButton.setTitle("I've been diagnosed \n with Dry Eye", for: UIControlState.normal)
            variableButton.setBackgroundImage( UIImage(named: "btnDefaultActive"), for: UIControlState.normal)
            messageLabel.text = "No product matches yet. \n \n \n \n Only once you have been diagnosed with Dry Eye can you journey to find a product match."
        }
      
    }

    @IBAction func variableButtonTapped(_ sender: Any) {
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
