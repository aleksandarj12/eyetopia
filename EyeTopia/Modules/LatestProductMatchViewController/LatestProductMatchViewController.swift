//
//  LatestProductMatchViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/30/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class LatestProductMatchViewController: UIViewController {
    
    //Constraints outlets
    @IBOutlet weak var systaneBalanceDescriptionLabel: UILabel!
    @IBOutlet weak var balanceHeightCon: NSLayoutConstraint!
    @IBOutlet weak var ultraUltraHeightCon: NSLayoutConstraint!
    @IBOutlet weak var ultraUDHeightCon: NSLayoutConstraint!
    @IBOutlet weak var ultraSingleHeightCon: NSLayoutConstraint!
    @IBOutlet weak var gelDropHeightCon: NSLayoutConstraint!
    @IBOutlet weak var eyeWashHeightCon: NSLayoutConstraint!
    
    
    @IBOutlet weak var mainDescriptionLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nonCompleteJourneyHolderView: UIView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var heightOfButton: NSLayoutConstraint!
    @IBOutlet weak var bottomUpperCorners: UIView!
    @IBOutlet weak var bottomUpperCorners2: UIView!
    
    @IBOutlet weak var balanceHolderView: UIView!
    
    
    @IBOutlet weak var ultraUltraUDHolderView: UIView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    @IBOutlet weak var ultraUDHolderView: UIView!
    
    
     @IBOutlet weak var ultraUDSingleUseHolderView: UIView!
   
    
    @IBOutlet weak var gelDropAndLidWipesView: UIView!
    @IBOutlet weak var introGelAndLid: UILabel!
    
    
    @IBOutlet weak var eyeWashView: UIView!
    @IBOutlet weak var introEyeWash: UILabel!
  
    
    var isMain : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpBorderAndRoundCornersOnViews()
        
        self.systaneBalanceDescriptionLabel.attributedText = self.setupSubtitle("Get restoring dry eye relief with SYSTANE® BALANCE Lubricant Eye Drops with the LipitechT‰M‰ system that locks in essential moisture.")
        
        let learnMoreAndFindARetailer  = NSMutableAttributedString(string: "Learn more     |     Find a retailer", attributes: [
            .font: UIFont(name: "AvenirNext-Medium", size: 16.0)!,
            .foregroundColor: UIColor(red: 0/255.0, green: 143/255.0, blue: 136/255.0, alpha: 1)
            ])
        learnMoreAndFindARetailer.addAttribute(.foregroundColor, value: UIColor(red: 206/255.0, green: 206/255.0, blue: 206/255.0, alpha: 1), range: NSRange(location: 15, length: 1))
        
        
        let attributedString = NSMutableAttributedString(string: "Based on your answers, EyeTopia has determined that you’re experiencing dry eye symptoms and may benefit from these eye drops.", attributes: [
            .font: UIFont(name: "AvenirNext-Bold", size: 16.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0),
            .kern: 0.0
            ])
        attributedString.addAttribute(.font, value: UIFont(name: "AvenirNext-Medium", size: 16.0)!, range: NSRange(location: 0, length: 53))
        
        mainDescriptionLabel.attributedText = attributedString


        
        let attributedGelLabel = NSMutableAttributedString(string: "Because you told us that you have dry eye symptoms at night, try these products for night time relief.", attributes: [
            .font: UIFont(name: "AvenirNext-Medium", size: 16.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0)
            ])
        attributedGelLabel.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 16.0)!, range: NSRange(location: 25, length: 34))
        introGelAndLid.attributedText = attributedGelLabel
        
        let eyeWashIntroAttribtedText =  NSMutableAttributedString(string: "Because you told us that you suffer from hayfever allergies, try our eye wash that helps remove irritants.", attributes: [
            .font: UIFont(name: "AvenirNext-Medium", size: 16.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0)
            ])
        eyeWashIntroAttribtedText.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 16.0)!, range: NSRange(location: 25, length: 35))
        introEyeWash.attributedText = eyeWashIntroAttribtedText

        
        if(UserManager.sharedInstance.getIsActiveUltraUD() && UserManager.sharedInstance.getIsActiveUltraUDSingleUse())
        {
            self.ultraUltraUDHolderView.isHidden = true
            self.ultraUDSingleUseHolderView.isHidden = false
            self.ultraUDHolderView.isHidden = false
        }
        else
        {
            self.ultraUDHolderView.isHidden = !UserManager.sharedInstance.getIsActiveUltraUD()
            self.ultraUltraUDHolderView.isHidden = !UserManager.sharedInstance.getIsActiveUltraUltraUD()
            self.ultraUDSingleUseHolderView.isHidden = !UserManager.sharedInstance.getIsActiveUltraUDSingleUse()
        }
        
    }
    
    func setupSubtitle(_ text : String) -> NSAttributedString
    {
        
        let fontSuper : UIFont? = UIFont(name: "AvenirNext-Medium", size:10)
        let font : UIFont = UIFont(name: "AvenirNext-Medium", size: 16)!
        
        var ranges : [NSRange] = [NSRange]()
        
        var string : NSString = text as NSString
        
        while (string.contains(k.text.fontSuper))
        {
            let range : NSRange = string.range(of: k.text.fontSuper)
            string = string.replacingCharacters(in: range, with: "") as NSString
            
            let newRange : NSRange = NSRange(location: range.location - 1, length: 1)
            ranges.append(newRange)
        }
        
        let attString : NSMutableAttributedString = NSMutableAttributedString(string: string as String, attributes: [.font:font])
        
        for range in ranges
        {
            attString.setAttributes([.font:fontSuper!,.baselineOffset:8], range: range)
        }
        
        return attString
    }
    

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if(self.isMain)
        {
            self.isMain = false
        }
        else
        {
            self.navigationController?.popViewController(animated: false)
        }
        
        
        if(UserManager.sharedInstance.getClearProductMatch() && UserManager.sharedInstance.getJourneyStation() == nil)
        {
            continueButton.titleLabel?.lineBreakMode = .byWordWrapping
            continueButton.titleLabel?.textAlignment = .center
            
            if(UserManager.sharedInstance.getNonDiagnosedJourneyCompletion())
            {
                self.nonCompleteJourneyHolderView.isHidden = false
                self.continueButton.setTitle("I've been diagnosed \n with Dry Eye", for: .normal)
                self.messageLabel.text = "No product matches yet. \n \n Only once you have been diagnosed with Dry Eye can you journey to find a product match."
                heightOfButton.constant = 70
            }
            else if(UserManager.sharedInstance.getDiagnosedJourneyCompletion())
            {
                self.nonCompleteJourneyHolderView.isHidden = false
                self.continueButton.setTitle("Start a new journey", for: .normal)
                self.messageLabel.text = "No product matches yet. \n \n  Please complete the EyeTopia journey to see your product recommendations."
                heightOfButton.constant = 55
            }
            else
            {
                self.nonCompleteJourneyHolderView.isHidden = false
                self.continueButton.setTitle("Continue journey", for: .normal)
                heightOfButton.constant = 55
            }
            
        }
        else if(UserManager.sharedInstance.getJourneyStation() != nil && UserManager.sharedInstance.getClearProductMatch())
        {
            self.nonCompleteJourneyHolderView.isHidden = false
            self.continueButton.setTitle("Continue journey", for: .normal)
        }
            
        else
        {
            self.ultraUDHolderView.isHidden = !UserManager.sharedInstance.getIsActiveUltraUD()
            self.balanceHolderView.isHidden = !UserManager.sharedInstance.getIsActiveBalance()
            self.ultraUltraUDHolderView.isHidden = !UserManager.sharedInstance.getIsActiveUltraUltraUD()
            self.ultraUDSingleUseHolderView.isHidden = !UserManager.sharedInstance.getIsActiveUltraUDSingleUse()
            self.eyeWashView.isHidden = !UserManager.sharedInstance.getSufferFromAllergy()
            self.gelDropAndLidWipesView.isHidden = !UserManager.sharedInstance.getDryEyesAtNight()
            if(UserManager.sharedInstance.getDiagnosedJourneyCompletion() && UserManager.sharedInstance.selectedEyeSymptom == nil)
            {
                self.nonCompleteJourneyHolderView.isHidden = false
                self.scrollView.isHidden = true
                continueButton.titleLabel?.lineBreakMode = .byWordWrapping
                continueButton.titleLabel?.textAlignment = .center
                continueButton.setTitle("Start a new journey", for: UIControlState.normal)
                continueButton.setBackgroundImage( UIImage(named: "btnDefaultActive"), for: UIControlState.normal)
                heightOfButton.constant = 55
                
                messageLabel.text = "No product matches yet. \n \n  Please complete the EyeTopia journey to see your product recommendations."
            }
            else if (UserManager.sharedInstance.getNonDiagnosedJourneyCompletion() && !UserManager.sharedInstance.getDiagnosedJourneyCompletion())
            {
                self.nonCompleteJourneyHolderView.isHidden = false
                self.scrollView.isHidden = true
                continueButton.titleLabel?.lineBreakMode = .byWordWrapping
                continueButton.titleLabel?.textAlignment = .center
                continueButton.setTitle("I've been diagnosed \n with Dry Eye", for: UIControlState.normal)
                continueButton.setBackgroundImage( UIImage(named: "btnDefaultActive"), for: UIControlState.normal)
                heightOfButton.constant = 70
                
                messageLabel.text = "No product matches yet. \n \n  Only once you have been diagnosed with Dry Eye can you journey to find a product match."
                
            } else if (!UserManager.sharedInstance.getNonDiagnosedJourneyCompletion() && !UserManager.sharedInstance.getDiagnosedJourneyCompletion()) {
                
                self.nonCompleteJourneyHolderView.isHidden = false
                self.scrollView.isHidden = true
                continueButton.titleLabel?.lineBreakMode = .byWordWrapping
                continueButton.titleLabel?.textAlignment = .center
                continueButton.setTitle("Continue journey", for: UIControlState.normal)
                continueButton.setBackgroundImage( UIImage(named: "btnDefaultActive"), for: UIControlState.normal)
                heightOfButton.constant = 55
                
                messageLabel.text = "No product matches yet. \n \n  Please complete the EyeTopia journey to see your product recommendations."
                
            } else {
                self.nonCompleteJourneyHolderView.isHidden = true
                self.scrollView.isHidden = false
                
            }
        }
        
        
        if UIScreen.main.bounds.size.width == 320 {
            balanceHeightCon.constant = 500
            ultraUltraHeightCon.constant = 800
            ultraUDHeightCon.constant = 485
            ultraSingleHeightCon.constant = 500
            gelDropHeightCon.constant = 640
            eyeWashHeightCon.constant = 390
        } else if UIScreen.main.bounds.size.width == 375 {
            balanceHeightCon.constant = 480
            ultraUltraHeightCon.constant = 750
            ultraUDHeightCon.constant = 465
            ultraSingleHeightCon.constant = 465
            gelDropHeightCon.constant = 620
            eyeWashHeightCon.constant = 360
        } else if UIScreen.main.bounds.size.width == 414 {
            balanceHeightCon.constant = 480
            ultraUltraHeightCon.constant = 750
            ultraUDHeightCon.constant = 470
            ultraSingleHeightCon.constant = 445
            gelDropHeightCon.constant = 620
            eyeWashHeightCon.constant = 385
        }
    }
    
    @IBAction func continueJourneyButtonTapped(_ sender: Any)
    {
        if continueButton.titleLabel?.text == "Start a new journey"
        {
            NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentStartNewJourneyKey), object: self)
            NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentHomeSelectionKey), object: self)
        }
        else if continueButton.titleLabel?.text == "Continue journey"
        {
            NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentContinueLeftJourneyKey), object: self)
            NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentHomeSelectionKey), object: self)
        }
        else if continueButton.titleLabel?.text == "I've been diagnosed \n with Dry Eye"
        {
            let controller : WelcomeBackToEyeTopiaViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeBackToEyeTopiaViewController") as! WelcomeBackToEyeTopiaViewController
            self.present(controller, animated: true, completion: nil)
            
        }
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func findARetailer()
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentLandingFindRetailerKey), object: self)
    }
    
    @IBAction func findARetailerPressed(_ sender: UIButton)
    {
        findARetailer()
    }
    
    @IBAction func learnMoreForSystaneBalance(_ sender: UIButton)
    {
        self.setupProdutDetails(0)
    }
    
    @IBAction func learnMoreForSystaneUltraLubricantEyeDrop(_ sender: UIButton)
    {
        self.setupProdutDetails(5)
    }
    
    @IBAction func learnMoreForSystaneUltraUdLubricantSingleUse(_ sender: UIButton)
    {
        self.setupProdutDetails(6)
    }
    
    @IBAction func learnMoreForSystaneGelDrop(_ sender: UIButton)
    {
        self.setupProdutDetails(2)
    }
    
    @IBAction func learnMoreForSystaneLidWipes(_ sender: UIButton)
    {
        self.setupProdutDetails(3)
    }
    
    @IBAction func learnMoreForSystaneEyeWash(_ sender: UIButton)
    {
        self.setupProdutDetails(1)
    }
    
    @IBAction func getSavingsTapped(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentLandingSavingsCardKey), object: self)
    }
    
    func setupProdutDetails(_ selectedIndex : Int)
    {
        TransitionManager.sharedInstance.productLearnMoreSelected = DataManager.sharedInstance.products[selectedIndex]
        TransitionManager.sharedInstance.setStoryboardID(storyboardID: "ProductDetailsVC")
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentLandingProductDetailsKey), object: self)
    }
    
    private func setUpBorderAndRoundCornersOnViews() {
        let requiredColor = UIColor(red: 206/255.0, green: 206/255.0, blue: 206/255.0, alpha: 1)
        
        balanceHolderView.layer.borderWidth = 1
        balanceHolderView.layer.borderColor = UIColor(red: 0/255.0, green: 143/255.0, blue: 136/255.0, alpha: 1).cgColor
        balanceHolderView.layer.cornerRadius = 5
        balanceHolderView.clipsToBounds = true
        bottomUpperCorners2.layer.cornerRadius = 5
        
        ultraUltraUDHolderView.layer.borderWidth = 1
        ultraUltraUDHolderView.layer.borderColor = requiredColor.cgColor
        ultraUltraUDHolderView.layer.cornerRadius = 5
        ultraUltraUDHolderView.clipsToBounds = true

        
        ultraUDHolderView.layer.borderWidth = 1
        ultraUDHolderView.layer.borderColor = UIColor(red: 0/255.0, green: 143/255.0, blue: 136/255.0, alpha: 1).cgColor
        ultraUDHolderView.layer.cornerRadius = 5
        ultraUDHolderView.clipsToBounds = true
        bottomUpperCorners.layer.cornerRadius = 5

        
        ultraUDSingleUseHolderView.layer.borderWidth = 1
        ultraUDSingleUseHolderView.layer.borderColor = requiredColor.cgColor
        ultraUDSingleUseHolderView.layer.cornerRadius = 5
        ultraUDSingleUseHolderView.clipsToBounds = true

        
        gelDropAndLidWipesView.layer.borderWidth = 1
        gelDropAndLidWipesView.layer.borderColor = requiredColor.cgColor
        gelDropAndLidWipesView.layer.cornerRadius = 5
        gelDropAndLidWipesView.clipsToBounds = true

        
        eyeWashView.layer.borderWidth = 1
        eyeWashView.layer.borderColor = requiredColor.cgColor
        eyeWashView.layer.cornerRadius = 5
        eyeWashView.clipsToBounds = true

    }
    
    @IBAction func zoomTapped(_ sender: UIButton) {
        var zoomedImaged = UIImage()
        
        switch sender.tag {
        case 110: zoomedImaged = UIImage(named: "PRODUCT_0003_Balance")!
        case 220: zoomedImaged = UIImage(named: "PRODUCT_0000_ULTRA")!
        case 330: zoomedImaged = UIImage(named: "PRODUCT_0001_ULTRA-UD")!
        case 440: zoomedImaged = UIImage(named: "PRODUCT_0004_Gel-Drops")!
        case 550: zoomedImaged = UIImage(named: "PRODUCT_0005_Lid-Wipes")!
        case 660: zoomedImaged = UIImage(named: "PRODUCT_0006_Eye-Wash")!
        case 2202: zoomedImaged = UIImage(named: "PRODUCT_0000_ULTRA")!
        case 3303: zoomedImaged = UIImage(named: "PRODUCT_0001_ULTRA-UD")!
        default:
            break
        }
        
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        let destinationVC = storyboard.instantiateViewController(withIdentifier: "ZoomVC") as? ZoomImageViewController
        destinationVC?.largeImage = zoomedImaged
        self.present(destinationVC!, animated: false, completion: nil)
        
    }
    
}
