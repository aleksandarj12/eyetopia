//
//  Product.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/20/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class Product: NSObject {
    
    var title : String?
    var titleForDetails: String?
    var subTitle : String?
    var mainImage : UIImage?
    var productInfo : [ProductInfo]?
    var benefitsDetails : [ProductInfo]?
    var productDescription : NSAttributedString?
    var additionalDescription : NSAttributedString?
    var filter : Filter?
    var externalLink: String?
    var zoomImage: UIImage?
    var extraDescription: String?
    var extraDisclaimer: String?
    var mainImageForDetail: UIImage?
    var disclaimerFirst: String?
    var disclaimerSecond: String?
}


class ProductInfo: NSObject {
    var image : UIImage?
    var details : String?
}

class Filter: NSObject {
    var dryEye : Bool = false
    var redEye : Bool = false
    var eyeHygiene : Bool = false
    var nightTime : Bool = false
    var preservativeFree : Bool = false
    var contactLenses : Bool = false
}
