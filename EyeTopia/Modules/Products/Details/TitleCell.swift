//
//  TitleCell.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 6/18/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class TitleCell: UITableViewCell {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
