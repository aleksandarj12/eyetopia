//
//  ButtonsCell.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 6/18/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

class ButtonsCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var buttonDelegate: ButtonCellDelegate?
    
    @IBAction func buyNowPressed(_ sender: Any) {
        self.buttonDelegate?.buyNowTapped()
    }
    
    @IBAction func findARetailer(_ sender: UIButton) {
        self.buttonDelegate?.findARetailerTapped()
    }
}

protocol ButtonCellDelegate {
    func buyNowTapped()
    func findARetailerTapped()
}
