//
//  DisclaimerCell.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 6/18/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class DisclaimerCell: UITableViewCell {
    
    @IBOutlet weak var disclaimerLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
