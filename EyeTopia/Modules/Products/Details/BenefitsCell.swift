//
//  BenefitsCell.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 6/18/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class BenefitsCell: UITableViewCell {
    
    
    
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var titleView: UIView!
    
    
  
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var detail1: UILabel!
    
   
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var detail2: UILabel!
    
   
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var detail3: UILabel!
    
 
    @IBOutlet weak var extraDescriptionInBenefits: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        holderView.layer.cornerRadius = 5
        holderView.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
