//
//  ProductDetailsViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 3/16/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

class ProductDetailsViewController: UIViewController, ButtonCellDelegate {
    
    @IBOutlet weak var productDetailsTableView: UITableView!
    //Outlets:
    
    
    var productObject = Product()
    var isJourney : Bool = false
    var isMain : Bool = false
    var numberOfRowsInTableView: Int {
        if  productObject.disclaimerFirst != nil || productObject.disclaimerSecond != nil {
            return 8
        } else {
            return 7
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.productDetailsTableView.dataSource = self
        self.productDetailsTableView.delegate = self
        self.productDetailsTableView.backgroundColor = UIColor(red: 249/255.0, green: 249/255.0, blue: 249/255.0, alpha: 1)
        
        self.productDetailsTableView.register(UINib(nibName: "TitleCell", bundle: nil), forCellReuseIdentifier: "titlecell")
        self.productDetailsTableView.register(UINib(nibName: "IntroductionCell", bundle: nil), forCellReuseIdentifier: "introductioncell")
        self.productDetailsTableView.register(UINib(nibName: "ImageCell", bundle: nil), forCellReuseIdentifier: "imagecell")
        self.productDetailsTableView.register(UINib(nibName: "DisclaimerCell", bundle: nil), forCellReuseIdentifier: "disclaimercell")
        self.productDetailsTableView.register(UINib(nibName: "BenefitsCell", bundle: nil), forCellReuseIdentifier: "benefitscell")
        self.productDetailsTableView.register(UINib(nibName: "ButtonsCell", bundle: nil), forCellReuseIdentifier: "buttonscell")
        self.productDetailsTableView.register(UINib(nibName: "GuideTitleCell", bundle: nil), forCellReuseIdentifier: "guidetitlecell")
        self.productDetailsTableView.register(UINib(nibName: "GuideDescriptionCell", bundle: nil), forCellReuseIdentifier: "guidedescription")
        
        productDetailsTableView.estimatedRowHeight = 50
        productDetailsTableView.rowHeight = UITableViewAutomaticDimension
       
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(self.isMain)
        {
            self.isMain = false
        }
        else
        {
            self.navigationController?.popViewController(animated: false)
        }
        Localytics.tagEvent(trackingEvents.productContentViewKey, attributes: ["Product Name" : productObject.title!])
        Localytics.tagEvent(trackingEvents.mostPopularViewKey, attributes: ["Most Popular" : productObject.title!], customerValueIncrease: 1)
    }
    
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//       return 1
//    }
    

    
    @IBAction func backButtonPressed(_ sender: UIButton) {

        if(self.isJourney)
        {
            self.isJourney = false
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func buyNowTapped() {
        if let link = productObject.externalLink, let urlString = URL(string: link), let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "interstitialVC") as? InterstitialViewController {
            destinationVC.link = urlString
            self.present(destinationVC, animated: true, completion: nil)
        }
    }
   
     func findARetailerTapped() {
        findARetailer()
        UserManager.sharedInstance.setSearchOption(searchOption: .retailer)
    }
    
    func setupSubtitle(_ text : String, withFont: UIFont, withSuperFont: UIFont) -> NSAttributedString
    {
        
        let fontSuper : UIFont? = withSuperFont
        let font : UIFont = withFont
        
        var ranges : [NSRange] = [NSRange]()
        
        var string : NSString = text as NSString
        
        while (string.contains(k.text.fontSuper))
        {
            let range : NSRange = string.range(of: k.text.fontSuper)
            string = string.replacingCharacters(in: range, with: "") as NSString
            
            let newRange : NSRange = NSRange(location: range.location - 1, length: 1)
            ranges.append(newRange)
        }
        
        let attString : NSMutableAttributedString = NSMutableAttributedString(string: string as String, attributes: [.font:font])
        
        for range in ranges
        {
            attString.setAttributes([.font:fontSuper!,.baselineOffset:10], range: range)
        }
        
        return attString
    }
    
    
    private func findARetailer()
    {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentLandingFindRetailerKey), object: self)

//        let appDelegate = TransitionManager.sharedInstance.appDelegate
//
//        if let controllers = appDelegate?.tabController.viewControllers{
//
//            for (index,controller) in controllers.enumerated() {
//                if let navController = controller as? UINavigationController, let root = navController.viewControllers[0] as? LocatorOptionViewController {
//                    appDelegate?.tabController.selectedIndex = index
//                    root.setupLocationDetailsViewController()
//                    UserManager.sharedInstance.setSearchOption(searchOption: .retailer)
//                    break
//                }
//            }
//        }
    }
}

extension ProductDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRowsInTableView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // if discalimer is before benefits
        if self.numberOfRowsInTableView == 8 && productObject.disclaimerFirst != nil {
            if indexPath.row == 0 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "titlecell") as! TitleCell
                if let myTitle = self.productObject.titleForDetails {
                    cell.titleLabel.attributedText = setupSubtitle(myTitle, withFont: UIFont(name: "Nunito-ExtraBold", size: 18.0)!, withSuperFont: UIFont(name: "Nunito-ExtraBold", size: 9.0)!)
                }
                return cell
            } else if indexPath.row == 1 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "introductioncell") as! IntroductionCell
                if let myIntro = self.productObject.subTitle {
                    cell.introductionLabel.attributedText = setupSubtitle(myIntro, withFont: UIFont(name: "AvenirNext-Medium", size: 16.0)!, withSuperFont: UIFont(name: "AvenirNext-Medium", size: 9.0)!)
                }
                return cell
            } else if indexPath.row == 2 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "imagecell") as! ImageCell
                if let myPic = self.productObject.mainImageForDetail {
                    cell.productImage.image = myPic
                }
                return cell
            } else if indexPath.row == 3 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "disclaimercell") as! DisclaimerCell
                if let myDisc = self.productObject.disclaimerFirst {
                    cell.disclaimerLabel.text = myDisc
                }
                return cell
            } else if indexPath.row == 4 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "benefitscell") as! BenefitsCell
                if let myBenefits = self.productObject.benefitsDetails {
                    cell.image1.image = myBenefits[0].image
                    cell.detail1.attributedText = setupSubtitle(myBenefits[0].details!, withFont: UIFont(name: "AvenirNext-DemiBold", size: 16.0)!, withSuperFont: UIFont(name: "AvenirNext-DemiBold", size: 9.0)!)
                    cell.image2.image = myBenefits[1].image
                    cell.detail2.attributedText = setupSubtitle(myBenefits[1].details!, withFont: UIFont(name: "AvenirNext-DemiBold", size: 16.0)!, withSuperFont: UIFont(name: "AvenirNext-DemiBold", size: 9.0)!)
                    cell.image3.image = myBenefits[2].image
                    cell.detail3.attributedText = setupSubtitle(myBenefits[2].details!, withFont: UIFont(name: "AvenirNext-DemiBold", size: 16.0)!, withSuperFont: UIFont(name: "AvenirNext-DemiBold", size: 9.0)!)
                    
                    if let extraDesc = productObject.extraDescription {
                        cell.extraDescriptionInBenefits.text = extraDesc
                    }
                }
                return cell
            } else if indexPath.row == 5 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "buttonscell") as! ButtonsCell
                cell.buttonDelegate = self
                return cell
            } else if indexPath.row == 6 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "guidetitlecell") as! GuideTitleCell
                
                return cell
            } else if indexPath.row == 7 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "guidedescription") as! GuideDescriptionCell
                if let myDesc = self.productObject.productDescription {
                    cell.guideDescriptionLabel.attributedText = myDesc
                }
                return cell
            }
            
        }
        
        // if disclaimer is after benefits
        else if self.numberOfRowsInTableView == 8 && productObject.disclaimerSecond != nil {
            if indexPath.row == 0 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "titlecell") as! TitleCell
                if let myTitle = self.productObject.titleForDetails {
                    cell.titleLabel.attributedText = setupSubtitle(myTitle, withFont: UIFont(name: "Nunito-ExtraBold", size: 18.0)!, withSuperFont: UIFont(name: "Nunito-ExtraBold", size: 9.0)!)
                }
                return cell
            } else if indexPath.row == 1 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "introductioncell") as! IntroductionCell
                if let myIntro = self.productObject.subTitle {
                    cell.introductionLabel.attributedText = setupSubtitle(myIntro, withFont: UIFont(name: "AvenirNext-Medium", size: 16.0)!, withSuperFont: UIFont(name: "AvenirNext-Medium", size: 9.0)!)
                }
                return cell
            } else if indexPath.row == 2 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "imagecell") as! ImageCell
                if let myPic = self.productObject.mainImageForDetail {
                    cell.productImage.image = myPic
                }
                return cell
            } else if indexPath.row == 3 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "benefitscell") as! BenefitsCell
                if let myBenefits = self.productObject.benefitsDetails {
                    cell.image1.image = myBenefits[0].image
                    cell.detail1.attributedText = setupSubtitle(myBenefits[0].details!, withFont: UIFont(name: "AvenirNext-DemiBold", size: 16.0)!, withSuperFont: UIFont(name: "AvenirNext-DemiBold", size: 9.0)!)
                    cell.image2.image = myBenefits[1].image
                    cell.detail2.attributedText = setupSubtitle(myBenefits[1].details!, withFont: UIFont(name: "AvenirNext-DemiBold", size: 16.0)!, withSuperFont: UIFont(name: "AvenirNext-DemiBold", size: 9.0)!)
                    cell.image3.image = myBenefits[2].image
                    cell.detail3.attributedText = setupSubtitle(myBenefits[2].details!, withFont: UIFont(name: "AvenirNext-DemiBold", size: 16.0)!, withSuperFont: UIFont(name: "AvenirNext-DemiBold", size: 9.0)!)
                    
                    if let extraDesc = productObject.extraDescription {
                        cell.extraDescriptionInBenefits.text = extraDesc
                    }
                }
                return cell
            } else if indexPath.row == 4 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "disclaimercell") as! DisclaimerCell
                if let myDisc = self.productObject.disclaimerSecond {
                    cell.disclaimerLabel.text = myDisc
                }
                return cell
            } else if indexPath.row == 5 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "buttonscell") as! ButtonsCell
                cell.buttonDelegate = self
                return cell
            } else if indexPath.row == 6 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "guidetitlecell") as! GuideTitleCell
                
                return cell
            } else if indexPath.row == 7 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "guidedescription") as! GuideDescriptionCell
                if let myDesc = self.productObject.productDescription {
                    cell.guideDescriptionLabel.attributedText = myDesc
                }
                return cell
            }
        }
        
        // if there is no disclaimer and number of rows will be 7
        else {
            if indexPath.row == 0 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "titlecell") as! TitleCell
                if let myTitle = self.productObject.titleForDetails {
                    cell.titleLabel.attributedText = setupSubtitle(myTitle, withFont: UIFont(name: "Nunito-ExtraBold", size: 18.0)!, withSuperFont: UIFont(name: "Nunito-ExtraBold", size: 9.0)!)
                }
                return cell
            } else if indexPath.row == 1 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "introductioncell") as! IntroductionCell
                if let myIntro = self.productObject.subTitle {
                    cell.introductionLabel.attributedText = setupSubtitle(myIntro, withFont: UIFont(name: "AvenirNext-Medium", size: 16.0)!, withSuperFont: UIFont(name: "AvenirNext-Medium", size: 9.0)!)
                }
                return cell
            } else if indexPath.row == 2 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "imagecell") as! ImageCell
                if let myPic = self.productObject.mainImageForDetail {
                    cell.productImage.image = myPic
                }
                return cell
            } else if indexPath.row == 3 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "benefitscell") as! BenefitsCell
                if let myBenefits = self.productObject.benefitsDetails {
                    cell.image1.image = myBenefits[0].image
                    cell.detail1.attributedText = setupSubtitle(myBenefits[0].details!, withFont: UIFont(name: "AvenirNext-DemiBold", size: 16.0)!, withSuperFont: UIFont(name: "AvenirNext-DemiBold", size: 9.0)!)
                    cell.image2.image = myBenefits[1].image
                    cell.detail2.attributedText = setupSubtitle(myBenefits[1].details!, withFont: UIFont(name: "AvenirNext-DemiBold", size: 16.0)!, withSuperFont: UIFont(name: "AvenirNext-DemiBold", size: 9.0)!)
                    cell.image3.image = myBenefits[2].image
                    cell.detail3.attributedText = setupSubtitle(myBenefits[2].details!, withFont: UIFont(name: "AvenirNext-DemiBold", size: 16.0)!, withSuperFont: UIFont(name: "AvenirNext-DemiBold", size: 9.0)!)
                    
                    if let extraDesc = productObject.extraDescription {
                        cell.extraDescriptionInBenefits.text = extraDesc
                    }
                }
                return cell
            } else if indexPath.row == 4 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "buttonscell") as! ButtonsCell
                cell.buttonDelegate = self
                return cell
            } else if indexPath.row == 5 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "guidetitlecell") as! GuideTitleCell
                
                return cell
            } else if indexPath.row == 6 {
                let cell = productDetailsTableView.dequeueReusableCell(withIdentifier: "guidedescription") as! GuideDescriptionCell
                if let myDesc = self.productObject.productDescription {
                    cell.guideDescriptionLabel.attributedText = myDesc
                }
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
}
