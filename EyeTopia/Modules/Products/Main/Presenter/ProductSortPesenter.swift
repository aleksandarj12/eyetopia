//
//  File.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 3/30/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class ProductSortPesenter: NSObject {
    
    var view : ProductSortAndFilterView!
    
    var isBrandNameAZ : Bool = true
    var isBrandNameZA : Bool = false
    
    var isDryEyeActive : (Bool, [Int]) = (false, [0,2,5,6])
    var isRedEyeActive: (Bool, [Int]) = (false, [4])
    var isEyeHygieneActive: (Bool, [Int]) = (false, [1,3])
    
    var isNighttimeActive : (Bool, [Int]) = (false, [2])
    var isPreservativeFreeActive : (Bool, [Int]) = (false, [6])
    var isContactLensesActive : (Bool, [Int]) = (false, [5])
    
    
    
    var multiSelectionCounter : Int = 0
    
    var searchTerm : String = ""
    
    func attachView(view: ProductSortAndFilterView){
        self.view = view
    }
    
    func detachView() -> Void
    {
        self.view = nil
    }
    
    func incrementMultiSelectionCounter()
    {
        self.multiSelectionCounter += 1
    }
    
    func decrementMultiSelectionCounter()
    {
        self.multiSelectionCounter -= 1
    }
    
    func getSortHolderView()
    {
        self.view.setSortHolderView()
    }
    
    func getFilterHolderView()
    {
       self.view.setFilterHolderView()
    }
    
    func getSearchResult()
    {
        self.getFilterData()
        let array : [Product] = DataManager.sharedInstance.updateSearchTermProduct(searchTerm: self.searchTerm)
        DataManager.sharedInstance.setSearchTermProducts(array: array)
        self.view.setReloadData()
    }
    
    func getSortData()
    {
        let array : [Product] = DataManager.sharedInstance.getSearchedProductArray()
        
        if(isBrandNameAZ)
        {
            let sortedArray = array.sorted {
                $0.title! < $1.title!
            }
            DataManager.sharedInstance.setSearchTermProducts(array: sortedArray)
        }
        else
        {
            let sortedArray = array.sorted {
                $0.title! > $1.title!
            }
            DataManager.sharedInstance.setSearchTermProducts(array: sortedArray)
        }
        
        self.view.setReloadData()
    }
    
    func getFilterData()
    {
        var filteredArray : [Product] = [Product]()
        let array : [Product] = DataManager.sharedInstance.getProductsArray()
        
        let filtersParametars = [isDryEyeActive, isRedEyeActive, isEyeHygieneActive, isNighttimeActive, isPreservativeFreeActive, isContactLensesActive]
        let reducedFiltersParametars = filtersParametars.filter {$0.0 == true}
        
        if reducedFiltersParametars.isEmpty {
            filteredArray = array
        } else {
            let indexParametars = reducedFiltersParametars.map {$0.1}.flatMap {$0}
            let reducedRepeatedIndex = Array(Set(indexParametars))
            
            for i in reducedRepeatedIndex {
                filteredArray.append(array[i])
            }
        }
        
        DataManager.sharedInstance.setSearchTermProducts(array: filteredArray)
       
        
        
//        if(self.multiSelectionCounter == 0)
//        {
//            DataManager.sharedInstance.setSearchTermProducts(array: DataManager.sharedInstance.getProductsArray())
//        }
//        else if(self.multiSelectionCounter > 2)
//        {
//            DataManager.sharedInstance.setSearchTermProducts(array: filteredArray)
//        }
//        else if(self.multiSelectionCounter == 2)
//        {
//            if(isDryEyeActive && isContactLensesActive)
//            {
//                filteredArray.append(array[0])
//            }
//            else if(isDryEyeActive && isPreservativeFreeActive)
//            {
//                filteredArray.append(array[1])
//            }
//            else if(isDryEyeActive && isNighttimeActive)
//            {
//                filteredArray.append(array[3])
//            }
//            DataManager.sharedInstance.setSearchTermProducts(array: filteredArray)
//        }
//        else
//        {
//            if(isDryEyeActive)
//            {
//                if(isContactLensesActive)
//                {
//                    filteredArray.append(array[0])
//                }
//                else if(isPreservativeFreeActive)
//                {
//                    filteredArray.append(array[1])
//                }
//                else if(isNighttimeActive)
//                {
//                    filteredArray.append(array[3])
//                }
//                else if(isRedEyeActive)
//                {
//
//                }
//                else if(isEyeHygieneActive)
//                {
//
//                }
//                else
//                {
//                    filteredArray.append(array[0])
//                    filteredArray.append(array[1])
//                    filteredArray.append(array[2])
//                    filteredArray.append(array[3])
//                }
//
//            }
//            else if(isRedEyeActive)
//            {
//                filteredArray.append(array[6])
//            }
//
//            else if(isEyeHygieneActive)
//            {
//                filteredArray.append(array[4])
//                filteredArray.append(array[5])
//            }
//            else if(isNighttimeActive)
//            {
//                filteredArray.append(array[3])
//            }
//
//            else if(isPreservativeFreeActive)
//            {
//                filteredArray.append(array[1])
//            }
//
//            else if(isContactLensesActive)
//            {
//                filteredArray.append(array[0])
//            }
//
//            DataManager.sharedInstance.setSearchTermProducts(array: filteredArray)
//
//        }
//
//        self.view.setReloadData()
   }

}
