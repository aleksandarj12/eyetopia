//
//  SortViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 4/3/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class SortViewController: UIViewController {

    var presenter : ProductSortPesenter!
    
    @IBOutlet weak var sortHolderVIew: UIView!
    @IBOutlet weak var checkBoxAZ: UIButton!
    @IBOutlet weak var checkBoxZA: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red:0.02, green:0.02, blue:0.06, alpha:0.4)
        sortHolderVIew.layer.cornerRadius = 5
        sortHolderVIew.clipsToBounds = true
        
        if presenter.isBrandNameAZ == true && presenter.isBrandNameZA == false {
            checkBoxAZ.setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
            checkBoxZA.setImage(UIImage(named: "check_box_off"), for: UIControlState.normal)
        } else if presenter.isBrandNameAZ == false && presenter.isBrandNameZA == true {
            checkBoxAZ.setImage(UIImage(named: "check_box_off"), for: UIControlState.normal)
            checkBoxZA.setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
        }
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func hideSortVC(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func checkBoxAZPressed(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "check_box_off") && checkBoxZA.currentImage == UIImage(named: "check_box_on") {
            sender.setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
            presenter.isBrandNameAZ = true
            checkBoxZA.setImage(UIImage(named: "check_box_off"), for: UIControlState.normal)
            presenter.isBrandNameZA = false
        }
    }
    
    @IBAction func checkBoxZAPressed(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "check_box_off") && checkBoxAZ.currentImage == UIImage(named: "check_box_on") {
            sender.setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
            presenter.isBrandNameZA = true
            checkBoxAZ.setImage(UIImage(named: "check_box_off"), for: UIControlState.normal)
            presenter.isBrandNameAZ = false
        }
    }
    
    @IBAction func applyButtonTapped(_ sender: Any)
    {
        
            self.presenter.getSearchResult()
            self.presenter.getSortData()
            self.dismiss(animated: false, completion: nil)
            
        
    
    }
    
    @IBAction func clearSelectionButtonTapped(_ sender: Any)
    {
        presenter.isBrandNameAZ = true
        presenter.isBrandNameZA = false
        presenter.getFilterData()
        presenter.getSortData()
        self.dismiss(animated: false, completion: nil)
    }
    
}
