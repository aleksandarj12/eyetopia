//
//  FilterViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 4/3/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {

    @IBOutlet weak var filterHolderView: UIView!
    
    @IBOutlet weak var dryEyeCheckBox: UIButton!
    @IBOutlet weak var redEyeCheckBox: UIButton!
    @IBOutlet weak var eyeHygieneCheckBox: UIButton!
    @IBOutlet weak var nightTimeCheckBox: UIButton!
    @IBOutlet weak var preservativeFreeCheckBox: UIButton!
    @IBOutlet weak var contactLensesCheckBox: UIButton!
    
    var presenter: ProductSortPesenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red:0.02, green:0.02, blue:0.06, alpha:0.4)
        filterHolderView.layer.cornerRadius = 5
        filterHolderView.clipsToBounds = true
        
        if presenter.isDryEyeActive.0 == true {
            dryEyeCheckBox.setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
        } else {
            dryEyeCheckBox.setImage(UIImage(named: "check_box_off"), for: UIControlState.normal)
        }
        
        if presenter.isRedEyeActive.0 == true {
            redEyeCheckBox.setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
        } else {
            redEyeCheckBox.setImage(UIImage(named: "check_box_off"), for: UIControlState.normal)
        }
        
        if presenter.isEyeHygieneActive.0 == true {
            eyeHygieneCheckBox.setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
        } else {
            eyeHygieneCheckBox.setImage(UIImage(named: "check_box_off"), for: UIControlState.normal)
        }
        
        if presenter.isNighttimeActive.0 == true {
            nightTimeCheckBox.setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
        } else {
            nightTimeCheckBox.setImage(UIImage(named: "check_box_off"), for: UIControlState.normal)
        }
        
        if presenter.isPreservativeFreeActive.0 == true {
            preservativeFreeCheckBox.setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
        } else {
            preservativeFreeCheckBox.setImage(UIImage(named: "check_box_off"), for: UIControlState.normal)
        }
        
        if presenter.isContactLensesActive.0 == true {
            contactLensesCheckBox.setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
        } else {
            contactLensesCheckBox.setImage(UIImage(named: "check_box_off"), for: UIControlState.normal)
        }
        
    }

    
    @IBAction func hideFilterView(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
   
    
    @IBAction func dryEyeCheckBoxPressed(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "check_box_off") {
            sender .setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
            self.presenter.isDryEyeActive.0 = true
            self.presenter.incrementMultiSelectionCounter()
        }
        else if sender.currentImage == UIImage(named: "check_box_on") {
            sender.setImage(UIImage(named: "check_box_off") , for: UIControlState.normal)
            self.presenter.isDryEyeActive.0 = false
            self.presenter.decrementMultiSelectionCounter()
        }
    }
    @IBAction func redEyeCheckBoxPressed(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "check_box_off") {
            sender .setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
            self.presenter.isRedEyeActive.0 = true
            self.presenter.incrementMultiSelectionCounter()
        }
        else if sender.currentImage == UIImage(named: "check_box_on") {
            sender.setImage(UIImage(named: "check_box_off") , for: UIControlState.normal)
            self.presenter.isRedEyeActive.0 = false
            self.presenter.decrementMultiSelectionCounter()
        }
    }
    @IBAction func eyeHygieneCheckBoxPressed(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "check_box_off") {
            sender .setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
            presenter.isEyeHygieneActive.0 = true
            self.presenter.incrementMultiSelectionCounter()
        }
        else if sender.currentImage == UIImage(named: "check_box_on") {
            sender.setImage(UIImage(named: "check_box_off") , for: UIControlState.normal)
            presenter.isEyeHygieneActive.0 = false
            self.presenter.decrementMultiSelectionCounter()
        }
    }
    @IBAction func nightTimeCheckBoxPressed(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "check_box_off") {
            sender .setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
            presenter.isNighttimeActive.0 = true
            self.presenter.incrementMultiSelectionCounter()
        }
       else if sender.currentImage == UIImage(named: "check_box_on") {
            sender.setImage(UIImage(named: "check_box_off") , for: UIControlState.normal)
            self.presenter.isNighttimeActive.0 = false
            self.presenter.decrementMultiSelectionCounter()
        }
    }
    @IBAction func preservativeFreeCheckBoxPressed(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "check_box_off") {
            sender .setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
            self.presenter.isPreservativeFreeActive.0 = true
            self.presenter.incrementMultiSelectionCounter()
        }
        else if sender.currentImage == UIImage(named: "check_box_on") {
            sender.setImage(UIImage(named: "check_box_off") , for: UIControlState.normal)
            presenter.isPreservativeFreeActive.0 = false
            self.presenter.decrementMultiSelectionCounter()
        }
    }
    @IBAction func contactLensesCheckBoxPressed(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "check_box_off") {
            sender .setImage(UIImage(named: "check_box_on"), for: UIControlState.normal)
            presenter.isContactLensesActive.0 = true
            presenter.incrementMultiSelectionCounter()
        }
        else if sender.currentImage == UIImage(named: "check_box_on") {
            sender.setImage(UIImage(named: "check_box_off") , for: UIControlState.normal)
            presenter.isContactLensesActive.0 = false
            presenter.decrementMultiSelectionCounter()
        }
    }
    
    @IBAction func applyButtonTapped(_ sender: Any)
    {
        self.presenter.getSearchResult()
        self.presenter.getSortData()
//        presenter.multiSelectionCounter = 0
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func clearSelectionButtonTapped(_ sender: Any)
    {
        presenter.isDryEyeActive.0 = false
        presenter.isPreservativeFreeActive.0 = false
        presenter.isRedEyeActive.0 = false
        presenter.isContactLensesActive.0 = false
        presenter.isNighttimeActive.0 = false
        presenter.isEyeHygieneActive.0 = false
        presenter.multiSelectionCounter = 0
        presenter.getFilterData()
        presenter.getSortData()
        self.dismiss(animated: false, completion: nil)
    }
    
    
}
