//
//  ZoomImageViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 5/11/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class ZoomImageViewController: UIViewController {
    
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var zoomedImage: UIImageView!
    @IBOutlet weak var viewForTapRecognizer: UIView!
    var largeImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        holderView.layer.cornerRadius = 5
        
        if let myImage = largeImage {
            zoomedImage.image = myImage
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        viewForTapRecognizer .addGestureRecognizer(tapGesture)
    }

    @objc func handleTap(){
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func minimizaButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
