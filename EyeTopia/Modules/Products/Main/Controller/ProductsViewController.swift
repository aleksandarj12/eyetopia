//
//  ProductsViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/16/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ProductSortAndFilterView, UITextFieldDelegate, HeaderTableViewCellDelegate {

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var scrollableContentHolderView: UIView!
    @IBOutlet weak var headerHolderView: UIView!
    
    
  
    
    
    var presenter: ProductSortPesenter = ProductSortPesenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 300
        self.tableView.backgroundColor = UIColor(red: 249/255.0, green: 249/255.0, blue: 249/255.0, alpha: 1)
        
        self.tableView.register(UINib(nibName: "ProductViewCell", bundle: nil), forCellReuseIdentifier: "ProductViewCell")
        self.tableView.register(UINib(nibName: "HeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderTableViewCell")

        self.presenter.attachView(view: self)
        self.setupSearchTextField()
        deleteButton.isHidden = true
        
    }

    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        if(TransitionManager.sharedInstance.storyboardID == "ProductDetailsVC")
        {
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
            
            if let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as? ProductDetailsViewController {
                destinationVC.productObject = TransitionManager.sharedInstance.productLearnMoreSelected
                TransitionManager.sharedInstance.productLearnMoreSelected = nil
                destinationVC.isMain = true
                self.navigationController?.pushViewController(destinationVC, animated: false)
            }
        }
        self.tableView.reloadData()
        
    }
    

    //MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        var fullString = textField.text
        let count : Int = (fullString?.count)!
        if(string == "" && count > 0)
        {
            fullString?.removeLast()
            
        }
        else
        {
            fullString?.append(string)
            self.deleteButton.isHidden = false
        }
        
        if fullString == "" {
            self.deleteButton.isHidden = true
        }
        self.presenter.searchTerm = fullString!
        self.presenter.getSearchResult()
        self.presenter.getSortData()
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.searchTextField.resignFirstResponder()
        return true
    }
    
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        self.searchTextField.text = ""
        self.presenter.searchTerm = ""
        self.presenter.getSearchResult()
        self.presenter.getSortData()
        self.deleteButton.isHidden = true
    }
    
    // MARK: - UITableView DataSource & Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if UserManager.sharedInstance.getDiagnosedJourneyCompletion() == true {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if numberOfSections(in: self.tableView) == 1 {
            return UITableViewAutomaticDimension
        }
        if numberOfSections(in: self.tableView) == 2 {
            if indexPath.section == 0 {
                return 156
            } else {
                return UITableViewAutomaticDimension
            }
        }
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {   if numberOfSections(in: self.tableView) == 1 {
        return DataManager.sharedInstance.getSearchedProductArray().count
    } else {
        if section == 0 {
            return 1
        } else {
            return DataManager.sharedInstance.getSearchedProductArray().count
        }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if numberOfSections(in: self.tableView) == 2 {
            if indexPath.section == 0 {
                let cell: HeaderTableViewCell = self.tableView.dequeueReusableCell(withIdentifier:"HeaderTableViewCell") as! HeaderTableViewCell
                cell.headerDelegate = self
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
            if indexPath.section == 1 {
                let cell : ProductViewCell = self.tableView.dequeueReusableCell(withIdentifier: "ProductViewCell") as! ProductViewCell
                
                let product : Product = DataManager.sharedInstance.getSearchedProductArray()[indexPath.row]
                
                if let mainImageView = product.mainImage {
                    cell.mainImageView.image = mainImageView
                }
                
                if let titleLabel = product.title {
                    cell.titleLabel.text = titleLabel
                }
                if(indexPath.row == DataManager.sharedInstance.getSearchedProductArray().count - 1)
                {
                    cell.separatorView.isHidden = true
                }
                else
                {
                    cell.separatorView.isHidden = false
                }
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.cellDelegate = self
                
                return cell
            }
        }
        
        if numberOfSections(in: self.tableView) == 1 {
            let cell : ProductViewCell = self.tableView.dequeueReusableCell(withIdentifier: "ProductViewCell") as! ProductViewCell
            
            let product : Product = DataManager.sharedInstance.getSearchedProductArray()[indexPath.row]
            
            if let mainImageView = product.mainImage {
                cell.mainImageView.image = mainImageView
            }
            
            if let titleLabel = product.title {
                cell.titleLabel.text = titleLabel
            }
            if(indexPath.row == DataManager.sharedInstance.getSearchedProductArray().count - 1)
            {
                cell.separatorView.isHidden = true
            }
            else
            {
                cell.separatorView.isHidden = false
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.cellDelegate = self
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let _ = tableView.cellForRow(at: indexPath) as? ProductViewCell,
            let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as? ProductDetailsViewController {
            destinationVC.productObject = DataManager.sharedInstance.getSearchedProductArray()[indexPath.row]
            destinationVC.isMain = true
            self.navigationController?.pushViewController(destinationVC, animated: true)
        }
      
    }
    

    
    
    //MARK: ProductSortAndFilterView
    
    func setReloadData()
    {
        self.tableView.reloadData()
    }

    
    //MARK: Sort
    
    func setSortHolderView() {
        
        if let sortVC: SortViewController = self.storyboard?.instantiateViewController(withIdentifier: "SortVCForProduct") as? SortViewController {
            sortVC.presenter = self.presenter
            self.present(sortVC, animated: false, completion: nil)
        }
        
    }
    
    
    //MARK: Action Methods
    
    @IBAction func showSortView(_ sender: UIButton) {
      
        presenter.getSortHolderView()
      
    }
    
//    @IBAction func viewCardButtonTapped(_ sender: Any)
//    {
//         NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentLandingSavingsCardKey), object: self)
//    }
    
    func showSavingCard() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentLandingSavingsCardKey), object: self)
    }
    
    
    //MARK: Filter
    
    func setFilterHolderView() {
        
        let controller : FilterViewController = (self.storyboard?.instantiateViewController(withIdentifier: "FilterVCForProduct") as? FilterViewController)!
            controller.presenter = self.presenter
            self.present(controller, animated: false, completion: nil)
        
    }
    
    
    @IBAction func showFilterView(_ sender: UIButton) {
        self.presenter.getFilterHolderView()
    }
    
    @objc func doneKeyboardButtonTapped()
    {
       self.searchTextField.resignFirstResponder()
    }
    
    //MARK: Settup and Support methods
    
    
    
    func setupSearchTextField()
    {
        self.searchTextField.delegate = self
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneKeyboardButtonTapped))
        
        keyboardToolbar.items = [flexBarButton, doneBarButton]
    
        self.searchTextField.layer.cornerRadius = 15
        self.searchTextField.clipsToBounds = true
        
        
        self.searchTextField.leftViewMode = .always
        let view : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: self.searchTextField.frame.size.height))
        self.searchTextField.leftView = view
        self.searchTextField.inputAccessoryView = keyboardToolbar
    }
    
}

extension ProductsViewController: ProductViewCellDelegate {
    
    func showDetailForProduct(sender: ProductViewCell) {
        if let indexPath = self.tableView.indexPath(for: sender) {
            
            if let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as? ProductDetailsViewController {
                destinationVC.productObject = DataManager.sharedInstance.getSearchedProductArray()[indexPath.row]
                destinationVC.isMain = true
                self.navigationController?.pushViewController(destinationVC, animated: true)
            }
        }
    }
    
    func showWebLink(sender: ProductViewCell) {
        if let indexPath = self.tableView.indexPath(for: sender) {
             let productObject = DataManager.sharedInstance.getSearchedProductArray()[indexPath.row]
            if let link = productObject.externalLink, let urlString = URL(string: link), let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "interstitialVC") as? InterstitialViewController {
                destinationVC.link = urlString
                self.present(destinationVC, animated: true, completion: nil)
            }
        }
    }
    
    func showZoomedImage(sender: ProductViewCell) {
        if let indexPath = self.tableView.indexPath(for: sender) {
            
           let requiredProduct = DataManager.sharedInstance.getSearchedProductArray()[indexPath.row]
            
            let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
            
            if let destinationVC = storyboard.instantiateViewController(withIdentifier: "ZoomVC") as? ZoomImageViewController, let imageL = requiredProduct.zoomImage {
                destinationVC.largeImage = imageL
                self.present(destinationVC, animated: false, completion: nil)
            }
        }
    }
    
}
