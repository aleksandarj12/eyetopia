//
//  File.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 3/30/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

protocol ProductSortAndFilterView {
    func setSortHolderView()
    func setFilterHolderView()
    func setReloadData()
    
}












