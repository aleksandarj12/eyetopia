//
//  HeaderTableViewCell.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 6/25/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

protocol HeaderTableViewCellDelegate {
    func showSavingCard()
}

class HeaderTableViewCell: UITableViewCell {

    var headerDelegate: HeaderTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func viewCardTapped(_ sender: Any) {
        headerDelegate?.showSavingCard()
    }
    
}
