//
//  ProductViewCell.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/21/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class ProductViewCell: UITableViewCell {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsButton: UIButton!
    @IBOutlet weak var separatorView: UIView!
    
    weak var cellDelegate: ProductViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func detailsButtonTapped(_ sender: Any)
    {
        self.cellDelegate?.showDetailForProduct(sender: self)
    }
    
    @IBAction func BuyNowTapped(_ sender: Any) {
        self.cellDelegate?.showWebLink(sender: self)
    }
    @IBAction func maximizeTapped(_ sender: UIButton) {
        self.cellDelegate?.showZoomedImage(sender: self)
    }
    
}


protocol ProductViewCellDelegate: class {
    func showDetailForProduct(sender: ProductViewCell)
    func showWebLink(sender: ProductViewCell)
    func showZoomedImage(sender: ProductViewCell)
}
