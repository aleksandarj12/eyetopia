//
//  ResultBalanceUltraPrimaryView.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/28/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

protocol ResultBalanceUltraView
{
    func setSystaneBalanceHolderView(flag : Bool)
    func setUltraUltraUDHolderView(flag : Bool)
    func setUltraPrimaryHolderView(flag : Bool)
    func setUltraUDSingleUse(flag : Bool)
    func setGelDropAndLidWipesView(flag: Bool)
    func setEyeWashView(flag: Bool)
}
