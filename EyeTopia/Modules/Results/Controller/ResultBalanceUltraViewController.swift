//
//  ResultBalanceAndUltraPrimaryViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 3/15/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

class ResultBalanceUltraViewController: UIViewController, ResultBalanceUltraView {
    
    @IBOutlet weak var systaneBalanceDescriptionLabel: UILabel!
    //Constraints outlets:
    @IBOutlet weak var balanceHeightCon: NSLayoutConstraint!
    @IBOutlet weak var ultraTogetherHeightCon: NSLayoutConstraint!
    @IBOutlet weak var ultraPrimaryHeightCon: NSLayoutConstraint!
    @IBOutlet weak var ultraUdSingleHeightCon: NSLayoutConstraint!
    @IBOutlet weak var gelDropHeightCon: NSLayoutConstraint!
    @IBOutlet weak var eyeWashHeightCon: NSLayoutConstraint!
    

    @IBOutlet weak var komilaOpinion: UILabel!
    @IBOutlet weak var gelIntroLabel: UILabel!
    @IBOutlet weak var eyeWashIntroLabel: UILabel!
    
    @IBOutlet weak var pollenForecastButton: UIButton!
    
    @IBOutlet weak var upperShadow: UIView!
    @IBOutlet weak var upperShadow2: UIView!
    
    //views in stack view
    @IBOutlet weak var systaneBalanceLubricantEyeDropsView: UIView!
    @IBOutlet weak var ultraUdTogetherInOneView: UIView!
    //@IBOutlet weak var ultraUd: UIView!
    @IBOutlet weak var ultraUdSingleUse: UIView!
    @IBOutlet weak var gelDropAndLidWipesView: UIView!
    @IBOutlet weak var eyeWashView: UIView!
    @IBOutlet weak var ultraPrimaryHolderView: UIView!
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    var presenter : ResultBalanceUltraPresenter = ResultBalanceUltraPresenter()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       self.setupText()
        
        UserManager.sharedInstance.setJourneyStation(nil, nil)
        if(!TransitionManager.sharedInstance.getFirstLaunchFlow())
        {
            TransitionManager.sharedInstance.setFirstLaunchFlow(flag: true)
        }
        
        UserManager.sharedInstance.setDiagnosedJourneyCompetion(flag: true)
        NotificationManager.sharedInstance.scheduleJourneyContinuationReminderNotification()
        NotificationManager.sharedInstance.scheduleJourneyAgain()
        
        self.presenter.attachView(view: self)
        self.presenter.setupResult()
        self.setupDescriptionText()
        self.setUpCornerRadiusForShadows()
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UIScreen.main.bounds.size.width == 320 {
            balanceHeightCon.constant = 500
            ultraTogetherHeightCon.constant = 835
            ultraPrimaryHeightCon.constant = 490
            ultraUdSingleHeightCon.constant = 510
            gelDropHeightCon.constant = 650
            eyeWashHeightCon.constant = 400
        } else if UIScreen.main.bounds.size.width == 375 {
            balanceHeightCon.constant = 480
            ultraTogetherHeightCon.constant = 775
            ultraPrimaryHeightCon.constant = 470
            ultraUdSingleHeightCon.constant = 465
            gelDropHeightCon.constant = 635
            eyeWashHeightCon.constant = 370
        } else if UIScreen.main.bounds.size.width == 414 {
            balanceHeightCon.constant = 480
            ultraTogetherHeightCon.constant = 775
            ultraPrimaryHeightCon.constant = 470
            ultraUdSingleHeightCon.constant = 450
            gelDropHeightCon.constant = 635
            eyeWashHeightCon.constant = 375
        }
        Localytics.tagEvent(trackingEvents.productMatchProvidedKey)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        Localytics.tagEvent(trackingEvents.userCompleteDiagnosedJourneyKey)
    }
    
    //MARK: ResultBalanceUltraView
    
    func setSystaneBalanceHolderView(flag: Bool)
    {
        systaneBalanceLubricantEyeDropsView.isHidden = flag
    }
    
    func setUltraUltraUDHolderView(flag : Bool)
    {
        ultraUdTogetherInOneView.isHidden = flag
    }
    
    func setUltraPrimaryHolderView(flag : Bool)
    {
       ultraPrimaryHolderView.isHidden = flag
    }
    
    func setUltraUDSingleUse(flag : Bool)
    {
        ultraUdSingleUse.isHidden = flag
    }
    
    func setGelDropAndLidWipesView(flag: Bool) {
        
        gelDropAndLidWipesView.isHidden = flag
    }
    
    func setEyeWashView(flag: Bool) {
        
        eyeWashView.isHidden = flag
    }
    
    //MARK: Setup & Support Methods
    
    func setupSubtitle(_ text : String) -> NSAttributedString
    {
        
        let fontSuper : UIFont? = UIFont(name: "AvenirNext-Medium", size:10)
        let font : UIFont = UIFont(name: "AvenirNext-Medium", size: 16)!
        
        var ranges : [NSRange] = [NSRange]()
        
        var string : NSString = text as NSString
        
        while (string.contains(k.text.fontSuper))
        {
            let range : NSRange = string.range(of: k.text.fontSuper)
            string = string.replacingCharacters(in: range, with: "") as NSString
            
            let newRange : NSRange = NSRange(location: range.location - 1, length: 1)
            ranges.append(newRange)
        }
        
        let attString : NSMutableAttributedString = NSMutableAttributedString(string: string as String, attributes: [.font:font])
        
        for range in ranges
        {
            attString.setAttributes([.font:fontSuper!,.baselineOffset:8], range: range)
        }
        
        return attString
    }
    
    func setupText()
    {
        self.systaneBalanceDescriptionLabel.attributedText = self.setupSubtitle("Get restoring dry eye relief with SYSTANE® BALANCE Lubricant Eye Drops with the LipitechT‰M‰ system that locks in essential moisture.")
    }
    
    func setupProdutDetails(_ selectedIndex : Int)
    {
        TransitionManager.sharedInstance.productLearnMoreSelected = DataManager.sharedInstance.products[selectedIndex]
        TransitionManager.sharedInstance.setStoryboardID(storyboardID: "ProductDetailsVC")
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentProductDetailsKey), object: self)
    }
    
    func setupProductDetails(product : Product)
    {
        let controller : ProductDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsViewController
        controller.isJourney = true
        self.present(controller, animated: true, completion: nil)
    }
    
    func setupDescriptionText()
    {
        let attributedString = NSMutableAttributedString(string: "Based on your answers, EyeTopia has determined that you’re experiencing dry eye symptoms and may benefit from these eye drops.", attributes: [
            .font: UIFont(name: "AvenirNext-Bold", size: 16.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0),
            .kern: 0.0
            ])
        attributedString.addAttribute(.font, value: UIFont(name: "AvenirNext-Medium", size: 16.0)!, range: NSRange(location: 0, length: 51))
        komilaOpinion.attributedText = attributedString
            
        let attributedGelLabel = NSMutableAttributedString(string: "Because you told us that you have dry eye symptoms at night, try these products for night time relief.", attributes: [
            .font: UIFont(name: "AvenirNext-Medium", size: 16.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0),
            .kern: 0.0
            ])
        attributedGelLabel.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 16.0)!, range: NSRange(location: 25, length: 34))

        gelIntroLabel.attributedText = attributedGelLabel
        
        let eyeWashIntroAttribtedText = NSMutableAttributedString(string: "Because you told us that you suffer from hayfever allergies, try SYSTANE® EYE WASH that helps remove irritants like pollen. ", attributes: [
            .font: UIFont(name: "AvenirNext-Medium", size: 16.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0),
            .kern: 0.0
            ])
        eyeWashIntroAttribtedText.addAttribute(.font, value: UIFont(name: "AvenirNext-Bold", size: 16.0)!, range: NSRange(location: 25, length: 34))
            
            
            
        eyeWashIntroLabel.attributedText = eyeWashIntroAttribtedText
    }
    
    // MARRK: Action for learn more and find a retailer and get savings
    
    
    @IBAction func learnMoreForSystaneBalanceLubricantEyeDrop(_ sender: UIButton) {
        
        self.setupProdutDetails(0)
    }
    
    @IBAction func learnMoreForSystaneULTRALubricantEyeDrop(_ sender: UIButton) {
        self.setupProdutDetails(5)
    }
    
    
    @IBAction func learnMoreForSystaneULTRAUDlubricantEyeDropPreservativeFree(_ sender: UIButton) {
        self.setupProdutDetails(6)
    }
    
    @IBAction func pollenForecastButtonTapped(_ sender: Any)
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentPollenForecastKey), object: self)
    }
    
    @IBAction func learnMoreForSystaneULTRALubricantEyeDropSingleView(_ sender: UIButton) {
        self.setupProdutDetails(5)
    }
    
    
    @IBAction func learnMoreForSystaneULTRAUDlubricantEyeDropPreservativeFreeSingleView(_ sender: UIButton) {
        self.setupProdutDetails(6)

    }
    
    @IBAction func learnMoreForSystaneGelDrop(_ sender: UIButton) {
        self.setupProdutDetails(2)
    }
    
    
    @IBAction func learnMoreForLidWipes(_ sender: UIButton) {
        self.setupProdutDetails(3)
    }
    
    
    @IBAction func learnMoreForEyeWash(_ sender: UIButton) {
        self.setupProdutDetails(1)
    }
    
    
    @IBAction func getSavingsPressed(_ sender: UIButton) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentCouponAndSavingCardKey), object: self)
//
//        if let savingCard = storyboard?.instantiateViewController(withIdentifier: "SystaneCardViewController") as? SystaneCardViewController {
//            self.present(savingCard, animated: false, completion: nil)
    }
    
    private func findARetailer() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentFindRetailerKey), object: self)
//        
//        let appDelegate = TransitionManager.sharedInstance.appDelegate
//
//        if let controllers = appDelegate?.tabController.viewControllers{
//
//            for (index,controller) in controllers.enumerated() {
//                if let navController = controller as? UINavigationController, let root = navController.viewControllers[0] as? LocatorOptionViewController {
//                    appDelegate?.tabController.selectedIndex = index
//                    root.setupLocationDetailsViewController()
//                    UserManager.sharedInstance.setSearchOption(searchOption: .retailer)
//                    break
//                }
//            }
    }
    
    @IBAction func savingsButtonTapped(_ sender: Any)
    {
       NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentCouponAndSavingCardKey), object: self)
    }
    
    @IBAction func findARetailerTapped(_ sender: UIButton) {
        findARetailer()
    }
    
    @IBAction func zoomTapped(_ sender: UIButton) {
        var zoomedImaged = UIImage()
        
        switch sender.tag {
        case 10: zoomedImaged = UIImage(named: "PRODUCT_0003_Balance")!
        case 20: zoomedImaged = UIImage(named: "PRODUCT_0000_ULTRA")!
        case 30: zoomedImaged = UIImage(named: "PRODUCT_0001_ULTRA-UD")!
        case 40: zoomedImaged = UIImage(named: "PRODUCT_0004_Gel-Drops")!
        case 50: zoomedImaged = UIImage(named: "PRODUCT_0005_Lid-Wipes")!
        case 60: zoomedImaged = UIImage(named: "PRODUCT_0006_Eye-Wash")!
        case 202: zoomedImaged = UIImage(named: "PRODUCT_0000_ULTRA")!
        case 303: zoomedImaged = UIImage(named: "PRODUCT_0001_ULTRA-UD")!
        default:
            break
        }
        
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        let destinationVC = storyboard.instantiateViewController(withIdentifier: "ZoomVC") as? ZoomImageViewController
        destinationVC?.largeImage = zoomedImaged
        self.present(destinationVC!, animated: false, completion: nil)
        
    }
    private func setUpCornerRadiusForShadows() {
        
        let requiredColor = UIColor(red: 206/255.0, green: 206/255.0, blue: 206/255.0, alpha: 1)
        
        self.ultraUdTogetherInOneView.layer.borderWidth = 1
        self.ultraUdTogetherInOneView.layer.cornerRadius = 5
        self.ultraUdTogetherInOneView.clipsToBounds = true
        self.ultraUdTogetherInOneView.layer.borderColor = requiredColor.cgColor
        
        self.ultraUdSingleUse.layer.borderColor = requiredColor.cgColor
        self.ultraUdSingleUse.layer.cornerRadius = 5
        self.ultraUdSingleUse.clipsToBounds = true
        self.ultraUdSingleUse.layer.borderWidth = 1
        
        self.eyeWashView.layer.borderColor = requiredColor.cgColor
        self.eyeWashView.layer.cornerRadius = 5
        self.eyeWashView.clipsToBounds = true
        self.eyeWashView.layer.borderWidth = 1
        
        self.gelDropAndLidWipesView.layer.borderColor = requiredColor.cgColor
        self.gelDropAndLidWipesView.layer.cornerRadius = 5
        self.gelDropAndLidWipesView.clipsToBounds = true
        self.gelDropAndLidWipesView.layer.borderWidth = 1
        
        systaneBalanceLubricantEyeDropsView.layer.cornerRadius = 5
        systaneBalanceLubricantEyeDropsView.layer.borderWidth = 1
        systaneBalanceLubricantEyeDropsView.layer.borderColor = UIColor(red: 0/255.0, green: 143/255.0, blue: 136/255.0, alpha: 1).cgColor
        ultraUdTogetherInOneView.layer.cornerRadius = 5
        ultraPrimaryHolderView.layer.cornerRadius = 5
        ultraPrimaryHolderView.layer.borderWidth = 1
        ultraPrimaryHolderView.layer.borderColor = UIColor(red: 0/255.0, green: 143/255.0, blue: 136/255.0, alpha: 1).cgColor
        ultraUdSingleUse.layer.cornerRadius = 5
        gelDropAndLidWipesView.layer.cornerRadius = 5
        eyeWashView.layer.cornerRadius = 5
        systaneBalanceLubricantEyeDropsView.clipsToBounds = true
        ultraUdTogetherInOneView.clipsToBounds = true
        ultraPrimaryHolderView.clipsToBounds = true
        ultraUdSingleUse.clipsToBounds = true
        gelDropAndLidWipesView.clipsToBounds = true
        eyeWashView.clipsToBounds = true
        
        upperShadow.layer.cornerRadius = 5
        upperShadow2.layer.cornerRadius = 5

    }
    
    
}
