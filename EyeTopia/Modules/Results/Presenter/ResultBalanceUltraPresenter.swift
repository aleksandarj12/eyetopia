//
//  ResultBalanceUltraPrimaryPresenter.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/28/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

class ResultBalanceUltraPresenter: NSObject {
    
    var view : ResultBalanceUltraView!
    
    func attachView(view: ResultBalanceUltraView)
    {
        self.view = view
    }
    
    func detachView() -> Void
    {
        self.view = nil
    }
    
    func setupResult()
    {
        let titleSymptom = UserManager.sharedInstance.getSelectedEyeSymptom()?.title
        
        if(UserManager.sharedInstance.getWearingContactLenses())
        {
            UserManager.sharedInstance.setIsActiveUltraUDSingleUse(flag: true)
            UserManager.sharedInstance.setIsActiveBalance(flag: false)
            UserManager.sharedInstance.setIsActiveUltraUltraUD(flag: false)
            UserManager.sharedInstance.setIsActiveUltraPrimary(flag: true)
        }
        else
        {
            if(TransitionManager.sharedInstance.isSkipToProductMatch)
            {
                if(titleSymptom == k.text.burning)
                {
                    UserManager.sharedInstance.setIsActiveUltraUDSingleUse(flag: false)
                    UserManager.sharedInstance.setIsActiveBalance(flag: true)
                    UserManager.sharedInstance.setIsActiveUltraUltraUD(flag: true)
                    UserManager.sharedInstance.setIsActiveUltraPrimary(flag: false)
                    
                }
                else if(titleSymptom == k.text.sandy || titleSymptom == k.text.dry)
                {
                    UserManager.sharedInstance.setIsActiveUltraUDSingleUse(flag: true)
                    UserManager.sharedInstance.setIsActiveBalance(flag: false)
                    UserManager.sharedInstance.setIsActiveUltraUltraUD(flag: false)
                    UserManager.sharedInstance.setIsActiveUltraPrimary(flag: true)
                    
                }
                
            }
            else
            {
                if(titleSymptom == k.text.sandy && !UserManager.sharedInstance.getScreenOrDrivingProblem() && !UserManager.sharedInstance.getAirconEnvironmentProblem())
                {
                    UserManager.sharedInstance.setIsActiveUltraUDSingleUse(flag: true)
                    UserManager.sharedInstance.setIsActiveBalance(flag: false)
                    UserManager.sharedInstance.setIsActiveUltraUltraUD(flag: false)
                    UserManager.sharedInstance.setIsActiveUltraPrimary(flag: true)
                }
                else if(titleSymptom == k.text.dry && !UserManager.sharedInstance.getScreenOrDrivingProblem() && !UserManager.sharedInstance.getAirconEnvironmentProblem())
                {
                    UserManager.sharedInstance.setIsActiveUltraUDSingleUse(flag: true)
                    UserManager.sharedInstance.setIsActiveBalance(flag: false)
                    UserManager.sharedInstance.setIsActiveUltraUltraUD(flag: false)
                    UserManager.sharedInstance.setIsActiveUltraPrimary(flag: true)
                }
                else
                {
                    UserManager.sharedInstance.setIsActiveUltraUDSingleUse(flag: false)
                    UserManager.sharedInstance.setIsActiveBalance(flag: true)
                    UserManager.sharedInstance.setIsActiveUltraUltraUD(flag: true)
                    UserManager.sharedInstance.setIsActiveUltraPrimary(flag: false)
                }
            }
        }
        
        self.setupMostMatchedTrackingInfo()
        
        self.view.setUltraUDSingleUse(flag: !UserManager.sharedInstance.getIsActiveUltraUDSingleUse())
        self.view.setSystaneBalanceHolderView(flag: !UserManager.sharedInstance.getIsActiveBalance())
        self.view.setUltraUltraUDHolderView(flag: !UserManager.sharedInstance.getIsActiveUltraUltraUD())
        self.view.setUltraPrimaryHolderView(flag: !UserManager.sharedInstance.getIsActiveUltraUD())
        self.view.setGelDropAndLidWipesView(flag: !UserManager.sharedInstance.getDryEyesAtNight())
        self.view.setEyeWashView(flag: !UserManager.sharedInstance.getSufferFromAllergy())
    }
    
    
    func setupMostMatchedTrackingInfo()
    {
        if(UserManager.sharedInstance.getIsActiveBalance())
        {
            Localytics.tagEvent(trackingEvents.productMostMatchedKey, attributes: ["Product" : "SYSTANE® BALANCE Lubricant Eye Drops"], customerValueIncrease: 1)
        }
        if(UserManager.sharedInstance.getIsActiveUltraUD())
        {
            Localytics.tagEvent(trackingEvents.productMostMatchedKey, attributes: ["Product" : "SYSTANE® ULTRA Lubricant Eye Drops"], customerValueIncrease: 1)
        }
    }
    
}
