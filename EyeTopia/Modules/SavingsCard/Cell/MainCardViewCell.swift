//
//  MainCardViewCell.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 6/1/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

protocol MainCardViewCellDelegate {
    func didSetAlert(_ message : String)
    func didHideLoadingIndicator()
    func didSelectBarcodeButton()
    func didSelectEnlarge()
    func didCallForNewCard()
}

class MainCardViewCell: UITableViewCell {

    @IBOutlet weak var showBarcodeButton: UIButton!
    @IBOutlet weak var timeLeftLabel: UILabel!
    @IBOutlet weak var countDownHolderView: UIView!
    @IBOutlet weak var mainTopCircleHolderView: UIView!
    @IBOutlet weak var barcodeHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var barcodeWidthLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var barcodeImageView: UIImageView!
    @IBOutlet weak var mainCardHolderView: UIView!
    @IBOutlet weak var mainTopHolderView: UIView!
    @IBOutlet weak var coverForLoading: UIView!
    
    @IBOutlet weak var enlargeHolderView: UIView!
    
    
    @IBOutlet weak var instructionsLabelHeight: NSLayoutConstraint!
    var isShownBarcode : Bool = false
    
    @IBOutlet weak var barcodeHolderView: UIView!
    
    @IBOutlet weak var expiredPeriodLabel: UILabel!
    var delegate : MainCardViewCellDelegate!
    
//    var barcodeImageFrame : CGRect = CGRect()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK: Setup & Support Methods
    
    func checkExpireTime() -> Bool
    {
        var date : Date = UserManager.sharedInstance.getDateActivationCard()!
        date = Calendar.current.date(byAdding: .day, value: UserManager.sharedInstance.getSavingCardTime()!, to: date)!
        
        if(date >= Date())
        {
            return false
        }
        else
        {
            return true
        }
    }
    
    func setupTimecell(_ timeString : String?, _ updateTime : String?)
    {
        
    }
    
    func setupTime()
    {
        let activatedDate = UserManager.sharedInstance.getDateActivationCard()
        let timeAvailable = UserManager.sharedInstance.getSavingCardTime()
        let newDate = Calendar.current.date(byAdding: .day, value: timeAvailable!, to: activatedDate!)

        
        
        let dateToExpire = UserManager.sharedInstance.getExpireDateCard()
//        var ourDate = Calendar.current.date(byAdding: .hour, value: 25, to: Date())!
        let gregorianCalendar : Calendar = Calendar(identifier: .gregorian)
        
        if(UserManager.sharedInstance.getNextCardPeriod() != nil)
        {
            let flag : Bool = UserManager.sharedInstance.getNextCardPeriod()?.compare(Date()) == .orderedDescending
            if(flag)
            {
                self.setupAvailableCardTime()
            }
            else
            {
                self.setupExpiredCard()
            }
        }
        
        if(newDate?.compare(Date()) == .orderedDescending)
        {
            let components : DateComponents = gregorianCalendar.dateComponents([.day, .hour, .minute], from: Date(), to: newDate!)
            let daysCount : Int = components.day!
            let hoursCount : Int = components.hour!
            
            if(daysCount == 0)
            {
                if(hoursCount == 0)
                {
                    self.timeLeftLabel.text = "<1 hr remaining to use this barcode"
                }
                else
                {
                    self.timeLeftLabel.text = hoursCount.description + " hr remaining to use this barcode"
                }
            }
            else
            {
                let daysAppendByOne = daysCount + 1
                self.timeLeftLabel.text = daysAppendByOne.description + " days remaining to use this barcode"
            }
        }
        else
        {
            self.setupExpiredCard()
        }
    }
    
    func setupExpiredCard()
    {
//        var ourDate = Calendar.current.date(byAdding: .month, value: 1, to: Date())!
//        ourDate = Calendar.current.date(byAdding: .day, value: 1, to: ourDate)!

        if(UserManager.sharedInstance.getNextCardPeriod()?.compare(Date()) == .orderedAscending)
        {
            if(!self.isShownBarcode)
            {
                self.isShownBarcode = true
                UserManager.sharedInstance.setIsSavingCardActivated(false)
                UserManager.sharedInstance.setDateSavingCardActivated(nil)
                UserManager.sharedInstance.setSavingCardID(cardID: nil)
                self.delegate.didCallForNewCard()
                self.showBarcodeButton.isEnabled = true
                self.showBarcodeButton.setBackgroundImage(UIImage(named: "btnDefaultActive"), for: .normal)
            }
        }
        else
        {
            self.mainLabel.isHidden = true
            self.instructionLabel.isHidden = true
            self.barcodeImageView.isHidden = true
            self.countDownHolderView.isHidden = true
            self.enlargeHolderView.isHidden = true
            
            self.expiredPeriodLabel.isHidden = false
            self.barcodeHolderView.isHidden = false
            self.showBarcodeButton.isEnabled = false
            self.showBarcodeButton.setBackgroundImage(UIImage(named: "btnDefaultInactive"), for: .disabled)
            
            let gregorianCalendar : Calendar = Calendar(identifier: .gregorian)
            let newCardDate = UserManager.sharedInstance.getNextCardPeriod()
            let components : DateComponents = gregorianCalendar.dateComponents([.day], from: Date(), to: newCardDate!)
            let daysCount : Int = components.day!
            let daysCountAppendedByOne = daysCount + 1
            
            if daysCountAppendedByOne == 1 {
                self.expiredPeriodLabel.text = daysCountAppendedByOne.description + " day until new coupon"
            } else {
                self.expiredPeriodLabel.text = daysCountAppendedByOne.description + " days until new coupon"
            }
        }
    }
    
    func setupAvailableCardTime()
    {
        self.mainLabel.isHidden = false
        self.instructionLabel.isHidden = false
        self.barcodeImageView.isHidden = false
        self.countDownHolderView.isHidden = false
        self.enlargeHolderView.isHidden = false
        
        self.expiredPeriodLabel.isHidden = true
        self.barcodeHolderView.isHidden = true
    }
    
    func setupCell()
    {
        self.mainTopCircleHolderView.layer.cornerRadius = 8
        self.mainTopCircleHolderView.clipsToBounds = true
        self.mainCardHolderView.layer.cornerRadius = 12
        self.mainTopHolderView.clipsToBounds = true
        self.countDownHolderView.clipsToBounds = true
        self.countDownHolderView.layer.cornerRadius = 8
        
        UserManager.sharedInstance.checkSavingCardTimeForReset()
        
        if(UserManager.sharedInstance.getIsSavingCardActivated())
        {
            self.enlargeHolderView.isHidden = false
            self.countDownHolderView.isHidden = false
            self.barcodeHolderView.isHidden = true
            self.barcodeWidthLayoutConstraint.constant = 133.0
            self.mainLabel.textAlignment = .left
            self.mainLabel.lineBreakMode = .byWordWrapping
            self.instructionLabel.textAlignment = .left
        }
        else
        {
            self.countDownHolderView.isHidden = true
            self.enlargeHolderView.isHidden = true
            self.barcodeHolderView.isHidden = false
            self.barcodeWidthLayoutConstraint.constant = 0.0
            self.mainLabel.textAlignment = .center
            self.instructionLabel.textAlignment = .center
        }
    }
    
    func heightForLabel(text: String, font: UIFont, width: CGFloat) -> CGFloat
    {
        let label : UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = .byTruncatingTail
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
        
    }
    
    //MARK: Action Methods
    
    @IBAction func showBarcodeButtonTapped(_ sender: Any)
    {
        self.delegate.didSelectBarcodeButton()
    }
    
    @IBAction func enlargeButtonTapped(_ sender: Any)
    {
        self.delegate.didSelectEnlarge()
    }
}
