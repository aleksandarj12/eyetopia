//
//  SystaneCardLogoViewCell.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 6/1/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class SystaneCardLogoViewCell: UITableViewCell {

    @IBOutlet weak var systaneCardLogoImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
