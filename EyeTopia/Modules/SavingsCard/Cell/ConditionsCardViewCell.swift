//
//  ConditionsCardViewCell.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 6/1/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class ConditionsCardViewCell: UITableViewCell {

    @IBOutlet weak var detailsTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
