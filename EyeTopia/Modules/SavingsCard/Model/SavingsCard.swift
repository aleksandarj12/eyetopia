//
//  SavingsCard.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 6/1/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class SavingsCard: NSObject {
    var id : Int?
    var retailerCopy : String?
    var instructions : String?
    var discount : Double?
    var timeAvailable : Int?
    var termsConditions : String?
    var retailerImageURL : String?
    var barcodeImage : String?
    var status : String?
    var created : String?
    var updated : String?
}
