//
//  SavingsCardViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 6/1/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import MBProgressHUD
import Localytics

class SavingsCardViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MainCardViewCellDelegate, ModalCouponDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var coverLoading: UIView!
    
    
    @IBOutlet weak var noCardHolderView: UIView!
    var barcodeImageView : UIImageView = UIImageView()
    var retailerImageView : UIImageView = UIImageView()
    var discountString: String?
    var instructionString: String?
    
    var didFinishLoading : Bool = false
    var monthPeriodFlag : Bool = false
    var timer : Timer!
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "MainCardViewCell", bundle: nil), forCellReuseIdentifier: "MainCardViewCell")
        self.tableView.register(UINib(nibName: "InstructionsCardViewCell", bundle: nil), forCellReuseIdentifier: "InstructionsCardViewCell")
        self.tableView.register(UINib(nibName: "SystaneCardLogoViewCell", bundle: nil), forCellReuseIdentifier: "SystaneCardLogoViewCell")
        self.tableView.register(UINib(nibName: "ConditionsCardViewCell", bundle: nil), forCellReuseIdentifier: "ConditionsCardViewCell")
        self.tableView.isHidden = true
        self.coverLoading.isHidden = true
        self.tableView.estimatedRowHeight = 385.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Localytics.tagEvent(trackingEvents.savingsCardKey)
        super.viewWillAppear(animated)
        self.loadSavingsCard()
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
    }
    
//    override func viewDidLayoutSubviews() {
//        self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        super.viewWillAppear(animated)
    }

    //MARK: ModalCouponDelegate
    
    func didShowBarcode()
    {
        self.coverLoading.isHidden = false
        MBProgressHUD.showAdded(to: self.coverLoading, animated: true)
        self.didFinishLoading = true
        self.tableView.isHidden = false
        self.tableView.reloadData()
        self.setupTimerLoading()
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func setupTimerLoading()
    {
        self.timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(updateTableView), userInfo: nil, repeats: false)
        
    }
    
    @objc func updateTableView()
    {
        self.timer.invalidate()
        MBProgressHUD.hide(for: self.coverLoading, animated: true)
        self.coverLoading.isHidden = true
        self.didFinishLoading = false
        self.tableView.reloadData()
    }
    
    
    //MARK: MainCardViewCellDelegate
    
    func didCallForNewCard()
    {
        self.loadSavingsCard()
    }
    
    func didSelectBarcodeButton()
    {
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ModalCouponVC") as? ModalCouponViewController
        controller?.delegate = self
        self.present(controller!, animated: true, completion: nil)
    }
    
    func didSelectEnlarge()
    {
//        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
//        let largeCouponController = storyboard.instantiateViewController(withIdentifier: "LargeCouponVC") as? LargeCouponViewController
        let largeCoupon = LargeCouponViewController()
        
        largeCoupon.barCodeImage = self.barcodeImageView.image
        largeCoupon.discountStr = self.discountString
        largeCoupon.instructionStr = self.instructionString
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.didChangeWindowOrientationKey), object: self)
        largeCoupon.modalTransitionStyle = .crossDissolve
        self.present(largeCoupon, animated: true, completion: nil)
    }
    
    func didSetAlert(_ message: String)
    {
        self.setupAlertView(message)
    }
    
    func didHideLoadingIndicator()
    {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    
    //MARK: UITableView DataSource & Delegate
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0)
        {
            let cell : MainCardViewCell = self.tableView.dequeueReusableCell(withIdentifier: "MainCardViewCell") as! MainCardViewCell
            
            cell.delegate = self
            cell.setupCell()
            
            
                UserManager.sharedInstance.checkSavingCardTimeForReset()
            
            if(UserManager.sharedInstance.getIsSavingCardActivated())
            {
                cell.setupTime()
            }
            
            cell.barcodeImageView.image = self.barcodeImageView.image

            UserManager.sharedInstance.checkSavingCardTimeForReset()
            
            if(UserManager.sharedInstance.getIsSavingCardActivated())
            {
//            cell.barcodeWidthLayoutConstraint.constant = 133.0
                cell.showBarcodeButton.isEnabled = false
            }
            else
            {
//                cell.barcodeWidthLayoutConstraint.constant = 0.0
                cell.showBarcodeButton.isEnabled = true
                cell.expiredPeriodLabel.isHidden = true
                cell.mainLabel.isHidden = false
                cell.instructionLabel.isHidden = false
            }
            if let retailerCopy = UserManager.sharedInstance.getSavingCardRetailerCopy() {
                MBProgressHUD.hide(for: self.view, animated: true)
                self.instructionString = retailerCopy
                cell.instructionLabel.text = retailerCopy
            }
            
            if let discount = UserManager.sharedInstance.getSavingCardDiscount() {
                
                self.discountString = "$" + discount.cleanValue + " off"
                cell.mainLabel.text = "$" + discount.cleanValue + " off"
            }
            
            if(UserManager.sharedInstance.getSavingCardTime() != nil)
            {
                let time : Int = UserManager.sharedInstance.getSavingCardTime()!
                    
                
                cell.setupTimecell(String(time), nil)
            }
            
            UserManager.sharedInstance.checkSavingCardTimeForReset()
            
            if(self.didFinishLoading && UserManager.sharedInstance.getIsSavingCardActivated() == true)
            {
                cell.coverForLoading.isHidden = false
                
            } else {
                cell.coverForLoading.isHidden = true
            }
            
            return cell
        }
        else if(indexPath.row == 1)
        {
            let cell : InstructionsCardViewCell = self.tableView.dequeueReusableCell(withIdentifier: "InstructionsCardViewCell") as! InstructionsCardViewCell
            cell.instructionsLabel.text = UserManager.sharedInstance.getSavingCardInstructions()
        
            return cell
        }
        else if(indexPath.row == 2)
        {
            let cell : SystaneCardLogoViewCell = self.tableView.dequeueReusableCell(withIdentifier: "SystaneCardLogoViewCell") as! SystaneCardLogoViewCell
                cell.systaneCardLogoImageView.image = self.retailerImageView.image
            
            
            
            return cell
        }
        else if(indexPath.row == 3)
        {
            let cell : ConditionsCardViewCell = self.tableView.dequeueReusableCell(withIdentifier: "ConditionsCardViewCell") as! ConditionsCardViewCell
            cell.detailsTitleLabel.text = "Conditions of Offer"
            cell.descriptionLabel.text = UserManager.sharedInstance.getSavingCardConditions()
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    
    //MARK: Setup & Support Methods
    
    func removeSystaneCard()
    {
        UserManager.sharedInstance.setSavingCardID(cardID: nil)
        UserManager.sharedInstance.setSavingCardTimeAvailable(nil)
        UserManager.sharedInstance.setSavingCardConditions(nil)
        UserManager.sharedInstance.setSavingCardDiscount(nil)
        UserManager.sharedInstance.setSavingCardBarcode(nil)
        UserManager.sharedInstance.setSavingCardInstructions(nil)
        UserManager.sharedInstance.setSavingCardRetailerCopy(nil)
        UserManager.sharedInstance.setSavingCardRetailerLogo(nil)
        UserManager.sharedInstance.setDateSavingCardActivated(nil)
        UserManager.sharedInstance.setIsSavingCardActivated(false)
        

    }
    
    func timeAvailableForUsingCard() -> Bool
    {
        var date : Date = UserManager.sharedInstance.getDateActivationCard()!
        date = Calendar.current.date(byAdding: .day, value: UserManager.sharedInstance.getSavingCardTime()!, to: date)!
        if(date >= Date())
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func setupAlertView(_ message : String)
    {
        let controller : UIAlertController = UIAlertController(title: "WARNING", message: k.alert.noInternetConnection, preferredStyle: .alert)
        let cancelAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func loadSavingsCard()
    {
        Alamofire.request(k.services.savingsCardUrl, headers: nil)
            .responseJSON { response in
                
                if(response.error == nil)
                {
                    let json = response.result.value
                    let barcode = (json as AnyObject).value(forKey: "barcode") as AnyObject
                    let status = (json as AnyObject).value(forKey: "status") as AnyObject
                    
                    if(status.description == k.text.noSystaneCard)
                    {
                        UserManager.sharedInstance.checkSavingCardTimeForReset()
                        if(UserManager.sharedInstance.getIsSavingCardActivated())
                        {
                            
                            if(UserManager.sharedInstance.getSavingCardID() != nil && self.timeAvailableForUsingCard())
                            {
                                self.noCardHolderView.isHidden = false
                                self.tableView.isHidden = true
                                self.removeSystaneCard()
                            }
                            else if(UserManager.sharedInstance.getNextCardPeriod()! >= Date())
                            {
                                self.noCardHolderView.isHidden = true
                                //30 days notice
                                self.monthPeriodFlag = true
                                self.tableView.reloadData()
                                self.tableView.isHidden = false
                            }
                            else
                            {
                                self.monthPeriodFlag = false
                                self.tableView.isHidden = true
                                self.noCardHolderView.isHidden = false
                                self.removeSystaneCard()
                            }
                            
                            self.didHideLoadingIndicator()
                        }
                        else
                        {
                            self.noCardHolderView.isHidden = false
                            self.removeSystaneCard()
                            self.didHideLoadingIndicator()
                        }
                        
                    }
                    else
                    {
                        let retailerImageStringURL = ((json as AnyObject).value(forKeyPath: "barcode.retailer_image") as AnyObject).description
                        let barcodeImageStringURL = ((json as AnyObject).value(forKeyPath: "barcode.barcode_image") as AnyObject).description
                        let retailerCopy = ((json as AnyObject).value(forKeyPath: "barcode.retailer_copy") as AnyObject).description
                        let discount = ((json as AnyObject).value(forKeyPath: "barcode.discount") as AnyObject) as! Double
                        let time = ((json as AnyObject).value(forKeyPath: "barcode.time_available") as AnyObject) as! Int
                        let instructions = ((json as AnyObject).value(forKeyPath: "barcode.instructions") as AnyObject).description
                        let conditions = ((json as AnyObject).value(forKeyPath: "barcode.terms_and_conditions") as AnyObject).description
                        
                        let savingsCard : SavingsCard = SavingsCard()
                        savingsCard.retailerCopy = retailerCopy
                        savingsCard.discount = discount
                        savingsCard.timeAvailable = time
                        savingsCard.instructions = instructions
                        savingsCard.termsConditions = conditions
                        savingsCard.retailerImageURL = retailerImageStringURL
                        savingsCard.barcodeImage = barcodeImageStringURL
                        
                        if(UserManager.sharedInstance.getSavingCardID() != (barcode.value(forKey: "id") as AnyObject) as? Int)
                        {
                            UserManager.sharedInstance.checkSavingCardTimeForReset()
                            
                            if(!UserManager.sharedInstance.getIsSavingCardActivated())
                            {
                                self.setCardData(savingsCard)
                                self.getImages(barcodeImageStringURL, retailerImageStringURL)
                                UserManager.sharedInstance.setSavingCardID(cardID: (barcode.value(forKey: "id") as AnyObject) as? Int)
                            }
                            else
                            {
                                if(UserManager.sharedInstance.getSavingCardID() == (barcode.value(forKey: "id") as AnyObject) as? Int)
                                {
                                    self.getImages(savingsCard.barcodeImage, savingsCard.retailerImageURL)
                                }
                                else
                                {
                                    self.getImages(UserManager.sharedInstance.getSavingCardBarcode(), UserManager.sharedInstance.getSavingCardRetailer())
                                }
                                
                                self.didShowBarcode()
                            }
                        }
                        else
                        {
                            UserManager.sharedInstance.checkSavingCardTimeForReset()
                            
                            if(UserManager.sharedInstance.getIsSavingCardActivated() && UserManager.sharedInstance.getSavingCardID() == (barcode.value(forKey: "id") as AnyObject) as? Int)
                            {
                                self.setCardData(savingsCard)
                                self.getImages(barcodeImageStringURL, retailerImageStringURL)
                            }
                                
                                else if(UserManager.sharedInstance.getIsSavingCardActivated() && UserManager.sharedInstance.getSavingCardID() != (barcode.value(forKey: "id") as AnyObject) as? Int)
                            {
                                
                            }
                            else
                            {
                                self.setCardData(savingsCard)
                                self.getImages(barcodeImageStringURL, retailerImageStringURL)
                                UserManager.sharedInstance.setSavingCardID(cardID: (barcode.value(forKey: "id") as AnyObject) as? Int)
                                //tuka
                                self.tableView.reloadData()
                            }
                        }
                        
                    }
                }
                else
                {
                    self.didSetAlert("Cannot retrieve data from server")
                    self.noCardHolderView.isHidden = false
                    self.removeSystaneCard()
                    self.tableView.isHidden = true
                }
        }
    }
    
    func getImages(_ barcodeURLString : String?, _ retailerURLString : String?)
    {
        SDImageCache.shared().clearMemory()
//        SDImageCac
        
        if let barcodeImageURL = barcodeURLString {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.barcodeImageView.sd_setImage(with: URL(string: k.card.baseURL+barcodeImageURL), placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                self.tableView.reloadData()
                MBProgressHUD.hide(for: self.view, animated: true)
            })
        }
        else
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.barcodeImageView.sd_setImage(with: URL(string: k.card.baseURL+UserManager.sharedInstance.getSavingCardBarcode()!), placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                let indexPath : IndexPath = IndexPath(row: 0, section: 0)
                self.tableView.reloadData()
                MBProgressHUD.hide(for: self.view, animated: true)
            })
        }
        
        if let retailerImageURL = retailerURLString {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.retailerImageView.sd_setImage(with: URL(string: k.card.baseURL+retailerImageURL), placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                let indexPath : IndexPath = IndexPath(row: 2, section: 0)
                self.tableView.reloadData()
                MBProgressHUD.hide(for: self.view, animated: true)
                //tuka
                self.didHideLoadingIndicator()
            })
        }
        else
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.retailerImageView.sd_setImage(with: URL(string: k.card.baseURL+UserManager.sharedInstance.getSavingCardRetailer()!), placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                let indexPath : IndexPath = IndexPath(row: 2, section: 0)
                self.tableView.reloadData()
                MBProgressHUD.hide(for: self.view, animated: true)
                self.didHideLoadingIndicator()
            })
        }
        
        self.noCardHolderView.isHidden = true
        self.tableView.isHidden = false
        self.tableView.reloadData()
        
    }
    
    func setCardData(_ card : SavingsCard)
    {
        UserManager.sharedInstance.setSavingCardRetailerCopy(card.retailerCopy)
        UserManager.sharedInstance.setSavingCardDiscount(card.discount)
        UserManager.sharedInstance.setSavingCardTimeAvailable(card.timeAvailable)
        UserManager.sharedInstance.setSavingCardBarcode(card.barcodeImage)
        UserManager.sharedInstance.setSavingCardRetailerLogo(card.retailerImageURL!)
        UserManager.sharedInstance.setSavingCardInstructions(card.instructions)
        UserManager.sharedInstance.setSavingCardConditions(card.termsConditions)
    }
    
    
    //MARK: Action Methods
    
    @IBAction func notificationButtonTapped(_ sender: Any)
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.didSelectNotificationControllerKey), object: self)
    }
    
    @IBAction func enLargeTapped(_ sender: UIButton)
    {
        let largeCouponController = LargeCouponViewController()
        largeCouponController.barCodeImage = self.barcodeImageView.image
        largeCouponController.discountStr = self.discountString
        largeCouponController.instructionStr = self.instructionString
        self.present(largeCouponController, animated: true, completion: nil)
    }
    
    @IBAction func barCodeTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension Float {
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
