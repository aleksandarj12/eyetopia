//
//  CouponViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 5/7/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import SDWebImage

class CouponViewController: UIViewController/*, ShowingBarCode*/ {

    @IBOutlet weak var infoImageView: UIImageView!
    @IBOutlet weak var barcodeImageView: UIImageView!
    @IBOutlet weak var instructionsLabel: UILabel!
    @IBOutlet weak var discountImageView: UIImageView!
    @IBOutlet weak var conditionsOfferLabel: UILabel!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var enLargeButton: UIButton!
    @IBOutlet weak var holderViewForTimeRemaining: UIView!
    @IBOutlet weak var timeRemainingLabel: UILabel!
    
    @IBOutlet weak var variableConstraint: NSLayoutConstraint!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstView.isHidden = false
        secondView.isHidden = true
        enLargeButton.layer.cornerRadius = 4
        holderViewForTimeRemaining.layer.cornerRadius = 4
        

        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       MBProgressHUD.showAdded(to: self.view, animated: true)
       self.loadSavingsCard()
    }
    
    override func viewWillLayoutSubviews() {
        setUpConstraint()
    }
    
    @IBAction func showBarCodeTapped(_ sender: UIButton) {
        
        if let modalCouponController = self.storyboard?.instantiateViewController(withIdentifier: "ModalCouponVC") as? ModalCouponViewController {
//            modalCouponController.modalCouponDelegate = self
            self.present(modalCouponController, animated: true, completion: nil)
        }
        
    }
    @IBAction func enLargeTapped(_ sender: UIButton) {
        
      let largeCouponController = LargeCouponViewController()
           
            self.present(largeCouponController, animated: true, completion: nil)
            
        
    }
    
    private func setUpConstraint() {
        if firstView.isHidden == false && secondView.isHidden == true {
            variableConstraint.constant = -50
        } else if firstView.isHidden == true && secondView.isHidden == false {
            variableConstraint.constant = 40
        }
    }
    
    func showSecondView() {
        firstView.isHidden = true
        secondView.isHidden = false
        self.setUpConstraint()
    }
    
    func loadSavingsCard()
    {
        Alamofire.request(k.services.savingsCardUrl, headers: nil)
            .responseJSON { response in
                
                if(response.error == nil)
                {
                    let json = response.result.value
                    let barcode = (json as AnyObject).value(forKey: "barcode") as AnyObject
                    
                    let savingsCard : SavingsCard = SavingsCard()
                    savingsCard.id = barcode.value(forKey: k.card.cardID) as? Int
                    savingsCard.retailerCopy = barcode.value(forKey: k.card.retailerCopy) as? String
                    savingsCard.instructions = barcode.value(forKey: k.card.instructions) as? String
                    savingsCard.discount = barcode.value(forKey: k.card.discount) as? Double
                    savingsCard.timeAvailable = barcode.value(forKey: k.card.timeAvailable) as? Int
                    savingsCard.termsConditions = barcode.value(forKey: k.card.termsConditions) as? String
                    savingsCard.retailerImageURL = barcode.value(forKey: k.card.retailerImage) as? String
                    savingsCard.barcodeImage = barcode.value(forKey: k.card.barcodeImage) as? String
                    savingsCard.status = barcode.value(forKey: k.card.status) as? String
                    savingsCard.created = barcode.value(forKey: k.card.created) as? String
                    savingsCard.updated = barcode.value(forKey: k.card.updated) as? String
                    
                    
                    self.getImages(savingsCard)
                    
//                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                else
                {
                    self.setupAlertView("Cannot retrieve data from server")
                }
        }
        
    }
    
    func getImages(_ model : SavingsCard?)
    {
        if let barcodeImageURL = model?.barcodeImage {
            
            self.barcodeImageView.sd_setImage(with: URL(string: barcodeImageURL), placeholderImage: UIImage(named: "App-Default"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                print("tuka")
                MBProgressHUD.hide(for: self.view, animated: true)
            })
        }
        
//        if let retailerImage = model?.retailerImage {
//            
//        }
        
        
        
//        imageView.sd_setImage(with: URL(string: "http://www.domain.com/path/to/image.jpg"), placeholderImage: UIImage(named: "placeholder.png"))

    }
    
    func setupAlertView(_ message : String)
    {
        let controller : UIAlertController = UIAlertController(title: "WARNING", message: k.alert.noInternetConnection, preferredStyle: .alert)
        let cancelAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }

}
