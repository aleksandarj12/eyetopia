//
//  ModalCouponViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 5/7/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class ModalCouponViewController: UIViewController {

    @IBOutlet weak var holderView: UIView!
    
    @IBOutlet weak var noticePeriodLabel: UILabel!
    var delegate: ModalCouponDelegate?
    var time : String = ""
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noticePeriodLabel.text = "Once the barcode is revealed, it will be available for "+(UserManager.sharedInstance.getSavingCardTime()?.description)!
        if(UserManager.sharedInstance.getSavingCardTime() == 1)
        {
            self.noticePeriodLabel.text?.append(" day before it expires")
        }
        else
        {
            self.noticePeriodLabel.text?.append(" days before it expires")
        }
        self.view.backgroundColor = UIColor(red:0, green:0, blue:0, alpha:0.5)
        holderView.layer.cornerRadius = 4
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func barCodeTapped(_ sender: UIButton) {
        UserManager.sharedInstance.setIsSavingCardActivated(true)
        UserManager.sharedInstance.setDateSavingCardActivated(Date())
        NotificationManager.sharedInstance.scheduleSavingCardDiscountNotifications()
        self.dismiss(animated: true, completion: {
            self.delegate?.didShowBarcode()
        })
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

protocol  ModalCouponDelegate: class {
    func didShowBarcode()
}
