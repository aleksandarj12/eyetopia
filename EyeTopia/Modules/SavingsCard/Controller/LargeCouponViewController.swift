//
//  CouponViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 5/7/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class LargeCouponViewController: UIViewController {
    
    
    
    @IBOutlet weak var shadow: UIView!
    @IBOutlet weak var barCodeImageView: UIImageView!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var cardHolderView: UIView!
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var mainHolderView: UIView!
    
    var barCodeImage: UIImage?
    var discountStr: String?
    var instructionStr : String?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cardHolderView.layer.cornerRadius = 12
        cardHolderView.clipsToBounds = true
        
        shadow.layer.cornerRadius = 12
        shadow.clipsToBounds = true
        
        if let image = barCodeImage {
            barCodeImageView.image = image
        }
        if let disc = discountStr {
            discountLabel.text = disc
        }
        if let instr = instructionStr {
            instructionLabel.text = instr
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func minimizeButtonTapped(_ sender: UIButton) {
        self.dismissWithAnimation()
    }
    
    private func dismissWithAnimation() {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.mainHolderView.alpha = 0
        }) { (finished: Bool) in
            NotificationCenter.default.post(name: Notification.Name(rawValue: k.didChangeWindowOrientationKey), object: self)
            self.dismiss(animated: false, completion: nil)
        }
    }
    
}
