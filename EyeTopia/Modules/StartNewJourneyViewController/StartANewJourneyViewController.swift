//
//  StartANewJourneyViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 4/20/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics


class StartANewJourneyViewController: UIViewController {

    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var continueCurrentJourney: UIButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        holderView.layer.cornerRadius = 5
        
        //setUpButtonInTwoLines()
    }

    

//    private func setUpButtonInTwoLines() {
//        continueCurrentJourney.titleLabel?.lineBreakMode = .byWordWrapping
//        continueCurrentJourney.titleLabel?.textAlignment = .center
//        continueCurrentJourney .setTitle("No, continue \n current journey", for: UIControlState.normal)
//        continueCurrentJourney.setBackgroundImage( UIImage(named: "btnDefaultActive"), for: UIControlState.normal)
//        continueCurrentJourney.titleLabel?.font = UIFont(name: "AvenirNext-Bold", size: 18)
//        continueCurrentJourney.titleLabel?.textColor = .white
//    }

    @IBAction func startNewJourney(_ sender: UIButton)
    {
        UserManager.sharedInstance.setNonDiagnosedJourneyCompetion(flag: false)
        UserManager.sharedInstance.setDiagnosedJourneyCompetion(flag: false)
        UserManager.sharedInstance.setSelectedEyeSymptom(eyeProblem: nil)
        NotificationManager.sharedInstance.scheduleJourneyContinuationReminderNotification()

        UserManager.sharedInstance.removeProductMatchData()
        TransitionManager.sharedInstance.storyboardID = ""
        TransitionManager.sharedInstance.storyboardID = ""
        Localytics.tagEvent(trackingEvents.journeyAgainKey)
        self.dismiss(animated: false, completion: {
            NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentStartNewJourneyKey), object: self)
        })
//        self.view.removeFromSuperview()
//
//        let controller : GuideMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
//        let navController : UINavigationController = UINavigationController(rootViewController: controller)
//        navController.setNavigationBarHidden(true, animated: false)
//        let appDelegate = TransitionManager.sharedInstance.appDelegate
//        appDelegate?.window?.rootViewController = navController
    }
    
    // this is cancel button
    @IBAction func continueCurrentJourney(_ sender: UIButton)
    {
//        TransitionManager.sharedInstance.continueLeftOff = true
//        UserManager.sharedInstance.setNonDiagnosedJourneyCompetion(flag: false)
//        UserManager.sharedInstance.setDiagnosedJourneyCompetion(flag: false)
//        NotificationManager.sharedInstance.scheduleJourneyContinuationReminderNotification()
//        UserManager.sharedInstance.removeProductMatchData()
//        TransitionManager.sharedInstance.storyboardID = ""
        self.dismiss(animated: false, completion: {
//            NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentContinueLeftJourneyKey), object: self)
//            self.delegate.didContinueJourneyLeftOff()
        })
        

        
        
//        self.view.removeFromSuperview()
//
//        let appDelegate = TransitionManager.sharedInstance.appDelegate
//
//        if let station = UserManager.sharedInstance.journeyStation
//        {
//            let controller : UINavigationController = station
//            appDelegate?.window?.rootViewController = controller
//        }
//        else
//        {
//            let controller : GuideMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
//            let navController : UINavigationController = UINavigationController(rootViewController: controller)
//            navController.setNavigationBarHidden(true, animated: false)
//            appDelegate?.window?.rootViewController = navController
//        }
        
        
    }
}
