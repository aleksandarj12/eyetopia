//
//  GuideMainPresenter.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/6/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class GuideMainPresenter: NSObject {
    
    var view : GuideMainView!
    
    func attachView(view: GuideMainView)
    {
        self.view = view
    }
    
    func detachView() -> Void
    {
        self.view = nil
    }
    
    func getSecondView()
    {
        self.view.setSecondScreenView()
        self.view.showExitButton(isHidden: false)
    }
    
//    func backButtonHandle()
//    {
//        self.view.hide3Show2Screen()
//    }
    
    func getThirdView()
    {
        self.view.setThirdScreenView()
    }
}
