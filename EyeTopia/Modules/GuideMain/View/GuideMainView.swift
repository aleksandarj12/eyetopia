//
//  GuideMainView.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/6/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

protocol GuideMainView
{
    func setSecondScreenView()
    func showExitButton(isHidden : Bool)
    func setThirdScreenView()
//    func hide3Show2Screen()
}
