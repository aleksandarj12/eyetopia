//
//  GuideMainViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/5/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class GuideMainViewController: UIViewController, GuideMainView, StartIntroductionSurveyDelegate {
    
    @IBOutlet weak var bottomHolderView: UIView!
    @IBOutlet weak var progressBarTrailing: NSLayoutConstraint!
//    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var bottomHolderView2: UIView!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var progressBarView: UIView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var headerFirstViewDimmed: UIView!
    
    @IBOutlet weak var topHolderView2: UIView!
    
    
    @IBOutlet weak var skyImageView: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
//    @IBOutlet weak var topHolderView1: UIView!
//    @IBOutlet weak var bottomHolderView1: UIView!
    var isBeingDiagnosedWithDryEyes : Bool = false
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var isContinueJourney : Bool = false
    
    var presenter : GuideMainPresenter = GuideMainPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNotification()
        self.setupStyle()
        self.showExitButton(isHidden: true)
        self.presenter.attachView(view: self)
//        self.checkDiagnosedJourney()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        print("tuka")
        if(TransitionManager.sharedInstance.storyboardID == "ExpirienceDryEyeSymptomViewController")
        {
            let storyboard : UIStoryboard = UIStoryboard(name: "DiagnosedFlow", bundle: nil)
            let controller : ExpirienceDryEyeSymptomViewController = storyboard.instantiateViewController(withIdentifier: "ExpirienceDryEyeSymptomViewController") as! ExpirienceDryEyeSymptomViewController
            self.navigationController?.pushViewController(controller, animated: false)
            TransitionManager.sharedInstance.storyboardID = ""
        }
        
    }
    
    override func viewDidLayoutSubviews()
    {
        //tuka
//        if(TransitionManager.sharedInstance.diagnosedWithDryEyeButton)
//        {
//            TransitionManager.sharedInstance.diagnosedWithDryEyeButton = false
//
//            let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.diagnosed, bundle: nil)
//
//            let contactLensController : ContactLensIntroductionViewController = storyboard.instantiateViewController(withIdentifier: "ContactLensIntroductionViewController") as! ContactLensIntroductionViewController
//
//            progressBarTrailing.constant = 240
//            self.firstView.alpha = 0.0
//            self.firstView.isHidden = true
//            self.skyImageView.frame.origin.x = -360.0
//            self.topHolderView2.alpha = 1
//            self.bottomHolderView2.alpha = 1
//            self.backgroundImageView.frame.origin.x = -390.0
//
//            self.present(contactLensController, animated: false, completion: nil)
//        }
//        else if(self.isContinueJourney)
//        {
//            self.isContinueJourney = false
//            self.backgroundImageView.frame.origin.x = -390.0
//            self.topHolderView2.alpha = 1
//            self.bottomHolderView2.alpha = 1
//            self.firstView.alpha = 0
//            self.showExitButton(isHidden: false)
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //MARK: StartIntroductionSurveyDelegate
    
    func didSelectNonDiagnosedFlow()
    {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: k.storyboard.nonDiagnosed, bundle: nil)

        let controller : BlinkExamSurveyViewController = mainStoryboard.instantiateViewController(withIdentifier: "BlinkExamSurveyViewController") as! BlinkExamSurveyViewController
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    
    //MARK: Notification Center
    
    @objc func pushExpirienceDryEyeViewController()
    {
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.diagnosed, bundle: nil)
        let controller : ExpirienceDryEyeSymptomViewController = storyboard.instantiateViewController(withIdentifier: "ExpirienceDryEyeSymptomViewController") as! ExpirienceDryEyeSymptomViewController
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    @objc func pushContactLensViewController()
    {
        let controller : StartIntroductionSurveyViewController = self.storyboard?.instantiateViewController(withIdentifier: "StartIntroductionSurveyViewController2") as! StartIntroductionSurveyViewController
        let navController : UINavigationController = UINavigationController(rootViewController: controller)
        navController.setNavigationBarHidden(true, animated: false)
        
        let contactLensController : ContactLensIntroductionViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactLensIntroductionViewController") as! ContactLensIntroductionViewController
        navController.present(contactLensController, animated: false, completion: nil)
        self.present(navController, animated: false, completion: nil)
    }
    
    
    //MARK: Setup & Support Methods
    
    func checkDiagnosedJourney()
    {
        if(self.isBeingDiagnosedWithDryEyes)
        {
            let diagnosedStoryboard : UIStoryboard = UIStoryboard(name: "DiagnosedFlow", bundle: nil)
            let presentController : ContactLensIntroductionViewController = diagnosedStoryboard.instantiateViewController(withIdentifier: "ContactLensIntroductionViewController") as! ContactLensIntroductionViewController
            self.present(presentController, animated: false, completion: nil)
        }
    }
    
    func setupNotification()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushExpirienceDryEyeViewController), name: NSNotification.Name(rawValue: k.pushExpirienceDryEyeKey), object: nil)
    }
    
    func setupJourneyStation()
    {
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
        UserManager.sharedInstance.setJourneyStation(self.navigationController!, controller)
    }
    
    func setupExitView()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to exit? You may continue later.", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes, exit", style: .destructive) { (action) -> Void in
            self.setupJourneyStation()
                NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentHomeKey), object: self)
            UserManager.sharedInstance.setDiagnosedJourneyCompetion(flag: false)
            UserManager.sharedInstance.setNonDiagnosedJourneyCompetion(flag: false)
            NotificationManager.sharedInstance.scheduleJourneyContinuationReminderNotification()
//            Localytics.closeSession()
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func setupStyle()
    {
        self.okButton.layer.borderWidth = 2
        self.okButton.layer.borderColor = (UIColor.white).cgColor
        self.okButton.layer.cornerRadius = 6
        self.okButton.clipsToBounds = true
    }
    
    //MARK: GuideMainView
    
//    func hide3Show2Screen()
//    {
//        UIView.animate(withDuration: 0.7, animations: {
//            self.backButton.alpha = 0
//            self.topHolderView3.alpha = 0
//            self.bottomHolderView2.alpha = 0
//        }, completion: { (flag : Bool) in
//            UIView.animate(withDuration: 0.7, animations: {
//                self.topHolderView2.alpha = 1
//            })
//        })
//    }
    
    func setThirdScreenView()
    {
        
        UIView.animate(withDuration: 0.7, animations: {
//            self.topHolderView1.alpha = 0
            
        }) { (flag : Bool) in
            
            
            
            UIView.animate(withDuration: 3, delay: 2, options: .allowAnimatedContent, animations: {
                self.skyImageView.frame.origin.x = -360.0
            }) { (flag : Bool) in
                UIView.animate(withDuration: 0.7, animations: {
                    self.topHolderView2.alpha = 1
                    self.bottomHolderView2.alpha = 1
                    self.bottomHolderView.alpha = 1
                    //                    self.topHolderView2.alpha = 1
                })
            }
            UIView.animate(withDuration: 3, delay: 1, options: .allowAnimatedContent, animations: {
                self.backgroundImageView.frame.origin.x = -390.0
            }, completion: { (flag : Bool) in
//                self.bottomHolderView1.alpha = 0
            })
            
        }
        
    }
    
    func setSecondScreenView()
    {
//            self.firstView.isHidden = true
//            self.secondView.isHidden = false

        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            self.firstView.alpha = 0.0
            self.headerFirstViewDimmed.alpha = 0.0
        }) { (flag : Bool) in
            
            UIView.animate(withDuration: 0.7, delay: 0.3, options: .allowAnimatedContent, animations: {
//                self.topHolderView1.alpha = 1

            }, completion: { (flag : Bool) in
            })
            self.firstView.isHidden = true
            self.headerFirstViewDimmed.isHidden = true
        }
        
        
        self.presenter.getThirdView()
        
    }
    
    func showExitButton(isHidden : Bool)
    {
        self.exitButton.isHidden = isHidden
    }
    
    
    //MARK: Action Methods

//    @IBAction func backButtonTapped(_ sender: Any)
//    {
//        self.presenter.backButtonHandle()
//
//    }
    
    @IBAction func nextButtonTapped(_ sender: Any)
    {
//        UIView.animate(withDuration: 0.5, animations: {
//            self.topHolderView2.alpha = 0
//
//        }) { (flag : Bool) in
//
//            UIView.animate(withDuration: 0.5, delay: 1, options: .allowAnimatedContent, animations: {
//                self.topHolderView3.alpha = 1
//            }) { (flag : Bool) in
//                UIView.animate(withDuration: 0.5, animations: {
//                    self.backButton.alpha = 1
//                    self.bottomHolderView2.alpha = 1
//                })
//            }
//
//
//        }
    }
    
    @IBAction func continueButtonTapped(_ sender: Any)
    {
        self.presenter.getThirdView()
    }
    
    @IBAction func exitButtonTapped(_ sender: Any)
    {
        self.setupExitView()
//        let controller : ExitViewController = self.storyboard?.instantiateViewController(withIdentifier: "ExitViewController") as! ExitViewController
//        controller.modalPresentationStyle = .custom
//        controller.modalTransitionStyle = .crossDissolve
//
//        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func okButtonTapped(_ sender: Any)
    {
      self.presenter.getSecondView()
        progressBarTrailing.constant = 240
    }
    
    @IBAction func startIntroductionSurvey(_ sender: UIButton) {
        if let root = self.storyboard?.instantiateViewController(withIdentifier: "StartIntroductionSurveyViewController2") as? StartIntroductionSurveyViewController {
            root.delegate = self
            let navController = UINavigationController(rootViewController: root)
            navController.navigationBar.isHidden = true
            navController.modalPresentationStyle = .overCurrentContext
            self.present(navController, animated: true, completion: nil)
        }
    }
}
