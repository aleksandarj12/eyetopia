//
//  SkipToMatchViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 4/5/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

protocol SkipToMatchViewDelegate {
    func didFinishWithSkipToMatch()
    func didSkipToFinish(_ flag : Bool)
}

class SkipToMatchViewController: UIViewController, ReceiveGiftView {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet weak var holderView: UIView!
    var delegate : SkipToMatchViewDelegate!
    var presenter : ReceiveGiftPresenter = ReceiveGiftPresenter()

    @IBOutlet weak var takeMeToMyProductMatchButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.attachView(view: self)
        self.view.backgroundColor = UIColor(red:0.02, green:0.02, blue:0.06, alpha:0.4)
    }

    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        takeMeToMyProductMatchButton.titleLabel?.lineBreakMode = .byWordWrapping
        takeMeToMyProductMatchButton.titleLabel?.textAlignment = .center
//        takeMeToMyProductMatchButton .setTitle("No, take me to my \n product match", for: UIControlState.normal)
        takeMeToMyProductMatchButton.setBackgroundImage( UIImage(named: "btnDefaultActive"), for: UIControlState.normal)
        holderView.layer.cornerRadius = 5
    }

    @IBAction func takeSurveyButtonTapped(_ sender: Any)
    {
        self.dismiss(animated: true, completion: {
            self.delegate.didFinishWithSkipToMatch()
        })
    }
    
    @IBAction func takeMeToProductMatchButtonPressed(_ sender: UIButton)
    {
        self.presenter.loadSystaneCard(true)
        UserManager.sharedInstance.setIsScreenOrDrivingProblem(flag: false)
        UserManager.sharedInstance.setIsAirconEnvironmentProblem(flag: false)
        UserManager.sharedInstance.setIsDryEyesAtNight(flag: false)
        UserManager.sharedInstance.setIsSufferFromAllergy(flag: false)
    }
    
    func setupAlertView(_ message : String)
    {
        let controller : UIAlertController = UIAlertController(title: "WARNING", message: k.alert.noInternetConnection, preferredStyle: .alert)
        let cancelAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    
    //MARK: ReceiveGiftView
    
    func presentSecondScreen()
    {
        
    }
    
    func presentThirdScreen()
    {
        
    }
    
    func setTransitionOtherWorld()
    {
        
    }
    func setSystaneCard(_ flag : Bool, _ cardID : Int?)
    {
        self.dismiss(animated: true, completion: {
            self.delegate.didSkipToFinish(flag)
        })
    }
    
    func didSetAlert(_ message : String)
    {
        self.setupAlertView(message)
    }
}
