//
//  ExitViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/22/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit



class ExitViewController: UIViewController {
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet weak var cancelHolderView: UIView!
    @IBOutlet weak var exitHolderView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.presentHome), name: NSNotification.Name(rawValue: k.presentExitKey), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.presentHome), name: NSNotification.Name(rawValue: k.presentExitKey), object: nil)
        self.exitHolderView.layer.cornerRadius = 9
        self.cancelHolderView.layer.cornerRadius = 9
        self.exitHolderView.clipsToBounds = true
        self.cancelHolderView.clipsToBounds = true
        self.view.backgroundColor = UIColor(red:0.02, green:0.02, blue:0.06, alpha:0.4)
        self.cancelHolderView.backgroundColor = UIColor.white
        self.exitHolderView.backgroundColor = UIColor.white

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func yesButtonTapped(_ sender: Any)
    {
        
        
//        self.view.removeFromSuperview()
//        let appDelegate : AppDelegate = TransitionManager.sharedInstance.appDelegate
//        TransitionManager.sharedInstance.setFirstLaunchFlow(flag: true)
//        let tabController : UITabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
////        UserManager.sharedInstance.setNonDiagnosedJourneyCompetion(flag: false)
////        UserManager.sharedInstance.setDiagnosedJourneyCompetion(flag: false)
//        appDelegate.tabController = tabController
//        appDelegate.window?.rootViewController = tabController
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any)
    {
        TransitionManager.sharedInstance.setContinueJourneyDescription(controllerDescription: "")
        TransitionManager.sharedInstance.setUnfinishedJourney(storyboardID: "")
        self.dismiss(animated: true, completion: nil)
    }
}
