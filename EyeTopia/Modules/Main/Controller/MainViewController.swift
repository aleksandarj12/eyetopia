//
//  ViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/5/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import UserNotifications

enum OptionShown {
    case firstFlow
    case termsConditions
    case privacyNotice
}

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var disagreeButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var topHolderView: UIView!
    @IBOutlet weak var topHolderViewHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomHolderView: UIView!
    @IBOutlet weak var topActivityHolderView: UIView!
    @IBOutlet weak var bottomHeightLayoutConstraints: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var agreeButton: UIButton!
    @IBOutlet weak var navigationBottomHolderView: UIView!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    var optionShown : OptionShown
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if(optionShown == .firstFlow)
        {
            return .lightContent
        }
        else
        {
            return .default
        }
    }
    
    
//    //MARK: MainCellDelegate
//    
//    func didSelectBack()
//    {
//        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let controller : StartViewController = storyboard.instantiateViewController(withIdentifier: "    StartViewController") as! StartViewController
//        self.navigationController?.pushViewController(controller, animated: true)
//    }
    
    
    //MARK: Initializers
    
    init(optionShown : OptionShown){
        self.optionShown = optionShown
        super.init(nibName:nil, bundle:nil);
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "MainViewCell", bundle: nil), forCellReuseIdentifier: "MainViewCell")
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 50
        self.view.backgroundColor = .green
        if(self.optionShown == .firstFlow)
        {
            self.bottomHeightLayoutConstraints.constant = 50
            self.topActivityHolderView.backgroundColor = Style.primaryColor
            self.tableView.backgroundColor = Style.primaryColor
            self.topHolderViewHeightLayoutConstraint.constant = 0.0
            self.topHolderView.isHidden = true
            self.separatorView.isHidden = true
            self.agreeButton.isHidden = false
            self.disagreeButton.isHidden = false
        }
        else
        {
            self.agreeButton.isHidden = true
            self.disagreeButton.isHidden = true
            if(self.optionShown == .privacyNotice)
            {
                self.titleLabel.text = "Privacy Notice"
            }
            else if(self.optionShown == .termsConditions)
            {
                self.titleLabel.text = "Terms of use"
            }
            self.topHolderView.isHidden = false
            self.topHolderViewHeightLayoutConstraint.constant = 110.0
            self.bottomHeightLayoutConstraints.constant = 0
            self.bottomHolderView.backgroundColor = .purple
            self.tableView.backgroundColor = UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.0)
            self.separatorView.isHidden = false
            
        }

//        self.setupStyle()
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {granted, error in
            if error == nil {
                if granted == true {
                    self.setUpAllNotifications(with: true)

                } else {
                    self.setUpAllNotifications(with: false)
                }
            }
        })

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UITableView DataSource & Delegate
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : MainViewCell = self.tableView.dequeueReusableCell(withIdentifier: "MainViewCell") as! MainViewCell
        cell.setupCell(self.optionShown, indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(self.optionShown == .privacyNotice)
        {
            return 1
        }
        else if(self.optionShown == .termsConditions)
        {
            return 2
        }
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    //MARK: Setup & Support Methods
    
    func setupStyle()
    {
        self.navigationBottomHolderView.backgroundColor = Style.navigationBottomBackgroundViewColor
        self.agreeButton.setTitleColor(Style.buttonTextColor, for: .normal)
        self.disagreeButton.setTitleColor(Style.buttonTextColor, for: .normal)
    }
    
    func setupAlertViewController()
    {
        let alert : UIAlertController = UIAlertController(title: "", message: k.text.mustAgreeTerms, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .cancel) { action in
         
        }
        
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func setUpAllNotifications(with flag: Bool){
        NotificationManager.sharedInstance.setBlinkExamReminders(flag: flag)
        NotificationManager.sharedInstance.setJourneyContinuationReminders(flag: flag)
        NotificationManager.sharedInstance.setDryEyeIndexAndPollenForecastNotices(flag: flag)
        NotificationManager.sharedInstance.setSavingCardDiscountNotifications(flag: flag)
    }
    
    //MARK: Action Methods
    
    @IBAction func disAgreeButtonTapped(_ sender: Any)
    {
        self.setupAlertViewController()
    }
    
    @IBAction func agreeButtonTapped(_ sender: Any)
    {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller : StartViewController = storyboard.instantiateViewController(withIdentifier: "StartViewController") as! StartViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func backButtonTapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
}

