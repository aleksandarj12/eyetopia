//
//  MainViewCell.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 6/12/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class MainViewCell: UITableViewCell {

    @IBOutlet weak var descriptionTopLayoutConstraints: NSLayoutConstraint!
    @IBOutlet weak var titleHeightLayoutConstraints: NSLayoutConstraint!
    @IBOutlet weak var separatorTitleHolderView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleHolderView: UIView!
    @IBOutlet weak var termsTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(_ option : OptionShown, _ indexPath : IndexPath)
    {
        if(option == .firstFlow)
        {
            self.titleHolderView.isHidden = false
            self.descriptionTopLayoutConstraints.constant = 0
            if(indexPath.row == 0)
            {
                self.titleHeightLayoutConstraints.constant = 90
                self.descriptionLabel.text = k.terms.termsConditions1
                self.termsTitleLabel.text = "Terms of use"
                let font : UIFont = UIFont(name: "Nunito-Bold", size: 34.0)!
                self.termsTitleLabel.font = font
            }
            else if(indexPath.row == 2)
            {
                self.titleHeightLayoutConstraints.constant = 60
                self.descriptionLabel.text = k.terms.privacyNotice
                self.termsTitleLabel.text = "Privacy Notice"
                
                let font : UIFont = UIFont(name: "AvenirNext-Bold", size: 14.0)!
                self.termsTitleLabel.font = font
            }
            else if(indexPath.row == 1)
            {
                self.termsTitleLabel.text = ""
                self.titleHeightLayoutConstraints.constant = 0.0
                self.descriptionLabel.text = k.terms.termsConditions2
            }
            if(indexPath.row != 1)
            {
                self.separatorTitleHolderView.isHidden = true
                self.termsTitleLabel.textColor = .white
                self.descriptionLabel.textColor = .white
                self.titleHolderView.backgroundColor = Style.primaryColor
                self.titleHeightLayoutConstraints.constant = 90.0
            }
        }
        else
        {
            self.titleHolderView.isHidden = true
            self.descriptionTopLayoutConstraints.constant = 20
            self.contentView.backgroundColor = UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.0)
            let titleFont : UIFont = UIFont(name: "AvenirNext-Bold", size: 24.0)!
            self.termsTitleLabel.font = titleFont
            self.termsTitleLabel.textColor = Style.navyDark
            self.descriptionLabel.textColor = Style.navyDark
            self.separatorTitleHolderView.isHidden = false
            self.titleHolderView.backgroundColor = .white
            self.titleHeightLayoutConstraints.constant = 0.0
            
            if(option == .privacyNotice)
            {
                self.termsTitleLabel.text = "Privacy Notice"
                self.descriptionLabel.text = k.terms.privacyNotice
            }
            else if(option == .termsConditions)
            {
                if(indexPath.row == 0)
                {
                    self.termsTitleLabel.text = "Terms of use"
                    self.descriptionLabel.text = k.terms.termsConditions1
                }
                else if(indexPath.row == 1)
                {
                    self.descriptionLabel.text = k.terms.termsConditions2
                }
                
            }
            
        }
    }
}
