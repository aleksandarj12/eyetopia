//
//  StartViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/5/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit


class StartViewController: UIViewController {

    @IBOutlet weak var cubeImageView: UIImageView!
    @IBOutlet weak var startSurveyView: UIView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var navigationBottomHolderView: UIView!
    @IBOutlet weak var sectionOneView: UIView!
    
    @IBOutlet weak var mainImageView: UIImageView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupImageSize()
        
//        self.setupStyle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool)
        {
//        if(TransitionManager.sharedInstance.didFinishTransition)
//        {
//            TransitionManager.sharedInstance.setDidFinishTransition(flag: false)
//            let controller : TransitionViewController = self.storyboard?.instantiateViewController(withIdentifier: "TransitionViewController") as! TransitionViewController
//            self.navigationController?.pushViewController(controller, animated: false)
//        }
        if(TransitionManager.sharedInstance.didFinishTransition && TransitionManager.sharedInstance.storyboardID == "GuideMainViewController")
        {
            TransitionManager.sharedInstance.setDidFinishTransition(flag: false)
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
            let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
            let controller : GuideMainViewController = storyboard.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
            self.navigationController?.pushViewController(controller, animated: false)
        }
    }
    
    
    //MARK: Action Methods
    
    @IBAction func startSurveyButtonTapped(_ sender: Any)
    {
//        let controller : SurveyMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "SurveyMainViewController") as! SurveyMainViewController
//
//        self.present(controller, animated: true, completion: nil)
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    //    @objc func backButtonTapped(_ sender: AnyObject)
//    {
//        self.navigationController?.popViewController(animated: true)
//    }
    
//    @objc func skipButtonTapped(_ sender: AnyObject)
//    {
//        let controller : GuideMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
//        self.navigationController?.pushViewController(controller, animated: true)
//    }
    
    
    //MARK: Setup & Support Methods
    
    func setupImageSize()
    {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
            case 1334:
                print("iPhone 6/6S/7/8")
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
            case 2436:
                print("iPhone X")
//                self.mainImageView.image = UIImage(named: "graphicTreesLanding")
//                var image = UIImage(named: "graphicTreesLanding")
//                image = UIImage.init(cgImage: (image?.cgImage)!, scale: 3, orientation: (image?.imageOrientation)!)
//                self.mainImageView.image = image
            default:
                print("unknown")
            }
        }
//        UIImage * img = [UIImage imageNamed:@"myimagename"];
//        img = [UIImage imageWithCGImage:img.CGImage scale:2 orientation:img.imageOrientation];

    }
    
    func setupStyle()
    {
        
            
        //let skipButton : UIBarButtonItem = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(skipButtonTapped(_:)) )
        
        
        //self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(backButtonTapped(_:)) )
        //self.navigationItem.rightBarButtonItem = skipButton
        
    }
    
}
