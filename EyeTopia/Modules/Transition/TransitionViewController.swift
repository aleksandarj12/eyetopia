//
//  BaseViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 3/7/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

class TransitionViewController: UIViewController, SkipToMatchViewDelegate, SystaneCardDelegate {

    @IBOutlet weak var blinkExamTitleLabel: UILabel!
    @IBOutlet weak var progressBarTrailing: NSLayoutConstraint!
    @IBOutlet weak var bottomHolderView1: UIView!
    @IBOutlet weak var bottomHolderView2: UIView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var topHolderView2: UIView!
    @IBOutlet weak var topHolderView1: UIView!
    @IBOutlet weak var skyImageView: UIImageView!
    @IBOutlet weak var continueToLifestyleSurveyImageView: UIImageView!
    @IBOutlet weak var blinkExamInstructionLabel: UILabel!
    @IBOutlet weak var skipToProductMatchButton: UIButton!
    var backButton : UIButton!
    var exitButton : UIButton!
    var isTransitioned : Bool = false
    var fullView : UIView!
    var storyboardTitle : String = ""
    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var descriptionSymptomDetailsLabel: UILabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if(self.blinkExamTitleLabel != nil)
        {
            return .default
        }
        else
        {
            return .lightContent
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkForTransitionScreen()
        
        self.setNeedsStatusBarAppearanceUpdate()
        
        self.setupNotification()
        TransitionManager.sharedInstance.setIsSkipToProductMatch(flag: false)
        if let skipProductButton = self.skipToProductMatchButton {
            skipProductButton.layer.cornerRadius = 4
            skipProductButton.clipsToBounds = true
        }
        
        if let instruction = self.blinkExamInstructionLabel {
            if(UIScreen.main.bounds.size.width == 320)
            {
                instruction.font = UIFont(name: "AvenirNext-Medium", size: 15.0)
                instruction.lineBreakMode = .byWordWrapping
            }
        }
        
        self.setupFirstDialogHolderView()

        
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        if(self.storyboardTitle == "BlinkExam2")
        {
//            Localytics.tagScreen("Blink exam")
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.S
    }
    
    func didFinishWithSkipToMatch()
    {
        self.didSelectForwardSurvey()
    }
    
    func didSkipToFinish(_ flag : Bool)
    {
        TransitionManager.sharedInstance.setIsSkipToProductMatch(flag: true)
        TransitionManager.sharedInstance.setStoryboardID(storyboardID: "11.0")
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.diagnosed, bundle: nil)
        
        if(flag)
        {
            let controller : SystaneCardViewController = storyboard.instantiateViewController(withIdentifier: "SystaneCardViewController") as! SystaneCardViewController
            controller.delegate = self
            self.present(controller, animated: true, completion: nil)
        }
        else
        {
            self.presentResultBalanace()
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        
        if(TransitionManager.sharedInstance.isSkipToProductMatch && TransitionManager.sharedInstance.storyboardID == "11.0")
        {
            
        }
        
        if(TransitionManager.sharedInstance.didFinishTransition && TransitionManager.sharedInstance.storyboardID == "TransitionViewControllerKomilia")
        {
            self.setupInitViewControllerWithStoryBoardID(storyboardID: "TransitionViewControllerKomilia")
        }
        
        else if(TransitionManager.sharedInstance.didFinishTransition && TransitionManager.sharedInstance.storyboardID == "SystaneCardViewController")
        {
            self.setupInitViewControllerWithStoryBoardID(storyboardID: "TransitionViewControllerStartLocation")
        }
        else if(TransitionManager.sharedInstance.didFinishTransition && TransitionManager.sharedInstance.storyboardID == "TransitionViewController11")
        {
            self.setupInitViewControllerWithStoryBoardID(storyboardID: "TransitionViewController11")
        }
        else if(TransitionManager.sharedInstance.didFinishTransition && TransitionManager.sharedInstance.storyboardID == "ReceiveGiftViewController")
        {
            let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.diagnosed, bundle: nil)
            TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
            let controller : ReceiveGiftViewController = storyboard.instantiateViewController(withIdentifier: "ReceiveGiftViewController") as! ReceiveGiftViewController
            self.navigationController?.pushViewController(controller, animated: false)
            
        }
        else if(TransitionManager.sharedInstance.didFinishTransition && TransitionManager.sharedInstance.storyboardID == "TransitionViewController13")
        {
            self.setupInitViewControllerWithStoryBoardID(storyboardID: "TransitionViewController13")
        }
    }

    
    //MARK: NSNotification Center
    
    @objc func didFinishLifestyleSurvey()
    {
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.diagnosed, bundle: nil)
        let controller : ReceiveGiftViewController = storyboard.instantiateViewController(withIdentifier: "ReceiveGiftViewController") as! ReceiveGiftViewController
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    
    // MARK: - Setup & Support Methods
    
    func presentResultBalanace()
    {
        let storyboard : UIStoryboard = UIStoryboard(name: "DiagnosedFlow", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ResultBalanceUltraPrimaryNavigationController") as! UINavigationController
        self.present(controller, animated: true, completion: nil)
    }
    
    func setupExitCancel()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to cancel?", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes", style: .destructive) { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
            TransitionManager.sharedInstance.blinkExamRepeat = false
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func setupNotification()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.didFinishLifestyleSurvey), name: NSNotification.Name(rawValue: k.pushReceiveGiftCardKey), object: nil)
    }
    
    func setupJourneyStation()
    {
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.main, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
        UserManager.sharedInstance.setJourneyStation(self.navigationController!, controller)
    }
    
    func setupExitView()
    {
        let controller : UIAlertController = UIAlertController(title: "", message: "Are you sure you want to exit? You may continue later.", preferredStyle: .actionSheet)
        
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes, exit", style: .destructive) { (action) -> Void in
            self.setupJourneyStation()

                NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentHomeKey), object: self)
            UserManager.sharedInstance.setDiagnosedJourneyCompetion(flag: false)
            UserManager.sharedInstance.setNonDiagnosedJourneyCompetion(flag: false)
            NotificationManager.sharedInstance.scheduleJourneyContinuationReminderNotification()
            Localytics.closeSession()
        }
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            
        }
        controller.addAction(yesAction)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func setupFirstDialogHolderView()
    {
        if let firstHolderView = self.topHolderView1 {
            UIView.animate(withDuration: 0.7, animations: {
                firstHolderView.alpha = 1
            })
        }
    }
    
    func dismissFirstDialogHolderView()
    {
        if let firstHolderView = self.topHolderView1 {
            UIView.animate(withDuration: 0.7, animations: {
                firstHolderView.alpha = 0
            })
        }
    }
    
    @IBAction func startLifestyleSurveyButtonTapped(_ sender: Any)
    {
        let controller : LifestyleSurveyViewController = self.storyboard?.instantiateViewController(withIdentifier: "LifestyleSurveyViewController1") as! LifestyleSurveyViewController
        let navController : UINavigationController = UINavigationController(rootViewController: controller)
        navController.setNavigationBarHidden(true, animated: false)
        navController.modalPresentationStyle = .overCurrentContext
        self.present(navController, animated: true, completion: nil)
    }
    
    func setupInitViewControllerWithStoryBoardID(storyboardID : String)
    {
        TransitionManager.sharedInstance.setDidFinishTransition(flag: false)
        self.storyboardTitle = storyboardID
        TransitionManager.sharedInstance.setStoryboardID(storyboardID: "")
        
        let controller : TransitionViewController = self.storyboard?.instantiateViewController(withIdentifier: storyboardID) as! TransitionViewController
        controller.storyboardTitle = storyboardID
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func checkForTransitionScreen()
    {
        if let descriptionLabel = self.descriptionSymptomDetailsLabel
        {
            let symptomEyeProblem = UserManager.sharedInstance.getSelectedEyeSymptom()
            descriptionLabel.text = (symptomEyeProblem?.title!)! + ", you say? Very interesting."
        }
    }
    
    func didSelectForwardSurvey()
    {
        self.isTransitioned = true
        
        
        UIView.animate(withDuration: 0.7, animations: {
            self.topHolderView1.alpha = 0
            self.skipToProductMatchButton.isHidden = true
            
        }) { (flag : Bool) in
            
            
            UIView.animate(withDuration: 4, delay: 1, options: .allowAnimatedContent, animations: {
                self.skyImageView.frame.origin.x = -808.0
            }) { (flag : Bool) in
                UIView.animate(withDuration: 0.7, animations: {
                    self.topHolderView2.alpha = 1
                    //                    self.topHolderView2.alpha = 1
                })
            }
            UIView.animate(withDuration: 4, animations: {
                self.continueToLifestyleSurveyImageView.frame.origin.x = -1750.0
            }, completion: { (flag : Bool) in
                self.bottomHolderView1.alpha = 0
            })
        }
    }
    
    @IBAction func continueToLifestyleSurveyButtonTapped(_ sender: Any)
    {
        self.didSelectForwardSurvey()
        progressBarTrailing.constant = 80
    }
    
    
    //
//    func setupTopViewWithProgress(progress : Int)
//    {
//        let holderView : UIView = UIView(frame: CGRect(x: 0, y: 44, width: UIScreen.main.bounds.size.width, height: 44))
//        holderView.backgroundColor = Style.secondaryColor
//        
//        self.backButton = UIButton(frame: CGRect(x: 15, y: 2, width: 60, height: 40))
//        backButton.setTitleColor(UIColor.white, for: .normal)
//        backButton.setTitle("< Back", for: .normal)
//        backButton.addTarget(self, action: #selector(backButtonTapped(_:)), for: UIControlEvents.touchUpInside)
//        holderView.addSubview(backButton)
//
//        self.exitButton = UIButton(frame: CGRect(x: UIScreen.main.bounds.size.width - 75, y: 2, width: 60, height: 40))
//        exitButton.setTitleColor(UIColor.white, for: .normal)
//        exitButton.setTitle("Exit", for: .normal)
//        //            exitButton.addTarget(self, action: #selector(skipButtonTapped(_:)), for: UIControlEvents.touchUpInside)
//
//        holderView.addSubview(exitButton)
//
//
//        self.view.addSubview(holderView)
    
    
    //MARK: Action Methods
    
    @IBAction func provideLocationButtonTapped(_ sender: Any)
    {
        
    }
    
    @IBAction func skipButtonTapped(_ sender: Any)
    {
        let controller : TransitionViewController = self.storyboard?.instantiateViewController(withIdentifier: "TransitionViewControllerKomilia") as! TransitionViewController
        controller.storyboardTitle = "TransitionViewControllerKomilia"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any)
    {
        self.setupExitCancel()
    }
    @IBAction func exitButtonTapped(_ sender: Any)
    {
        self.setupExitView()
        //let controller : ExitViewController = self.storyboard?.instantiateViewController(withIdentifier: "ExitViewController") as! ExitViewController
//        let controller : ExitViewController = self.storyboard?.instantiateViewController(withIdentifier: "ExitViewController") as! ExitViewController
        //self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func backButtonTapped(_ sender: Any)
    {
        if(self.isTransitioned)
        {
            self.isTransitioned = false
            
            UIView.animate(withDuration: 0.7, animations: {
                self.topHolderView2.alpha = 0
                
            }) { (flag : Bool) in
                
                
                
                UIView.animate(withDuration: 4, delay: 1, options: .allowAnimatedContent, animations: {
                    self.skyImageView.frame.origin.x = -28.0
                }) { (flag : Bool) in
                    UIView.animate(withDuration: 0.7, animations: {
                        self.topHolderView1.alpha = 1
                    })
                }
                UIView.animate(withDuration: 4, animations: {
                    self.continueToLifestyleSurveyImageView.frame.origin.x = -970.0
                }, completion: { (flag : Bool) in
                    self.skipToProductMatchButton.isHidden = false
                    self.bottomHolderView1.alpha = 1

                })
                
            }
    
            
        }
        else
        {
            self.navigationController?.popViewController(animated: false)

        }
        progressBarTrailing.constant = 140
    }
    
    @IBAction func startSurveyButtonTapped(_ sender: Any)
    {
        let controller : LifestyleSurveyViewController = self.storyboard?.instantiateViewController(withIdentifier: "LifestyleSurveyViewController1") as! LifestyleSurveyViewController
        TransitionManager.sharedInstance.transitionLifeStyle = 1
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func seeResultsButtonTapped(_ sender: Any)
    {
    
    }
    
    @IBAction func previousScreenButtonTapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //
//    @objc func backButtonTapped(_ sender: AnyObject)
//    {
//
    
//    @objc func skipButtonTapped(_ sender: AnyObject)
//    {
//        let controller : GuideMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "GuideMainViewController") as! GuideMainViewController
//        self.navigationController?.pushViewController(controller, animated: true)
//    }
    @IBAction func receiveGiftButtonTapped(_ sender: Any)
    {
        let storyboard : UIStoryboard = UIStoryboard(name: k.storyboard.diagnosed, bundle: nil)
        let controller : SystaneCardViewController = storyboard.instantiateViewController(withIdentifier: "SystaneCardViewController") as! SystaneCardViewController
        self.present(controller, animated: true, completion: nil)
    }
    @IBAction func skipToProductMatchButtonTapped(_ sender: Any)
    {
        let controller : SkipToMatchViewController = self.storyboard?.instantiateViewController(withIdentifier: "SkipToMatchViewController") as! SkipToMatchViewController
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
        
    }
    
    
    //MARK: SystaneCardDelegate
    
    func didFinishSystaneCard()
    {
        let resultVC = storyboard?.instantiateViewController(withIdentifier: "ResultBalanceUltraPrimaryNavigationController") as! UINavigationController
        self.present(resultVC, animated: true, completion: nil)
    }
    
}
