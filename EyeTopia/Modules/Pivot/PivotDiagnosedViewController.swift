//
//  PivotDiagnosedViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 3/15/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

class PivotDiagnosedViewController: UIViewController {

    @IBOutlet weak var systaneProductsTitle: UILabel!
    @IBOutlet weak var dashboardButton: UIButton!
    @IBOutlet weak var productsButton: UIButton!
    @IBOutlet weak var locatorButton: UIButton!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var tipsButton: UIButton!
    @IBOutlet weak var productMatchButton: UIButton!
    @IBOutlet weak var pollenForecastButton: UIButton!
    @IBOutlet weak var upperCornerRounds: UIView!
    @IBOutlet weak var systaneProductsDetailsLabel: UILabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        holderView.layer.cornerRadius = 5
        holderView.clipsToBounds = true
        upperCornerRounds.layer.cornerRadius = 5
        
        let nunitoExtraBold : UIFont = UIFont(name: "Nunito-ExtraBold", size: 18)!
        let nunitoExtraBoldSuper : UIFont = UIFont(name: "Nunito-ExtraBold", size: 12)!
        
        self.systaneProductsTitle.attributedText = self.setupSubtitle("SYSTANE®‰ products", nunitoExtraBold, nunitoExtraBoldSuper)
        
        let avenirNextMedium : UIFont = UIFont(name: "AvenirNext-Medium", size: 14)!
        let avenirNextMediumSuper : UIFont = UIFont(name: "AvenirNext-Medium", size: 10)!
        
        
        self.systaneProductsDetailsLabel.attributedText = self.setupSubtitle("View all our SYSTANE®‰ products, read product details and benefits.", avenirNextMedium, avenirNextMediumSuper)
    }
    
    func setupSubtitle(_ text : String, _ font : UIFont, _ superFont : UIFont) -> NSAttributedString
    {
        
        var ranges : [NSRange] = [NSRange]()
        
        var string : NSString = text as NSString
        
        while (string.contains(k.text.fontSuper))
        {
            let range : NSRange = string.range(of: k.text.fontSuper)
            string = string.replacingCharacters(in: range, with: "") as NSString
            
            let newRange : NSRange = NSRange(location: range.location - 1, length: 1)
            ranges.append(newRange)
        }
        
        let attString : NSMutableAttributedString = NSMutableAttributedString(string: string as String, attributes: [.font:font])
        
        for range in ranges
        {
            attString.setAttributes([.font:superFont,.baselineOffset:8], range: range)
        }
        
        return attString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func pollenForecastButtonTapped(_ sender: Any)
    {
        didFinishDiagnosedFlow()
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentPollenForecastKey), object: self)
    }
    
    @IBAction func productMatchButtonTapped(_ sender: Any)
    {
        didFinishDiagnosedFlow()
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentLatestProductMatchKey), object: self)
    }
    
    @IBAction func dashboardButtonTapped(_ sender: Any)
    {
        didFinishDiagnosedFlow()
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentBlinkExamChartKey), object: self)
    }
    
    @IBAction func tipsButtonTapped(_ sender: Any)
    {
        didFinishDiagnosedFlow()
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentEducationKey), object: self)
    }
    
    @IBAction func productsButtonTapped(_ sender: Any)
    {
        didFinishDiagnosedFlow()
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentProductsKey), object: self)
    }
    
    @IBAction func locatorButtonTapped(_ sender: Any)
    {
        didFinishDiagnosedFlow()
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentLocatorKey), object: self)
    }
    
    @IBAction func goToHomeScreenButtonTapped(_ sender: Any)
    {
        didFinishDiagnosedFlow()
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentHomeKey), object: self)
    }
    
    internal func didFinishDiagnosedFlow()
    {
//        Localytics.tagScreen("Diagnose matches")
//        Localytics.closeSession()
    }

}
