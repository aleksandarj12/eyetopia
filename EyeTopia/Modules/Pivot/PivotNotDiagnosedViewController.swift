//
//  PivotViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 3/14/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import Localytics

protocol RedoExamDelegate {
    func didSelectRedoExam()
}

class PivotNotDiagnosedViewController: UIViewController {

    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var resultDescription: UILabel!
    @IBOutlet weak var findEyeCareButton: UIButton!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var upperCornerRounds: UIView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var delegate : RedoExamDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTimeLabel()
        
        if(!TransitionManager.sharedInstance.getFirstLaunchFlow())
        {
            TransitionManager.sharedInstance.setFirstLaunchFlow(flag: true)
        }
        
        
        self.setupContentByResults()
        
        
        findEyeCareButton.titleLabel?.lineBreakMode = .byWordWrapping
        findEyeCareButton.titleLabel?.textAlignment = .center
        findEyeCareButton .setTitle("Find an Optometrist\n near me", for: UIControlState.normal)
        findEyeCareButton.setBackgroundImage( UIImage(named: "btnDefaultActive"), for: UIControlState.normal)
        
        holderView.layer.cornerRadius = 5
        holderView.clipsToBounds = true
        upperCornerRounds.layer.cornerRadius = 5
    }

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        Localytics.tagEvent(trackingEvents.userCompleteNonDiagnosedJourneyKey)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Action Methods
    
    @IBAction func redoExamButtonTapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
        TransitionManager.sharedInstance.setDidFinishTransition(flag: true)
        self.delegate.didSelectRedoExam()
    }
    @IBAction func educationButtonTapped(_ sender: Any)
    {
        self.didFinishedBlinkTest()
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentEducationKey), object: self)
    }
    
    @IBAction func blinkExamChartButtonTapped(_ sender: Any)
    {
        self.didFinishedBlinkTest()
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentBlinkExamChartKey), object: self)
    }
    
    @IBAction func dryIndexPollenForecastButtonTapped(_ sender: Any)
    {
        self.didFinishedBlinkTest()
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentPollenForecastKey), object: self)
    }
    
    @IBAction func goHomeScreenButtonTapped(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
        self.didFinishedBlinkTest()
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentHomeKey), object: self)
    }
    
    @IBAction func findEyeCareProffesional(_ sender: UIButton)
    {
        self.didFinishedBlinkTest()
        NotificationCenter.default.post(name: Notification.Name(rawValue: k.presentEyeCareProffesionalKey), object: self)
    }
    
    //MARK: Setup & Support Methods

    internal func didFinishedBlinkTest()
    {
//        Localytics.tagScreen("Non-Diagnosed Journey completed")
//        Localytics.closeSession()
        UserManager.sharedInstance.setJourneyStation(nil, nil)
//        Localytics.tagEvent(trackingEvents.blinkExamCompletedKey)
    }
    
    func setupContentByResults()
    {
        if(UserManager.sharedInstance.getBlinkExamTime() <= 8.0)
        {
            let attributedStringResultDescription = NSMutableAttributedString(string: "This score indicates that you may  be experiencing dry eye symptoms. \nConsult with your Optometrist to see if you have dry eye or not.", attributes: [
                .font: UIFont(name: "AvenirNext-Medium", size: 16.0)!,
                .foregroundColor: UIColor(white: 1.0, alpha: 1.0)
                ])
            attributedStringResultDescription.addAttribute(.font, value: UIFont(name: "AvenirNext-Medium", size: 16.0)!, range: NSRange(location: 30, length: 37))
            resultDescription.attributedText = attributedStringResultDescription
            resultDescription.attributedText = attributedStringResultDescription
            resultDescription.attributedText = attributedStringResultDescription
        }
        else if(UserManager.sharedInstance.getBlinkExamTime() >= 8.01 && UserManager.sharedInstance.getBlinkExamTime() <= 12.0)
        {
            let attributedStringResultDescription = NSMutableAttributedString(string: "This score indicates that you may be experiencing mild dry eye symptoms. \nConsult with your Optometrist to see if you have dry eye or not.", attributes: [
                .font: UIFont(name: "AvenirNext-Medium", size: 16.0)!,
                .foregroundColor: UIColor(white: 1.0, alpha: 1.0)
                ])
            attributedStringResultDescription.addAttribute(.font, value: UIFont(name: "AvenirNext-Medium", size: 16.0)!, range: NSRange(location: 30, length: 42))
            resultDescription.attributedText = attributedStringResultDescription
        }
        else if(UserManager.sharedInstance.getBlinkExamTime() >= 12.01)
        {
            let attributedStringResultDescription = NSMutableAttributedString(string: "Good news! Based on your responses, you likely do not have dry eye symptoms. However, this test is only a simple indicator and is not designed to consider your individual case. It’s best to see your optometrist to check your eye health.", attributes: [
                .font: UIFont(name: "AvenirNext-Medium", size: 16.0)!,
                .foregroundColor: UIColor(white: 1.0, alpha: 1.0)
                ])
            attributedStringResultDescription.addAttribute(.font, value: UIFont(name: "AvenirNext-Medium", size: 16.0)!, range: NSRange(location: 40, length: 27))
            resultDescription.attributedText = attributedStringResultDescription
        }
    }
    
    func setupTimeLabel()
    {
        let attributedString = NSMutableAttributedString(string: UserManager.sharedInstance.blinkExamTimeString, attributes: [
            .font: UIFont(name: "AvenirNext-Regular", size: 80.0)!,
            .foregroundColor: UIColor(white: 1.0, alpha: 1.0)
            ])
       // attributedString.addAttribute(.font, value: UIFont(name: "AvenirNext-Regular", size: 80.0)!, range: NSRange(location: 0, length: 8))
        resultLabel.attributedText = attributedString
    }
    


}
