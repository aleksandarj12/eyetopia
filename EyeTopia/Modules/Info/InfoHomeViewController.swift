//
//  InfoHomeViewController.swift
//  EyeTopia
//
//  Created by Aleksandar Jovanov on 4/19/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class InfoHomeViewController: UIViewController {

    @IBOutlet weak var infoHolderView: UIView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.infoHolderView.layer.cornerRadius = 10
        self.infoHolderView.clipsToBounds = true
        self.view.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha:0.5)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func okButtonTapped(_ sender: Any)
    {
     self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
