//
//  BlinkExamInfoViewController.swift
//  EyeTopia
//
//  Created by NIKOLA RUSEV on 4/4/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class BlinkExamInfoViewController: UIViewController {

    @IBOutlet weak var holderView: UIView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha:0.4)
        holderView.layer.cornerRadius = 7

    }

    
    @IBAction func OKbuttonPressed(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
}
